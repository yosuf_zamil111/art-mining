package com.kona.artmining.Interface;

import android.app.Activity;
import android.content.Context;
import android.webkit.JavascriptInterface;

import com.kona.artmining.UI.Activity.UserAuthenticationActivity;

public class TheJavascriptInterface {

    Activity mActivity;
    listener l;

    /** Instantiate the interface and set the context */
    public TheJavascriptInterface(Activity c) {
        mActivity = c;
    }

    /** Close Webview */
    @JavascriptInterface
    public void closeWebview() {
        if(l != null)
        {
            l.message("closeWebView");
        }
    }

    @JavascriptInterface
    public void doLogOut() {
        if(l != null)
        {
            l.message("doLogOut");
        }
    }

    @JavascriptInterface
    public void updateTheCart() {
        if(l != null)
        {
            l.message("updateTheCart");
        }
    }

    @JavascriptInterface
    public void showDetailsPage(String art_id) {
        if(l != null)
        {
            l.showDetailsPage(art_id);
        }
    }

    public void setListener(listener lsnr)
    {
        this.l = lsnr;
    }

    public interface listener
    {
        void message(String m);
        void showDetailsPage(String art_id);
    }
}
