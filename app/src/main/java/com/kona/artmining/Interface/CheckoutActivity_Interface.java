package com.kona.artmining.Interface;


public interface CheckoutActivity_Interface {

    void ShowShippingMethods();
    void OpenFragmentForm_UAFM(String className);
    void ShowTermsConditions();
    void ShowOrderDetailsPage(String OrderID);
}
