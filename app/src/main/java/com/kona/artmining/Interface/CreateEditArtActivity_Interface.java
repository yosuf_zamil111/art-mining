package com.kona.artmining.Interface;


import com.kona.artmining.Model.SearchListItem;

public interface CreateEditArtActivity_Interface
{
    void PickedArtist(SearchListItem selectedArtist);
    void CloseFragment();

    void ShowProgressDialogue();
    void HideProgressDialogue();
}
