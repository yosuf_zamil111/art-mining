package com.kona.artmining.Interface;


public interface ActivityCommunicator
{
    void passDataToActivity(String someValue);
    void openLoginPage();
    void CloseFragmentAndOpenLoginScreen();
    void loadWebPage(String url);
    void showArtDetailsPage(int ID);
    void showArtDetailsPageWV(int ID);

    void ShowProgressDialogue();
    void HideProgressDialogue();

    void ShowParentToolbar();
    void HideParentToolbar();

    void ShowExhibitionsFrg(String ID, boolean forceToEditScreen);
    void ShowReviewFrg(String ID);
    void ShowEventFrg(String ID);
    void ShowReviewFrgToWriteNew();
    void UpdateTheCart();
    void CloseMe();
    void GoToGalleryTab();
}
