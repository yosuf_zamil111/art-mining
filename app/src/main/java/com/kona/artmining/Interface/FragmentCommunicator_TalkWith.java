package com.kona.artmining.Interface;


public interface FragmentCommunicator_TalkWith
{
    void passDataToFragment(String someValue);
    void _DevicebackPressed();

    boolean ifDetailsPageHiden();
    void ShowUpArtist(String artistID);
}