package com.kona.artmining.Interface;


public interface UserActivity_FrgMng_Interface {

    void ShowPaymentProcessing(String tc_type, String curr_type);
    void finishActivity();
    void doLogout();
    void updateProfileDetails();
    void showArtDetailsPage(int artID);
}
