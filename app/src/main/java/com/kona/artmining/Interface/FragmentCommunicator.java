package com.kona.artmining.Interface;


public interface FragmentCommunicator
{
    void passDataToFragment(String someValue);
    void reloadThePage();
    void _DevicebackPressed();


    boolean ifDetailsPageHiden();
    void showDetails(int id);
}