package com.kona.artmining.Interface;

public interface MessagingActivity_Interface
{
    void updateSentMails();
    void updateInbox();
    void replyAgainstMessage(long message_id, int type);
    void trashMessage(long message_id, int type);
    void removeMessage(long message_id, int type);
    void readMail_topBackPressed();
    void writeMail_topBackPressed();
    void exit();
}
