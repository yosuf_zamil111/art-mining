package com.kona.artmining.Model;


public class AuctionItem_T1
{
    String catrt_id;
    String art_id;
    String ship_cost;
    String ship_cost_cash;
    String title_en;
    String title_kr;
    String currency;
    double price;
    String name_en;
    String name_kr;
    String main_image_watermarked;
    String main_image_thumb;
    boolean checked;
    String time;
    boolean sold_out;
    boolean purchased;
    boolean winner;

    public boolean isPurchased() {
        return purchased;
    }

    public void setPurchased(boolean purchased) {
        this.purchased = purchased;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getTitle_kr() {
        return title_kr;
    }

    public void setTitle_kr(String title_kr) {
        this.title_kr = title_kr;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getArtistName_en() {
        return name_en;
    }

    public void setArtistName_en(String artistName_en) {
        this.name_en = artistName_en;
    }

    public String getArtistName_kr() {
        return name_kr;
    }

    public void setArtistName_kr(String artistName_kr) {
        this.name_kr = artistName_kr;
    }

    public String getMain_image_watermarked() {
        return main_image_watermarked;
    }

    public void setMain_image_watermarked(String main_image_watermarked) {
        this.main_image_watermarked = main_image_watermarked;
    }

    public String getMain_image_thumb() {
        return main_image_thumb;
    }

    public void setMain_image_thumb(String main_image_thumb) {
        this.main_image_thumb = main_image_thumb;
    }

    public String getId() {
        return catrt_id;
    }

    public void setId(String id) {
        this.catrt_id = id;
    }

    public boolean isSold_out() {
        return sold_out;
    }

    public void setSold_out(boolean sold_out) {
        this.sold_out = sold_out;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isWinner() {
        return winner;
    }

    public void setWinner(boolean winner) {
        this.winner = winner;
    }

    public String getArt_id() {
        return art_id;
    }

    public void setArt_id(String art_id) {
        this.art_id = art_id;
    }

    public String getShip_cost() {
        return ship_cost;
    }

    public void setShip_cost(String ship_cost) {
        this.ship_cost = ship_cost;
    }

    public String getShip_cost_cash() {
        return ship_cost_cash;
    }

    public void setShip_cost_cash(String ship_cost_cash) {
        this.ship_cost_cash = ship_cost_cash;
    }

}
