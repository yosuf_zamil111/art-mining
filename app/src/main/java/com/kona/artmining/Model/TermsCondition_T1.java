package com.kona.artmining.Model;

public class TermsCondition_T1 {

    int sectionId;
    String sectionTitle;
    String sectionDescription;
    boolean section_required;
    boolean checked;

    public boolean isSection_required() {
        return section_required;
    }

    public void setSection_required(boolean section_required) {
        this.section_required = section_required;
    }

    public int getSectionId() {
        return sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionTitle() {
        return sectionTitle;
    }

    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
    }

    public String getSectionDescription() {
        return sectionDescription;
    }

    public void setSectionDescription(String sectionDescription) {
        this.sectionDescription = sectionDescription;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
