package com.kona.artmining.Model;

public class UserProfile
{
    int id;
    String email;
    String password;
    String name;
    String name_kr;
    String birth_date;
    String sex;
    String phone_country_code;
    String phone_operator;
    String phone;
    String zip_code;
    String address;
    String address2;
    String about_en;
    String about_kr;
    String profile_en;
    String profile_kr;
    int type = 0;
    String main_image;
    String thumb_image;
    String curator_percentage, phone_ext;

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAbout_en() {
        return about_en;
    }

    public void setAbout_en(String about_en) {
        this.about_en = about_en;
    }

    public String getAbout_kr() {
        return about_kr;
    }

    public void setAbout_kr(String about_kr) {
        this.about_kr = about_kr;
    }

    public String getProfile_en() {
        return profile_en;
    }

    public void setProfile_en(String profile_en) {
        this.profile_en = profile_en;
    }

    public String getProfile_kr() {
        return profile_kr;
    }

    public void setProfile_kr(String profile_kr) {
        this.profile_kr = profile_kr;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getMain_image() {
        return main_image;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone_country_code() {
        return phone_country_code;
    }

    public void setPhone_country_code(String phone_country_code) {
        this.phone_country_code = phone_country_code;
    }

    public String getPhone_operator() {
        return phone_operator;
    }

    public void setPhone_operator(String phone_operator) {
        this.phone_operator = phone_operator;
    }

    public String getName_kr() {
        return name_kr;
    }

    public void setName_kr(String name_kr) {
        this.name_kr = name_kr;
    }

    public String getCurator_percentage() {
        return curator_percentage;
    }

    public void setCurator_percentage(String curator_percentage) {
        this.curator_percentage = curator_percentage;
    }

    public String getPhone_ext() {
        return phone_ext;
    }

    public void setPhone_ext(String phone_ext) {
        this.phone_ext = phone_ext;
    }


}
