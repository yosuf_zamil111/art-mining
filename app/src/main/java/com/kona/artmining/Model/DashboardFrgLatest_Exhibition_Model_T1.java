package com.kona.artmining.Model;

public class DashboardFrgLatest_Exhibition_Model_T1 {

    int exb_id;
    String exb_image_thump;
    String title;
    String details_info;
    String image;
    String image_thumb;
    String date;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails_info() {
        return details_info;
    }

    public void setDetails_info(String details_info) {
        this.details_info = details_info;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_thumb() {
        return image_thumb;
    }

    public void setImage_thumb(String image_thumb) {
        this.image_thumb = image_thumb;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getExb_id() {
        return exb_id;
    }

    public void setExb_id(int exb_id) {
        this.exb_id = exb_id;
    }

    public String getExb_image_thump() {
        return exb_image_thump;
    }

    public void setExb_image_thump(String exb_image_thump) {
        this.exb_image_thump = exb_image_thump;
    }
}
