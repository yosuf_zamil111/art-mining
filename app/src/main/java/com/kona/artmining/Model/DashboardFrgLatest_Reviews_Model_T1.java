package com.kona.artmining.Model;

public class DashboardFrgLatest_Reviews_Model_T1 {

    String review_id;
    String user_id;
    String image;
    String image_thumb;
    String details_info;
    String share_url;
    String posted_by;
    String date;
    String review_creator_name_en;
    String review_creator_name_kr;
    String like_count;
    String review_title;

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getReview_id() {
        return review_id;
    }

    public void setReview_id(String review_id) {
        this.review_id = review_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_thumb() {
        return image_thumb;
    }

    public void setImage_thumb(String image_thumb) {
        this.image_thumb = image_thumb;
    }

    public String getDetails_info() {
        return details_info;
    }

    public void setDetails_info(String details_info) {
        this.details_info = details_info;
    }

    public String getShare_url() {
        return share_url;
    }

    public void setShare_url(String share_url) {
        this.share_url = share_url;
    }

    public String getPosted_by() {
        return posted_by;
    }

    public void setPosted_by(String posted_by) {
        this.posted_by = posted_by;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReview_creator_name_en() {
        return review_creator_name_en;
    }

    public void setReview_creator_name_en(String review_creator_name_en) {
        this.review_creator_name_en = review_creator_name_en;
    }

    public String getReview_creator_name_kr() {
        return review_creator_name_kr;
    }

    public void setReview_creator_name_kr(String review_creator_name_kr) {
        this.review_creator_name_kr = review_creator_name_kr;
    }

    public String getReview_title() {
        return review_title;
    }

    public void setReview_title(String review_title) {
        this.review_title = review_title;
    }

}
