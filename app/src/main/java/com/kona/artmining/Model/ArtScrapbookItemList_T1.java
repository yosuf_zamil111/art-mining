package com.kona.artmining.Model;


public class ArtScrapbookItemList_T1 {

    int scrap_id, art_id, art_image_id;
    String added_on;
    int status;

    String main_image;
    String main_image_watermarked;
    String main_image_thumb;

    boolean clickable = false;
    boolean selected = false;



    public int getArt_image_id() {
        return art_image_id;
    }

    public void setArt_image_id(int art_image_id) {
        this.art_image_id = art_image_id;
    }

    public int getArt_id() {
        return art_id;
    }

    public void setArt_id(int art_id) {
        this.art_id = art_id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isClickable() {
        return clickable;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    public int getScrap_id() {
        return scrap_id;
    }

    public void setScrap_id(int scrap_id) {
        this.scrap_id = scrap_id;
    }

    public String getAdded_on() {
        return added_on;
    }

    public void setAdded_on(String added_on) {
        this.added_on = added_on;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMain_image() {
        return main_image;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public String getMain_image_watermarked() {
        return main_image_watermarked;
    }

    public void setMain_image_watermarked(String main_image_watermarked) {
        this.main_image_watermarked = main_image_watermarked;
    }

    public String getMain_image_thumb() {
        return main_image_thumb;
    }

    public void setMain_image_thumb(String main_image_thumb) {
        this.main_image_thumb = main_image_thumb;
    }
}
