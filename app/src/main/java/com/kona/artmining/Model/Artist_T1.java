package com.kona.artmining.Model;


import java.util.ArrayList;
import java.util.List;

public class Artist_T1 {

    int id;
    String email;
    String name_en;
    String name_kr;
    String profilePicture;
    List<String> latest_art = new ArrayList<>();
    List<Integer> latest_art_id;
    String profile_en;
    String profile_kr;
    String about_en;
    String about_kr;
    String profile_image_main, profile_image_thumb, cover_image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_kr() {
        return name_kr;
    }

    public void setName_kr(String name_kr) {
        this.name_kr = name_kr;
    }

    public List<String> getLatest_art() {
        return latest_art;
    }

    public void setLatest_art(List<String> latest_art) {
        this.latest_art.addAll(latest_art);
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getProfile_en() {
        return profile_en;
    }

    public void setProfile_en(String profile_en) {
        this.profile_en = profile_en;
    }

    public String getProfile_kr() {
        return profile_kr;
    }

    public void setProfile_kr(String profile_kr) {
        this.profile_kr = profile_kr;
    }

    public String getAbout_en() {
        return about_en;
    }

    public void setAbout_en(String about_en) {
        this.about_en = about_en;
    }

    public String getAbout_kr() {
        return about_kr;
    }

    public void setAbout_kr(String about_kr) {
        this.about_kr = about_kr;
    }

    public List<Integer> getLatest_art_id() {
        return latest_art_id;
    }

    public void setLatest_art_id(List<Integer> latest_art_id) {
        this.latest_art_id = latest_art_id;
    }

    public String getProfile_image_main() {
        return profile_image_main;
    }

    public void setProfile_image_main(String profile_image_main) {
        this.profile_image_main = profile_image_main;
    }

    public String getProfile_image_thumb() {
        return profile_image_thumb;
    }

    public void setProfile_image_thumb(String profile_image_thumb) {
        this.profile_image_thumb = profile_image_thumb;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }

}
