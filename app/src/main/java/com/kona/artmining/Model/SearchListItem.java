package com.kona.artmining.Model;


import java.util.ArrayList;
import java.util.List;

public class SearchListItem {

    int ID;
    String Text;
    String email;
    String profileImg;
    List<String> portfolioLinks = new ArrayList<String>();

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public List<String> getPortfolioLinks() {
        return portfolioLinks;
    }

    public void setPortfolioLinks(List<String> portfolioLinks) {
        this.portfolioLinks = portfolioLinks;
    }

}
