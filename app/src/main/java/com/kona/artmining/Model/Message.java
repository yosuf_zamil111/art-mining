package com.kona.artmining.Model;


public class Message
{
    long id;
    long from_id;
    String sender_name;
    long to_id;
    String receiver_name;

    int mail_status;
    String name;
    String date;
    String subject;
    String mail_details;
    int unread_info;

    int Type;

    boolean checked;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFrom_id() {
        return from_id;
    }

    public void setFrom_id(long from_id) {
        this.from_id = from_id;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public long getTo_id() {
        return to_id;
    }

    public void setTo_id(long to_id) {
        this.to_id = to_id;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public int getMail_status() {
        return mail_status;
    }

    public void setMail_status(int mail_status) {
        this.mail_status = mail_status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMail_details() {
        return mail_details;
    }

    public void setMail_details(String mail_details) {
        this.mail_details = mail_details;
    }

    public int getUnread_info() {
        return unread_info;
    }

    public void setUnread_info(int unread_info) {
        this.unread_info = unread_info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

}
