package com.kona.artmining.Model;


import java.util.List;

public class Order_T1 {

    List<CartItem_T1> items;
    List<AuctionItem_T1> AuctionItem;
    Shipping_T1 shippingDetails;

    double item_total;
    double shipping_cost;
    double tax;
    double pay_total;
    String gateway_used;
    String order_type;
    String pay_id;
    String currency;

    int order_id;
    int user_id;
    String transaction_id;
    int status;
    CartItem_T1 featuredItem;
    CartItem_T1 featuredItem_Auction;
    String orderDate;

    boolean ifAuctionItem;

    double total;
    double sub_total;
    String pay_via;

    double total_price;
    double art_id;
    double item_price;
    int buyer_id;
    String title_en;
    String title_kr;
    String main_image_thumb;
    String main_image_watermarked;
    String name_en;
    String name_kr;
    String buyer_email;
    String buyer_name_kr;
    String buyer_phone;
    String buyer_name_en;

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public double getArt_id() {
        return art_id;
    }

    public void setArt_id(double art_id) {
        this.art_id = art_id;
    }

    public double getItem_price() {
        return item_price;
    }

    public void setItem_price(double item_price) {
        this.item_price = item_price;
    }

    public int getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(int buyer_id) {
        this.buyer_id = buyer_id;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getTitle_kr() {
        return title_kr;
    }

    public void setTitle_kr(String title_kr) {
        this.title_kr = title_kr;
    }

    public String getMain_image_thumb() {
        return main_image_thumb;
    }

    public void setMain_image_thumb(String main_image_thumb) {
        this.main_image_thumb = main_image_thumb;
    }

    public String getMain_image_watermarked() {
        return main_image_watermarked;
    }

    public void setMain_image_watermarked(String main_image_watermarked) {
        this.main_image_watermarked = main_image_watermarked;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_kr() {
        return name_kr;
    }

    public void setName_kr(String name_kr) {
        this.name_kr = name_kr;
    }

    public String getBuyer_email() {
        return buyer_email;
    }

    public void setBuyer_email(String buyer_email) {
        this.buyer_email = buyer_email;
    }

    public String getBuyer_name_kr() {
        return buyer_name_kr;
    }

    public void setBuyer_name_kr(String buyer_name_kr) {
        this.buyer_name_kr = buyer_name_kr;
    }

    public String getBuyer_phone() {
        return buyer_phone;
    }

    public void setBuyer_phone(String buyer_phone) {
        this.buyer_phone = buyer_phone;
    }

    public String getBuyer_name_en() {
        return buyer_name_en;
    }

    public void setBuyer_name_en(String buyer_name_en) {
        this.buyer_name_en = buyer_name_en;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getSub_total() {
        return sub_total;
    }

    public void setSub_total(double sub_total) {
        this.sub_total = sub_total;
    }

    public String getPay_via() {
        return pay_via;
    }

    public void setPay_via(String pay_via) {
        this.pay_via = pay_via;
    }

    public boolean isIfAuctionItem() {
        return ifAuctionItem;
    }

    public void setIfAuctionItem(boolean ifAuctionItem) {
        this.ifAuctionItem = ifAuctionItem;
    }

    public CartItem_T1 getFeaturedItem() {
        return featuredItem;
    }

    public void setFeaturedItem(CartItem_T1 featuredItem) {
        this.featuredItem = featuredItem;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public List<CartItem_T1> getItems() {
        return items;
    }

    public void setItems(List<CartItem_T1> items) {
        this.items = items;
    }

    public Shipping_T1 getShippingDetails() {
        return shippingDetails;
    }

    public void setShippingDetails(Shipping_T1 shippingDetails) {
        this.shippingDetails = shippingDetails;
    }

    public double getItem_total() {
        return item_total;
    }

    public void setItem_total(double item_total) {
        this.item_total = item_total;
    }

    public double getShipping_cost() {
        return shipping_cost;
    }

    public void setShipping_cost(double shipping_cost) {
        this.shipping_cost = shipping_cost;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getPay_total() {
        return pay_total;
    }

    public void setPay_total(double pay_total) {
        this.pay_total = pay_total;
    }

    public String getGateway_used() {
        return gateway_used;
    }

    public void setGateway_used(String gateway_used) {
        this.gateway_used = gateway_used;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getPay_id() {
        return pay_id;
    }

    public void setPay_id(String pay_id) {
        this.pay_id = pay_id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public List<AuctionItem_T1> getAuctionItem() {
        return AuctionItem;
    }

    public void setAuctionItem(List<AuctionItem_T1> auctionItem) {
        AuctionItem = auctionItem;
    }

    public CartItem_T1 getFeaturedItem_Auction() {
        return featuredItem_Auction;
    }

    public void setFeaturedItem_Auction(CartItem_T1 featuredItem_Auction) {
        this.featuredItem_Auction = featuredItem_Auction;
    }


}
