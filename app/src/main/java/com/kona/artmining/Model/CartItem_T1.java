package com.kona.artmining.Model;


public class CartItem_T1
{

    String art_id;
    String catrt_id;
    String ship_cost;
    String ship_cost_cash;
    String title_en;
    String title_kr;
    boolean sold_out;
    String currency;
    double price;
    String name_en;
    String name_kr;
    String main_image_watermarked;
    String main_image_thumb;
    boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isSold_out() {
        return sold_out;
    }

    public void setSold_out(boolean sold_out) {
        this.sold_out = sold_out;
    }

    public String getCatrt_id() {
        return catrt_id;
    }

    public void setCatrt_id(String catrt_id) {
        this.catrt_id = catrt_id;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getTitle_kr() {
        return title_kr;
    }

    public void setTitle_kr(String title_kr) {
        this.title_kr = title_kr;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getArtistName_en() {
        return name_en;
    }

    public void setArtistName_en(String artistName_en) {
        this.name_en = artistName_en;
    }

    public String getArtistName_kr() {
        return name_kr;
    }

    public void setArtistName_kr(String artistName_kr) {
        this.name_kr = artistName_kr;
    }

    public String getMain_image_watermarked() {
        return main_image_watermarked;
    }

    public void setMain_image_watermarked(String main_image_watermarked) {
        this.main_image_watermarked = main_image_watermarked;
    }

    public String getMain_image_thumb() {
        return main_image_thumb;
    }

    public void setMain_image_thumb(String main_image_thumb) {
        this.main_image_thumb = main_image_thumb;
    }

    public String getArt_id() {
        return art_id;
    }

    public void setArt_id(String art_id) {
        this.art_id = art_id;
    }


    public String getShip_cost() {
        return ship_cost;
    }

    public void setShip_cost(String ship_cost) {
        this.ship_cost = ship_cost;
    }

    public String getShip_cost_cash() {
        return ship_cost_cash;
    }

    public void setShip_cost_cash(String ship_cost_cash) {
        this.ship_cost_cash = ship_cost_cash;
    }
}
