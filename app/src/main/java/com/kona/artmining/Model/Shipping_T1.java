package com.kona.artmining.Model;


public class Shipping_T1
{

    boolean Overseas;
    String country;
    String country_code;
    String full_name;
    String address_1;
    String address_2;
    String city;
    String state;
    String zipcode;
    String DomesticOperator;
    String PhoneNumber;
    String extension;
    String address_type;
    String mobile_or_phone;

    public String getMobile_or_phone() {
        return mobile_or_phone;
    }

    public void setMobile_or_phone(String mobile_or_phone) {
        this.mobile_or_phone = mobile_or_phone;
    }

    public String getAddress_type() {
        return address_type;
    }

    public void setAddress_type(String address_type) {
        this.address_type = address_type;
    }

    public boolean isOverseas() {
        return Overseas;
    }

    public void setOverseas(boolean overseas) {
        Overseas = overseas;
    }

    public String getCountryName() {
        return country;
    }

    public void setCountryName(String countryName) {
        this.country = countryName;
    }

    public String getFullName() {
        return full_name;
    }

    public void setFullName(String fullName) {
        this.full_name = fullName;
    }

    public String getAddressLine1() {
        return address_1;
    }

    public void setAddressLine1(String addressLine1) {
        address_1 = addressLine1;
    }

    public String getAddressLine2() {
        return address_2;
    }

    public void setAddressLine2(String addressLine2) {
        address_2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String _city) {
        city = _city;
    }

    public String getState() {
        return state;
    }

    public void setState(String _state) {
        state = _state;
    }

    public String getZipCode() {
        return zipcode;
    }

    public void setZipCode(String zipCode) {
        zipcode = zipCode;
    }

    public String getPhoneNumber() {
        return mobile_or_phone;
    }

    public void setPhoneNumber(String phoneNumber) {
        mobile_or_phone = phoneNumber;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String _Extension) {
        extension = _Extension;
    }

    public String getDomesticOperator() {
        return DomesticOperator;
    }

    public void setDomesticOperator(String domesticOperator) {
        DomesticOperator = domesticOperator;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }


}
