package com.kona.artmining.Model;

import java.util.List;

public class Art_T1
{
    String title_en,title_kr,details_en,details_kr,size,ship_id, ship_type, material, year, expired_on, not_ship_to, currency, curator_percentage, created_on;

    int art_id, art_type,theme_id,artist_id,curator_id, status, modifed_by, delivery_in_person, details_id, posted_by;

    boolean sold_out, selected;

    double min_price,max_price,price;

    String main_image, main_image_watermarked, main_image_thumb;


    public String getMain_image() {
        return main_image;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public String getMain_image_watermarked() {
        return main_image_watermarked;
    }

    public void setMain_image_watermarked(String main_image_watermarked) {
        this.main_image_watermarked = main_image_watermarked;
    }

    public String getMain_image_thumb() {
        return main_image_thumb;
    }

    public void setMain_image_thumb(String main_image_thumb) {
        this.main_image_thumb = main_image_thumb;
    }

    public double getMin_price() {
        return min_price;
    }

    public void setMin_price(double min_price) {
        this.min_price = min_price;
    }

    public double getMax_price() {
        return max_price;
    }

    public void setMax_price(double max_price) {
        this.max_price = max_price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    List<ArtImages_T1> images;

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getTitle_kr() {
        return title_kr;
    }

    public void setTitle_kr(String title_kr) {
        this.title_kr = title_kr;
    }

    public String getDetails_en() {
        return details_en;
    }

    public void setDetails_en(String details_en) {
        this.details_en = details_en;
    }

    public String getDetails_kr() {
        return details_kr;
    }

    public void setDetails_kr(String details_kr) {
        this.details_kr = details_kr;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getShip_id() {
        return ship_id;
    }

    public void setShip_id(String ship_id) {
        this.ship_id = ship_id;
    }

    public String getShip_type() {
        return ship_type;
    }

    public void setShip_type(String ship_type) {
        this.ship_type = ship_type;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getExpired_on() {
        return expired_on;
    }

    public void setExpired_on(String expired_on) {
        this.expired_on = expired_on;
    }

    public String getNot_ship_to() {
        return not_ship_to;
    }

    public void setNot_ship_to(String not_ship_to) {
        this.not_ship_to = not_ship_to;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurator_percentage() {
        return curator_percentage;
    }

    public void setCurator_percentage(String curator_percentage) {
        this.curator_percentage = curator_percentage;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public int getArt_id() {
        return art_id;
    }

    public void setArt_id(int art_id) {
        this.art_id = art_id;
    }

    public int getArt_type() {
        return art_type;
    }

    public void setArt_type(int art_type) {
        this.art_type = art_type;
    }

    public int getTheme_id() {
        return theme_id;
    }

    public void setTheme_id(int theme_id) {
        this.theme_id = theme_id;
    }

    public int getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(int artist_id) {
        this.artist_id = artist_id;
    }

    public int getCurator_id() {
        return curator_id;
    }

    public void setCurator_id(int curator_id) {
        this.curator_id = curator_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getModifed_by() {
        return modifed_by;
    }

    public void setModifed_by(int modifed_by) {
        this.modifed_by = modifed_by;
    }

    public int getDelivery_in_person() {
        return delivery_in_person;
    }

    public void setDelivery_in_person(int delivery_in_person) {
        this.delivery_in_person = delivery_in_person;
    }

    public int getDetails_id() {
        return details_id;
    }

    public void setDetails_id(int details_id) {
        this.details_id = details_id;
    }

    public int getPosted_by() {
        return posted_by;
    }

    public void setPosted_by(int posted_by) {
        this.posted_by = posted_by;
    }

    public boolean isSold_out() {
        return sold_out;
    }

    public void setSold_out(boolean sold_out) {
        this.sold_out = sold_out;
    }

    public List<ArtImages_T1> getImages() {
        return images;
    }

    public void setImages(List<ArtImages_T1> images) {
        this.images = images;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
