package com.kona.artmining.Model;


public class DashboardFrgLatest_Event_Model_T1 {

    int event_id;
    String title;
    String details_info;
    String image;
    String image_thumb;
    String date;

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails_info() {
        return details_info;
    }

    public void setDetails_info(String details_info) {
        this.details_info = details_info;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_thumb() {
        return image_thumb;
    }

    public void setImage_thumb(String image_thumb) {
        this.image_thumb = image_thumb;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
