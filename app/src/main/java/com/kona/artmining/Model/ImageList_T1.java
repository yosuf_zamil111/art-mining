package com.kona.artmining.Model;


import java.util.Date;

public class ImageList_T1 {

    int art_id;
    int artist_id;
    String title_en;
    String title_kr;
    String email;
    int art_type;
    int status;
    Date posted_on;
    Date modifed_on;
    int modifed_by;
    int details_id;
    String main_image;
    String main_image_watermarked;
    String main_image_thumb;
    String profile_image_thumb;
    String sold_out;

    boolean clickable = true;

    public int getArt_id() {
        return art_id;
    }

    public void setArt_id(int art_id) {
        this.art_id = art_id;
    }

    public String getTitle_en() {
        return title_en;
    }

    public void setTitle_en(String title_en) {
        this.title_en = title_en;
    }

    public String getTitle_kr() {
        return title_kr;
    }

    public void setTitle_kr(String title_kr) {
        this.title_kr = title_kr;
    }

    public int getArt_type() {
        return art_type;
    }

    public void setArt_type(int art_type) {
        this.art_type = art_type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getPosted_on() {
        return posted_on;
    }

    public void setPosted_on(Date posted_on) {
        this.posted_on = posted_on;
    }

    public Date getModifed_on() {
        return modifed_on;
    }

    public void setModifed_on(Date modifed_on) {
        this.modifed_on = modifed_on;
    }

    public int getModifed_by() {
        return modifed_by;
    }

    public void setModifed_by(int modifed_by) {
        this.modifed_by = modifed_by;
    }

    public int getDetails_id() {
        return details_id;
    }

    public void setDetails_id(int details_id) {
        this.details_id = details_id;
    }

    public String getMain_image() {
        return main_image;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public String getMain_image_watermarked() {
        return main_image_watermarked;
    }

    public void setMain_image_watermarked(String main_image_watermarked) {
        this.main_image_watermarked = main_image_watermarked;
    }

    public String getMain_image_thumb() {
        return main_image_thumb;
    }

    public void setMain_image_thumb(String main_image_thumb) {
        this.main_image_thumb = main_image_thumb;
    }

    public boolean isClickable() {
        return clickable;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

    public int getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(int artist_id) {
        this.artist_id = artist_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile_image_thumb() {
        return profile_image_thumb;
    }

    public void setProfile_image_thumb(String profile_image_thumb) {
        this.profile_image_thumb = profile_image_thumb;
    }

    public String getSold_out() {
        return sold_out;
    }

    public void setSold_out(String sold_out) {
        this.sold_out = sold_out;
    }

}
