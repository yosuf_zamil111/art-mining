package com.kona.artmining.Model;


import java.util.List;

public class DashboardFrgLatest_NewRelease_Page_Model_T1
{

    String title;
    int id;
    List<DashboardFrgLatest_NewRelease_Model_T1> data;
    long count;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<DashboardFrgLatest_NewRelease_Model_T1> getData() {
        return data;
    }

    public void setData(List<DashboardFrgLatest_NewRelease_Model_T1> data) {
        this.data = data;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
