package com.kona.artmining.Model;



public class Comment_T2 {

    int comment_id, commentor_id;
    String comment_details, comment_date_time, commentor_english_name, commentor_korean_name, profile_image_thumb;

    public int getComment_id() {
        return comment_id;
    }

    public void setComment_id(int comment_id) {
        this.comment_id = comment_id;
    }

    public int getCommentor_id() {
        return commentor_id;
    }

    public void setCommentor_id(int commentor_id) {
        this.commentor_id = commentor_id;
    }

    public String getComment_details() {
        return comment_details;
    }

    public void setComment_details(String comment_details) {
        this.comment_details = comment_details;
    }

    public String getComment_date_time() {
        return comment_date_time;
    }

    public void setComment_date_time(String comment_date_time) {
        this.comment_date_time = comment_date_time;
    }

    public String getCommentor_english_name() {
        return commentor_english_name;
    }

    public void setCommentor_english_name(String commentor_english_name) {
        this.commentor_english_name = commentor_english_name;
    }

    public String getCommentor_korean_name() {
        return commentor_korean_name;
    }

    public void setCommentor_korean_name(String commentor_korean_name) {
        this.commentor_korean_name = commentor_korean_name;
    }

    public String getProfile_image_thumb() {
        return profile_image_thumb;
    }

    public void setProfile_image_thumb(String profile_image_thumb) {
        this.profile_image_thumb = profile_image_thumb;
    }
}
