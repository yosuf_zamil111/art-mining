package com.kona.artmining.Model;

public class ShippingMethod {

    int group_id;
    int user_id;
    double usd_cost;
    double won_cost;
    String group_name;
    int create_by;
    String created_date;

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public double getUsd_cost() {
        return usd_cost;
    }

    public void setUsd_cost(double usd_cost) {
        this.usd_cost = usd_cost;
    }

    public double getWon_cost() {
        return won_cost;
    }

    public void setWon_cost(double won_cost) {
        this.won_cost = won_cost;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public int getCreate_by() {
        return create_by;
    }

    public void setCreate_by(int create_by) {
        this.create_by = create_by;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }
}
