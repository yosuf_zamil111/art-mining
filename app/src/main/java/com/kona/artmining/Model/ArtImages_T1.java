package com.kona.artmining.Model;

public class ArtImages_T1 {

    int art_image_id, art_id, order_no, status;
    String main_image, main_image_watermarked, main_image_thumb;

    public int getArt_image_id() {
        return art_image_id;
    }

    public void setArt_image_id(int art_image_id) {
        this.art_image_id = art_image_id;
    }

    public int getArt_id() {
        return art_id;
    }

    public void setArt_id(int art_id) {
        this.art_id = art_id;
    }

    public int getOrder_no() {
        return order_no;
    }

    public void setOrder_no(int order_no) {
        this.order_no = order_no;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMain_image() {
        return main_image;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public String getMain_image_watermarked() {
        return main_image_watermarked;
    }

    public void setMain_image_watermarked(String main_image_watermarked) {
        this.main_image_watermarked = main_image_watermarked;
    }

    public String getMain_image_thumb() {
        return main_image_thumb;
    }

    public void setMain_image_thumb(String main_image_thumb) {
        this.main_image_thumb = main_image_thumb;
    }
}
