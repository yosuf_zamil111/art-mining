package com.kona.artmining.UI.FragmentActivity;


import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kona.artmining.Interface.ActivityCommunicator;
import com.kona.artmining.Interface.FragmentCommunicator;
import com.kona.artmining.R;
import com.kona.artmining.UI.Activity.DashboardActivity;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs.DFLF_Event;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs.DFLF_Exhibitions;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs.DFLF_NewRelease;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs.DFLF_Reviews;
import com.kona.artmining.custom_components.PagerSlidingTabStrip;


public class DashboardFrgLatest extends Fragment implements FragmentCommunicator
{
    public static final String ARG_INITIAL_POSITION = "ARG_INITIAL_POSITION";

    FragmentActivity me;
    ActivityCommunicator ac;

    ViewPager dashboard_frg_latest_pager;
    DashboardFrgLatestPagerAdapter adapter;
    PagerSlidingTabStrip latestPager_sts;

    TextView exhibitionsBtt, newreleaseBtt, reviewsBtt, eventBtt;

    String[] TabNames = { "Exhibitions", "New Release", "Reviews", "Event"};

    public static final String TAG = DashboardFrgLatest.class.getName();

    public DashboardFrgLatest(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        me=getActivity();
    }

    public static DashboardFrgLatest newInstance(int param1) {
        DashboardFrgLatest fragment = new DashboardFrgLatest();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ui_dashboard_frgmnts_latest, container, false);
        PrepareStage(view);

        return view;
    }

    void PrepareStage(View view)
    {
        latestPager_sts = (PagerSlidingTabStrip) view.findViewById(R.id.latestPager_sts);
        dashboard_frg_latest_pager = (ViewPager) view.findViewById(R.id.latestPager);

        adapter = new DashboardFrgLatestPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.setTitles(TabNames);
        dashboard_frg_latest_pager.setAdapter(adapter);
        latestPager_sts.setViewPager(dashboard_frg_latest_pager);

        latestPager_sts.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {}

            @Override
            public void onPageSelected(int i) {
                ResetAllTabs();
                MakeTabSelected(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        exhibitionsBtt = (TextView) view.findViewById(R.id.exhibitionsBtt);
        exhibitionsBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dashboard_frg_latest_pager.setCurrentItem(0);
            }
        });
        newreleaseBtt = (TextView) view.findViewById(R.id.newreleaseBtt);
        newreleaseBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dashboard_frg_latest_pager.setCurrentItem(1);
            }
        });
        reviewsBtt = (TextView) view.findViewById(R.id.reviewsBtt);
        reviewsBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dashboard_frg_latest_pager.setCurrentItem(2);
            }
        });
        eventBtt = (TextView) view.findViewById(R.id.eventBtt);
        eventBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dashboard_frg_latest_pager.setCurrentItem(3);
            }
        });


        dashboard_frg_latest_pager.setCurrentItem(0);
        MakeTabSelected(0);
    }

    void ResetAllTabs()
    {
        exhibitionsBtt.setBackgroundColor(Color.parseColor("#4b4b4b"));
        exhibitionsBtt.setTextColor(Color.parseColor("#dcdcdc"));
        newreleaseBtt.setBackgroundColor(Color.parseColor("#4b4b4b"));
        newreleaseBtt.setTextColor(Color.parseColor("#dcdcdc"));
        reviewsBtt.setBackgroundColor(Color.parseColor("#4b4b4b"));
        reviewsBtt.setTextColor(Color.parseColor("#dcdcdc"));
        eventBtt.setBackgroundColor(Color.parseColor("#4b4b4b"));
        eventBtt.setTextColor(Color.parseColor("#dcdcdc"));
    }

    void MakeTabSelected(int position)
    {
        switch(position) {
            case 0:
                exhibitionsBtt.setBackgroundColor(Color.parseColor("#262626"));
                exhibitionsBtt.setTextColor(Color.parseColor("#ffd200"));
                break;
            case 1:
                newreleaseBtt.setBackgroundColor(Color.parseColor("#262626"));
                newreleaseBtt.setTextColor(Color.parseColor("#ffd200"));
                break;
            case 2:
                reviewsBtt.setBackgroundColor(Color.parseColor("#262626"));
                reviewsBtt.setTextColor(Color.parseColor("#ffd200"));
                break;
            case 3:
                eventBtt.setBackgroundColor(Color.parseColor("#262626"));
                eventBtt.setTextColor(Color.parseColor("#ffd200"));
                break;
            default:
                exhibitionsBtt.setBackgroundColor(Color.parseColor("#262626"));
                exhibitionsBtt.setTextColor(Color.parseColor("#ffd200"));
                break;
        }
    }

    public class DashboardFrgLatestPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = { };

        DashboardFrgLatestPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        void setTitles(String[] TITLES)
        {
            this.TITLES = TITLES;
        }

        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return DFLF_Exhibitions.newInstance(position);
                case 1:
                    return DFLF_NewRelease.newInstance(position);
                case 2:
                    return DFLF_Reviews.newInstance(position);
                case 3:
                    return DFLF_Event.newInstance(position);
                default:
                    return null;
            }
        }
    }

    @Override
    public void passDataToFragment(String someValue) {

        Log.i(TAG, someValue);

    }

    @Override
    public void reloadThePage() {

    }

    @Override
    public void _DevicebackPressed() {

    }

    @Override
    public boolean ifDetailsPageHiden() {
        return false;
    }

    @Override
    public void showDetails(int id) {

    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        DashboardActivity DA = (DashboardActivity) getActivity();
        DA.fc_latest = this;
        ac = (ActivityCommunicator) getActivity();
    }
}
