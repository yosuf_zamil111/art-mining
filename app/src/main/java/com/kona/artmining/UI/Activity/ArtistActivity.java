package com.kona.artmining.UI.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kona.artmining.Helper.Artist;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.R;
import com.kona.artmining.custom_components.FormField_T1_TextBox;
import com.kona.artmining.custom_components.SquareButton_T1;
import com.kona.artmining.utils.Network;
import com.kona.artmining.utils.PicassoRoundTransform;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;
import net.hockeyapp.android.metrics.MetricsManager;

public class ArtistActivity extends AppCompatActivity {

    LinearLayout ui_user_reset_search_bckBtt;

    FrameLayout ArtistProfileEditPage,ui_artist_layout;

    int artist_id;
    UserProfile currentArtistProfile;

    FormField_T1_TextBox ui_artist_edit_profile_about_kr;
    FormField_T1_TextBox ui_artist_edit_profile_about_eng;
    FormField_T1_TextBox ui_artist_edit_profile_pkr;
    FormField_T1_TextBox ui_artist_edit_profile_peng;

    TextView ui_artist_edit_profile_heading_name;
    TextView ui_artist_edit_profile_email;

    SquareButton_T1 ui_artist_edit_profile_done_button;

    CircularImageView profilePicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist);
        getIntentMessage();

        //HockeyApp();
        prepareStage();
    }

    void prepareStage()
    {
        ui_user_reset_search_bckBtt = (LinearLayout) findViewById(R.id.ui_user_reset_search_bckBtt);
        ui_user_reset_search_bckBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getActivity().finish();
                Toast.makeText(getWindow().getContext(),"Finish", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        if(!Network.isNetworkAvailable(getWindow().getContext()))
        {
            Toast.makeText(getApplicationContext(), "Internet Connection Required", Toast.LENGTH_LONG).show();
            finish();
        }

        profilePicture = (CircularImageView) findViewById(R.id.profilePicture);

        ArtistProfileEditPage = (FrameLayout) findViewById(R.id.ArtistProfileEditPage);
        ArtistProfileEditPage.setVisibility(View.VISIBLE);

        ui_artist_edit_profile_about_kr  = (FormField_T1_TextBox) findViewById(R.id.ui_artist_edit_profile_about_kr);
        ui_artist_edit_profile_about_eng = (FormField_T1_TextBox) findViewById(R.id.ui_artist_edit_profile_about_eng);
        ui_artist_edit_profile_pkr = (FormField_T1_TextBox) findViewById(R.id.ui_artist_edit_profile_pkr);
        ui_artist_edit_profile_peng = (FormField_T1_TextBox) findViewById(R.id.ui_artist_edit_profile_peng);

        ui_artist_edit_profile_done_button = (SquareButton_T1) findViewById(R.id.ui_artist_edit_profile_done_button);

        ui_artist_edit_profile_heading_name = (TextView) findViewById(R.id.ui_artist_edit_profile_heading_name);
        ui_artist_edit_profile_email = (TextView) findViewById(R.id.ui_artist_edit_profile_email);

        Artist.GetProfileDataProfile(String.valueOf(artist_id), new Artist.artistProfileRetrieve() {
            @Override
            public void Success(UserProfile userProfile) {
                currentArtistProfile = userProfile;
                FillUp_EditProfilePage_WithData();
            }

            @Override
            public void TokenRefreshRequired() {
                Artist.GetProfileDataProfile(String.valueOf(artist_id), new Artist.artistProfileRetrieve() {
                    @Override
                    public void Success(UserProfile userProfile) {
                        currentArtistProfile = userProfile;
                        FillUp_EditProfilePage_WithData();
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        Toast.makeText(getApplicationContext(),"Token loop found.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
            }
        });

        ui_artist_edit_profile_done_button._onClick(new SquareButton_T1._click() {
            @Override
            public void done() {

                currentArtistProfile.setAbout_kr(ui_artist_edit_profile_about_kr.getText());
                currentArtistProfile.setAbout_en(ui_artist_edit_profile_about_eng.getText());
                currentArtistProfile.setProfile_kr(ui_artist_edit_profile_pkr.getText());
                currentArtistProfile.setProfile_en(ui_artist_edit_profile_peng.getText());

                Artist.UpdateArtistProfile(currentArtistProfile, new Artist.message() {
                    @Override
                    public void Success(String message) {
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        Artist.UpdateArtistProfile(currentArtistProfile, new Artist.message() {
                            @Override
                            public void Success(String message) {
                                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                                finish();
                            }

                            @Override
                            public void TokenRefreshRequired() {

                            }

                            @Override
                            public void Error(String message) {
                                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
    }

    void FillUp_EditProfilePage_WithData()
    {
        ui_artist_edit_profile_heading_name.setText(currentArtistProfile.getName());
        ui_artist_edit_profile_email.setText(currentArtistProfile.getEmail());
        ui_artist_edit_profile_about_kr.setText(currentArtistProfile.getAbout_kr());
        ui_artist_edit_profile_about_eng.setText(currentArtistProfile.getAbout_en());

        ui_artist_edit_profile_pkr.setText(currentArtistProfile.getProfile_kr());
        ui_artist_edit_profile_peng.setText(currentArtistProfile.getProfile_en());

        Picasso.with(getWindow().getContext())
                .load(currentArtistProfile.getThumb_image())
                .placeholder(R.drawable.transparent)
                .error(R.drawable.transparent)
                .transform(new PicassoRoundTransform())
                .into(profilePicture);
    }

    void getIntentMessage()
    {
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            artist_id = extras.getInt("artist_id");
            // and get whatever type user account id is
        }
    }

    void HockeyApp()
    {
        checkForUpdates();
        MetricsManager.register(this, getApplication());
    }

    @Override
    public void onBackPressed()
    {
        /*if(findViewById(R.id.UserProfileEditPage).getVisibility()==View.VISIBLE)
        {
            findViewById(R.id.UserProfileEditPage).setVisibility(View.GONE);
        }else{
            finish();
        }*/
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        // ... your own onResume implementation
        checkForCrashes();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }
}
