package com.kona.artmining.UI.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.kona.artmining.Adapter.OrderListItemAdapter_T1;
import com.kona.artmining.Helper.Orders;
import com.kona.artmining.Helper.User;
import com.kona.artmining.Model.Order_T1;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.R;
import com.kona.artmining.utils.PicassoRoundTransform;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OrderActivity extends AppCompatActivity {


    ObservableRecyclerView OrderList_RecyclerView;
    OrderListItemAdapter_T1 Order_rvListAdapter;
    SwipeRefreshLayout OrderList_swipeRefreshLayout;
    List<Order_T1> OrderContents = new ArrayList<Order_T1>();
    LinearLayout backBtt;
    LinearLayout orderDetailsPage, detailsBackBtt, detailsPage_TopSuccessfulMessage, orderDetailsPage_paymentItemShippingOnCash_holder;
    TextView detailsPage_TopSuccessfulMessage_title, detailsPage_TopSuccessfulMessage_Subtitle, detailsPage_TopSuccessfulMessage_Subtitle2, orderDetailsPage_date, orderDetailsPage_orderNumber, orderDetailsPage_featuredTitle, orderDetailsPage_ArtistName, orderDetailsPage_amount, orderDetailsPage_paymentItemShippingOnCash;

    TextView orderDetailsPage_shippingFullName, orderDetailsPage_shippingPhoneNumber, orderDetailsPage_shippingAddress, orderDetailsPage_paymentMethod, orderDetailsPage_paymentItem, orderDetailsPage_paymentItemTotals, orderDetailsPage_paymentItemShipping, orderDetailsPage_paymentItemTotal;

    ImageView orderDetailsPage_featuredimage, orderDetailsPage_ifAuctioned;

    Button showOrderList;

    boolean ShowDetailsPage;

    ProgressDialog progress;

    UserProfile currentUserProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);


        initMessageFromBundle();

        OrderList_RecyclerView = (ObservableRecyclerView) findViewById(R.id.OrderList_RecyclerView);
        ObservableRecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        OrderList_RecyclerView.setLayoutManager(mLayoutManager);
        Order_rvListAdapter = new OrderListItemAdapter_T1(OrderContents, new OrderListItemAdapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(Order_T1 ot1) {
                detailsPage_TopSuccessfulMessage.setVisibility(View.GONE);
                ShowDetailsPage(ot1);
            }
        });
        OrderList_RecyclerView.setAdapter(Order_rvListAdapter);

        OrderList_swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.OrderList_swipeRefreshLayout);
        OrderList_swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                PopulateData();
            }
        });

        backBtt = (LinearLayout) findViewById(R.id.backBtt);
        backBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        detailsBackBtt = (LinearLayout) findViewById(R.id.detailsBackBtt);
        detailsBackBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderDetailsPage.setVisibility(View.GONE);
            }
        });

        orderDetailsPage = (LinearLayout) findViewById(R.id.orderDetailsPage);
        orderDetailsPage.setVisibility(View.GONE);

        detailsPage_TopSuccessfulMessage = (LinearLayout) findViewById(R.id.detailsPage_TopSuccessfulMessage);
        detailsPage_TopSuccessfulMessage.setVisibility(View.GONE);

        orderDetailsPage_paymentItemShippingOnCash_holder = (LinearLayout) findViewById(R.id.orderDetailsPage_paymentItemShippingOnCash_holder);
        orderDetailsPage_paymentItemShippingOnCash_holder.setVisibility(View.GONE);

        orderDetailsPage_paymentItemShippingOnCash = (TextView) findViewById(R.id.orderDetailsPage_paymentItemShippingOnCash);

        detailsPage_TopSuccessfulMessage_title = (TextView) findViewById(R.id.detailsPage_TopSuccessfulMessage_title);
        detailsPage_TopSuccessfulMessage_Subtitle = (TextView) findViewById(R.id.detailsPage_TopSuccessfulMessage_Subtitle);
        detailsPage_TopSuccessfulMessage_Subtitle2 = (TextView) findViewById(R.id.detailsPage_TopSuccessfulMessage_Subtitle2);

        orderDetailsPage_date = (TextView) findViewById(R.id.orderDetailsPage_date);
        orderDetailsPage_orderNumber = (TextView) findViewById(R.id.orderDetailsPage_orderNumber);
        orderDetailsPage_featuredimage = (ImageView) findViewById(R.id.orderDetailsPage_featuredimage);
        orderDetailsPage_ifAuctioned = (ImageView) findViewById(R.id.orderDetailsPage_ifAuctioned);
        orderDetailsPage_ArtistName = (TextView) findViewById(R.id.orderDetailsPage_ArtistName);
        orderDetailsPage_featuredTitle = (TextView) findViewById(R.id.orderDetailsPage_featuredTitle);
        orderDetailsPage_amount = (TextView) findViewById(R.id.orderDetailsPage_amount);


        orderDetailsPage_shippingFullName = (TextView) findViewById(R.id.orderDetailsPage_shippingFullName);
        orderDetailsPage_shippingPhoneNumber = (TextView) findViewById(R.id.orderDetailsPage_shippingPhoneNumber);
        orderDetailsPage_shippingAddress = (TextView) findViewById(R.id.orderDetailsPage_shippingAddress);
        orderDetailsPage_paymentMethod = (TextView) findViewById(R.id.orderDetailsPage_paymentMethod);
        orderDetailsPage_paymentItem = (TextView) findViewById(R.id.orderDetailsPage_paymentItem);
        orderDetailsPage_paymentItemTotals = (TextView) findViewById(R.id.orderDetailsPage_paymentItemTotals);
        orderDetailsPage_paymentItemShipping = (TextView) findViewById(R.id.orderDetailsPage_paymentItemShipping);
        orderDetailsPage_paymentItemTotal = (TextView) findViewById(R.id.orderDetailsPage_paymentItemTotal);

        showOrderList = (Button) findViewById(R.id.showOrderList);
        showOrderList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderDetailsPage.setVisibility(View.GONE);
            }
        });


        progress = ProgressDialog.show(getWindow().getContext(), "Retrieving details.", "Please wait for a while.", true);
        User.GetProfileDataProfile(getWindow().getContext(), new User.userProfileRetrieve() {
            @Override
            public void Success(UserProfile userProfile) {
                currentUserProfile = userProfile;
                //detailsPage_TopSuccessfulMessage_title.setText(currentUserProfile.getProfile_en());
                PopulateData();
                Fill_upScreen();
            }

            @Override
            public void TokenRefreshRequired()
            {
                User.GetProfileDataProfile(getWindow().getContext(), new User.userProfileRetrieve()
                {
                    @Override
                    public void Success(UserProfile userProfile)
                    {
                        currentUserProfile = userProfile;
                        //detailsPage_TopSuccessfulMessage_title.setText(currentUserProfile.getProfile_en());
                        PopulateData();
                        Fill_upScreen();
                    }

                    @Override
                    public void TokenRefreshRequired()
                    {
                        Toast.makeText(getApplicationContext(),"Token loop found.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void Error(String message)
                    {
                        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initMessageFromBundle() {
        Bundle data = getIntent().getExtras();
        if (data != null)
        {
            ShowDetailsPage = true;
            /*tc_type = data.getString(GeneralConstants.TC_TYPE);
            SelectedCurrency  = data.getString(GeneralConstants.SELECTED_CURRENCY);
            Prefs.putString(GeneralConstants.TC_TYPE, tc_type);*/
        }
    }

    void ShowDetailsPage(Order_T1 orderDetails)
    {
        orderDetailsPage.setVisibility(View.VISIBLE);

        orderDetailsPage_date.setText(orderDetails.getOrderDate());


        orderDetailsPage_orderNumber.setText("(Order Num : "+orderDetails.getTransaction_id()+")");

        Picasso.with(getWindow().getContext())
                .load(orderDetails.getFeaturedItem().getMain_image_thumb())
                .placeholder(R.drawable.transparent)
                .error(R.drawable.transparent)
                .fit()
                .into(orderDetailsPage_featuredimage);

        orderDetailsPage_featuredTitle.setText(orderDetails.getFeaturedItem().getTitle_en());
        orderDetailsPage_ArtistName.setText(orderDetails.getFeaturedItem().getArtistName_en());
        orderDetailsPage_amount.setText(orderDetails.getFeaturedItem().getPrice()+" "+orderDetails.getFeaturedItem().getCurrency());

        System.out.println(orderDetails.getShippingDetails().getFullName());

        orderDetailsPage_shippingFullName.setText(orderDetails.getShippingDetails().getFullName());
        orderDetailsPage_shippingPhoneNumber.setText(orderDetails.getShippingDetails().getExtension()+"-"+orderDetails.getShippingDetails().getPhoneNumber());
        orderDetailsPage_shippingAddress.setText(orderDetails.getShippingDetails().getAddressLine1() + " \n " +orderDetails.getShippingDetails().getAddressLine2());
        orderDetailsPage_paymentMethod.setText(orderDetails.getGateway_used());

        orderDetailsPage_paymentItem.setText("Items ("+orderDetails.getItems().size()+")");
        orderDetailsPage_paymentItemTotals.setText(orderDetails.getCurrency()+" "+orderDetails.getItem_total());
        orderDetailsPage_paymentItemShipping.setText(orderDetails.getCurrency()+" "+orderDetails.getShipping_cost());
        orderDetailsPage_paymentItemTotal.setText(orderDetails.getCurrency()+" "+orderDetails.getPay_total());

        orderDetailsPage_paymentItemShippingOnCash.setText(orderDetails.getCurrency()+" "+orderDetails.getFeaturedItem().getShip_cost_cash());

        if(orderDetails.getFeaturedItem().getShip_cost_cash() !=null)
        {
            if(Double.valueOf(orderDetails.getFeaturedItem().getShip_cost_cash())>0)
            {
                orderDetailsPage_paymentItemShippingOnCash_holder.setVisibility(View.VISIBLE);
            }
            else
            {
                orderDetailsPage_paymentItemShippingOnCash_holder.setVisibility(View.GONE);
            }
        }
        else
        {
            orderDetailsPage_paymentItemShippingOnCash_holder.setVisibility(View.GONE);
        }

        if(orderDetails.getOrder_type().equals("2"))
        {
            orderDetailsPage_ifAuctioned.setVisibility(View.VISIBLE);
        }
        else
        {
            orderDetailsPage_ifAuctioned.setVisibility(View.GONE);
        }
    }

    void SetTopMessage(String title, String subtitle, String subtitle2)
    {
        detailsPage_TopSuccessfulMessage_title.setText(title);
        detailsPage_TopSuccessfulMessage_Subtitle.setText(subtitle);
        detailsPage_TopSuccessfulMessage_Subtitle2.setText(subtitle2);
    }

    void ShowAOrder(Order_T1 orderDetails)
    {
        ShowDetailsPage(orderDetails);
        detailsPage_TopSuccessfulMessage.setVisibility(View.VISIBLE);
        SetTopMessage("Your order has been successfully completed", ("Order Num : "+orderDetails.getTransaction_id()), "When shipping begins the shipping information will be sent via SMS.\n" +
                "You can track the delivery.");



    }
    void Fill_upScreen()
    {
        //ui_user_profile_heading_name.setText(currentUserProfile.getName());
        detailsPage_TopSuccessfulMessage_title.setText(currentUserProfile.getName());
    }



    void PopulateData()
    {
        Orders.RetrieveAllOrders(new Orders.RetrieveAllOrders_response() {
            @Override
            public void Success(List<Order_T1> orders) {
                progress.dismiss();
                clearInboxRVData();
                OrderContents.addAll(orders);
                Order_rvListAdapter.notifyDataSetChanged();
                if(OrderList_swipeRefreshLayout != null)
                {
                    OrderList_swipeRefreshLayout.setRefreshing(false);
                }
                if(ShowDetailsPage)
                {
                    ShowDetailsPage = false;
                    ShowAOrder(OrderContents.get(0));
                }
            }

            @Override
            public void TokenRefreshRequired() {
                Orders.RetrieveAllOrders(new Orders.RetrieveAllOrders_response() {
                    @Override
                    public void Success(List<Order_T1> orders) {
                        progress.dismiss();
                        clearInboxRVData();
                        OrderContents.addAll(orders);
                        Order_rvListAdapter.notifyDataSetChanged();
                        if(OrderList_swipeRefreshLayout != null)
                        {
                            OrderList_swipeRefreshLayout.setRefreshing(false);
                        }
                        if(ShowDetailsPage)
                        {
                            ShowDetailsPage = false;
                            ShowAOrder(OrderContents.get(OrderContents.size()-1));
                        }
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        if(OrderList_swipeRefreshLayout != null)
                        {
                            OrderList_swipeRefreshLayout.setRefreshing(false);
                        }
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                if(OrderList_swipeRefreshLayout != null)
                {
                    OrderList_swipeRefreshLayout.setRefreshing(false);
                    OrderList_swipeRefreshLayout.setRefreshing(false);
                }
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void clearInboxRVData()
    {
        int size = OrderContents.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                OrderContents.remove(0);
            }
            Order_rvListAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void onBackPressed()
    {
        if(orderDetailsPage.getVisibility() == View.VISIBLE)
        {
            orderDetailsPage.setVisibility(View.GONE);
        }
        else
        {
            finish();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
}
