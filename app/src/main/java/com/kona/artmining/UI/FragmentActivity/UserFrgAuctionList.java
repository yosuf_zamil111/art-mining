package com.kona.artmining.UI.FragmentActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.google.gson.Gson;
import com.kona.artmining.Adapter.UserAuctionAdapter_T1;
import com.kona.artmining.Adapter.UserCartAdapter_T1;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Helper.Auction;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Interface.UserActivity_FrgMng_Interface;
import com.kona.artmining.Interface.UserFrgMngCommunicator;
import com.kona.artmining.Model.AuctionItem_T1;
import com.kona.artmining.R;
import com.kona.artmining.UI.Activity.ArtDetailsActivity;
import com.kona.artmining.UI.FragmentController.UserActivity_FragmentsManager;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;


public class UserFrgAuctionList extends Fragment implements UserFrgMngCommunicator
{
    LinearLayout shopping_cart_select_all_btt, shopping_cart_delete_btt;
    Button order_btt;

    ProgressDialog progress;

    ObservableRecyclerView recyclerView;
    UserAuctionAdapter_T1 rvListAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    List<AuctionItem_T1> Content = new ArrayList<AuctionItem_T1>();

    boolean selectAll = false;

    Activity activity;
    UserActivity_FrgMng_Interface mCallback;

    String SelectedCurrency = "";
    boolean allowToOrder = true;

    public static final int ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE = 400;

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            mCallback = (UserActivity_FrgMng_Interface) activity;
            UserActivity_FragmentsManager DA = (UserActivity_FragmentsManager) getActivity();
            DA.frg_listener = this;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement UserActivity_FrgMng_Interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.user_frg_auction_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStage(view);
    }

    void prepareStage(View view)
    {
        shopping_cart_select_all_btt = (LinearLayout) view.findViewById(R.id.shopping_cart_select_all_btt);
        shopping_cart_delete_btt = (LinearLayout) view.findViewById(R.id.shopping_cart_delete_btt);
        order_btt = (Button) view.findViewById(R.id.order_btt);

        recyclerView = (ObservableRecyclerView) view.findViewById(R.id.recycler_view);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        /*layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(position == 0 || position == 1)
                {
                    return 2;
                }else{
                    return 1;
                }
            }
        });*/
        recyclerView.setLayoutManager(layoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        rvListAdapter = new UserAuctionAdapter_T1(Content, new UserAuctionAdapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(AuctionItem_T1 item, int position, boolean isChecked)
            {
                Content.get(position).setChecked(isChecked);
                Update_OrderButtonStatus();
            }

            @Override
            public void doOrder(AuctionItem_T1 item) {
                SelectedCurrency = item.getCurrency();
                SaveSingleItemModifiedCart(item);
                mCallback.ShowPaymentProcessing("auc", SelectedCurrency);
                getActivity().finish();
            }

            @Override
            public void remove(AuctionItem_T1 item)
            {
                RemoveItem(item.getId(), new TheResponse() {
                    @Override
                    public void success(String response) {
                        RefreshPage();
                    }

                    @Override
                    public void failed() {

                    }
                });
            }

            @Override
            public void ShowDetails(AuctionItem_T1 item) {
                //mCallback.showArtDetailsPage(Integer.valueOf(item.getArt_id()));

                Intent intent = new Intent(getActivity(), ArtDetailsActivity.class);
                intent.putExtra(GeneralConstants.ID, Integer.valueOf(item.getArt_id()));
                getActivity().startActivityForResult(intent, ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE);
            }
        });
        recyclerView.setAdapter(rvListAdapter);

        swipeRefreshLayout.setProgressViewOffset(false,100,200);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(){
                RefreshPage();
            }
        });

        shopping_cart_select_all_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(selectAll)
                {
                    selectAll = false;
                }else
                {
                    selectAll = true;
                }

                for (int i = 0; i < Content.size(); i++) {
                    if(Content.get(i).isSold_out() && Content.get(i).isWinner() && !Content.get(i).isPurchased())
                    {
                        Content.get(i).setChecked(selectAll);
                        System.out.println("Position > "+i);
                    }
                }

                FillUpRV(Content, true);
            }
        });

        view.findViewById(R.id.shopping_cart_backBtt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        shopping_cart_delete_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String removeItems = "";

                for (int i = 0; i < Content.size(); i++) {
                    if(Content.get(i).isChecked())
                    {
                        if(i==0)
                        {
                            removeItems = removeItems.concat(Content.get(i).getId());
                        }else{
                            removeItems = removeItems.concat(","+Content.get(i).getId());
                        }
                        System.out.println("We have to delete it: "+Content.get(i).getId());
                    }
                }

                if(!TextUtils.isEmpty(removeItems))
                {
                    RemoveItem(removeItems, new TheResponse() {
                        @Override
                        public void success(String response) {
                            RefreshPage();
                        }

                        @Override
                        public void failed() {

                        }
                    });
                }else{
                    Toast.makeText(getContext(), "Please select some items.", Toast.LENGTH_LONG).show();
                }
            }
        });


        
        order_btt.setEnabled(false);
        order_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SaveModifiedCart();
                if(allowToOrder)
                {
                    mCallback.ShowPaymentProcessing("auc", SelectedCurrency);
                    getActivity().finish();
                }
                else
                {
                    Toast.makeText(getContext(), "Same Currency Please.", Toast.LENGTH_LONG).show();
                }
            }
        });

        RefreshPage();
    }

    void SaveModifiedCart()
    {
        List<AuctionItem_T1> ModifiedAuctionCart = new ArrayList<AuctionItem_T1>();

        SelectedCurrency = "";
        int Pointer = 0;

        for (int i = 0; i < Content.size(); i++) {
            if (Content.get(i).isChecked()) {
                ModifiedAuctionCart.add(Content.get(i));
                if(Pointer == 0)
                {
                    SelectedCurrency = Content.get(i).getCurrency();
                    Pointer = Pointer + 1;
                }
                else
                {
                    if(!SelectedCurrency.equals(Content.get(i).getCurrency()))
                    {
                        allowToOrder = false;
                        break;
                    }
                    else
                    {
                        allowToOrder = true;
                        SelectedCurrency = Content.get(i).getCurrency();
                    }
                }
            }
        }

        Prefs.putString(GeneralConstants.MODIFIED_AUCTION_CART_JSON, new Gson().toJson(ModifiedAuctionCart));
    }

    void SaveSingleItemModifiedCart(AuctionItem_T1 CI_T1)
    {
        List<AuctionItem_T1> ModifiedCart = new ArrayList<AuctionItem_T1>();
        ModifiedCart.add(CI_T1);
        Prefs.putString(GeneralConstants.MODIFIED_AUCTION_CART_JSON, new Gson().toJson(ModifiedCart));
    }

    void RemoveItem(final String itemId, final TheResponse resp)
    {
        /*Cart.RemoveItem(itemId, new Cart.CartDataRemove() {
            @Override
            public void Success() {
                resp.success("");
            }

            @Override
            public void NotFound() {
                resp.failed();
            }

            @Override
            public void TokenRefreshRequired() {
                Cart.RemoveItem(itemId, new Cart.CartDataRemove() {
                    @Override
                    public void Success() {
                        resp.success("");
                    }

                    @Override
                    public void NotFound() {
                        resp.failed();
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        resp.failed();
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                        resp.failed();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                resp.failed();
            }
        });*/
    }

    void RefreshPage()
    {
        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

        Auction.GetDetails(getContext(), new Auction.userAuctionDataRetrieve() {
            @Override
            public void Success(List<AuctionItem_T1> scrapBookData) {
                FillUpRV(scrapBookData, false);
            }

            @Override
            public void TokenRefreshRequired() {
                Auction.GetDetails(getContext(), new Auction.userAuctionDataRetrieve() {
                    @Override
                    public void Success(List<AuctionItem_T1> scrapBookData) {
                        FillUpRV(scrapBookData, false);
                    }

                    @Override
                    public void TokenRefreshRequired()
                    {

                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void FillUpRV(List<AuctionItem_T1> CartItem, boolean refresh)
    {
        if(!refresh)
        {
            clearRVData();
            Content.addAll(CartItem);
        }

        if(Content.size()>0)
        {
            getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.GONE);
        }else{
            getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.VISIBLE);
        }

        rvListAdapter.notifyDataSetChanged();

        if(swipeRefreshLayout != null)
        {
            swipeRefreshLayout.setRefreshing(false);
        }
        Update_OrderButtonStatus();
        progress.dismiss();
    }

    public void clearRVData()
    {
        int size = Content.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Content.remove(0);
            }
            rvListAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    void Update_OrderButtonStatus()
    {
        boolean showBtt = false;

        for (int i = 0; i < Content.size(); i++) {
            if(Content.get(i).isSold_out() && Content.get(i).isWinner())
            {
                if(Content.get(i).isChecked())
                {
                    showBtt = true;
                    break;
                }
            }
        }

        order_btt.setEnabled(showBtt);

        if(Content.size() < 1)
        {
            order_btt.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        /*
        if (requestCode == FILE_CODE && resultCode == Activity.RESULT_OK)
        {

        }
        */
        if (requestCode == ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE && resultCode == Activity.RESULT_OK)
        {
            //showArtDetailsPage(int artID) {
            //setResult(ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE,new Intent().putExtra(GeneralConstants.ID, artID));
            Bundle data = intent.getExtras();
            mCallback.showArtDetailsPage(data.getInt(GeneralConstants.ID));
        }
    }

    @Override
    public void _DevicebackPressed() {

    }

    @Override
    public boolean ifDetailsPageHiden() {
        return true;
    }
}
