package com.kona.artmining.UI.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.kona.artmining.Adapter.TermsConditionListingAdapter;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.UserAuthentication;
import com.kona.artmining.Interface.TheJavascriptInterface;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.TermsCondition_T1;
import com.kona.artmining.R;
import com.kona.artmining.custom_components.FormField_T1_TextBox;
import com.kona.artmining.custom_components.FormField_T2_TextBox;
import com.kona.artmining.custom_components.FormField_T3_TextBox_Spinner;
import com.kona.artmining.custom_components.OvalButton_T1;
import com.kona.artmining.custom_components.Spinner_T2;
import com.kona.artmining.custom_components.UserTypeRadioGroup;
import com.kona.artmining.custom_popup_box.GenderSelectionPopUpBox;
import com.kona.artmining.custom_popup_box.OptionListing_T1;
import com.kona.artmining.custom_popup_box.SmsVerification;
import com.kona.artmining.decent_date_picker.DPicker;
import com.kona.artmining.model.RadioButtonInfo;
import com.kona.artmining.option_table.Table_T1;
import com.kona.artmining.option_table.modals.Col2RowDetails;
import com.kona.artmining.utils.Network;
import com.kona.artmining.utils.Utils_Helper;
import com.loopj.android.http.RequestParams;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.pixplicity.easyprefs.library.Prefs;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;
import net.hockeyapp.android.metrics.MetricsManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class UserAuthenticationActivity extends Activity {


    TextView registration_terms_and_conditions, login_terms_and_conditions;

    EditText ui_user_reset_password_input_edittext;
    OvalButton_T1 ui_user_reset_confirm_button;
    Button ui_user_reset_password_search_button;
    LinearLayout ui_user_authentication_reset_password_form, ui_user_authentication_reset_password_confirmation;

    OvalButton_T1 ui_login_signup_button, ui_login_login_button, ui_user_registration_back_button, ui_user_terms_conditions_agree_all_button,
            ui_user_registration_send_button, ui_user_terms_conditions_back_button, ui_user_terms_conditions_next_button, ui_login_reset_button;

    FormField_T1_TextBox ui_login_email_textbox, ui_login_password_textbox, ui_user_registration_input_password,
            ui_user_registration_input_repassword, ui_user_registration_input_name_korean, ui_user_registration_input_name_english,
            ui_user_registration_input_birthdate, ui_user_registration_input_gender;

    FormField_T3_TextBox_Spinner ui_user_registration_input_mobile_number;

    FormField_T2_TextBox ui_user_registration_address_zip, ui_user_registration_input_email;

    FrameLayout ui_user_registration_layout;
    LinearLayout reg_text_hide;
    FrameLayout ui_user_authentication_terms_and_conditions_layout;

    LinearLayout ui_user_login_layout, ui_login_autologin, ui_user_registration_zip_search_window;

    FormField_T2_TextBox ui_user_registration_address1, ui_user_registration_address2;
    LinearLayout htmlLayoutContainer;
    WebView htmlLayout;
    ImageView btn;

    UserTypeRadioGroup usrTypeRegGroup;

    Table_T1 ui_user_registration_zip_search_window_street_st, ui_user_registration_zip_search_window_house_st;

    Spinner_T2 ui_user_registration_zip_search_window_street_city_spinner, ui_user_registration_zip_search_window_street_city_province;

    Calendar calendar;

    ObservableRecyclerView recyclerView;
    TermsConditionListingAdapter rvListAdapter;
    List<TermsCondition_T1> Content = new ArrayList<TermsCondition_T1>();

    RelativeLayout ui_user_authentication__terms_conditions_details_page;
    TextView ui_user_authentication__terms_conditions_details_page_title, ui_user_authentication__terms_conditions_details_page_details;
    Button ui_user_authentication__terms_conditions_details_page_back_btt;
    CheckBox checkedIfAbove14;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_authentication);

        //HockeyApp();

        registration_terms_and_conditions = (TextView) findViewById(R.id.registration_terms_and_conditions);
        registration_terms_and_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ServerConstants.BASE_URL + "user/terms_and_conditions/7?toc_on=1"));
                startActivity(browserIntent);
            }
        });

        login_terms_and_conditions = (TextView) findViewById(R.id.login_terms_and_conditions);
        login_terms_and_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ServerConstants.BASE_URL + "user/terms_and_conditions/7?toc_on=1"));
                startActivity(browserIntent);
            }
        });


        ui_user_registration_zip_search_window = (LinearLayout) findViewById(R.id.ui_user_registration_zip_search_window);

        reg_text_hide = (LinearLayout) findViewById(R.id.reg_text_hide);
        reg_text_hide.setVisibility(View.GONE);
        /*ui_reg_text1 = (LinearLayout) findViewById(R.id.ui_reg_text1);
        ui_reg_text2 = (LinearLayout) findViewById(R.id.ui_reg_text2);
        ui_reg_text1.setVisibility(View.GONE);
        ui_reg_text2.setVisibility(View.GONE);*/


        /*final Intent intent = getIntent();
        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action)) {
            final List<String> segments = intent.getData().getPathSegments();
            if (segments.size() > 1) {
                System.out.println(segments.get(1));
            }
        }*/

        init();
    }

    void HockeyApp() {
        checkForUpdates();
        MetricsManager.register(this, getApplication());
    }

    @Override
    public void onResume() {
        super.onResume();
        // ... your own onResume implementation
        checkForCrashes();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    private void init() {
        if (!Network.isNetworkAvailable(getWindow().getContext())) {
            Toast.makeText(getApplicationContext(), "Internet Connection Required", Toast.LENGTH_LONG).show();
            finish();
        }

        calendar = Calendar.getInstance();
        LoginScreen_init();
        reset_password();
        RegistrationTC_init();
        RegistrationInputScreen_init();
        RegistrationInputScreen_ZipCodeScreen_init();
    }

    @Override
    public void onBackPressed() {
        if (ui_user_registration_zip_search_window.getVisibility() == View.VISIBLE) {
            ui_user_registration_zip_search_window.setVisibility(View.GONE);
            ui_user_registration_layout.setVisibility(View.VISIBLE);
        } else {
            if (ui_user_registration_layout.getVisibility() == View.VISIBLE) {
                ui_user_registration_layout.setVisibility(View.GONE);
                //ui_user_authentication_terms_and_conditions_layout.setVisibility(View.VISIBLE);
                ui_user_login_layout.setVisibility(View.VISIBLE);
            } else {
                if (ui_user_authentication__terms_conditions_details_page.getVisibility() == View.VISIBLE) {

                    ui_user_authentication__terms_conditions_details_page.setVisibility(View.GONE);
                    findViewById(R.id.TC_main_screen).setVisibility(View.VISIBLE);
                    //System.out.println("OK");
                } else {
                    if (ui_user_authentication_terms_and_conditions_layout.getVisibility() == View.VISIBLE) {
                        ui_user_authentication_terms_and_conditions_layout.setVisibility(View.GONE);
                        ui_user_login_layout.setVisibility(View.VISIBLE);
                    } else {
                        //finish();
                        /*if(ui_user_authentication_reset_password_form.getVisibility() ==View.VISIBLE){
                            ui_user_authentication_reset_password_form.setVisibility(View.GONE);
                            ui_user_login_layout.setVisibility(View.VISIBLE);
                        }else {
                            finish();
                        }*/
                        if (ui_user_authentication_reset_password_confirmation.getVisibility() == View.VISIBLE) {
                            ui_user_authentication_reset_password_confirmation.setVisibility(View.GONE);
                            ui_user_authentication_reset_password_form.setVisibility(View.VISIBLE);
                        } else {
                            if (ui_user_authentication_reset_password_form.getVisibility() == View.VISIBLE) {
                                ui_user_authentication_reset_password_form.setVisibility(View.GONE);
                                ui_user_login_layout.setVisibility(View.VISIBLE);
                            } else {
                                finish();
                            }
                        }
                    }
                }
            }
        }
    }

    private void LoginScreen_init() {
        TheJavascriptInterface JI = new TheJavascriptInterface(UserAuthenticationActivity.this);
        btn = (ImageView) findViewById(R.id.imgClk);
        ui_user_login_layout = (LinearLayout) findViewById(R.id.ui_user_login_layout);
        ui_login_email_textbox = (FormField_T1_TextBox) findViewById(R.id.ui_login_email_textbox);
        ui_login_email_textbox.setText("");
        ui_login_password_textbox = (FormField_T1_TextBox) findViewById(R.id.ui_login_password_textbox);
        ui_login_password_textbox.setText("");
        ui_login_autologin = (LinearLayout) findViewById(R.id.ui_login_autologin);
        ui_login_login_button = (OvalButton_T1) findViewById(R.id.ui_login_login_button);
        ui_login_reset_button = (OvalButton_T1) findViewById(R.id.ui_login_reset_button);
        ui_login_signup_button = (OvalButton_T1) findViewById(R.id.ui_login_signup_button);

        htmlLayout = (WebView) findViewById(R.id.fullscreen_webview_container);
        htmlLayoutContainer = (LinearLayout) findViewById(R.id.fullscreen_webview);
        htmlLayout.addJavascriptInterface(JI, "Android");
        WebSettings webSettings = htmlLayout.getSettings();
        webSettings.setJavaScriptEnabled(true);

        JI.setListener(new TheJavascriptInterface.listener() {
            @Override
            public void message(String m) {
                if (m.equals("closeWebView")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ui_user_login_layout.setVisibility(View.VISIBLE);
                            htmlLayoutContainer.setVisibility(View.GONE);
                        }
                    });
                }
            }

            @Override
            public void showDetailsPage(String art_id) {

            }
        });

        ui_user_login_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                return false;
            }
        });

        /*ui_login_email_textbox._onClick(new FormField_T1_TextBox._click() {
            @Override
            public void done() {
                String email = ui_login_email_textbox.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if (!email.matches(emailPattern)) {
                    Toast.makeText(getApplicationContext(), "Enter your valid email.", Toast.LENGTH_LONG).show();
                }
            }
        });*/

        findViewById(R.id.BackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ui_user_login_layout.setVisibility(View.VISIBLE);
                htmlLayoutContainer.setVisibility(View.GONE);
            }
        });

        ui_login_login_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {
                System.out.println(ui_login_email_textbox.getText());
                System.out.println(ui_login_password_textbox.getText());

                if (!TextUtils.isEmpty(ui_login_email_textbox.getText()) && !TextUtils.isEmpty(ui_login_password_textbox.getText())) {
                    if (android.util.Patterns.EMAIL_ADDRESS.matcher(ui_login_email_textbox.getText()).matches()) {
                        UserAuthentication.DoLogin(ui_login_email_textbox.getText(), ui_login_password_textbox.getText(), new TheResponse() {
                            @Override
                            public void success(String response) {
                                System.out.println(response);
                                Intent resultIntent = new Intent();
                                setResult(Activity.RESULT_OK, resultIntent);
                                finish();
                            }

                            @Override
                            public void failed() {
                                Toast.makeText(getWindow().getContext(), "Login error.", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        Toast.makeText(getWindow().getContext(), "Invalid Email Address", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getWindow().getContext(), "Put valid data.", Toast.LENGTH_LONG).show();
                }
            }
        });

        ui_login_signup_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {
               /* findViewById(R.id.ui_user_registration_layout).setVisibility(View.VISIBLE);*/
                //findViewById(R.id.ui_user_authentication_terms_and_conditions_layout).setVisibility(View.VISIBLE);
                findViewById(R.id.ui_user_registration_layout).setVisibility(View.VISIBLE);
                findViewById(R.id.ui_user_login_layout).setVisibility(View.GONE);
            }
        });

        ui_login_reset_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {
                /*htmlLayoutContainer.setVisibility(View.VISIBLE);
                ui_user_login_layout.setVisibility(View.GONE);
                htmlLayout.loadUrl(ServerConstants.URL_FORGOT_PASSWORD);*/
                // ui_user_login_layout.setVisibility(View.VISIBLE);
                ui_user_login_layout.setVisibility(View.GONE);
                findViewById(R.id.ui_user_reset_password_layout).setVisibility(View.VISIBLE);
                findViewById(R.id.ui_user_authentication_reset_password_form).setVisibility(View.VISIBLE);

                /* UserAuthentication.ResetPasswordMail(ui_user_reset_password_search_button.getText().toString(), new UserAuthentication.message() {
                    @Override
                    public void Success(String message) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        ui_user_login_layout.setVisibility(View.GONE);
                        findViewById(R.id.ui_user_reset_password_layout).setVisibility(View.VISIBLE);
                        findViewById(R.id.ui_user_authentication_reset_password_form).setVisibility(View.VISIBLE);
                    }
                    @Override
                    public void Error(String message) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }
                });*/
            }
        });

        ui_login_autologin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Prefs.getBoolean(UserConstants.DO_AUTO_LOGIN, false)) {
                    Prefs.putBoolean(UserConstants.DO_AUTO_LOGIN, false);
                    btn.setImageResource(R.drawable.btn_checkbox_nor);
                } else {
                    btn.setImageResource(R.drawable.btn_checkbox_sel);
                    Prefs.putBoolean(UserConstants.DO_AUTO_LOGIN, true);
                }
            }
        });
    }

    private void reset_password() {

        findViewById(R.id.ui_user_reset_search_bckBtt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.ui_user_authentication_reset_password_form).setVisibility(View.GONE);
                findViewById(R.id.ui_user_login_layout).setVisibility(View.VISIBLE);
            }
        });
        findViewById(R.id.ui_user_reset_password_confirmation_top_bckBtt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.ui_user_authentication_reset_password_confirmation).setVisibility(View.GONE);
                findViewById(R.id.ui_user_authentication_reset_password_form).setVisibility(View.VISIBLE);
            }
        });
        ui_user_reset_password_input_edittext = (EditText) findViewById(R.id.ui_user_reset_password_input_edittext);
        ui_user_reset_confirm_button = (OvalButton_T1) findViewById(R.id.ui_user_reset_confirm_button);
        ui_user_reset_confirm_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {
                if (ui_user_authentication_reset_password_confirmation.getVisibility() == View.VISIBLE) {
                    ui_user_authentication_reset_password_confirmation.setVisibility(View.GONE);
                    ui_user_login_layout.setVisibility(View.VISIBLE);
                } else {
                    finish();
                }
            }
        });
        ui_user_authentication_reset_password_form = (LinearLayout) findViewById(R.id.ui_user_authentication_reset_password_form);
        ui_user_authentication_reset_password_confirmation = (LinearLayout) findViewById(R.id.ui_user_authentication_reset_password_confirmation);
        ui_user_reset_password_search_button = (Button) findViewById(R.id.ui_user_reset_password_search_button);

        ui_user_reset_password_search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserAuthentication.ResetPasswordMail(ui_user_reset_password_input_edittext.getText().toString(), new UserAuthentication.message() {
                    @Override
                    public void Success(String message) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        ui_user_authentication_reset_password_form.setVisibility(View.GONE);
                        findViewById(R.id.ui_user_reset_password_layout).setVisibility(View.VISIBLE);
                        findViewById(R.id.ui_user_authentication_reset_password_confirmation).setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }
                });

               /* String email = ui_user_reset_password_input_edittext.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                if (email.matches(emailPattern)) {
                    //Toast.makeText(getApplicationContext(),"Valid email address",Toast.LENGTH_SHORT).show();
                    UserAuthentication.CheckIfEmailExists(ui_user_registration_input_email.getText(), new TheResponse() {

                        @Override
                        public void success(String response) {
                            Toast.makeText(getApplicationContext(), "Email Already Exists", Toast.LENGTH_LONG).show();
                        }
                        @Override
                        public void failed() {
                            Toast.makeText(getApplicationContext(), "No such email address found", Toast.LENGTH_LONG).show();
                        }
                    });

                }
                else {
                    Toast.makeText(getApplicationContext(),"Invalid email address", Toast.LENGTH_SHORT).show();
                }*/

            }
        });
    }

    /*LinearLayout Jubaka;*/

    private void RegistrationTC_init() {
        UserAuthentication.RetrieveTermsCondition(new UserAuthentication.termsConditions() {
            @Override
            public void Success(List<TermsCondition_T1> tc) {
                Content.addAll(tc);
                rvListAdapter.notifyDataSetChanged();
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_SHORT).show();
            }
        }, "reg");

        recyclerView = (ObservableRecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        rvListAdapter = new TermsConditionListingAdapter(Content, new TermsConditionListingAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TermsCondition_T1 tc) {
                findViewById(R.id.TC_main_screen).setVisibility(View.GONE);
                ui_user_authentication__terms_conditions_details_page.setVisibility(View.VISIBLE);
                ui_user_authentication__terms_conditions_details_page_title.setText(tc.getSectionTitle());
                ui_user_authentication__terms_conditions_details_page_details.setText(tc.getSectionDescription());
            }

            @Override
            public void checkUncheck(TermsCondition_T1 tc, int position, boolean ifChecked) {
                Content.get(position).setChecked(ifChecked);

                if (tc.isSection_required() && !ifChecked) {
                    ui_user_terms_conditions_agree_all_button.BackBackground(getResources().getDrawable(R.drawable.resetbuttonshape));
                }
                UpdateNextbuttonView();
            }
        });
        recyclerView.setAdapter(rvListAdapter);

        ui_user_registration_layout = (FrameLayout) findViewById(R.id.ui_user_registration_layout);
        ui_user_authentication_terms_and_conditions_layout = (FrameLayout) findViewById(R.id.ui_user_authentication_terms_and_conditions_layout);
        ui_user_terms_conditions_agree_all_button = (OvalButton_T1) findViewById(R.id.ui_user_terms_conditions_agree_all_button);
        ui_user_terms_conditions_agree_all_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {

                for (int i = 0; i < Content.size(); i++) {
                    Content.get(i).setChecked(true);
                }
                rvListAdapter.notifyDataSetChanged();
                UpdateNextbuttonView();
                ui_user_terms_conditions_agree_all_button.ChangeLayoutBackground(getResources().getDrawable(R.drawable.registrationbuttonshape3));
            }
        });

        ui_user_authentication__terms_conditions_details_page = (RelativeLayout) findViewById(R.id.ui_user_authentication__terms_conditions_details_page);
        ui_user_authentication__terms_conditions_details_page_title = (TextView) findViewById(R.id.ui_user_authentication__terms_conditions_details_page_title);
        ui_user_authentication__terms_conditions_details_page_details = (TextView) findViewById(R.id.ui_user_authentication__terms_conditions_details_page_details);

        ui_user_authentication__terms_conditions_details_page_back_btt = (Button) findViewById(R.id.ui_user_authentication__terms_conditions_details_page_back_btt);
        ui_user_authentication__terms_conditions_details_page_back_btt.setClickable(true);
        ui_user_authentication__terms_conditions_details_page_back_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.TC_main_screen).setVisibility(View.VISIBLE);
                ui_user_authentication__terms_conditions_details_page.setVisibility(View.GONE);
            }
        });

        ui_user_terms_conditions_back_button = (OvalButton_T1) findViewById(R.id.ui_user_terms_conditions_back_button);
        ui_user_terms_conditions_next_button = (OvalButton_T1) findViewById(R.id.ui_user_terms_conditions_next_button);
        ui_user_terms_conditions_next_button.setEnabled(false);
        ui_user_terms_conditions_next_button.ChangeLayoutBackground(getResources().getDrawable(R.drawable.disable_button_shape_2));
        ui_user_terms_conditions_next_button.ChangeLabelColor(Color.parseColor("#ffffff"));

        ui_user_terms_conditions_back_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {
                for (int i = 0; i < Content.size(); i++) {
                    Content.get(i).setChecked(false);
                }
                rvListAdapter.notifyDataSetChanged();

                ui_user_terms_conditions_agree_all_button.BackBackground(getResources().getDrawable(R.drawable.resetbuttonshape));
                findViewById(R.id.ui_user_authentication_terms_and_conditions_layout).setVisibility(View.GONE);
                findViewById(R.id.ui_user_login_layout).setVisibility(View.VISIBLE);
            }
        });
        ui_user_terms_conditions_next_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {

                boolean agreed = true;

                for (int i = 0; i < Content.size(); i++) {
                    if (!Content.get(i).isChecked() && Content.get(i).isSection_required()) {
                        agreed = false;
                        break;
                    }
                }

                if (agreed) {
                    findViewById(R.id.ui_user_authentication_terms_and_conditions_layout).setVisibility(View.GONE);
                    findViewById(R.id.ui_user_registration_layout).setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(getWindow().getContext(), "You need to agree on required Terms & Conditions.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    void UpdateNextbuttonView() {
        int p = 0;

        for (int i = 0; i < Content.size(); i++) {

            if (Content.get(i).isSection_required()) {
                p = p + 1;
            }

            if (Content.get(i).isSection_required() && Content.get(i).isChecked()) {
                p = p - 1;
            }
        }

        System.out.println(p);

        if (p == 0) {
            ui_user_terms_conditions_next_button.makeEnable(true);
            ui_user_terms_conditions_next_button.ChangeLayoutBackground(getResources().getDrawable(R.drawable.registrationbuttonshape2));
            ui_user_terms_conditions_next_button.ChangeLabelColor(Color.parseColor("#000000"));
        } else {
            ui_user_terms_conditions_next_button.makeEnable(false);
            ui_user_terms_conditions_next_button.ChangeLayoutBackground(getResources().getDrawable(R.drawable.disable_button_shape_2));
            ui_user_terms_conditions_next_button.ChangeLabelColor(Color.parseColor("#ffffff"));
        }
    }

    private void RegistrationInputScreen_init() {

        checkedIfAbove14 = (CheckBox) findViewById(R.id.checkedIfAbove14);

        ui_user_registration_send_button = (OvalButton_T1) findViewById(R.id.ui_user_registration_send_button);
        ui_user_registration_back_button = (OvalButton_T1) findViewById(R.id.ui_user_registration_back_button);
        ui_user_registration_back_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {
                //findViewById(R.id.ui_user_authentication_terms_and_conditions_layout).setVisibility(View.VISIBLE);
                findViewById(R.id.ui_user_login_layout).setVisibility(View.VISIBLE);
                findViewById(R.id.ui_user_registration_layout).setVisibility(View.GONE);
            }
        });

        ui_user_registration_input_email = (FormField_T2_TextBox) findViewById(R.id.ui_user_registration_input_email);
        ui_user_registration_input_email.setText("");
        ui_user_registration_input_password = (FormField_T1_TextBox) findViewById(R.id.ui_user_registration_input_password);
        ui_user_registration_input_password.setText("");
        ui_user_registration_input_password.setListener(new FormField_T1_TextBox._click() {
            @Override
            public void done() {

            }

            @Override
            public void Focused() {
                LetsActiveDoneButton();
            }

            @Override
            public void Unfocused() {
                LetsActiveDoneButton();
            }
        });
        ui_user_registration_input_repassword = (FormField_T1_TextBox) findViewById(R.id.ui_user_registration_input_repassword);
        ui_user_registration_input_repassword.setText("");
        ui_user_registration_input_repassword.setListener(new FormField_T1_TextBox._click() {
            @Override
            public void done() {

            }

            @Override
            public void Focused() {
                LetsActiveDoneButton();
            }

            @Override
            public void Unfocused() {
                LetsActiveDoneButton();
            }
        });
        ui_user_registration_input_name_korean = (FormField_T1_TextBox) findViewById(R.id.ui_user_registration_input_name_korean);
        ui_user_registration_input_name_korean.setText("");
        ui_user_registration_input_name_korean.setListener(new FormField_T1_TextBox._click() {
            @Override
            public void done() {
            }

            @Override
            public void Focused() {
                LetsActiveDoneButton();
            }

            @Override
            public void Unfocused() {
                LetsActiveDoneButton();
            }
        });
        ui_user_registration_input_name_english = (FormField_T1_TextBox) findViewById(R.id.ui_user_registration_input_name_english);
        ui_user_registration_input_name_english.setText("");
        ui_user_registration_input_name_english.setListener(new FormField_T1_TextBox._click() {
            @Override
            public void done() {

            }

            @Override
            public void Focused() {
                LetsActiveDoneButton();
            }

            @Override
            public void Unfocused() {
                LetsActiveDoneButton();
            }
        });
        ui_user_registration_input_birthdate = (FormField_T1_TextBox) findViewById(R.id.ui_user_registration_input_birthdate);
        ui_user_registration_input_birthdate.setText("");
        ui_user_registration_input_gender = (FormField_T1_TextBox) findViewById(R.id.ui_user_registration_input_gender);
        ui_user_registration_input_gender.setText("");
        ui_user_registration_input_mobile_number = (FormField_T3_TextBox_Spinner) findViewById(R.id.ui_user_registration_input_mobile_number);
        ui_user_registration_input_mobile_number.setText("");
        ui_user_registration_address_zip = (FormField_T2_TextBox) findViewById(R.id.ui_user_registration_address_zip);
        ui_user_registration_address_zip.setText("");
        ui_user_registration_address1 = (FormField_T2_TextBox) findViewById(R.id.ui_user_registration_address1);
        ui_user_registration_address1.setText("");
        ui_user_registration_address1.setListener(new FormField_T2_TextBox.Success() {
            @Override
            public void done() {

            }

            @Override
            public void Focused() {
                LetsActiveDoneButton();
            }

            @Override
            public void Unfocused() {
                LetsActiveDoneButton();
            }
        });
        ui_user_registration_address2 = (FormField_T2_TextBox) findViewById(R.id.ui_user_registration_address2);
        ui_user_registration_address2.setText("");
        ui_user_registration_address2.setListener(new FormField_T2_TextBox.Success() {
            @Override
            public void done() {

            }

            @Override
            public void Focused() {
                LetsActiveDoneButton();
            }

            @Override
            public void Unfocused() {
                LetsActiveDoneButton();
            }
        });

        ui_user_registration_send_button.makeEnable(false);
        ui_user_registration_send_button.setA_Background(
                getResources().getDrawable(R.drawable.registrationbuttonshape_dsbl),
                getResources().getColor(R.color.white)
        );

        ui_user_registration_send_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {

                if (ValidateRegForm(true)) {
                    ui_user_registration_send_button.setEnabled(false);

                    final RequestParams rp = new RequestParams();
                    rp.put(UserConstants.REG_FIELD_EMAIL, ui_user_registration_input_email.getText());
                    rp.put(UserConstants.REG_FIELD_PASSWORD, ui_user_registration_input_password.getText());
                    rp.put(UserConstants.REG_FIELD_PHONE_EXT, ui_user_registration_input_mobile_number.getSpinnerText());
                    rp.put(UserConstants.REG_FIELD_PHONE, ui_user_registration_input_mobile_number.getTextOnly());
                    rp.put(UserConstants.REG_FIELD_USER_TYPE, usrTypeRegGroup.getValue());
                    rp.put(UserConstants.REG_FIELD_NAME_EN, ui_user_registration_input_name_english.getText());
                    rp.put(UserConstants.REG_FIELD_NAME_KR, ui_user_registration_input_name_korean.getText());
                    rp.put(UserConstants.REG_FIELD_BIRTHDATE, ui_user_registration_input_birthdate.getTheOtherValue().replaceAll(". ", "/"));
                    rp.put(UserConstants.REG_FIELD_SEX, ui_user_registration_input_gender.getTheOtherValue());
                    rp.put(UserConstants.REG_FIELD_ADDRESS, ui_user_registration_address1.getText());
                    rp.put(UserConstants.REG_FIELD_ADDRESS2, ui_user_registration_address2.getText());
                    rp.put(UserConstants.REG_FIELD_ZIP, ui_user_registration_address_zip.getText());
                    rp.put("language", Locale.getDefault().getLanguage());
                    //Locale.getDefault().getLanguage()

                    UserAuthentication.DoRegistration(rp, new UserAuthentication.message() {
                        @Override
                        public void Success(String message) {
                            ui_user_registration_send_button.setEnabled(true);

                            final SmsVerification smsV = new SmsVerification(getWindow().getContext());
                            smsV.setDoneBttBg(getResources().getDrawable(R.drawable.next_button));
                            smsV.setCancelable(false);
                            smsV.setListener(new SmsVerification.Success() {
                                @Override
                                public void Done(String value) {

                                    UserAuthentication.VerifyBySMS(value, new UserAuthentication.message() {
                                        @Override
                                        public void Success(String message) {

                                            UserAuthentication.DoRegistration(rp, new UserAuthentication.message() {
                                                @Override
                                                public void Success(String message) {
                                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

                                                    restartMe();
                                                }

                                                @Override
                                                public void Error(String message) {
                                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                                                }
                                            });

                                            smsV.hideMe();
                                        }

                                        @Override
                                        public void Error(String message) {
                                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }

                                @Override
                                public void ResendSMS() {
                                    UserAuthentication.ResendSMS("+" + ui_user_registration_input_mobile_number.getText(), new UserAuthentication.message() {
                                        @Override
                                        public void Success(String message) {
                                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                                        }

                                        @Override
                                        public void Error(String message) {
                                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                            smsV.show();
                        }

                        @Override
                        public void Error(String message) {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            ui_user_registration_send_button.setEnabled(true);
                        }
                    });
                }

                /**/

                /*UserAuthentication.VerifyBySMS("187184", new UserAuthentication.message() {
                    @Override
                    public void Success(String message) {

                    }

                    @Override
                    public void Error(String message) {

                    }
                });*/

                /*if(ValidateRegForm())
                {

                    SmsVerification smsV = new SmsVerification(getWindow().getContext());
                    smsV.setDoneBttBg(getResources().getDrawable(R.drawable.next_button));
                    smsV.setListener(new SmsVerification.Success() {
                        @Override
                        public void Done(String value) {
                            System.out.println();
                        }
                    });
                    smsV.show();
                }*/
            }
        });

        usrTypeRegGroup = (UserTypeRadioGroup) findViewById(R.id.usrTypeRegGroup);
        usrTypeRegGroup.setElements();
        usrTypeRegGroup.setListener(new UserTypeRadioGroup.Success() {
            @Override
            public void done(String id) {
                if (!id.contains("1")) {
                    reg_text_hide.setVisibility(View.VISIBLE);
                    /*ui_reg_text1.setVisibility(View.VISIBLE);
                    ui_reg_text2.setVisibility(View.VISIBLE);*/
                } else {
                    /*ui_reg_text1.setVisibility(View.GONE);
                    ui_reg_text2.setVisibility(View.GONE);*/
                    reg_text_hide.setVisibility(View.GONE);
                }
                LetsActiveDoneButton();
            }
        });

        ui_user_registration_input_email.setListener(new FormField_T2_TextBox.Success() {

            @Override
            public void done() {

                if (!TextUtils.isEmpty(ui_user_registration_input_email.getText())) {
                    String email = ui_user_registration_input_email.getText().toString().trim();
                    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

                    if (email.matches(emailPattern)) {
                        //Toast.makeText(getApplicationContext(),"Valid email address",Toast.LENGTH_SHORT).show();
                        UserAuthentication.CheckIfEmailExists(ui_user_registration_input_email.getText(), new TheResponse() {

                            @Override
                            public void success(String response) {
                                Toast.makeText(getApplicationContext(), "Email Already Exists", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void failed() {
                                Toast.makeText(getApplicationContext(), "No such email address found", Toast.LENGTH_LONG).show();
                            }
                        });

                    } else {
                        Toast.makeText(getApplicationContext(), "Invalid email address", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(getApplicationContext(), "Please provide a valid email.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void Focused() {
                LetsActiveDoneButton();
            }

            @Override
            public void Unfocused() {
                LetsActiveDoneButton();
            }
        });

        ui_user_registration_input_birthdate.disableEditable();
        ui_user_registration_input_birthdate._onClick(new FormField_T1_TextBox._click() {
            @Override
            public void done() {
                DPicker dp = new DPicker(getWindow().getContext());
                dp.setTheTitle("Birthday");
                dp.setDoneBttBg(getResources().getDrawable(R.drawable.next_button));
                //dp.setCancelBttBg(getResources().getDrawable(R.drawable.loginbuttonshape));
                dp.setCancelBttBg(getResources().getDrawable(R.drawable.birthday_box_cancel));

                if (TextUtils.isEmpty(ui_user_registration_input_birthdate.getText())) {
                    dp.setDefaultValue(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
                } else {

                    String i = ui_user_registration_input_birthdate.getText();
                    System.out.println(i);
                    String[] _split = i.split(". ");
                    dp.setDefaultValue(Integer.valueOf(_split[2]), (Integer.valueOf(_split[1]) - 1), Integer.valueOf(_split[0]));
                }

                dp.setListener(new DPicker.Success() {
                    @Override
                    public void Done(Date _date) {
                        ui_user_registration_input_birthdate.setText(_date.getYear() + ". " + (_date.getMonth() + 1) + ". " + _date.getDate());
                        ui_user_registration_input_birthdate.setTheOtherValue((_date.getMonth() < 10 ? "0" : "") + (_date.getMonth() + 1) + ". " + (_date.getDate() < 10 ? "0" : "") + _date.getDate() + ". " + _date.getYear());
                        LetsActiveDoneButton();
                    }
                });
                dp.show();
            }

            @Override
            public void Focused() {

            }

            @Override
            public void Unfocused() {

            }
        });


        ui_user_registration_input_gender.disableEditable();
        ui_user_registration_input_gender._onClick(new FormField_T1_TextBox._click() {
            @Override
            public void done() {

                final GenderSelectionPopUpBox gp = new GenderSelectionPopUpBox(getWindow().getContext());
                gp.setDoneBttBg(getResources().getDrawable(R.drawable.next_button));
                if (!TextUtils.isEmpty(ui_user_registration_input_gender.getText())) {
                    gp.setDefaultValue(ui_user_registration_input_gender.getText());
                }

                gp.setListener(new GenderSelectionPopUpBox.Success() {
                    @Override
                    public void Done(String value) {
                        ui_user_registration_input_gender.setText(value);
                        ui_user_registration_input_gender.setTheOtherValue(gp.get_value2());
                        LetsActiveDoneButton();
                    }
                });
                gp.show();
            }

            @Override
            public void Focused() {

            }

            @Override
            public void Unfocused() {

            }
        });

        ui_user_registration_input_mobile_number = (FormField_T3_TextBox_Spinner) findViewById(R.id.ui_user_registration_input_mobile_number);
        ui_user_registration_input_mobile_number.setSpinnerText("+880");
        ui_user_registration_input_mobile_number.setListener(new FormField_T3_TextBox_Spinner.Success() {
            @Override
            public void done() {

                OptionListing_T1 t = new OptionListing_T1(getWindow().getContext());

                List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();

              /*  final String[] theList = new String[]{"880", "32", "226", "359", "387", "1-246", "681", "590", "1-441", "673", "591", "973", "257", "229", "975", "1-876", "267", "685", "599", "55", "1-242", "44-1534", "375", "501", "7", "250", "381", "670", "262", "993", "992", "40", "690", "245", "1-671", "502", "30", "240", "590", "81", "592", "44-1481", "594", "995", "1-473", "44", "241", "503", "224", "220", "299", "350", "233", "968", "216", "962", "385", "509", "36", "852", "504", "58", "1-787","1-939", "970", "680", "351", "47", "595", "964", "507", "689", "675", "51", "92", "63", "870", "48", "508", "260", "212", "372", "20", "27", "593", "39", "84", "677", "251", "252", "263", "966", "34", "291", "382", "373", "261", "590", "212", "377", "998", "95", "223", "853", "976", "692", "389", "230", "356", "265", "960", "596", "1-670", "1-664", "222", "44-1624", "256", "255", "60", "52", "972", "33", "246", "290", "358", "679", "500", "691", "298", "505", "31", "47", "264", "678", "687", "227", "672", "234", "64", "977", "674", "683", "682", "225", "41", "57", "86", "237", "56", "61", "1", "242", "236", "243", "420", "357", "61", "506", "599", "238", "53", "268", "963", "599", "996", "254", "211", "597", "686", "855", "1-869", "269", "239", "421", "82", "386", "850", "965", "221", "378", "232", "248", "7", "1-345", "65", "46", "249", "1-809", "1-829", "1-767", "253", "45", "1-284", "49", "967", "213", "1", "598", "262", "1", "961", "1-758", "856", "688", "886", "1-868", "90", "94", "423", "371", "676", "370", "352", "231", "266", "66", "228", "235", "1-649", "218", "379", "1-784", "971", "376", "1-268", "93", "1-264", "1-340", "354", "98", "374", "355", "244", "1-684", "54", "61", "43", "297", "91", "358-18", "994", "353", "62", "380", "974", "258"};*/


                String[] country = new String[]{"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla",
                        "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria",
                        "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium",
                        "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana",
                        "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria",
                        "Burkina Faso", "Burma (Myanmar)", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde",
                        "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island",
                        "Cocos (Keeling) Islands", "Colombia", "Comoros", "Cook Islands", "Costa Rica",
                        "Croatia", "Cuba", "Cyprus", "Czech Republic", "Democratic Republic of the Congo",
                        "Denmark", "Djibouti", "Dominica", "Dominican Republic",
                        "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia",
                        "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia",
                        "Gabon", "Gambia", "Gaza Strip", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece",
                        "Greenland", "Grenada", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana",
                        "Haiti", "Holy See (Vatican City)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India",
                        "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Ivory Coast", "Jamaica",
                        "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait",
                        "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein",
                        "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia",
                        "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mayotte", "Mexico",
                        "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco",
                        "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia",
                        "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "North Korea",
                        "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama",
                        "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland",
                        "Portugal", "Puerto Rico", "Qatar", "Republic of the Congo", "Romania", "Russia", "Rwanda",
                        "Saint Barthelemy", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin",
                        "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino",
                        "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone",
                        "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea",
                        "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland",
                        "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tokelau",
                        "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands",
                        "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "US Virgin Islands","Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Wallis and Futuna", "West Bank", "Yemen", "Zambia", "Zimbabwe"};

                String[] code = new String[]{"+93", "+355", "+213", "+1 684", "+376", "+244", "+1 264", "+672", "+1 268", "+54", "+374",
                        "+297", "+61", "+43", "+994", "+1 242", "+973", "+880", "+1 246", "+375", "+32", "+501",
                        "+229", "+1 441", "+975", "+591", "+387", "+267", "+55", "+246", "+1 284", "+673", "+359",
                        "+226", "+95", "+257", "+855", "+237", "+1", "+238", "+1 345", "+236", "+235", "+56", "+86",
                        "+61", "+891", "+57", "+269", "+682", "+506", "+385", "+53", "+357", "+420", "+243", "+45",
                        "+253", "+1 767", "+1 809", "+593", "+20", "+503", "+240", "+291", "+372",
                        "+251", "+500", "+298", "+679", "+358", "+33", "+689", "+241", "+220", "+970", "+995", "+49",
                        "+233", "+350", "+30", "+299", "+1 473", "+1 671", "+502", "+224", "+245", "+592", "+509",
                        "+379", "+504", "+852", "+36", "+354", "+91", "+62", "+98", "+964", "+353", "+44", "+972",
                        "+39", "+225", "+1 876", "+81", "+44", "+962", "+7", "+254", "+686", "+381", "+965", "+996",
                        "+856", "+371", "+961", "+266", "+231", "+218", "+423", "+370", "+352", "+853", "+389",
                        "+261", "+265", "+60", "+960", "+223", "+356", "+692", "+222", "+230", "+262", "+52", "+691",
                        "+373", "+377", "+976", "+382", "+1 664", "+212", "+258", "+264", "+674", "+977", "+31",
                        "+599", "+687", "+64", "+505", "+227", "+234", "+683", "+672", "+850", "+1 670", "+47",
                        "+968", "+92", "+680", "+507", "+675", "+595", "+51", "+63", "+870", "+48", "+351", "+1",
                        "+974", "+242", "+40", "+7", "+250", "+590", "+290", "+1 869", "+1 758", "+1 599", "+508",
                        "+1 784", "+685", "+378", "+239", "+966", "+221", "+381", "+248", "+232", "+65", "+421",
                        "+386", "+677", "+252", "+27", "+82", "+34", "+94", "+249", "+597", "+268", "+46", "+41",
                        "+963", "+886", "+992", "+255", "+66", "+670", "+228", "+690", "+676", "+1 868", "+216",
                        "+90", "+993", "+1 649", "+688", "+256", "+380", "+971", "+44", "+1", "+598", "+1 340",
                        "+998", "+678", "+58", "+84", "+681", "+970", "+967", "+260", "+263"};

               /* String countryName = "South Africa";
                for(int i=0; i<country.length; i++){
                    *//*if(countryName.equals(code)){
                        System.out.println("Position :"+i);
                    }*//*
                    System.out.println(i+1+" Contry Name :"+country[i]+" "+code[i]);
                }*/

            /*    String[] isoCountryCodes = Locale.getISOCountries();
                for (String countryCode : isoCountryCodes) {
                    Locale locale = new Locale("", countryCode);
                    String countryName = locale.getDisplayCountry();
                    String codes=locale.ge;

                    RadioButtonInfo io1 = new RadioButtonInfo();

                    io1.setText(countryName);
                    io1.setCode(codes);
                    btns.add(io1);

                }*/


                for (int i = 0; i < country.length; i++) {
                    RadioButtonInfo io1 = new RadioButtonInfo();
                    io1.setID(i);
                    io1.setText(country[i]);
                    io1.setCode(code[i]);
                    btns.add(io1);
                }

                //t.setup("Mobile", btns, "1");
                t.setup2("Mobile", btns, ui_user_registration_input_mobile_number.getSpinnerText());
                t.setListener(new OptionListing_T1.Success() {
                    @Override
                    public void Done(RadioButtonInfo value) {
                        LetsActiveDoneButton();
                        ui_user_registration_input_mobile_number.setSpinnerText(value.getCode());
                    }
                });
                t.show();

            }

            @Override
            public void Focused() {
                LetsActiveDoneButton();
            }

            @Override
            public void Unfocused() {
                LetsActiveDoneButton();
            }
        });
    }

    private void RegistrationInputScreen_ZipCodeScreen_init() {
        findViewById(R.id.ui_user_registration_zip_search_window_bckBtt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.ui_user_registration_zip_search_window).setVisibility(View.GONE);
            }
        });
        ui_user_registration_zip_search_window_street_st = (Table_T1) findViewById(R.id.ui_user_registration_zip_search_window_street_st);
        ui_user_registration_zip_search_window_street_st.setColumnNames(new String[]{"zipcode", "address"});
        List<Col2RowDetails> rows = new ArrayList<>();

        Col2RowDetails crd = new Col2RowDetails();
        crd.set_value("12345");
        crd.setCol1_Contents("12345");
        crd.setCol2_Contents("Place 1");
        rows.add(crd);

        Col2RowDetails crd22 = new Col2RowDetails();
        crd22.set_value("67890");
        crd22.setCol1_Contents("67890");
        crd22.setCol2_Contents("Place 2");
        rows.add(crd22);

        Col2RowDetails crd23 = new Col2RowDetails();
        crd23.set_value("69876");
        crd23.setCol1_Contents("69876");
        crd23.setCol2_Contents("Place 3");
        rows.add(crd23);

        Col2RowDetails crd24 = new Col2RowDetails();
        crd24.set_value("91937");
        crd24.setCol1_Contents("91937");
        crd24.setCol2_Contents("Place 4");
        rows.add(crd24);

        Col2RowDetails crd25 = new Col2RowDetails();
        crd25.set_value("50923");
        crd25.setCol1_Contents("50923");
        crd25.setCol2_Contents("Place 5");
        rows.add(crd25);

        ui_user_registration_zip_search_window_street_st.setContainerElements(rows, "8878");
        ui_user_registration_zip_search_window_street_st.setListener(new Table_T1.Success() {
            @Override
            public void done(Col2RowDetails _selected) {
                findViewById(R.id.ui_user_registration_zip_search_window).setVisibility(View.GONE);
                ui_user_registration_address_zip.setText(_selected.get_value());
                ui_user_registration_address1.setText(_selected.getCol2_Contents());
                LetsActiveDoneButton();
            }
        });

        ui_user_registration_zip_search_window_house_st = (Table_T1) findViewById(R.id.ui_user_registration_zip_search_window_house_st);
        ui_user_registration_zip_search_window_house_st.setColumnNames(new String[]{"zipcode", "address"});
        List<Col2RowDetails> rows2 = new ArrayList<>();

        Col2RowDetails crd2 = new Col2RowDetails();
        crd2.set_value("88887");
        crd2.setCol1_Contents("88887");
        crd2.setCol2_Contents("Lishfh");
        rows2.add(crd2);

        Col2RowDetails crd3 = new Col2RowDetails();
        crd3.set_value("826843");
        crd3.setCol1_Contents("826843");
        crd3.setCol2_Contents("Kbaak");
        rows2.add(crd3);

        Col2RowDetails crd4 = new Col2RowDetails();
        crd4.set_value("90755");
        crd4.setCol1_Contents("90755");
        crd4.setCol2_Contents("Auorty");
        rows2.add(crd4);

        ui_user_registration_zip_search_window_house_st.setContainerElements(rows2, "8878");
        ui_user_registration_zip_search_window_house_st.setListener(new Table_T1.Success() {
            @Override
            public void done(Col2RowDetails _selected) {
                findViewById(R.id.ui_user_registration_zip_search_window).setVisibility(View.GONE);
                ui_user_registration_address_zip.setText(_selected.get_value());
                ui_user_registration_address1.setText(_selected.getCol2_Contents());
                LetsActiveDoneButton();
            }
        });


        ui_user_registration_zip_search_window_street_city_spinner = (Spinner_T2) findViewById(R.id.ui_user_registration_zip_search_window_street_city_spinner);
        ui_user_registration_zip_search_window_street_city_spinner.setText("City");
        List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();
        RadioButtonInfo io1 = new RadioButtonInfo();
        io1.setID(0);
        io1.setText("Haka");
        btns.add(io1);

        RadioButtonInfo io2 = new RadioButtonInfo();
        io2.setID(1);
        io2.setText("Haka2");
        btns.add(io2);

        RadioButtonInfo io3 = new RadioButtonInfo();
        io3.setID(2);
        io3.setText("Japan");
        btns.add(io3);

        RadioButtonInfo io4 = new RadioButtonInfo();
        io4.setID(3);
        io4.setText("Shanhai");
        btns.add(io4);

        RadioButtonInfo io5 = new RadioButtonInfo();
        io5.setID(4);
        io5.setText("Kung");
        btns.add(io5);

        RadioButtonInfo io6 = new RadioButtonInfo();
        io6.setID(5);
        io6.setText("Alishohor");
        btns.add(io6);

        RadioButtonInfo io7 = new RadioButtonInfo();
        io7.setID(6);
        io7.setText("Vill City");
        btns.add(io7);

        ui_user_registration_zip_search_window_street_city_spinner.setOptions("City", btns, "Haka2");


        ui_user_registration_zip_search_window_street_city_province = (Spinner_T2) findViewById(R.id.ui_user_registration_zip_search_window_street_city_province);
        ui_user_registration_zip_search_window_street_city_province.setText("Province");
        List<RadioButtonInfo> btns2 = new ArrayList<RadioButtonInfo>();
        RadioButtonInfo io12 = new RadioButtonInfo();
        io12.setID(0);
        io12.setText("Haksad ada");
        btns2.add(io12);

        RadioButtonInfo io22 = new RadioButtonInfo();
        io22.setID(1);
        io22.setText("Haksadasdasda2");
        btns2.add(io22);

        RadioButtonInfo io23 = new RadioButtonInfo();
        io23.setID(2);
        io23.setText("Aka Sukaj sdf");
        btns2.add(io23);

        RadioButtonInfo io24 = new RadioButtonInfo();
        io24.setID(3);
        io24.setText("Aka Sukaj sdf");
        btns2.add(io24);

        RadioButtonInfo io25 = new RadioButtonInfo();
        io25.setID(4);
        io25.setText("Aka Sukaj sdf");
        btns2.add(io25);

        RadioButtonInfo io26 = new RadioButtonInfo();
        io26.setID(5);
        io26.setText("Swong Ki");
        btns2.add(io26);

        RadioButtonInfo io27 = new RadioButtonInfo();
        io27.setID(6);
        io27.setText("Shanghai");
        btns2.add(io27);
        ui_user_registration_zip_search_window_street_city_province.setOptions("Province", btns2, "Aka Sukaj sdf");


        ui_user_registration_address_zip.setListener(new FormField_T2_TextBox.Success() {
            @Override
            public void done() {
                findViewById(R.id.ui_user_registration_zip_search_window).setVisibility(View.VISIBLE);
                findViewById(R.id.ui_user_registration_zip_search_window_street).setVisibility(View.VISIBLE);
                findViewById(R.id.ui_user_registration_zip_search_window_house).setVisibility(View.GONE);
            }

            @Override
            public void Focused() {
                LetsActiveDoneButton();
            }

            @Override
            public void Unfocused() {
                LetsActiveDoneButton();
            }
        });

        findViewById(R.id.ui_user_registration_zip_search_window_street_show_house).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.ui_user_registration_zip_search_window_street).setVisibility(View.GONE);
                findViewById(R.id.ui_user_registration_zip_search_window_house).setVisibility(View.VISIBLE);
            }
        });

        findViewById(R.id.ui_user_registration_zip_search_window_house_show_street).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.ui_user_registration_zip_search_window_street).setVisibility(View.VISIBLE);
                findViewById(R.id.ui_user_registration_zip_search_window_house).setVisibility(View.GONE);
            }
        });
    }

    private void LetsActiveDoneButton() {
        if (!ValidateRegForm(false)) {
            System.out.println("Button should disabled");
            ui_user_registration_send_button.makeEnable(false);
            ui_user_registration_send_button.setA_Background(
                    getResources().getDrawable(R.drawable.registrationbuttonshape_dsbl),
                    getResources().getColor(R.color.white)
            );
        } else {
            System.out.println("Button should not disabled");
            ui_user_registration_send_button.makeEnable(true);
            ui_user_registration_send_button.setA_Background(
                    getResources().getDrawable(R.drawable.registrationbuttonshape2),
                    getResources().getColor(R.color.black)
            );
        }
    }

    private boolean ValidateRegForm(boolean showToast) {

        if (!checkedIfAbove14.isChecked()) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "Your age must have to be above 14.", Toast.LENGTH_LONG).show();
            }

            return false;
        }

        if (TextUtils.isEmpty(ui_user_registration_input_email.getText())) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "Email field must be field up.", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(ui_user_registration_input_email.getText()).matches()) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "Invalid email address", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        if (TextUtils.isEmpty(ui_user_registration_input_password.getText())) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "Password field must be field up.", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        if (TextUtils.isEmpty(ui_user_registration_input_repassword.getText())) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "Password field must be field up.", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        String password = ui_user_registration_input_password.getText().toString();
        String repassword = ui_user_registration_input_repassword.getText().toString();

        if (!password.equals(repassword)) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "Password field doesn't match.", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        if (TextUtils.isEmpty(ui_user_registration_input_name_english.getText())) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "English name field must be field up.", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        if (usrTypeRegGroup.getValue().contains("2")) {
            if (TextUtils.isEmpty(ui_user_registration_input_name_korean.getText())) {
                if (showToast) {
                    Toast.makeText(getWindow().getContext(), "Korean name field must be field up.", Toast.LENGTH_LONG).show();
                }
                return false;
            }
        }

        if (TextUtils.isEmpty(ui_user_registration_input_birthdate.getText())) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "Birthday field must be field up.", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        String birthdate = ui_user_registration_input_birthdate.getTheOtherValue().replaceAll(". ", "/");

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(birthdate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (Utils_Helper.getYearsBetweenDates(convertedDate, calendar.getTime()) < 14) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "You need to be 14 years old minimum.", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        if (TextUtils.isEmpty(ui_user_registration_input_gender.getText())) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "Gender field must be field up.", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        String phone = ui_user_registration_input_mobile_number.getText();
        phone = phone.substring(3);
        if (TextUtils.isEmpty(phone)) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "Mobile number field must be field up.", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        if (TextUtils.isEmpty(ui_user_registration_address_zip.getText())) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "Zip code field must be field up.", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        if (TextUtils.isEmpty(ui_user_registration_address1.getText())) {
            if (showToast) {
                Toast.makeText(getWindow().getContext(), "Address1 field must be field up.", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        /*if(TextUtils.isEmpty(ui_user_registration_address2.getText()))
        {
            if(showToast) {
                Toast.makeText(getWindow().getContext(), "Address2 field must be field up.", Toast.LENGTH_LONG).show();
            }
            return false;
        }*/

        /*
        if(TextUtils.isEmpty(ui_user_registration_input_mobile_number.getText()))
        {
            Toast.makeText(getWindow().getContext(), "Mobile number field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        */


        return true;
    }

    void restartMe() {
        /*
            ui_user_authentication_terms_and_conditions_layout.setVisibility(View.GONE);
            ui_user_registration_layout.setVisibility(View.GONE);
            ui_user_login_layout.setVisibility(View.VISIBLE);
         */

        /*
        Intent intent = getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
        overridePendingTransition(0, 0);
        */

        Intent resultIntent = new Intent();
        setResult(Activity.DEFAULT_KEYS_SEARCH_GLOBAL, resultIntent);   // Ask to restart activity.
        finish();
    }
}
