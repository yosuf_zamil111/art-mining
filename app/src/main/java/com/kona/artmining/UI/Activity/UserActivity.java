package com.kona.artmining.UI.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.kona.artmining.Adapter.UserArtScrapbookAdapter_T1;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Dashboard;
import com.kona.artmining.Helper.User;
import com.kona.artmining.Helper.UserAuthentication;
import com.kona.artmining.Interface.TheJavascriptInterface;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Interface.UserActivity_Interface;
import com.kona.artmining.Model.ArtScrapbookItemList_T1;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.R;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.kona.artmining.UI.FragmentActivity.TheWebView;
import com.kona.artmining.UI.FragmentActivity.UserFrgAuctionList;
import com.kona.artmining.UI.FragmentActivity.UserFrgBuyerOrderList;
import com.kona.artmining.UI.FragmentActivity.UserFrgEditProfile;
import com.kona.artmining.UI.FragmentActivity.UserFrgMyArtReviews;
import com.kona.artmining.UI.FragmentActivity.UserFrgMyArtworks;
import com.kona.artmining.UI.FragmentActivity.UserFrgSellerOrderList;
import com.kona.artmining.UI.FragmentActivity.UserFrgShoppingCart;
import com.kona.artmining.UI.FragmentController.UserActivity_FragmentsManager;
import com.kona.artmining.custom_popup_box.CCImagePicker;
import com.kona.artmining.custom_popup_box.ConfirmationBox;
import com.kona.artmining.utils.Network;
import com.kona.artmining.utils.PicassoRoundTransform;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nineoldandroids.view.ViewHelper;
import com.pixplicity.easyprefs.library.Prefs;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;
import net.hockeyapp.android.metrics.MetricsManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;

public class UserActivity extends AppCompatActivity
{
    TextView CartItemsCounter33;

    UserProfile currentUserProfile;

    public final static int USER_LOGGED_OUT = 13;

    ProgressDialog progress;

    CircularImageView userProfileImage;
    TextView ui_user_profile_heading_name, ui_user_profile_email;

    ObservableRecyclerView recyclerView;
    UserArtScrapbookAdapter_T1 rvListAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    List<ArtScrapbookItemList_T1> Content = new ArrayList<ArtScrapbookItemList_T1>();

    LinearLayout ui_user_profile_header;
    LinearLayout ui_user_profile_header_userDetails, deleteSelected, artistDetails;

    ImageView ShowAuctionList, ShowCartView, ShowMailBox, ShowOrderList;

    long scrapBookScrollY=0;

    public static final int ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE = 400;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        //HockeyApp();

        progress = ProgressDialog.show(getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);
        CheckUserProfile();

        prepareStage();
        UpdateCart();

        /*new Timer().scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                UpdateCart();
            }
        },0,(10 * 1000));*/
    }

    void UpdateCart()
    {
        Dashboard.getCartInfo(new TheResponse() {
            @Override
            public void success(String response) {
                CartItemsCounter33.setText(response);
            }

            @Override
            public void failed() {
                CartItemsCounter33.setText("0");
            }
        });
    }

    void prepareStage()
    {
        if(!Network.isNetworkAvailable(getWindow().getContext()))
        {
            Toast.makeText(getApplicationContext(), "Internet Connection Required", Toast.LENGTH_LONG).show();
            finish();
        }

        CartItemsCounter33 = (TextView) findViewById(R.id.CartItemsCounter33);

        findViewById(R.id.BackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateCart();
            }
        });

        findViewById(R.id.ui_user_profile_back_btt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.ui_user_profile_edit_btt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //findViewById(R.id.UserProfileEditPage).setVisibility(View.VISIBLE);
                OpenRelevantFragment(UserFrgEditProfile.class.getName());
            }
        });
        findViewById(R.id.update_cover_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getWindow().getContext(), "Update cover photo.", Toast.LENGTH_LONG).show();
                CoverPhotoUpdated();
            }
        });

        artistDetails = (LinearLayout) findViewById(R.id.artistDetails);

        ShowCartView  = (ImageView) findViewById(R.id.ui_user_profile_theCart);
        ShowCartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenRelevantFragment(UserFrgShoppingCart.class.getName());
            }
        });

        ShowMailBox  = (ImageView) findViewById(R.id.ui_user_profile_mailBox);
        ShowMailBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(UserActivity.this, MessagingActivity.class);
                UserActivity.this.startActivity(myIntent);
            }
        });

        ShowAuctionList = (ImageView) findViewById(R.id.ui_user_profile_auctionList);
        ShowAuctionList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenRelevantFragment(UserFrgAuctionList.class.getName());
            }
        });

        ShowOrderList = (ImageView) findViewById(R.id.ui_user_profile_orderList);
        ShowOrderList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(UserActivity.this, OrderActivity.class);
                UserActivity.this.startActivity(myIntent);
            }
        });

        findViewById(R.id.showSettingsPanel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.ui_user_settings).setVisibility(View.VISIBLE);
            }
        });

        PopulateData();

        userProfileImage = (CircularImageView) findViewById(R.id.userProfileImage);
        ui_user_profile_heading_name = (TextView) findViewById(R.id.ui_user_profile_heading_name);
        ui_user_profile_email = (TextView) findViewById(R.id.ui_user_profile_email);
        deleteSelected = (LinearLayout) findViewById(R.id.deleteSelected);

        recyclerView = (ObservableRecyclerView) findViewById(R.id.recycler_view);
        GridLayoutManager layoutManager = new GridLayoutManager(getWindow().getContext(), 3);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(position == 0)
                {
                    return 3;
                }else{
                    return 1;
                }
            }
        });
        recyclerView.setLayoutManager(layoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        rvListAdapter = new UserArtScrapbookAdapter_T1(Content, new UserArtScrapbookAdapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(ArtScrapbookItemList_T1 item, int position, boolean isSelected) {
                Content.get(position).setSelected(isSelected);
                Update_DeleteSelectedButtonStatus();
            }

            @Override
            public void loadItemDetails(ArtScrapbookItemList_T1 item) {
                Intent intent = new Intent(UserActivity.this, ArtDetailsActivity.class);
                intent.putExtra(GeneralConstants.ID, item.getArt_id());
                UserActivity.this.startActivityForResult(intent, ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE);
            }
        });
        rvListAdapter.SetCurrentUserProfile(currentUserProfile);
        recyclerView.setAdapter(rvListAdapter);
        recyclerView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
                if((ui_user_profile_header_userDetails.getHeight()-10)>scrollY)
                {
                    ViewHelper.setTranslationY(ui_user_profile_header, (-1) * scrollY);
                }
                scrapBookScrollY = scrollY;
            }

            @Override
            public void onDownMotionEvent() {
                if((ui_user_profile_header_userDetails.getHeight()-10)>scrapBookScrollY)
                {
                    ViewHelper.setTranslationY(ui_user_profile_header, (-1) * scrapBookScrollY);
                }
            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {
                if((ui_user_profile_header_userDetails.getHeight()-10)>scrapBookScrollY)
                {
                    ViewHelper.setTranslationY(ui_user_profile_header, (-1) * scrapBookScrollY);
                }
            }
        });

        swipeRefreshLayout.setProgressViewOffset(false,600,700);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                RetrieveScrapBookData();
            }
        });

        ui_user_profile_header = (LinearLayout) findViewById(R.id.ui_user_profile_header);
        ui_user_profile_header_userDetails = (LinearLayout) findViewById(R.id.ui_user_profile_header_userDetails);

        deleteSelected.setVisibility(View.GONE);
        deleteSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String removeItems = "";

                for (int i = 0; i < Content.size(); i++) {
                    if(Content.get(i).isSelected())
                    {
                        if(i==1)
                        {
                            removeItems = removeItems.concat(""+Content.get(i).getScrap_id());
                        }else{
                            removeItems = removeItems.concat(","+Content.get(i).getScrap_id());
                        }
                        System.out.println("We have to delete it: "+Content.get(i).getScrap_id());
                    }
                }

                if(!TextUtils.isEmpty(removeItems))
                {
                    RemoveItem(removeItems, new TheResponse() {
                        @Override
                        public void success(String response) {
                            RetrieveScrapBookData();
                            progress.dismiss();
                        }

                        @Override
                        public void failed() {
                            progress.dismiss();
                        }
                    });
                }else{
                    Toast.makeText(getWindow().getContext(), "Please select some items.", Toast.LENGTH_LONG).show();
                }

                //ItemDelete
            }
        });

        findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogOut();
            }
        });

        findViewById(R.id.createAnArt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserActivity.this, CreateEditAnArt_Activity.class);
                //intent.putExtra(GeneralConstants.ID, 236);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                //ShowWebView(ServerConstants.POST_AN_ART+Prefs.getString(UserConstants.AccessToken,null));
            }
        });

        findViewById(R.id.postedArtlist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowWebView(ServerConstants.POSTED_ART_LISTING+Prefs.getString(UserConstants.AccessToken,null));
            }
        });

        findViewById(R.id.UserThemeListing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestParams params=new RequestParams();
                params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));
                artmineRestClient.get("/theme/listing.json", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                        System.out.println("-------->"+response.toString());
                        try {
                            if(response.getBoolean("success")){
                                ShowWebView(ServerConstants.THEME_LISTING+Prefs.getString(UserConstants.AccessToken,null));
                            }else{
                                Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                        Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
        findViewById(R.id.UserUserListing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestParams params=new RequestParams();
                params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));
                artmineRestClient.get("/theme/listing.json", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                        System.out.println("-------->"+response.toString());
                        try {
                            if(response.getBoolean("success")){
                                ShowWebView(ServerConstants.USER_LISTING+Prefs.getString(UserConstants.AccessToken,null));
                            }else{
                                Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                        Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
        findViewById(R.id.UserRejectedUserListing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestParams params=new RequestParams();
                params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));
                artmineRestClient.get("/theme/listing.json", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                        System.out.println("-------->"+response.toString());
                        try {
                            if(response.getBoolean("success")){
                                ShowWebView(ServerConstants.REJECTED_USER_LISTING+Prefs.getString(UserConstants.AccessToken,null));
                            }else{
                                Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                        Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
        findViewById(R.id.UserApprovedUserListing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestParams params=new RequestParams();
                params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));
                artmineRestClient.get("/theme/listing.json", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                        System.out.println("-------->"+response.toString());
                        try {
                            if(response.getBoolean("success")){
                                ShowWebView(ServerConstants.APPROVED_USER_LISTING+Prefs.getString(UserConstants.AccessToken,null));
                            }else{
                                Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                        Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

        findViewById(R.id.userSettings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowWebView(ServerConstants.PROFILE_SETTINGS_OPTIONS+Prefs.getString(UserConstants.AccessToken,null));
            }
        });

        findViewById(R.id.__viewUserProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenRelevantFragment(UserFrgEditProfile.class.getName());
            }
        });

        findViewById(R.id.BuyerOrderList).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenRelevantFragment(UserFrgBuyerOrderList.class.getName());
            }
        });

        findViewById(R.id.SellerOrderList).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenRelevantFragment(UserFrgSellerOrderList.class.getName());
            }
        });

        findViewById(R.id.my_art_work).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenRelevantFragment(UserFrgMyArtworks.class.getName());
            }
        });

        findViewById(R.id.my_review).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OpenRelevantFragment(UserFrgMyArtReviews.class.getName());
            }
        });

        findViewById(R.id.changeCoverPhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CoverPhotoUpdated();
            }
        });
    }

    void PopulateData()
    {
        User.GetProfileDataProfile(getWindow().getContext(), new User.userProfileRetrieve() {
            @Override
            public void Success(UserProfile userProfile) {
                currentUserProfile = userProfile;
                Fill_upScreen();
                progress.dismiss();
            }

            @Override
            public void TokenRefreshRequired()
            {
                User.GetProfileDataProfile(getWindow().getContext(), new User.userProfileRetrieve()
                {
                    @Override
                    public void Success(UserProfile userProfile)
                    {
                        currentUserProfile = userProfile;
                        Prefs.putString(UserConstants.UserFullDetails, new Gson().toJson(userProfile));
                        Fill_upScreen();
                        progress.dismiss();
                    }

                    @Override
                    public void TokenRefreshRequired()
                    {
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(),"Token loop found.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void Error(String message)
                    {
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("-------> requestCode "+requestCode);
        System.out.println("-------> resultCode "+resultCode);
        if(requestCode == 1)
        {
            if(resultCode == 9)
            {
                LogOut();
            }
            if(resultCode == 8)
            {
                PopulateData();
            }
        }
        if(requestCode == ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                Bundle data_ = data.getExtras();
                setResult(ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE,new Intent().putExtra(GeneralConstants.POSITION, 0).putExtra(GeneralConstants.ID, data_.getInt(GeneralConstants.ID)));
                finish();
            }
        }
        if(requestCode == 100)
        {
            if(resultCode == ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE)
            {
                Bundle data_ = data.getExtras();
                setResult(ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE,new Intent().putExtra(GeneralConstants.POSITION, 0).putExtra(GeneralConstants.ID, data_.getInt(GeneralConstants.ID)));
                finish();
            }
            if(resultCode == 9)
            {
                LogOut();
            }
            if(resultCode == 8)
            {
                PopulateData();
            }
        }

        if (requestCode == Picker.PICK_IMAGE_DEVICE) {
            System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
                CoverPhoto_imagePicker.submit(data);
            }
        }else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
            System.out.println("PICK_IMAGE_CAMERA");
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("PICK_IMAGE_CAMERA  RESULT_OK");
                CoverPhoto_cameraPicker.submit(data);
            }
        }
    }


    CCImagePicker CoverPhoto_IP;
    File CoverPhoto;
    ImagePicker CoverPhoto_imagePicker;
    CameraImagePicker CoverPhoto_cameraPicker;

    void CoverPhotoUpdated() {
        CoverPhoto_IP = new CCImagePicker(this);
        CoverPhoto_IP.setListener(new CCImagePicker.task() {
            @Override
            public void fromCamera() {
                CoverPhoto_cameraPicker = new CameraImagePicker(UserActivity.this);
                CoverPhoto_cameraPicker.setImagePickerCallback(new ImagePickerCallback() {
                                                        @Override
                                                        public void onImagesChosen(List<ChosenImage> images) {
                                                            // Display images
                                                            System.out.println(images.toString());
                                                            System.out.println(images.get(0).toString());
                                                            System.out.println(images.get(0).getOriginalPath());

                                                            CoverPhoto = new File(images.get(0).getOriginalPath());
                                                            uploadCoverPhoto();
                                                        }

                                                        @Override
                                                        public void onError(String message) {
                                                            // Do error handling
                                                            Toast.makeText(UserActivity.this.getApplicationContext(),message, Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                );
                String outputPath = CoverPhoto_cameraPicker.pickImage();
            }

            @Override
            public void fromGallery() {
                CoverPhoto_imagePicker = new ImagePicker(UserActivity.this);
                CoverPhoto_imagePicker.setImagePickerCallback(new ImagePickerCallback() {
                                                       @Override
                                                       public void onImagesChosen(List<ChosenImage> images) {
                                                           System.out.println(images.toString());
                                                           System.out.println(images.get(0).toString());
                                                           System.out.println(images.get(0).getOriginalPath());

                                                           CoverPhoto = new File(images.get(0).getOriginalPath());
                                                           uploadCoverPhoto();
                                                       }

                                                       @Override
                                                       public void onError(String message) {
                                                           // Do error handling
                                                           Toast.makeText(UserActivity.this.getApplicationContext(),message, Toast.LENGTH_LONG).show();
                                                       }
                                                   }
                );
                CoverPhoto_imagePicker.pickImage();
            }
        });
        CoverPhoto_IP.show();
    }

    void uploadCoverPhoto()
    {
        progress = ProgressDialog.show(getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

        User.UploadUserCoverPhoto(CoverPhoto, new User.response2() {
            @Override
            public void Success() {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Cover Photo Updated.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void TokenRefreshRequired() {
                User.UploadUserCoverPhoto(CoverPhoto, new User.response2() {
                    @Override
                    public void Success() {
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(), "Cover Photo Updated.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void LogOut()
    {
        Prefs.remove(UserConstants.AccessToken);
        Prefs.remove(UserConstants.RefreshToken);
        Prefs.remove(UserConstants.DO_AUTO_LOGIN);
        Prefs.remove(UserConstants.ProfilePicture);
        Intent resultIntent = new Intent();
        setResult(USER_LOGGED_OUT, resultIntent);
        finish();
    }

    void ShowWebView(String url)
    {
        Intent intent = new Intent(this, UserActivity_FragmentsManager.class);
        intent.putExtra(GeneralConstants.FRAGMENT_NAME, TheWebView.class.getName());
        intent.putExtra(GeneralConstants.URL, url);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, 100);
    }

    void Update_DeleteSelectedButtonStatus()
    {
        boolean showBtt = false;

        for (int i = 0; i < Content.size(); i++) {
            if(Content.get(i).isSelected())
            {
                showBtt = true;
                break;
            }
        }

        if(showBtt){
            deleteSelected.setVisibility(View.VISIBLE);
        }else{
            deleteSelected.setVisibility(View.GONE);
        }
    }

    void RetrieveScrapBookData()
    {
        User.GetUserScrapBookDataProfile(getWindow().getContext(), new User.userScrapBookDataRetrieve() {
            @Override
            public void Success(List<ArtScrapbookItemList_T1> scrapBookData) {
                FillUpRV(scrapBookData);
            }

            @Override
            public void TokenRefreshRequired() {
                User.GetUserScrapBookDataProfile(getWindow().getContext(), new User.userScrapBookDataRetrieve() {
                    @Override
                    public void Success(List<ArtScrapbookItemList_T1> scrapBookData) {
                        FillUpRV(scrapBookData);
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        //Toast.makeText(getApplicationContext(),"Token loop found.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void RemoveItem(final String itemIds, final TheResponse theResponse)
    {
        ConfirmationBox cb = new ConfirmationBox(getWindow().getContext());
        cb.Setup("Remove Scrapbook Item", "Are you sure you want to delete it ?", "Delete", "Cancel");
        cb.setCancelable(false);
        cb.setListener(new ConfirmationBox.Confirmed() {
            @Override
            public void Yes() {

                progress = ProgressDialog.show(getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

                User.RemoveScrapBookItem(itemIds, new User.response2() {
                    @Override
                    public void Success() {
                        theResponse.success("");
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        User.RemoveScrapBookItem(itemIds, new User.response2() {
                            @Override
                            public void Success() {
                                theResponse.success("");
                            }

                            @Override
                            public void TokenRefreshRequired() {
                                Toast.makeText(getApplicationContext(),"Token loop found.", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void Error(String message) {
                                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                                theResponse.failed();
                            }
                        });
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                        theResponse.failed();
                    }
                });

            }

            @Override
            public void No() {

            }
        });
        cb.show();
    }

    public void FillUpRV(List<ArtScrapbookItemList_T1> scrapBookData)
    {
        deleteSelected.setVisibility(View.GONE);
        clearRVData();
        Content.addAll(scrapBookData);
        rvListAdapter.SetCurrentUserProfile(currentUserProfile);

        if(Content.size()>1)
        {
            findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.GONE);
        }else{
            findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.VISIBLE);
        }

        rvListAdapter.notifyDataSetChanged();

        if(swipeRefreshLayout != null)
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void clearRVData()
    {
        int size = Content.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Content.remove(0);
            }
            rvListAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    void Fill_upScreen()
    {
        Picasso.with(getWindow().getContext())
                .load(currentUserProfile.getThumb_image())
                .placeholder(R.drawable.transparent)
                .error(R.drawable.transparent)
                .transform(new PicassoRoundTransform())
                .into(userProfileImage);

        ui_user_profile_heading_name.setText(currentUserProfile.getName());
        ui_user_profile_email.setText(currentUserProfile.getEmail());
        RetrieveScrapBookData();

        if(currentUserProfile.getType() == 1)
        {
            artistDetails.setVisibility(View.GONE);
        }
        else
        {
            artistDetails.setVisibility(View.VISIBLE);
        }
    }

    void OpenRelevantFragment(String fragmentName)
    {
        Intent intent = new Intent(this, UserActivity_FragmentsManager.class);
        intent.putExtra(GeneralConstants.FRAGMENT_NAME, fragmentName);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, 100);
    }

    void HockeyApp()
    {
        checkForUpdates();
        MetricsManager.register(this, getApplication());
    }

    @Override
    public void onBackPressed()
    {
        if(findViewById(R.id.ui_user_settings).getVisibility() == View.VISIBLE)
        {
            findViewById(R.id.ui_user_settings).setVisibility(View.GONE);
        }else{
            finish();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        // ... your own onResume implementation
        checkForCrashes();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    void CheckUserProfile()
    {
        currentUserProfile = new Gson().fromJson(Prefs.getString(UserConstants.UserFullDetails, "{}"),  new TypeToken<UserProfile>(){}.getType());
    }
}
