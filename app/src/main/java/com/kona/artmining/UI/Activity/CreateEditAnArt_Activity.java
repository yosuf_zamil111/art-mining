package com.kona.artmining.UI.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Art;
import com.kona.artmining.Helper.Dashboard;
import com.kona.artmining.Helper.UserAuthentication;
import com.kona.artmining.Interface.CreateEditArtActivity_Interface;
import com.kona.artmining.Model.Art_T1;
import com.kona.artmining.Model.SearchListItem;
import com.kona.artmining.Model.ShippingMethodNames;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.R;
import com.kona.artmining.UI.FragmentActivity.frg_CreateEditAnArt_Activity_SearchArtist;
import com.kona.artmining.custom_popup_box.CCImagePicker;
import com.kona.artmining.custom_popup_box.OptionListing_T1;
import com.kona.artmining.custom_popup_box.OptionListing_T3;
import com.kona.artmining.decent_date_picker.DPicker;
import com.kona.artmining.model.RadioButtonInfo;
import com.kona.artmining.utils.PicassoRoundTransform;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class CreateEditAnArt_Activity extends AppCompatActivity implements CreateEditArtActivity_Interface
{

    ScrollView MasterLayout_SV;

    LinearLayout SelectATheme_btt, SearchArtistName_Btt, AuctionPriceHolder, ShopPriceHolder, ArtSaleCurrencySelector,auctionExpiresOn_dateHolder, ArtSize_SelectSize, shippingServiceContainer, searchTheArtistName_row, backMe;
    TextView SelectATheme_bttTxt, ArtSize_SelectSize_TV, ArtSaleCurrencySelector_Text, auctionExpiresOn_date, shipNotAllowedTo_TV, selectedShippingService, ArtistName;

    ImageView uploadedImage_1, uploadedImage_1_close, uploadedImage_2, uploadedImage_2_close, uploadedImage_3, uploadedImage_3_close, uploadedImage_4, uploadedImage_4_close, ArtSalesMethodShop_IMGV, ArtSalesMethodAuction_IMGV;

    ImageView st_noShipping, st_freeShipping, st_shippingService;

    EditText ArtTitle, ArtDescription, ArtSize_W, ArtSize_L, ArtSize_H, MaterialName, AuctionPriceFrom, AuctionPriceTo, ShopPrice, ArtTitle_KR, ArtDescription_KR, ArtYear;

    CheckBox deliveryOnPerson;

    Button submitButton;

    FrameLayout content;
    ProgressDialog progress;

    int SelectedThemeID = 0;
    String auctionExpiresOn = "";

    String TAG = CreateEditAnArt_Activity.class.getName();

    CCImagePicker CCIP;
    ImagePicker imagePicker;
    CameraImagePicker cameraPicker;
    Calendar calendar;

    File Selected_uploadedImage_1, Selected_uploadedImage_2, Selected_uploadedImage_3, Selected_uploadedImage_4;

    int artType = 0;
    int ART_SHIPPING_TYPE = 0;
    int ART_SHIPPING_SS_TYPE = 0;
    int SelectedArtistId = 0;

    String unAllowedShipping_CountryCodes = "";

    int SHOP_ART = 1;
    int AUCTION_ART = 2;

    int ART_SHIPPING_TYPE__FREE_SHIPPING = 1;
    int ART_SHIPPING_TYPE__NO_SHIPPING = 2;
    int ART_SHIPPING_TYPE__SHIPPING_SERVICE = 3;

    boolean ifAdmin = false;

    RequestParams artParams;

    int ExistingArtID = 0;
    Art_T1 ExistingArt;

    UserProfile currentUserProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_art_activity);

        calendar = Calendar.getInstance();

        CheckUserProfile();
        PrepareStage();
        AddListeners();
        initMessageFromBundle();
    }

    private void initMessageFromBundle()
    {
        Bundle data = getIntent().getExtras();
        if (data != null)
        {
            ExistingArtID = data.getInt(GeneralConstants.ID);
            LoadTheDetails();
        }
    }

    void PrepareStage()
    {
        backMe = (LinearLayout) findViewById(R.id.backMe);
        MasterLayout_SV = (ScrollView) findViewById(R.id.MasterLayout_SV);

        SelectATheme_btt = (LinearLayout) findViewById(R.id.SelectATheme_btt);
        ArtSize_SelectSize = (LinearLayout) findViewById(R.id.ArtSize_SelectSize);
        SearchArtistName_Btt = (LinearLayout) findViewById(R.id.SearchArtistName_Btt);
        AuctionPriceHolder = (LinearLayout) findViewById(R.id.AuctionPriceHolder);
        ShopPriceHolder = (LinearLayout) findViewById(R.id.ShopPriceHolder);
        ArtSaleCurrencySelector = (LinearLayout) findViewById(R.id.ArtSaleCurrencySelector);
        auctionExpiresOn_dateHolder = (LinearLayout) findViewById(R.id.auctionExpiresOn_dateHolder);
        shippingServiceContainer = (LinearLayout) findViewById(R.id.shippingServiceContainer);
        searchTheArtistName_row = (LinearLayout) findViewById(R.id.searchTheArtistName_row);

        SelectATheme_bttTxt = (TextView) findViewById(R.id.SelectATheme_bttTxt);
        ArtSize_SelectSize_TV = (TextView) findViewById(R.id.ArtSize_SelectSize_TV);
        ArtSaleCurrencySelector_Text = (TextView) findViewById(R.id.ArtSaleCurrencySelector_Text);
        auctionExpiresOn_date = (TextView) findViewById(R.id.auctionExpiresOn_date);
        shipNotAllowedTo_TV = (TextView) findViewById(R.id.shipNotAllowedTo_TV);
        selectedShippingService = (TextView) findViewById(R.id.selectedShippingService);
        ArtistName = (TextView) findViewById(R.id.ArtistName);

        uploadedImage_1 = (ImageView) findViewById(R.id.uploadedImage_1);
        uploadedImage_1_close = (ImageView) findViewById(R.id.uploadedImage_1_close);
        uploadedImage_2 = (ImageView) findViewById(R.id.uploadedImage_2);
        uploadedImage_2_close = (ImageView) findViewById(R.id.uploadedImage_2_close);
        uploadedImage_3 = (ImageView) findViewById(R.id.uploadedImage_3);
        uploadedImage_3_close = (ImageView) findViewById(R.id.uploadedImage_3_close);
        uploadedImage_4 = (ImageView) findViewById(R.id.uploadedImage_4);
        uploadedImage_4_close = (ImageView) findViewById(R.id.uploadedImage_4_close);
        ArtSalesMethodShop_IMGV = (ImageView) findViewById(R.id.ArtSalesMethodShop_IMGV);
        ArtSalesMethodAuction_IMGV = (ImageView) findViewById(R.id.ArtSalesMethodAuction_IMGV);

        st_noShipping = (ImageView) findViewById(R.id.st_noShipping);
        st_freeShipping = (ImageView) findViewById(R.id.st_freeShipping);
        st_shippingService = (ImageView) findViewById(R.id.st_shippingService);


        ArtTitle = (EditText) findViewById(R.id.ArtTitle);
        ArtDescription = (EditText) findViewById(R.id.ArtDescription);
        ArtTitle_KR = (EditText) findViewById(R.id.ArtTitle_KR);
        ArtDescription_KR = (EditText) findViewById(R.id.ArtDescription_KR);
        ArtSize_W = (EditText) findViewById(R.id.ArtSize_W);
        ArtSize_L = (EditText) findViewById(R.id.ArtSize_L);
        ArtSize_H = (EditText) findViewById(R.id.ArtSize_H);
        MaterialName = (EditText) findViewById(R.id.MaterialName);
        AuctionPriceFrom = (EditText) findViewById(R.id.AuctionPriceFrom);
        AuctionPriceTo = (EditText) findViewById(R.id.AuctionPriceTo);
        ShopPrice = (EditText) findViewById(R.id.ShopPrice);
        ArtYear = (EditText) findViewById(R.id.ArtYear);

        deliveryOnPerson = (CheckBox) findViewById(R.id.deliveryOnPerson);

        submitButton = (Button) findViewById(R.id.submitButton);

        content = (FrameLayout) findViewById(R.id.content);

        artParams = new RequestParams();

        artType = SHOP_ART;
        ART_SHIPPING_TYPE = ART_SHIPPING_TYPE__SHIPPING_SERVICE;

        PrepareShopView();
        CheckIfCurrentUserIsAdmin();
    }

    void LoadTheDetails()
    {
        ShowProgressDialogue();
        Art.GetArtDetails(ExistingArtID + "", new Art.ArtDetailsReceived() {
            @Override
            public void Success(Art_T1 itemData) {
                HideProgressDialogue();
                ExistingArt = itemData;
                populateThePage();
            }

            @Override
            public void NotFound() {
                HideProgressDialogue();
            }

            @Override
            public void Error(String message) {
                HideProgressDialogue();
                Log.e(TAG, message);
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void populateThePage()
    {
        SelectedThemeID = ExistingArt.getTheme_id();

        Dashboard.RetrieveThemeList_v2(new Dashboard.RadioButtonInfo_Retrieved() {
            @Override
            public void Success(final List<RadioButtonInfo> items) {

                for (RadioButtonInfo rbi : items)
                {
                    if(rbi.getID() == SelectedThemeID)
                    {
                        SelectATheme_bttTxt.setText(rbi.getText());
                    }
                }
            }

            @Override
            public void Error(String message) {
                HideProgressDialogue();
                Log.e(TAG, message);
            }
        });

        if(ExistingArt.getImages().size() > 0)
        {
            Picasso.with(getWindow().getContext())
                    .load(ExistingArt.getImages().get(0).getMain_image_thumb())
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .transform(new PicassoRoundTransform())
                    .fit()
                    .into(uploadedImage_1);
        }
        if(ExistingArt.getImages().size() > 1)
        {
            Picasso.with(getWindow().getContext())
                    .load(ExistingArt.getImages().get(1).getMain_image_thumb())
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .transform(new PicassoRoundTransform())
                    .fit()
                    .into(uploadedImage_2);
        }
        if(ExistingArt.getImages().size() > 2)
        {
            Picasso.with(getWindow().getContext())
                    .load(ExistingArt.getImages().get(2).getMain_image_thumb())
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .transform(new PicassoRoundTransform())
                    .fit()
                    .into(uploadedImage_3);
        }
        if(ExistingArt.getImages().size() > 3)
        {
            Picasso.with(getWindow().getContext())
                    .load(ExistingArt.getImages().get(3).getMain_image_thumb())
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .transform(new PicassoRoundTransform())
                    .fit()
                    .into(uploadedImage_4);
        }

        ArtTitle.setText(ExistingArt.getTitle_en());
        ArtTitle_KR.setText(ExistingArt.getTitle_kr());
        ArtDescription.setText(ExistingArt.getDetails_en());
        ArtDescription_KR.setText(ExistingArt.getDetails_kr());
        artType = ExistingArt.getArt_type();
        SelectedArtistId = ExistingArt.getArtist_id();

        ArtSize_W.setText(ExistingArt.getSize().split(" ")[0]);
        ArtSize_L.setText(ExistingArt.getSize().split(" ")[3]);
        ArtSize_H.setText(ExistingArt.getSize().split(" ")[6]);
        ArtSize_SelectSize_TV.setText(ExistingArt.getSize().split(" ")[1]);

        MaterialName.setText(ExistingArt.getMaterial());
        ArtYear.setText(ExistingArt.getYear());
        ArtSaleCurrencySelector_Text.setText(ExistingArt.getCurrency());
        AuctionPriceFrom.setText(ExistingArt.getMin_price()+"");
        AuctionPriceTo.setText(ExistingArt.getMax_price()+"");
        ShopPrice.setText(ExistingArt.getPrice()+"");
        auctionExpiresOn_date.setText(ExistingArt.getExpired_on());

        if(ExistingArt.getDelivery_in_person()==1)
        {
            deliveryOnPerson.setChecked(true);
        }
        else
        {
            deliveryOnPerson.setChecked(false);
        }

        ART_SHIPPING_SS_TYPE = Integer.valueOf(ExistingArt.getShip_id());
        ART_SHIPPING_TYPE = Integer.valueOf(ExistingArt.getShip_type());

        toggleArtType();
        toggleShippingType();

        Dashboard.RetrieveCountryList(new Dashboard.countryListRetrived() {
            @Override
            public void Success(List<RadioButtonInfo> IL_T1) {
                FillUpCountryData(IL_T1);
            }

            @Override
            public void TokenRefreshRequired() {
                Dashboard.RetrieveCountryList(new Dashboard.countryListRetrived() {
                    @Override
                    public void Success(List<RadioButtonInfo> IL_T1) {
                        FillUpCountryData(IL_T1);
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        Log.e(TAG, message);
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                HideProgressDialogue();
                Log.e(TAG, message);
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });


        Art.GetShippingMethods(getWindow().getContext(), new Art.ShippingServiceRetrived() {
            @Override
            public void Success(List<ShippingMethodNames> itemData) {
                for (ShippingMethodNames rbi : itemData)
                {
                    if(ART_SHIPPING_SS_TYPE == rbi.getGroup_id())
                    {
                        selectedShippingService.setText(rbi.getGroup_name());
                    }
                }
            }

            @Override
            public void NotFound() {
                Toast.makeText(getWindow().getContext(), "No Method Found", Toast.LENGTH_LONG).show();
            }

            @Override
            public void TokenRefreshRequired() {
                Art.GetShippingMethods(getWindow().getContext(), new Art.ShippingServiceRetrived() {
                    @Override
                    public void Success(List<ShippingMethodNames> itemData) {
                        for (ShippingMethodNames rbi : itemData)
                        {
                            if(ART_SHIPPING_SS_TYPE == rbi.getGroup_id())
                            {
                                selectedShippingService.setText(rbi.getGroup_name());
                            }
                        }
                    }

                    @Override
                    public void NotFound() {
                        Toast.makeText(getWindow().getContext(), "No Method Found", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        Log.e(TAG, message);
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Log.e(TAG, message);
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void CheckIfCurrentUserIsAdmin()
    {
        ShowProgressDialogue();
        UserAuthentication.CheckIfCurrentUserIsAdmin(new UserAuthentication.checkIfAdmin() {
            @Override
            public void Yes() {
                ifAdmin = true;
                searchTheArtistName_row.setVisibility(View.VISIBLE);
                HideProgressDialogue();
            }

            @Override
            public void No() {
                ifAdmin = false;
                if(currentUserProfile.getType()==3)
                {
                    // Curator
                    searchTheArtistName_row.setVisibility(View.VISIBLE);
                }
                else
                {
                    searchTheArtistName_row.setVisibility(View.GONE);
                }
                HideProgressDialogue();
            }

            @Override
            public void TokenRefresh() {
                UserAuthentication.CheckIfCurrentUserIsAdmin(new UserAuthentication.checkIfAdmin() {
                    @Override
                    public void Yes() {
                        ifAdmin = true;
                        searchTheArtistName_row.setVisibility(View.VISIBLE);
                        HideProgressDialogue();
                    }

                    @Override
                    public void No() {
                        ifAdmin = false;
                        if(currentUserProfile.getType()==3)
                        {
                            // Curator
                            searchTheArtistName_row.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            searchTheArtistName_row.setVisibility(View.GONE);
                        }
                        HideProgressDialogue();
                    }

                    @Override
                    public void TokenRefresh() {
                        ifAdmin = false;
                        searchTheArtistName_row.setVisibility(View.GONE);
                        HideProgressDialogue();
                    }
                });
            }
        });
    }

    void AddListeners()
    {
        SelectATheme_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowProgressDialogue();
                Dashboard.RetrieveThemeList_v2(new Dashboard.RadioButtonInfo_Retrieved() {
                    @Override
                    public void Success(final List<RadioButtonInfo> items) {
                        HideProgressDialogue();
                        OptionListing_T1 themeList = new OptionListing_T1(getWindow().getContext());
                        themeList.setup3("Theme", items, SelectedThemeID);
                        themeList.setListener(new OptionListing_T1.Success() {
                            @Override
                            public void Done(RadioButtonInfo value) {
                                SelectedThemeID = value.getID();
                                SelectATheme_bttTxt.setText(value.getText());
                            }
                        });
                        themeList.show();
                    }

                    @Override
                    public void Error(String message) {
                        HideProgressDialogue();
                        Log.e(TAG, message);
                    }
                });

            }
        });

        uploadedImage_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RunImagePicker(uploadedImage_1, 1);
            }
        });
        uploadedImage_1_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResetImageHolder(uploadedImage_1, 1);
            }
        });

        uploadedImage_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RunImagePicker(uploadedImage_2, 2);
            }
        });
        uploadedImage_2_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResetImageHolder(uploadedImage_2, 1);
            }
        });

        uploadedImage_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RunImagePicker(uploadedImage_3, 3);
            }
        });
        uploadedImage_3_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResetImageHolder(uploadedImage_3, 1);
            }
        });

        uploadedImage_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RunImagePicker(uploadedImage_4, 4);
            }
        });
        uploadedImage_4_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResetImageHolder(uploadedImage_4, 1);
            }
        });

        ArtSalesMethodShop_IMGV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                artType = SHOP_ART;
                toggleArtType();
            }
        });

        ArtSalesMethodAuction_IMGV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                artType = AUCTION_ART;
                toggleArtType();
            }
        });

        ArtSize_SelectSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OptionListing_T1 t = new OptionListing_T1(getWindow().getContext());
                List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();
                final String[] theList = new String[]{"mm", "cm", "Foot", "Inch"};
                for (int i = 0; i < theList.length; i++) {
                    RadioButtonInfo io1=new RadioButtonInfo();
                    io1.setID(i);
                    io1.setText(theList[i]);
                    btns.add(io1);
                }
                t.setup2("Size", btns, ArtSize_SelectSize_TV.getText().toString());
                t.setListener(new OptionListing_T1.Success() {
                    @Override
                    public void Done(RadioButtonInfo value) {
                        ArtSize_SelectSize_TV.setText(value.getText());
                    }
                });
                t.show();
            }
        });

        ArtSaleCurrencySelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OptionListing_T1 t = new OptionListing_T1(getWindow().getContext());
                List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();
                final String[] theList = new String[]{"USD", "WON"};
                for (int i = 0; i < theList.length; i++) {
                    RadioButtonInfo io1=new RadioButtonInfo();
                    io1.setID(i);
                    io1.setText(theList[i]);
                    btns.add(io1);
                }
                t.setup2("Currency", btns, ArtSaleCurrencySelector_Text.getText().toString());
                t.setListener(new OptionListing_T1.Success() {
                    @Override
                    public void Done(RadioButtonInfo value) {
                        ArtSaleCurrencySelector_Text.setText(value.getText());
                    }
                });
                t.show();
            }
        });
        auctionExpiresOn_date.setText(calendar.get(Calendar.YEAR)+". "+(calendar.get(Calendar.MONTH) + 1)+". "+calendar.get(Calendar.DATE));
        auctionExpiresOn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DPicker dp= new DPicker(getWindow().getContext());
                dp.setTheTitle("You will be expired on");
                dp.setDoneBttBg(getResources().getDrawable(R.drawable.next_button));
                //dp.setCancelBttBg(getResources().getDrawable(R.drawable.loginbuttonshape));
                dp.setCancelBttBg(getResources().getDrawable(R.drawable.birthday_box_cancel));

                if(TextUtils.isEmpty(auctionExpiresOn_date.getText()))
                {
                    dp.setDefaultValue(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
                }else{

                    String i = auctionExpiresOn_date.getText().toString();
                    System.out.println(i);
                    String[] _split = i.split(". ");
                    dp.setDefaultValue(Integer.valueOf(_split[2]), (Integer.valueOf(_split[1]) - 1), Integer.valueOf(_split[0]));
                }

                dp.setListener(new DPicker.Success() {
                    @Override
                    public void Done(Date _date) {
                        auctionExpiresOn_date.setText(_date.getYear()+". "+(_date.getMonth() + 1)+". "+_date.getDate());
                        auctionExpiresOn = (_date.getMonth() < 10 ? "0":"")+(_date.getMonth() + 1)+". "+(_date.getDate() < 10 ? "0":"")+_date.getDate()+". "+_date.getYear();
                    }
                });
                dp.show();
            }
        });


        st_noShipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ART_SHIPPING_TYPE = ART_SHIPPING_TYPE__NO_SHIPPING;
                toggleShippingType();
            }
        });

        st_freeShipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ART_SHIPPING_TYPE = ART_SHIPPING_TYPE__FREE_SHIPPING;
                toggleShippingType();
            }
        });

        st_shippingService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ART_SHIPPING_TYPE = ART_SHIPPING_TYPE__SHIPPING_SERVICE;
                toggleShippingType();
            }
        });

        shipNotAllowedTo_TV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                shipNotAllowedTo_TV.setEnabled(false);

                ShowProgressDialogue();
                Dashboard.RetrieveCountryList(new Dashboard.countryListRetrived() {
                    @Override
                    public void Success(List<RadioButtonInfo> IL_T1) {
                        HideProgressDialogue();
                        ShowCountryPopup(IL_T1);
                        shipNotAllowedTo_TV.setEnabled(true);
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        Dashboard.RetrieveCountryList(new Dashboard.countryListRetrived() {
                            @Override
                            public void Success(List<RadioButtonInfo> IL_T1) {
                                HideProgressDialogue();
                                ShowCountryPopup(IL_T1);
                                shipNotAllowedTo_TV.setEnabled(true);
                            }

                            @Override
                            public void TokenRefreshRequired() {

                                shipNotAllowedTo_TV.setEnabled(true);
                            }

                            @Override
                            public void Error(String message) {
                                HideProgressDialogue();
                                shipNotAllowedTo_TV.setEnabled(true);
                                Log.e(TAG, message);
                                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    @Override
                    public void Error(String message) {
                        HideProgressDialogue();
                        shipNotAllowedTo_TV.setEnabled(true);
                        Log.e(TAG, message);
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

        selectedShippingService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowProgressDialogue();
                Art.GetShippingMethods(getWindow().getContext(), new Art.ShippingServiceRetrived() {
                    @Override
                    public void Success(List<ShippingMethodNames> itemData) {
                        HideProgressDialogue();
                        ShowShippingServicePopup(itemData);
                    }

                    @Override
                    public void NotFound() {
                        HideProgressDialogue();
                        Toast.makeText(getWindow().getContext(), "No Method Found", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        Art.GetShippingMethods(getWindow().getContext(), new Art.ShippingServiceRetrived() {
                            @Override
                            public void Success(List<ShippingMethodNames> itemData) {
                                HideProgressDialogue();
                                ShowShippingServicePopup(itemData);
                            }

                            @Override
                            public void NotFound() {
                                HideProgressDialogue();
                                Toast.makeText(getWindow().getContext(), "No Method Found", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void TokenRefreshRequired() {

                            }

                            @Override
                            public void Error(String message) {
                                HideProgressDialogue();
                                Log.e(TAG, message);
                                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    @Override
                    public void Error(String message) {
                        HideProgressDialogue();
                        Log.e(TAG, message);
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        SearchArtistName_Btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowProgressDialogue();
                Bundle args = new Bundle();
                FragmentReplace(new frg_CreateEditAnArt_Activity_SearchArtist(), args);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateForm(true))
                {
                    ShowProgressDialogue();
                    Art.SaveAnArt(artParams, new Art.SaveTheArt() {
                        @Override
                        public void Success() {
                            HideProgressDialogue();
                            Toast.makeText(getWindow().getContext(), "Art uploaded successfully.", Toast.LENGTH_LONG).show();
                            onBackPressed();
                        }

                        @Override
                        public void NotFound(){}

                        @Override
                        public void TokenRefreshRequired() {
                            Art.SaveAnArt(artParams, new Art.SaveTheArt() {
                                @Override
                                public void Success() {
                                    HideProgressDialogue();
                                    Toast.makeText(getWindow().getContext(), "Art uploaded successfully.", Toast.LENGTH_LONG).show();
                                    onBackPressed();
                                }

                                @Override
                                public void NotFound(){}

                                @Override
                                public void TokenRefreshRequired(){}

                                @Override
                                public void Error(String message) {
                                    HideProgressDialogue();
                                    Log.e(TAG, message);
                                    Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                                }
                            });
                        }

                        @Override
                        public void Error(String message) {
                            HideProgressDialogue();
                            Log.e(TAG, message);
                            Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        backMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    void CheckUserProfile()
    {
        currentUserProfile = new Gson().fromJson(Prefs.getString(UserConstants.UserFullDetails, "{}"),  new TypeToken<UserProfile>(){}.getType());
    }

    boolean validateForm(boolean showToast)
    {
        artParams.put(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        if(TextUtils.isEmpty(SelectATheme_bttTxt.getText()))
        {
            if(showToast)
            {
                Toast.makeText(getWindow().getContext(), "Select A Theme Please", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        artParams.put("theme_id", SelectedThemeID+"");



        if(TextUtils.isEmpty(ArtTitle.getText()))
        {
            if(showToast)
            {
                Toast.makeText(getWindow().getContext(), "Set Art Title", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        artParams.put("title_en", ArtTitle.getText().toString());
        artParams.put("title_kr", ArtTitle_KR.getText().toString());

        if(TextUtils.isEmpty(ArtDescription.getText()))
        {
            if(showToast)
            {
                Toast.makeText(getWindow().getContext(), "Set Details", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        artParams.put("details_en", ArtDescription.getText().toString());
        artParams.put("details_kr", ArtDescription_KR.getText().toString());
        artParams.put("type", artType+"");

        if(ifAdmin)
        {
            if(SelectedArtistId == 0)
            {
                if(showToast)
                {
                    Toast.makeText(getWindow().getContext(), "You have to select an artist", Toast.LENGTH_LONG).show();
                }
                return false;
            }
            artParams.put("artist_id", SelectedArtistId+"");
        }

        if(TextUtils.isEmpty(ArtSize_W.getText()))
        {
            if(showToast)
            {
                Toast.makeText(getWindow().getContext(), "Set Art Width", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        if(TextUtils.isEmpty(ArtSize_L.getText()))
        {
            if(showToast)
            {
                Toast.makeText(getWindow().getContext(), "Set Art Length", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        if(TextUtils.isEmpty(ArtSize_H.getText()))
        {
            if(showToast)
            {
                Toast.makeText(getWindow().getContext(), "Set Art Height", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        artParams.put("size",
                ArtSize_W.getText().toString()+" "+ArtSize_SelectSize_TV.getText().toString()+" X "+
                        ArtSize_L.getText().toString()+" "+ArtSize_SelectSize_TV.getText().toString()+" X "+
                        ArtSize_H.getText().toString()+" "+ArtSize_SelectSize_TV.getText().toString()
        );

        if(TextUtils.isEmpty(MaterialName.getText()))
        {
            if(showToast)
            {
                Toast.makeText(getWindow().getContext(), "Set Material Name", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        artParams.put("material", MaterialName.getText()+"");


        if(TextUtils.isEmpty(ArtYear.getText()))
        {
            if(showToast)
            {
                Toast.makeText(getWindow().getContext(), "Set Art Year", Toast.LENGTH_LONG).show();
            }
            return false;
        }
        artParams.put("year", ArtYear.getText()+"");

        artParams.put("currency", ArtSaleCurrencySelector_Text.getText().toString()+"");

        if(artType == AUCTION_ART)
        {
            if(TextUtils.isEmpty(AuctionPriceFrom.getText()))
            {
                if(showToast)
                {
                    Toast.makeText(getWindow().getContext(), "Set Minimum Auction Price", Toast.LENGTH_LONG).show();
                }
                return false;
            }

            artParams.put("min_price", AuctionPriceFrom.getText().toString()+"");


            if(TextUtils.isEmpty(AuctionPriceTo.getText()))
            {
                if(showToast)
                {
                    Toast.makeText(getWindow().getContext(), "Set Maximum Auction Price", Toast.LENGTH_LONG).show();
                }
                return false;
            }
            artParams.put("max_price", AuctionPriceTo.getText().toString()+"");
        }
        else
        {

            if(TextUtils.isEmpty(ShopPrice.getText()))
            {
                if(showToast)
                {
                    Toast.makeText(getWindow().getContext(), "Set Price", Toast.LENGTH_LONG).show();
                }
                return false;
            }

            artParams.put("price", ShopPrice.getText().toString()+"");
        }

        if(artType == AUCTION_ART)
        {
            if(TextUtils.isEmpty(auctionExpiresOn_date.getText()))
            {
                if(showToast)
                {
                    Toast.makeText(getWindow().getContext(), "Set Art Year", Toast.LENGTH_LONG).show();
                }
                return false;
            }

            String[] birthDate = auctionExpiresOn_date.getText().toString().split(". ");
            artParams.put("expired_on", (birthDate[1]+"/"+birthDate[2]+"/"+birthDate[0])+"");
        }

        if(currentUserProfile.getType() == 3)
        {
            artParams.put("curator_id", currentUserProfile.getId());
        }

        if(deliveryOnPerson.isChecked())
        {
            artParams.put("delivery_person_checker", "1");
        }
        else
        {
            artParams.put("delivery_person_checker", "0");
        }

        artParams.put("shipping_group_type", ART_SHIPPING_TYPE+"");


        if(ART_SHIPPING_TYPE == ART_SHIPPING_TYPE__SHIPPING_SERVICE)
        {
            if(ART_SHIPPING_SS_TYPE==0)
            {
                if(showToast)
                {
                    Toast.makeText(getWindow().getContext(), "Set Shipping Service Types", Toast.LENGTH_LONG).show();
                }
                return false;
            }

            artParams.put("shipping_group", ART_SHIPPING_SS_TYPE+"");
            artParams.put("not_ship_list", unAllowedShipping_CountryCodes.split(", "));
        }

        String contentType = RequestParams.APPLICATION_OCTET_STREAM;

        if(Selected_uploadedImage_1 == null && Selected_uploadedImage_2 == null && Selected_uploadedImage_3 == null && Selected_uploadedImage_4 == null)
        {
            if(showToast)
            {
                Toast.makeText(getWindow().getContext(), "At Least Select an image", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        List<File> uploadedFiles = new ArrayList<File>();
        if(Selected_uploadedImage_1 !=null)
        {
            try {
                //artParams.put("art_image["+uploadedFiles.size()+"]", Selected_uploadedImage_1);
                artParams.put("art_image["+uploadedFiles.size()+"]", Selected_uploadedImage_1, contentType);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            uploadedFiles.add(Selected_uploadedImage_1);
        }
        if(Selected_uploadedImage_2 !=null)
        {
            try {
                //artParams.put("art_image["+uploadedFiles.size()+"]", Selected_uploadedImage_2);
                artParams.put("art_image["+uploadedFiles.size()+"]", Selected_uploadedImage_2, contentType);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            uploadedFiles.add(Selected_uploadedImage_2);
        }
        if(Selected_uploadedImage_3 !=null)
        {
            try {
                //artParams.put("art_image["+uploadedFiles.size()+"]", Selected_uploadedImage_3);
                artParams.put("art_image["+uploadedFiles.size()+"]", Selected_uploadedImage_3, contentType);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            uploadedFiles.add(Selected_uploadedImage_3);
        }
        if(Selected_uploadedImage_4 !=null)
        {
            try {
                //artParams.put("art_image["+uploadedFiles.size()+"]", Selected_uploadedImage_4);
                artParams.put("art_image["+uploadedFiles.size()+"]", Selected_uploadedImage_4, contentType);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            uploadedFiles.add(Selected_uploadedImage_4);
        }

        if(uploadedFiles.size()<1)
        {
            if(showToast)
            {
                Toast.makeText(getWindow().getContext(), "At Least Select an image", Toast.LENGTH_LONG).show();
            }
            return false;
        }

        artParams.setHttpEntityIsRepeatable(true);
        artParams.setUseJsonStreamer(false);

        return true;
    }

    void ShowShippingServicePopup(List<ShippingMethodNames> smn)
    {
        OptionListing_T1 t = new OptionListing_T1(getWindow().getContext());
        List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();
        for (int i = 0; i < smn.size(); i++) {
            RadioButtonInfo io1=new RadioButtonInfo();
            io1.setID(smn.get(i).getGroup_id());
            io1.setText(smn.get(i).getGroup_name());
            btns.add(io1);
        }
        t.setup2("Shipping Methods", btns, selectedShippingService.getText().toString());
        t.setListener(new OptionListing_T1.Success() {
            @Override
            public void Done(RadioButtonInfo value) {
                ART_SHIPPING_SS_TYPE = value.getID();
                selectedShippingService.setText(value.getText());
            }
        });
        t.show();
    }

    void ShowCountryPopup(List<RadioButtonInfo> btns)
    {
        OptionListing_T3 t = new OptionListing_T3(getWindow().getContext());
        t.setup("Shipping Not Allowed To", btns, unAllowedShipping_CountryCodes);
        t.setListener(new OptionListing_T3.Success() {
            @Override
            public void SubmitResult(String countryCodes, String countryNames) {
                shipNotAllowedTo_TV.setText(countryNames);
                unAllowedShipping_CountryCodes = countryCodes;
            }
        });
        t.show();
    }

    void FillUpCountryData(List<RadioButtonInfo> btns)
    {
        String optionsV1="", optionsV2="";

        for (RadioButtonInfo rbi : btns)
        {
            optionsV1 = optionsV1 + rbi.getsID()+", ";
            optionsV2 = optionsV2 + rbi.getText()+", ";
        }
        shipNotAllowedTo_TV.setText(optionsV2);
        unAllowedShipping_CountryCodes = optionsV1;
    }

    public void RunImagePicker(final ImageView imageHolder, final int setFileTo)
    {
        CCIP = new CCImagePicker(CreateEditAnArt_Activity.this);
        CCIP.setListener(new CCImagePicker.task() {
            @Override
            public void fromCamera() {
                cameraPicker = new CameraImagePicker(CreateEditAnArt_Activity.this);
                cameraPicker.setImagePickerCallback(new ImagePickerCallback(){
                                                        @Override
                                                        public void onImagesChosen(List<ChosenImage> images) {
                                                            // Display images
                                                            System.out.println(images.toString());
                                                            System.out.println(images.get(0).toString());
                                                            System.out.println(images.get(0).getOriginalPath());

                                                            if(setFileTo == 1)
                                                            {
                                                                Selected_uploadedImage_1 = new File(images.get(0).getOriginalPath());
                                                            }
                                                            else if(setFileTo == 2)
                                                            {
                                                                Selected_uploadedImage_2 = new File(images.get(0).getOriginalPath());
                                                            }
                                                            else if(setFileTo == 3)
                                                            {
                                                                Selected_uploadedImage_3 = new File(images.get(0).getOriginalPath());
                                                            }
                                                            else if(setFileTo == 4)
                                                            {
                                                                Selected_uploadedImage_4 = new File(images.get(0).getOriginalPath());
                                                            }

                                                            /*Picasso.with(getWindow().getContext())
                                                                    .load(new File(images.get(0).getOriginalPath()))
                                                                    .placeholder(R.drawable.img_photo_01_list)
                                                                    .error(R.drawable.img_photo_01_list)
                                                                    .fit().centerCrop()
                                                                    .into(imageHolder);*/

                                                            Picasso.Builder builder = new Picasso.Builder(getWindow().getContext());
                                                            builder.listener(new Picasso.Listener()
                                                            {
                                                                @Override
                                                                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception)
                                                                {
                                                                    exception.printStackTrace();
                                                                }
                                                            });
                                                            builder.build().load(new File(images.get(0).getOriginalPath()))
                                                                    .placeholder(R.drawable.img_photo_01_list)
                                                                    .error(R.drawable.img_photo_01_list)
                                                                    .fit().centerCrop()
                                                                    .into(imageHolder);
                                                        }

                                                        @Override
                                                        public void onError(String message) {
                                                            // Do error handling
                                                            Log.e(TAG, message);
                                                        }
                                                    }
                );
                cameraPicker.pickImage();
            }

            @Override
            public void fromGallery() {
                imagePicker = new ImagePicker(CreateEditAnArt_Activity.this);
                imagePicker.setImagePickerCallback(new ImagePickerCallback(){
                                                       @Override
                                                       public void onImagesChosen(List<ChosenImage> images) {

                                                           System.out.println(images.toString());
                                                           System.out.println(images.get(0).toString());
                                                           System.out.println(images.get(0).getOriginalPath());

                                                           if(setFileTo == 1)
                                                           {
                                                               Selected_uploadedImage_1 = new File(images.get(0).getOriginalPath());
                                                           }
                                                           else if(setFileTo == 2)
                                                           {
                                                               Selected_uploadedImage_2 = new File(images.get(0).getOriginalPath());
                                                           }
                                                           else if(setFileTo == 3)
                                                           {
                                                               Selected_uploadedImage_3 = new File(images.get(0).getOriginalPath());
                                                           }
                                                           else if(setFileTo == 4)
                                                           {
                                                               Selected_uploadedImage_4 = new File(images.get(0).getOriginalPath());
                                                           }

                                                           Picasso.with(getWindow().getContext())
                                                                   .load(new File(images.get(0).getOriginalPath()))
                                                                   .placeholder(R.drawable.img_photo_01_list)
                                                                   .error(R.drawable.img_photo_01_list)
                                                                   .transform(new PicassoRoundTransform())
                                                                   .fit()
                                                                   .into(imageHolder);

                                                       }

                                                       @Override
                                                       public void onError(String message) {
                                                           // Do error handling
                                                           Log.e(TAG, message);
                                                       }
                                                   }
                );
                imagePicker.pickImage();
            }
        });

        CCIP.show();
    }

    public void ResetImageHolder(final ImageView imageHolder, final int setFileTo)
    {
        if(setFileTo == 1)
        {
            Selected_uploadedImage_1 = null;
        }
        else if(setFileTo == 2)
        {
            Selected_uploadedImage_2 = null;
        }
        else if(setFileTo == 3)
        {
            Selected_uploadedImage_3 = null;
        }
        else if(setFileTo == 4)
        {
            Selected_uploadedImage_4 = null;
        }

        Picasso.with(getWindow().getContext())
                .load(R.drawable.img_photo_01_list)
                .placeholder(R.drawable.img_photo_01_list)
                .error(R.drawable.img_photo_01_list)
                .transform(new PicassoRoundTransform())
                .fit()
                .into(imageHolder);
    }

    void toggleArtType()
    {
        if(artType == AUCTION_ART)
        {
            Picasso.with(getWindow().getContext()).load(R.drawable.img_radio_on).into(ArtSalesMethodAuction_IMGV);
            Picasso.with(getWindow().getContext()).load(R.drawable.img_radio_off).into(ArtSalesMethodShop_IMGV);
            PrepareAuctionView();
        }
        else
        {
            Picasso.with(getWindow().getContext()).load(R.drawable.img_radio_on).into(ArtSalesMethodShop_IMGV);
            Picasso.with(getWindow().getContext()).load(R.drawable.img_radio_off).into(ArtSalesMethodAuction_IMGV);
            PrepareShopView();
        }
    }

    void toggleShippingType()
    {
        if(ART_SHIPPING_TYPE == ART_SHIPPING_TYPE__NO_SHIPPING)
        {
            shippingServiceContainer.setVisibility(View.GONE);

            Picasso.with(getWindow().getContext()).load(R.drawable.btn_line02_pre).into(st_noShipping);
            Picasso.with(getWindow().getContext()).load(R.drawable.btn_line02_dim).into(st_freeShipping);
            Picasso.with(getWindow().getContext()).load(R.drawable.btn_line02_dim).into(st_shippingService);
        }
        else if (ART_SHIPPING_TYPE == ART_SHIPPING_TYPE__FREE_SHIPPING)
        {
            shippingServiceContainer.setVisibility(View.GONE);

            Picasso.with(getWindow().getContext()).load(R.drawable.btn_line02_dim).into(st_noShipping);
            Picasso.with(getWindow().getContext()).load(R.drawable.btn_line02_pre).into(st_freeShipping);
            Picasso.with(getWindow().getContext()).load(R.drawable.btn_line02_dim).into(st_shippingService);
        }
        else
        {
            shippingServiceContainer.setVisibility(View.VISIBLE);

            Picasso.with(getWindow().getContext()).load(R.drawable.btn_line02_dim).into(st_noShipping);
            Picasso.with(getWindow().getContext()).load(R.drawable.btn_line02_dim).into(st_freeShipping);
            Picasso.with(getWindow().getContext()).load(R.drawable.btn_line02_pre).into(st_shippingService);

            MasterLayout_SV.post(new Runnable() {
                @Override
                public void run() {
                    MasterLayout_SV.fullScroll(View.FOCUS_DOWN);
                }
            });
        }
    }

    void PrepareAuctionView()
    {
        ShopPriceHolder.setVisibility(View.GONE);
        AuctionPriceHolder.setVisibility(View.VISIBLE);
        auctionExpiresOn_dateHolder.setVisibility(View.VISIBLE);
    }

    void PrepareShopView()
    {
        ShopPriceHolder.setVisibility(View.VISIBLE);
        AuctionPriceHolder.setVisibility(View.GONE);
        auctionExpiresOn_dateHolder.setVisibility(View.GONE);
    }

    private void FragmentReplace(Fragment fragment, Bundle arg)
    {
        content.setVisibility(View.VISIBLE);
        fragment.setArguments(arg);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, fragment);
        fragmentTransaction.commit();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent_data) {

        System.out.println(requestCode+"---------->"+resultCode);

        if (requestCode == Picker.PICK_IMAGE_DEVICE) {
            System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
                imagePicker.submit(intent_data);
            }
        }else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
            System.out.println("PICK_IMAGE_CAMERA");
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("PICK_IMAGE_CAMERA  RESULT_OK");
                cameraPicker.submit(intent_data);
            }
        }else{
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
                imagePicker.submit(intent_data);
            }
        }

    }

    @Override
    public void ShowProgressDialogue()
    {
        if(progress == null)
        {
            progress = ProgressDialog.show(getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);
        }
    }

    @Override
    public void HideProgressDialogue()
    {
        if(progress != null)
        {
            progress.dismiss();
            progress = null;
        }
    }

    @Override
    public void PickedArtist(SearchListItem selectedArtist) {

        SelectedArtistId = selectedArtist.getID();
        ArtistName.setText(selectedArtist.getText());
        content.setVisibility(View.GONE);
    }

    @Override
    public void CloseFragment() {
        content.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed()
    {
        if(content.getVisibility() == View.VISIBLE)
        {
            content.setVisibility(View.GONE);
        }
        else
        {
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        }
    }
}
