package com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.kona.artmining.Adapter.DashboardFrgLatest_Event_Adapter_T1;
import com.kona.artmining.Helper.DashboardFrgLatest;
import com.kona.artmining.Interface.ActivityCommunicator;
import com.kona.artmining.Model.DashboardFrgLatest_Event_Model_T1;
import com.kona.artmining.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DFLF_Event extends Fragment {

    SwipeRefreshLayout swipeRefreshLayout;
    ObservableRecyclerView recycler_view;
    DashboardFrgLatest_Event_Adapter_T1 latestAdapter;

    private List<DashboardFrgLatest_Event_Model_T1> Content = new ArrayList<DashboardFrgLatest_Event_Model_T1>();
    ActivityCommunicator ac;


    public DFLF_Event() {
        // Required empty public constructor
    }

    public static DFLF_Event newInstance(int param1) {
        DFLF_Event fragment = new DFLF_Event();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ui_dashboard_frgmnts_latest_frg_event, container, false);
        PrepareStage(view);
        PopulateEvents();
        return view;
    }

    void PrepareStage(View view)
    {
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        recycler_view = (ObservableRecyclerView) view.findViewById(R.id.recycler_view);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        recycler_view.setLayoutManager(layoutManager);

        latestAdapter = new DashboardFrgLatest_Event_Adapter_T1(Content, new DashboardFrgLatest_Event_Adapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(DashboardFrgLatest_Event_Model_T1 item) {
                ac.ShowEventFrg(item.getEvent_id()+"");
            }
        });

        /*for(int i=0; i<20; i++) {
            DashboardFrgLatest_Event_Model_T1 lm = new DashboardFrgLatest_Event_Model_T1();
            lm.setExb_image_thump("http://i.stack.imgur.com/dCyzH.png");
            Content.add(lm);
        }*/

        recycler_view.setAdapter(latestAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                PopulateEvents();
            }
        });
    }

    void PopulateEvents() {
        DashboardFrgLatest.RetrieveAllEvents(new DashboardFrgLatest.Event_Retrieved() {
            @Override
            public void Success(List<DashboardFrgLatest_Event_Model_T1> data) {
                clearRVData();
                Content.addAll(data);
                latestAdapter.notifyDataSetChanged();
                if(swipeRefreshLayout != null)
                {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void Error(String message) {

            }
        });
    }

    public void clearRVData()
    {
        int size = Content.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Content.remove(0);
            }
            latestAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ac = (ActivityCommunicator) getActivity();
    }

}
