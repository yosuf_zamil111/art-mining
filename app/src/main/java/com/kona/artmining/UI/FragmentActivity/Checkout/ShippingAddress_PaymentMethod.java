package com.kona.artmining.UI.FragmentActivity.Checkout;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Cart;
import com.kona.artmining.Helper.Dashboard;
import com.kona.artmining.Interface.CheckoutActivity_Interface;
import com.kona.artmining.Interface.FragmentCommunicator;
import com.kona.artmining.Model.AuctionItem_T1;
import com.kona.artmining.Model.CartItem_T1;
import com.kona.artmining.Model.Order_T1;
import com.kona.artmining.Model.Shipping_T1;
import com.kona.artmining.R;
import com.kona.artmining.UI.FragmentActivity.TheWebView;
import com.kona.artmining.UI.FragmentController.UserActivity_FragmentsManager;
import com.kona.artmining.custom_components.FormField_T2_TextBox;
import com.kona.artmining.custom_components.RadioButton_T2;
import com.kona.artmining.custom_components.RadioButton_T3;
import com.kona.artmining.custom_components.Spinner_T2;
import com.kona.artmining.custom_popup_box.OptionListing_T1;
import com.kona.artmining.model.RadioButtonInfo;
import com.kona.artmining.option_table.Table_T1;
import com.kona.artmining.option_table.modals.Col2RowDetails;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class ShippingAddress_PaymentMethod extends Fragment implements FragmentCommunicator{


    private static final int PAYMENT_METHOD__PAYPAL = 1;
    private static final int PAYMENT_METHOD__CREDIT_CARD = 2;
    private static final int PAYMENT_METHOD__INI_PAY = 3;

    private static final int SHIPPING_METHOD__DOMESTIC = 1;
    private static final int SHIPPING_METHOD__OVERSEAS = 2;

    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    String SHIPPING_COST = GeneralConstants.SHIPPING_COST;
    String TAX = "0.00";

    public ShippingAddress_PaymentMethod() {}

    TextView backBgTitle;
    Button bottomButton;
    int CurrentSteps = 0;
    int SelectedPaymentMethod = PAYMENT_METHOD__PAYPAL;
    int SelectedShippingType = SHIPPING_METHOD__DOMESTIC;

    int STEP_SHIPPING = 1;
    int STEP_PAYMENT = 2;
    int STEP_ORDER_SUMMARY = 3;

    FrameLayout payment_layout;
    LinearLayout backBtt;
    LinearLayout RadioButtonPanel;
    ScrollView theOver;
    //FrameLayout theDom;

    ScrollView theDom;

    CheckoutActivity_Interface mCallback;
    List<CartItem_T1> ModifiedCart;
    List<AuctionItem_T1> ModifiedAuctionCart;

    Order_T1 PlacedOrder;

    private static final String TAG = "ShippingAddressPm";

    ProgressDialog progress;

    LinearLayout shippingZipSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ui_checkout_frg_shipping, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStage(view);
    }

    void prepareStage(final View view)
    {
        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);

        payment_layout = (FrameLayout) view.findViewById(R.id.payment_layout);
        payment_layout.setVisibility(View.GONE);
        RadioButtonPanel  = (LinearLayout) view.findViewById(R.id.RadioButtonPanel);

        view.findViewById(R.id.showTheOverSease).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                theOver.setVisibility(View.VISIBLE);
                theDom.setVisibility(View.GONE);
                shippingInfo.setOverseas(true);
                shippingInfo.setAddress_type("overseas");
                ifShippingOverseas = true;
                SelectedShippingType = SHIPPING_METHOD__OVERSEAS;
            }
        });
        view.findViewById(R.id.showTheDom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                theOver.setVisibility(View.GONE);
                theDom.setVisibility(View.VISIBLE);
                shippingInfo.setOverseas(false);
                shippingInfo.setAddress_type("domestic");
                ifShippingOverseas = false;
                SelectedShippingType = SHIPPING_METHOD__DOMESTIC;
            }
        });

        CurrentSteps = STEP_SHIPPING;

        backBgTitle = (TextView) view.findViewById(R.id.backBgTitle);
        backBgTitle.setText("Enter Shipping Address");

        bottomButton = (Button) view.findViewById(R.id.bottomButton);
        bottomButton.setText("Continue");
        bottomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CurrentSteps == STEP_SHIPPING)
                {
                    if(ifShippingOverseas)
                    {
                        if(validateOverseasShippingForm())
                        {
                            if(DS_over)
                            {
                                SaveCurrentShippingData();
                            }
                            else
                            {
                                ProcessTheOrder();
                            }
                        }
                    }else{
                        if(validateDomesticShippingForm())
                        {
                            if(DS_dom)
                            {
                                SaveCurrentShippingData();
                            }
                            else
                            {
                                ProcessTheOrder();
                            }
                        }
                    }
                }
                else if(CurrentSteps == STEP_PAYMENT)
                {
                    if(SelectedPaymentMethod == PAYMENT_METHOD__PAYPAL)
                    {
                        if(getArguments().getString(GeneralConstants.SELECTED_CURRENCY).equals("USD"))
                        {
                            OpenPayPalActivity();
                        }
                        else
                        {
                            Toast.makeText(getContext(), "We are not supporting this currency.", Toast.LENGTH_LONG).show();
                        }
                    }
                    else if(SelectedPaymentMethod == PAYMENT_METHOD__INI_PAY)
                    {
                        PrepareIniPayData();
                    }
                    else
                    {
                        Toast.makeText(getContext(), "Right now we only support Paypal.", Toast.LENGTH_LONG).show();
                    }
                }

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        });

        PlacedOrder = new Order_T1();

        PrepareShippingZipcodeSearch(view);

        PrepareShippingScreen(view);
        SetupPaymentMethods();
        RetriveModifiedCart();

    }

    void PrepareIniPayData()
    {
        double TotalAmount = 0;

        if(PlacedOrder.getOrder_type().equals("1"))
        {
            TotalAmount = 0;
            for(int i=0; i < ModifiedCart.size(); i++)
            {
                TotalAmount = TotalAmount + ModifiedCart.get(i).getPrice();
            }
        }
        else
        {
            TotalAmount = 0;
            for(int i=0; i < ModifiedAuctionCart.size(); i++)
            {
                TotalAmount = TotalAmount + ModifiedCart.get(i).getPrice();
            }
        }

        //TotalAmount = TotalAmount + Double.parseDouble(GeneralConstants.SHIPPING_COST);
        TotalAmount = TotalAmount + PlacedOrder.getShipping_cost();

        Intent intent = new Intent(getActivity(), UserActivity_FragmentsManager.class);
        intent.putExtra(GeneralConstants.FRAGMENT_NAME, TheWebView.class.getName());
        intent.putExtra(GeneralConstants.URL, ServerConstants.INIPAY_URL+Prefs.getString(UserConstants.AccessToken,"")+"&order_amount="+TotalAmount);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    Table_T1 ui_user_registration_zip_search_window_street_st, ui_user_registration_zip_search_window_house_st;
    Spinner_T2 ui_user_registration_zip_search_window_street_city_spinner, ui_user_registration_zip_search_window_street_city_province;
    LinearLayout ui_user_registration_zip_search_window;
    FormField_T2_TextBox ui_user_registration_address_zip;
    FormField_T2_TextBox ui_user_registration_address1;
    LinearLayout ui_user_registration_zip_search_window_bckBtt;
    LinearLayout ui_user_registration_zip_search_window_street_show_house;
    TextView ui_user_registration_zip_search_window_house_show_street;
    ScrollView ui_user_registration_zip_search_window_street, ui_user_registration_zip_search_window_house;

    void PrepareShippingZipcodeSearch(final View v){

        ui_user_registration_zip_search_window = (LinearLayout) v.findViewById(R.id.ui_user_registration_zip_search_window);
        shippingZipSearch = (LinearLayout) v.findViewById(R.id.shippingZipSearch);
        shippingZipSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
                ui_user_registration_zip_search_window.setVisibility(View.VISIBLE);
            }
        });

        ui_user_registration_zip_search_window_bckBtt = (LinearLayout) v.findViewById(R.id.ui_user_registration_zip_search_window_bckBtt);
        ui_user_registration_zip_search_window_bckBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ui_user_registration_zip_search_window.setVisibility(View.GONE);
            }
        });

        ui_user_registration_zip_search_window_street_show_house = (LinearLayout) v.findViewById(R.id.ui_user_registration_zip_search_window_street_show_house);
        ui_user_registration_zip_search_window_house_show_street = (TextView) v.findViewById(R.id.ui_user_registration_zip_search_window_house_show_street);
        ui_user_registration_zip_search_window_street = (ScrollView) v.findViewById(R.id.ui_user_registration_zip_search_window_street);
        ui_user_registration_zip_search_window_house = (ScrollView) v.findViewById(R.id.ui_user_registration_zip_search_window_house);

        ui_user_registration_zip_search_window_street_st = (Table_T1) v.findViewById(R.id.ui_user_registration_zip_search_window_street_st);
        ui_user_registration_zip_search_window_street_st.setColumnNames(new String[]{"zipcode", "address"});
        List<Col2RowDetails> rows = new ArrayList<>();

        Col2RowDetails crd = new Col2RowDetails();
        crd.set_value("12345");
        crd.setCol1_Contents("12345");
        crd.setCol2_Contents("Place 1");
        rows.add(crd);

        Col2RowDetails crd22 = new Col2RowDetails();
        crd22.set_value("67890");
        crd22.setCol1_Contents("67890");
        crd22.setCol2_Contents("Place 2");
        rows.add(crd22);

        Col2RowDetails crd23 = new Col2RowDetails();
        crd23.set_value("69876");
        crd23.setCol1_Contents("69876");
        crd23.setCol2_Contents("Place 3");
        rows.add(crd23);

        Col2RowDetails crd24 = new Col2RowDetails();
        crd24.set_value("91937");
        crd24.setCol1_Contents("91937");
        crd24.setCol2_Contents("Place 4");
        rows.add(crd24);

        Col2RowDetails crd25 = new Col2RowDetails();
        crd25.set_value("50923");
        crd25.setCol1_Contents("50923");
        crd25.setCol2_Contents("Place 5");
        rows.add(crd25);

        ui_user_registration_zip_search_window_street_st.setContainerElements(rows, "8878");
        ui_user_registration_zip_search_window_street_st.setListener(new Table_T1.Success() {
            @Override
            public void done(Col2RowDetails _selected) {
                ui_user_registration_zip_search_window.setVisibility(View.GONE);
                dom_zip_code.setText(_selected.get_value());
                dom_address_line_1.setText(_selected.getCol2_Contents());
                //ui_user_registration_address_zip.setText(_selected.get_value());
                //ui_user_registration_address1.setText(_selected.getCol2_Contents());
                //LetsActiveDoneButton();
            }
        });

        ui_user_registration_zip_search_window_house_st = (Table_T1) v.findViewById(R.id.ui_user_registration_zip_search_window_house_st);
        ui_user_registration_zip_search_window_house_st.setColumnNames(new String[]{"zipcode", "address"});
        List<Col2RowDetails> rows2 = new ArrayList<>();

        Col2RowDetails crd2 = new Col2RowDetails();
        crd2.set_value("88887");
        crd2.setCol1_Contents("88887");
        crd2.setCol2_Contents("Lishfh");
        rows2.add(crd2);

        Col2RowDetails crd3 = new Col2RowDetails();
        crd3.set_value("826843");
        crd3.setCol1_Contents("826843");
        crd3.setCol2_Contents("Kbaak");
        rows2.add(crd3);

        Col2RowDetails crd4 = new Col2RowDetails();
        crd4.set_value("90755");
        crd4.setCol1_Contents("90755");
        crd4.setCol2_Contents("Auorty");
        rows2.add(crd4);

        ui_user_registration_zip_search_window_house_st.setContainerElements(rows2, "8878");
        ui_user_registration_zip_search_window_house_st.setListener(new Table_T1.Success() {
            @Override
            public void done(Col2RowDetails _selected) {
                ui_user_registration_zip_search_window.setVisibility(View.GONE);
                dom_zip_code.setText(_selected.get_value());
                dom_address_line_1.setText(_selected.getCol2_Contents());
                //ui_user_registration_address_zip.setText(_selected.get_value());
                //ui_user_registration_address1.setText(_selected.getCol2_Contents());
                //LetsActiveDoneButton();
            }
        });


        ui_user_registration_zip_search_window_street_city_spinner = (Spinner_T2) v.findViewById(R.id.ui_user_registration_zip_search_window_street_city_spinner);
        ui_user_registration_zip_search_window_street_city_spinner.setText("City");
        List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();
        RadioButtonInfo io1=new RadioButtonInfo();
        io1.setID(0);
        io1.setText("Haka");
        btns.add(io1);

        RadioButtonInfo io2=new RadioButtonInfo();
        io2.setID(1);
        io2.setText("Haka2");
        btns.add(io2);

        RadioButtonInfo io3=new RadioButtonInfo();
        io3.setID(2);
        io3.setText("Japan");
        btns.add(io3);

        RadioButtonInfo io4=new RadioButtonInfo();
        io4.setID(3);
        io4.setText("Shanhai");
        btns.add(io4);

        RadioButtonInfo io5=new RadioButtonInfo();
        io5.setID(4);
        io5.setText("Kung");
        btns.add(io5);

        RadioButtonInfo io6=new RadioButtonInfo();
        io6.setID(5);
        io6.setText("Alishohor");
        btns.add(io6);

        RadioButtonInfo io7=new RadioButtonInfo();
        io7.setID(6);
        io7.setText("Vill City");
        btns.add(io7);

        ui_user_registration_zip_search_window_street_city_spinner.setOptions("City", btns,"Haka2");


        ui_user_registration_zip_search_window_street_city_province = (Spinner_T2) v.findViewById(R.id.ui_user_registration_zip_search_window_street_city_province);
        ui_user_registration_zip_search_window_street_city_province.setText("Province");
        List<RadioButtonInfo> btns2 = new ArrayList<RadioButtonInfo>();
        RadioButtonInfo io12=new RadioButtonInfo();
        io12.setID(0);
        io12.setText("Haksad ada");
        btns2.add(io12);

        RadioButtonInfo io22=new RadioButtonInfo();
        io22.setID(1);
        io22.setText("Haksadasdasda2");
        btns2.add(io22);

        RadioButtonInfo io23=new RadioButtonInfo();
        io23.setID(2);
        io23.setText("Aka Sukaj sdf");
        btns2.add(io23);

        RadioButtonInfo io24=new RadioButtonInfo();
        io24.setID(3);
        io24.setText("Aka Sukaj sdf");
        btns2.add(io24);

        RadioButtonInfo io25=new RadioButtonInfo();
        io25.setID(4);
        io25.setText("Aka Sukaj sdf");
        btns2.add(io25);

        RadioButtonInfo io26=new RadioButtonInfo();
        io26.setID(5);
        io26.setText("Swong Ki");
        btns2.add(io26);

        RadioButtonInfo io27=new RadioButtonInfo();
        io27.setID(6);
        io27.setText("Shanghai");
        btns2.add(io27);
        ui_user_registration_zip_search_window_street_city_province.setOptions("Province", btns2,"Aka Sukaj sdf");



        /*ui_user_registration_address_zip.setListener(new FormField_T2_TextBox.Success() {
            @Override
            public void done() {
                v.findViewById(R.id.ui_user_registration_zip_search_window).setVisibility(View.VISIBLE);
                v.findViewById(R.id.ui_user_registration_zip_search_window_street).setVisibility(View.VISIBLE);
                v.findViewById(R.id.ui_user_registration_zip_search_window_house).setVisibility(View.GONE);
            }

            @Override
            public void Focused() {
                //LetsActiveDoneButton();
            }

            @Override
            public void Unfocused() {
                //LetsActiveDoneButton();
            }
        });*/



        ui_user_registration_zip_search_window_street_show_house.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ui_user_registration_zip_search_window_street.setVisibility(View.GONE);
                ui_user_registration_zip_search_window_house.setVisibility(View.VISIBLE);
            }
        });

        ui_user_registration_zip_search_window_house_show_street.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ui_user_registration_zip_search_window_street.setVisibility(View.VISIBLE);
                ui_user_registration_zip_search_window_house.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void passDataToFragment(String someValue) {

    }

    @Override
    public void reloadThePage() {

    }

    @Override
    public void _DevicebackPressed() {
        if(CurrentSteps == STEP_PAYMENT)
        {
            if(SelectedShippingType == SHIPPING_METHOD__DOMESTIC)
            {
                theOver.setVisibility(View.GONE);
                theDom.setVisibility(View.VISIBLE);
                ifShippingOverseas = false;
            }
            else
            {
                theOver.setVisibility(View.VISIBLE);
                theDom.setVisibility(View.GONE);
                ifShippingOverseas = true;
            }

            payment_layout.setVisibility(View.GONE);
            CurrentSteps = STEP_SHIPPING;
            bottomButton.setText("Continue");
            backBgTitle.setText("Enter Shipping Address");
            shippingInfo.setOverseas(true);
            shippingInfo.setAddress_type("overseas");
        }
        else
        {
            mCallback.ShowTermsConditions();
        }
    }

    @Override
    public boolean ifDetailsPageHiden() {
        return false;
    }

    @Override
    public void showDetails(int id) {

    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            mCallback = (CheckoutActivity_Interface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement CheckoutActivity_Interface");
        }
    }





    boolean DS_over = false;
    boolean DS_dom = false;
    boolean ifShippingOverseas = false;

    TextView showMobileDropDownText;
    LinearLayout showMobileDropDown, setDefaultShipping_over, setDefaultShipping_dom;
    ImageView setDefaultShipping_checkBox_over, setDefaultShipping_checkBox_dom;
    EditText dom_full_name, dom_zip_code, dom_address_line_1, dom_address_line_2, showMobileDropDown_RestNumber;

    RelativeLayout ShowCountryList_over;
    TextView ShowCountryList_over_Txt;
    String selectedCountryShortCode = "";
    EditText over_full_name, over_address_line_1, over_address_line_2, city_over, state_over, zip_code_over, phone_number_over, extension_over;

    Shipping_T1 shippingInfo;

    void PrepareShippingScreen(final View view)
    {
        theOver = (ScrollView) view.findViewById(R.id.theOver);
        theOver.setVisibility(View.GONE);
        //theDom = (FrameLayout) view.findViewById(R.id.theDom);

        theDom = (ScrollView) view.findViewById(R.id.theDom);

        backBtt = (LinearLayout) view.findViewById(R.id.backBtt);
        backBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CurrentSteps == STEP_PAYMENT)
                {
                    if(SelectedShippingType == SHIPPING_METHOD__DOMESTIC)
                    {
                        theOver.setVisibility(View.GONE);
                        theDom.setVisibility(View.VISIBLE);
                        ifShippingOverseas = false;
                    }
                    else
                    {
                        theOver.setVisibility(View.VISIBLE);
                        theDom.setVisibility(View.GONE);
                        ifShippingOverseas = true;
                    }

                    payment_layout.setVisibility(View.GONE);
                    CurrentSteps = STEP_SHIPPING;
                    bottomButton.setText("Continue");
                    backBgTitle.setText("Enter Shipping Address");
                    shippingInfo.setOverseas(true);
                    shippingInfo.setAddress_type("overseas");
                }
                else
                {
                    mCallback.ShowTermsConditions();
                }
            }
        });

        showMobileDropDownText = (TextView) view.findViewById(R.id.showMobileDropDownText);
        showMobileDropDown = (LinearLayout) view.findViewById(R.id.showMobileDropDown);
        showMobileDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowPhoneOperatorSelection();
            }
        });


        setDefaultShipping_over = (LinearLayout) view.findViewById(R.id.setDefaultShipping_over);
        setDefaultShipping_checkBox_over = (ImageView) view.findViewById(R.id.setDefaultShipping_checkBox_over);

        setDefaultShipping_over.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DS_over)
                {
                    DS_over = false;
                    setDefaultShipping_checkBox_over.setImageResource(R.drawable.btn_checkbox_nor);
                }else{
                    DS_over = true;
                    setDefaultShipping_checkBox_over.setImageResource(R.drawable.btn_checkbox_sel);
                    setDefaultShipping_checkBox_dom.setImageResource(R.drawable.btn_checkbox_nor);
                }
            }
        });

        setDefaultShipping_dom = (LinearLayout) view.findViewById(R.id.setDefaultShipping_dom);
        setDefaultShipping_checkBox_dom = (ImageView) view.findViewById(R.id.setDefaultShipping_checkBox_dom);

        setDefaultShipping_dom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(DS_dom)
                {
                    DS_dom = false;
                    setDefaultShipping_checkBox_dom.setImageResource(R.drawable.btn_checkbox_nor);
                }
                else
                {
                    DS_dom = true;
                    setDefaultShipping_checkBox_dom.setImageResource(R.drawable.btn_checkbox_sel);
                    setDefaultShipping_checkBox_over.setImageResource(R.drawable.btn_checkbox_nor);
                }
            }
        });

        dom_full_name = (EditText) view.findViewById(R.id.dom_full_name);
        dom_zip_code = (EditText) view.findViewById(R.id.dom_zip_code);
        dom_address_line_1 = (EditText) view.findViewById(R.id.dom_address_line_1);
        dom_address_line_2 = (EditText) view.findViewById(R.id.dom_address_line_2);
        showMobileDropDown_RestNumber = (EditText) view.findViewById(R.id.showMobileDropDown_RestNumber);


        ShowCountryList_over = (RelativeLayout) view.findViewById(R.id.ShowCountryList_over);
        ShowCountryList_over.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowCountrySelection();
            }
        });
        ShowCountryList_over_Txt = (TextView) view.findViewById(R.id.ShowCountryList_over_Txt);
        over_full_name = (EditText) view.findViewById(R.id.over_full_name);
        over_address_line_1 = (EditText) view.findViewById(R.id.over_address_line_1);
        over_address_line_2 = (EditText) view.findViewById(R.id.over_address_line_2);
        city_over = (EditText) view.findViewById(R.id.city_over);
        state_over = (EditText) view.findViewById(R.id.state_over);
        zip_code_over = (EditText) view.findViewById(R.id.zip_code_over);
        phone_number_over = (EditText) view.findViewById(R.id.phone_number_over);
        extension_over = (EditText) view.findViewById(R.id.extension_over);

        shippingInfo = new Shipping_T1();
        shippingInfo.setOverseas(false);
        shippingInfo.setAddress_type("domestic");

        RetrievePreviousShippingData();


    }

    public void RetrievePreviousShippingData()
    {
        progress = ProgressDialog.show(getContext(), "Retrieving shipping details", "Please wait for a while.", true);

        Cart.GetShippingInfo(new Cart.shippingInfoRetrieved() {
            @Override
            public void Success(Shipping_T1 info) {
                shippingInfo = info;
                ifShippingOverseas = shippingInfo.isOverseas();
                FillUpForm();
                progress.dismiss();
            }

            @Override
            public void NotFound() {
                progress.dismiss();
                FillUpForm();
            }

            @Override
            public void TokenRefreshRequired() {
                Cart.GetShippingInfo(new Cart.shippingInfoRetrieved() {
                    @Override
                    public void Success(Shipping_T1 info) {
                        shippingInfo = info;
                        ifShippingOverseas = shippingInfo.isOverseas();
                        FillUpForm();
                        progress.dismiss();
                    }

                    @Override
                    public void NotFound() {
                        progress.dismiss();
                        FillUpForm();
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        progress.dismiss();
                        FillUpForm();
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                        progress.dismiss();
                        FillUpForm();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                progress.dismiss();
                FillUpForm();
            }
        });
    }

    public void SaveCurrentShippingData()
    {
        progress = ProgressDialog.show(getContext(), "Retrieving shipping details", "Please wait for a while.", true);
        Cart.SaveShippingInfo(shippingInfo, new Cart.response() {
            @Override
            public void Success() {
                progress.dismiss();
                ProcessTheOrder();
            }

            @Override
            public void NotFound() {
                progress.dismiss();
            }

            @Override
            public void TokenRefreshRequired() {
                Cart.SaveShippingInfo(shippingInfo, new Cart.response() {
                    @Override
                    public void Success() {
                        progress.dismiss();
                        ProcessTheOrder();
                    }

                    @Override
                    public void NotFound() {
                        progress.dismiss();
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        progress.dismiss();
                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void FillUpForm()
    {

        if(ifShippingOverseas)
        {
            ShowCountryList_over_Txt.setText(shippingInfo.getCountryName());
            over_full_name.setText(shippingInfo.getFullName());
            over_address_line_1.setText(shippingInfo.getAddressLine1());
            over_address_line_2.setText(shippingInfo.getAddressLine2());
            city_over.setText(shippingInfo.getCity());
            state_over.setText(shippingInfo.getState());
            zip_code_over.setText(shippingInfo.getZipCode());
            phone_number_over.setText(shippingInfo.getPhoneNumber());
            extension_over.setText(shippingInfo.getExtension());

            SelectedShippingType = SHIPPING_METHOD__OVERSEAS;
            theOver.setVisibility(View.VISIBLE);
            theDom.setVisibility(View.GONE);
        }
        else
        {
            dom_full_name.setText(shippingInfo.getFullName());
            dom_zip_code.setText(shippingInfo.getZipCode());
            dom_address_line_1.setText(shippingInfo.getAddressLine1());
            dom_address_line_2.setText(shippingInfo.getAddressLine2());

            if(TextUtils.isEmpty(shippingInfo.getExtension()))
            {
                showMobileDropDownText.setText("015");
            }
            else
            {
                showMobileDropDownText.setText(shippingInfo.getExtension());
            }
            showMobileDropDown_RestNumber.setText(shippingInfo.getPhoneNumber());

            SelectedShippingType = SHIPPING_METHOD__DOMESTIC;
        }




        PlacedOrder.setOrder_type((getArguments().getString(GeneralConstants.TC_TYPE).equals("cart"))?"1":"2");
        PlacedOrder.setShippingDetails(shippingInfo);
    }

    public void ShowPhoneOperatorSelection()
    {
        OptionListing_T1 t = new OptionListing_T1(getActivity().getWindow().getContext());
        List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();
        final String[] theList = new String[]{"015", "011", "016", "017", "018", "019"};
        for (int i = 0; i < theList.length; i++) {
            RadioButtonInfo io1=new RadioButtonInfo();
            io1.setID(i);
            io1.setText(theList[i]);
            btns.add(io1);
        }
        t.setup("Mobile", btns, showMobileDropDownText.getText().toString());
        t.setListener(new OptionListing_T1.Success() {
            @Override
            public void Done(RadioButtonInfo value) {
                showMobileDropDownText.setText(value.getText());
            }
        });
        t.show();
    }


    public void ShowCountrySelection()
    {
        final OptionListing_T1 t = new OptionListing_T1(getActivity().getWindow().getContext());
        List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();

        progress = ProgressDialog.show(getContext(), "Retrieving shipping details", "Please wait for a while.", true);
        Dashboard.RetrieveCountryList(new Dashboard.countryListRetrived() {
            @Override
            public void Success(List<RadioButtonInfo> IL_T1) {
                progress.dismiss();
                t.setup("Country", IL_T1, ShowCountryList_over_Txt.getText().toString());
                t.setListener(new OptionListing_T1.Success() {
                    @Override
                    public void Done(RadioButtonInfo value) {
                        ShowCountryList_over_Txt.setText(value.getText());
                        selectedCountryShortCode = value.getsID();

                        System.out.println("Get Country List: "+value.getText());
                        System.out.println("Get Country Code: "+selectedCountryShortCode);
                    }
                });
                t.show();
            }

            @Override
            public void TokenRefreshRequired() {
                Dashboard.RetrieveCountryList(new Dashboard.countryListRetrived() {
                    @Override
                    public void Success(List<RadioButtonInfo> IL_T1) {
                        progress.dismiss();
                        t.setup("Country", IL_T1, ShowCountryList_over_Txt.getText().toString());
                        t.setListener(new OptionListing_T1.Success() {
                            @Override
                            public void Done(RadioButtonInfo value) {
                                ShowCountryList_over_Txt.setText(value.getText());
                                selectedCountryShortCode = value.getsID();

                                System.out.println("Get Country List: "+value.getText());
                                System.out.println("Get Country Code: "+selectedCountryShortCode);
                            }
                        });
                        t.show();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Log.e(TAG, message);
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Log.e(TAG, message);
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });


       /*

        final String[] theList = new String[]{"Andorra", "United Arab Emirates", "Afghanistan", "Antigua and Barbuda", "Anguilla", "Albania", "Armenia", "Angola", "Antarctica", "Argentina", "American Samoa", "Austria", "Australia", "Aruba", "Åland", "Azerbaijan", "Bosnia and Herzegovina", "Barbados", "Bangladesh", "Belgium", "Burkina Faso", "Bulgaria", "Bahrain", "Burundi", "Benin", "Saint Barthélemy", "Bermuda", "Brunei", "Bolivia", "Bonaire", "Brazil", "Bahamas", "Bhutan", "Bouvet Island", "Botswana", "Belarus", "Belize", "Canada", "Cocos [Keeling] Islands", "Democratic Republic of the Congo", "Central African Republic", "Republic of the Congo", "Switzerland", "Ivory Coast", "Cook Islands", "Chile", "Cameroon", "China", "Colombia", "Costa Rica", "Cuba", "Cape Verde", "Curacao", "Christmas Island", "Cyprus", "Czechia", "Germany", "Djibouti", "Denmark", "Dominica", "Dominican Republic", "Algeria", "Ecuador", "Estonia", "Egypt", "Western Sahara", "Eritrea", "Spain", "Ethiopia", "Finland", "Fiji", "Falkland Islands", "Micronesia", "Faroe Islands", "France", "Gabon", "United Kingdom", "Grenada", "Georgia", "French Guiana", "Guernsey", "Ghana", "Gibraltar", "Greenland", "Gambia", "Guinea", "Guadeloupe", "Equatorial Guinea", "Greece", "South Georgia and the South Sandwich Islands", "Guatemala", "Guam", "Guinea-Bissau", "Guyana", "Hong Kong", "Heard Island and McDonald Islands", "Honduras", "Croatia", "Haiti", "Hungary", "Indonesia", "Ireland", "Israel", "Isle of Man", "India", "British Indian Ocean Territory", "Iraq", "Iran", "Iceland", "Italy", "Jersey", "Jamaica", "Jordan", "Japan", "Kenya", "Kyrgyzstan", "Cambodia", "Kiribati", "Comoros", "Saint Kitts and Nevis", "North Korea", "South Korea", "Kuwait", "Cayman Islands", "Kazakhstan", "Laos", "Lebanon", "Saint Lucia", "Liechtenstein", "Sri Lanka", "Liberia", "Lesotho", "Lithuania", "Luxembourg", "Latvia", "Libya", "Morocco", "Monaco", "Moldova", "Montenegro", "Saint Martin", "Madagascar", "Marshall Islands", "Macedonia", "Mali", "Myanmar [Burma]", "Mongolia", "Macao", "Northern Mariana Islands", "Martinique", "Mauritania", "Montserrat", "Malta", "Mauritius", "Maldives", "Malawi", "Mexico", "Malaysia", "Mozambique", "Namibia", "New Caledonia", "Niger", "Norfolk Island", "Nigeria", "Nicaragua", "Netherlands", "Norway", "Nepal", "Nauru", "Niue", "New Zealand", "Oman", "Panama", "Peru", "French Polynesia", "Papua New Guinea", "Philippines", "Pakistan", "Poland", "Saint Pierre and Miquelon", "Pitcairn Islands", "Puerto Rico", "Palestine", "Portugal", "Palau", "Paraguay", "Qatar", "Réunion", "Romania", "Serbia", "Russia", "Rwanda", "Saudi Arabia", "Solomon Islands", "Seychelles", "Sudan", "Sweden", "Singapore", "Saint Helena", "Slovenia", "Svalbard and Jan Mayen", "Slovakia", "Sierra Leone", "San Marino", "Senegal", "Somalia", "Suriname", "South Sudan", "São Tomé and Príncipe", "El Salvador", "Sint Maarten", "Syria", "Swaziland", "Turks and Caicos Islands", "Chad", "French Southern Territories", "Togo", "Thailand", "Tajikistan", "Tokelau", "East Timor", "Turkmenistan", "Tunisia", "Tonga", "Turkey", "Trinidad and Tobago", "Tuvalu", "Taiwan", "Tanzania", "Ukraine", "Uganda", "U.S. Minor Outlying Islands", "United States", "Uruguay", "Uzbekistan", "Vatican City", "Saint Vincent and the Grenadines", "Venezuela", "British Virgin Islands", "U.S. Virgin Islands", "Vietnam", "Vanuatu", "Wallis and Futuna", "Samoa", "Kosovo", "Yemen", "Mayotte", "South Africa", "Zambia", "Zimbabwe" };

        final String[] theCodeList = new String[]{"AD", "AE", "AF", "AG", "AI", "AL", "AM", "AO", "AQ", "AR", "AS", "AT", "AU", "AW", "AX", "AZ", "BA", "BB", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BL", "BM", "BN", "BO", "BQ", "BR", "BS", "BT", "BV", "BW", "BY", "BZ", "CA", "CC", "CD", "CF", "CG", "CH", "CI", "CK", "CL", "CM", "CN", "CO", "CR", "CU", "CV", "CW", "CX", "CY", "CZ", "DE", "DJ", "DK", "DM", "DO", "DZ", "EC", "EE", "EG", "EH", "ER", "ES", "ET", "FI", "FJ", "FK", "FM", "FO", "FR", "GA", "GB", "GD", "GE", "GF", "GG", "GH", "GI", "GL", "GM", "GN", "GP", "GQ", "GR", "GS", "GT", "GU", "GW", "GY", "HK", "HM", "HN", "HR", "HT", "HU", "ID", "IE", "IL", "IM", "IN", "IO", "IQ", "IR", "IS", "IT", "JE", "JM", "JO", "JP", "KE", "KG", "KH", "KI", "KM", "KN", "KP", "KR", "KW", "KY", "KZ", "LA", "LB", "LC", "LI", "LK", "LR", "LS", "LT", "LU", "LV", "LY", "MA", "MC", "MD", "ME", "MF", "MG", "MH", "MK", "ML", "MM", "MN", "MO", "MP", "MQ", "MR", "MS", "MT", "MU", "MV", "MW", "MX", "MY", "MZ", "NA", "NC", "NE", "NF", "NG", "NI", "NL", "NO", "NP", "NR", "NU", "NZ", "OM", "PA", "PE", "PF", "PG", "PH", "PK", "PL", "PM", "PN", "PR", "PS", "PT", "PW", "PY", "QA", "RE", "RO", "RS", "RU", "RW", "SA", "SB", "SC", "SD", "SE", "SG", "SH", "SI", "SJ", "SK", "SL", "SM", "SN", "SO", "SR", "SS", "ST", "SV", "SX", "SY", "SZ", "TC", "TD", "TF", "TG", "TH", "TJ", "TK", "TL", "TM", "TN", "TO", "TR", "TT", "TV", "TW", "TZ", "UA", "UG", "UM", "US", "UY", "UZ", "VA", "VC", "VE", "VG", "VI", "VN", "VU", "WF", "WS", "XK", "YE", "YT", "ZA", "ZM", "ZW"};

        */
    }

    boolean validateDomesticShippingForm()
    {
        if(TextUtils.isEmpty(dom_full_name.getText().toString()))
        {
            Toast.makeText(getContext(), "Full Name Must Be Filled Up", Toast.LENGTH_LONG).show();
            return false;
        }
        shippingInfo.setFullName(dom_full_name.getText().toString());

        if(TextUtils.isEmpty(dom_zip_code.getText().toString()))
        {
            Toast.makeText(getContext(), "Zip Code must be filled Up", Toast.LENGTH_LONG).show();
            return false;
        }
        shippingInfo.setZipCode(dom_zip_code.getText().toString());

        if(TextUtils.isEmpty(dom_address_line_1.getText().toString()))
        {
            Toast.makeText(getContext(), "Address must be filled Up", Toast.LENGTH_LONG).show();
            return false;
        }
        shippingInfo.setAddressLine1(dom_address_line_1.getText().toString());
        shippingInfo.setAddressLine2(dom_address_line_2.getText().toString());

        if(TextUtils.isEmpty(showMobileDropDownText.getText().toString()))
        {
            Toast.makeText(getContext(), "Select Mobile Operator", Toast.LENGTH_LONG).show();
            return false;
        }
        shippingInfo.setExtension(showMobileDropDownText.getText().toString());

        if(TextUtils.isEmpty(showMobileDropDown_RestNumber.getText().toString()))
        {
            Toast.makeText(getContext(), "Set Mobile Phone Number", Toast.LENGTH_LONG).show();
            return false;
        }
        shippingInfo.setPhoneNumber(showMobileDropDown_RestNumber.getText().toString());
        shippingInfo.setCountryName(null);
        shippingInfo.setCountry_code(null);

        return true;
    }

    boolean validateOverseasShippingForm()
    {
        if(TextUtils.isEmpty(ShowCountryList_over_Txt.getText().toString()) || ShowCountryList_over_Txt.getText().toString().contentEquals("Tap for list"))
        {
            Toast.makeText(getContext(), "Set your Country", Toast.LENGTH_LONG).show();
            return false;
        }
        shippingInfo.setCountryName(ShowCountryList_over_Txt.getText().toString());
        shippingInfo.setCountry_code(selectedCountryShortCode);




        if(TextUtils.isEmpty(over_full_name.getText().toString()))
        {
            Toast.makeText(getContext(), "Full Name Must Be Filled Up", Toast.LENGTH_LONG).show();
            return false;
        }
        shippingInfo.setFullName(over_full_name.getText().toString());

        if(TextUtils.isEmpty(over_address_line_1.getText().toString()))
        {
            Toast.makeText(getContext(), "Address must be filled Up", Toast.LENGTH_LONG).show();
            return false;
        }
        shippingInfo.setAddressLine1(over_address_line_1.getText().toString());
        shippingInfo.setAddressLine2(over_address_line_2.getText().toString());

        if(TextUtils.isEmpty(city_over.getText().toString()))
        {
            Toast.makeText(getContext(), "City must be filled Up", Toast.LENGTH_LONG).show();
            return false;
        }
        shippingInfo.setCity(city_over.getText().toString());
        shippingInfo.setState(state_over.getText().toString());

        //System.out.println("City "+shippingInfo.getCity()+" > "+city_over.getText().toString());
        //System.out.println("State "+shippingInfo.getState()+" > "+state_over.getText().toString());

        shippingInfo.setZipCode(zip_code_over.getText().toString());

        if(TextUtils.isEmpty(phone_number_over.getText().toString()))
        {
            Toast.makeText(getContext(), "Phone number must be filled Up", Toast.LENGTH_LONG).show();
            return false;
        }
        shippingInfo.setPhoneNumber(phone_number_over.getText().toString());
        shippingInfo.setExtension(extension_over.getText().toString());

        return true;
    }


    void ShowPaymentScreen()
    {
        CurrentSteps = STEP_PAYMENT;
        bottomButton.setText("Pay");
        backBgTitle.setText("Payment methods");
        theOver.setVisibility(View.GONE);
        theDom.setVisibility(View.GONE);
        payment_layout.setVisibility(View.VISIBLE);
    }

    public void ProcessTheOrder()
    {
        progress = ProgressDialog.show(getContext(), "Retrieving shipping details", "Please wait for a while.", true);
        Cart.ProcessTheOrder(PlacedOrder, new Cart.orderProcessed() {
            @Override
            public void Success(Order_T1 processedOrder) {
                progress.dismiss();
                ShowPaymentScreen();
            }

            @Override
            public void NotFound(String message) {
                progress.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }

            @Override
            public void TokenRefreshRequired()
            {
                Cart.ProcessTheOrder(PlacedOrder, new Cart.orderProcessed() {
                    @Override
                    public void Success(Order_T1 processedOrder) {
                        progress.dismiss();
                        ShowPaymentScreen();
                    }

                    @Override
                    public void NotFound(String message) {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });

            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }



    void SetupPaymentMethods()
    {
        List<RadioButtonInfo> btns = new ArrayList<>();

        // Methods are ....
        RadioButtonInfo paypal = new RadioButtonInfo();
        paypal.setID(PAYMENT_METHOD__PAYPAL);
        paypal.setText("Paypal");
        btns.add(paypal);

        RadioButtonInfo kona_money = new RadioButtonInfo();
        kona_money.setID(PAYMENT_METHOD__INI_PAY);
        kona_money.setText("INI Pay");
        btns.add(kona_money);

        RadioButtonInfo CreditCard = new RadioButtonInfo();
        CreditCard.setID(PAYMENT_METHOD__CREDIT_CARD);
        CreditCard.setText("Credit Card");
        btns.add(CreditCard);

        int i = 0;
        for (RadioButtonInfo rbi : btns)
        {
            RadioButton_T3 rb_t1=new RadioButton_T3(getContext());
            rb_t1.setup(rbi, i, false);
            rb_t1.setBottomLine();
            rb_t1.setListener(new RadioButton_T3.hitted() {
                @Override
                public void done(RadioButtonInfo id) {
                    //_value=id;
                    SelectedPaymentMethod = id.getID();
                    resetRadioView(id);
                }
            });

            if(rbi.getID()==PAYMENT_METHOD__PAYPAL)
            {
                rb_t1.DoChecked();
            }

            RadioButtonPanel.addView(rb_t1, i);

            i = i + 1;
        }
    }

    private void resetRadioView(RadioButtonInfo id)
    {
        int childs = RadioButtonPanel.getChildCount();
        for(int i=0; i < childs; i++)
        {
            RadioButton_T3 mi = (RadioButton_T3) RadioButtonPanel.getChildAt(i);
            if(!mi.getMyID().contains(id.getID()+"")){
                mi.DoReset();
            }else{
                mi.DoUnReset();
            }
        }
    }

    public void RetriveModifiedCart()
    {
        Type collectionType = new TypeToken<List<CartItem_T1>>(){}.getType();
        ModifiedCart = new Gson().fromJson(Prefs.getString(GeneralConstants.MODIFIED_CART_JSON, "[]"), collectionType);
        PlacedOrder.setItems(ModifiedCart);

        Type AuctionCollectionType = new TypeToken<List<AuctionItem_T1>>(){}.getType();
        ModifiedAuctionCart = new Gson().fromJson(Prefs.getString(GeneralConstants.MODIFIED_AUCTION_CART_JSON, "[]"), AuctionCollectionType);
        PlacedOrder.setAuctionItem(ModifiedAuctionCart);

        System.out.println("Auction Data ----------------> "+Prefs.getString(GeneralConstants.MODIFIED_AUCTION_CART_JSON, "[]"));
    }



    //---------------------------------------   Paypal -------------------------------------------------------------------------------


    public void PaypalSuccess(String payId, JSONObject data)
    {
        try {
            //PlacedOrder.setPay_total(data.getDouble("amount"));
            PlacedOrder.setCurrency(data.getString("currency_code"));
            //PlacedOrder.setShipping_cost(data.getJSONObject("details").getDouble("shipping"));
            PlacedOrder.setTax(data.getJSONObject("details").getDouble("tax"));
            //PlacedOrder.setItem_total(data.getJSONObject("details").getDouble("subtotal"));
            PlacedOrder.setGateway_used("paypal");
            PlacedOrder.setPay_id(payId);

            progress = ProgressDialog.show(getContext(), "Retrieving terms & conditions.", "Please wait for a while.", true);

            Cart.MakeAPayPalOrder(PlacedOrder, new Cart.orderPlaced() {
                @Override
                public void Success(int orderID) {
                    progress.dismiss();
                    mCallback.ShowOrderDetailsPage(""+orderID);
                }

                @Override
                public void NotFound() {

                }

                @Override
                public void TokenRefreshRequired() {
                    Cart.MakeAPayPalOrder(PlacedOrder, new Cart.orderPlaced() {
                        @Override
                        public void Success(int orderID) {
                            progress.dismiss();
                            mCallback.ShowOrderDetailsPage(""+orderID);
                        }

                        @Override
                        public void NotFound() {

                        }

                        @Override
                        public void TokenRefreshRequired() {

                        }

                        @Override
                        public void Error(String message) {
                            progress.dismiss();
                            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                        }
                    });
                }

                @Override
                public void Error(String message) {
                    progress.dismiss();
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void PayPalNotSuccess(JSONObject response, JSONObject data)
    {
        Toast.makeText(getContext(), "Paypal no success", Toast.LENGTH_LONG).show();
    }

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AUOGV_WZ8NItwtbvHm4ISYE1g6V9RTG_Ob3XR7A9q50fc0_C0xw_FUE1wTvHulR5iFTKVsog79Fej-9Q";

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);

    private void OpenPayPalActivity()
    {
        PayPalPayment thingToBuy = getStuffToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(getActivity(), PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        getActivity().startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getStuffToBuy(String paymentIntent) {
        //--- include an item list, payment amount details
        /*PayPalItem[] items =
                {
                        new PayPalItem("sample item #1", 2, new BigDecimal("87.50"), "USD",
                                "sku-12345678"),
                        new PayPalItem("free sample item #2", 1, new BigDecimal("0.00"),
                                "USD", "sku-zero-price"),
                        new PayPalItem("sample item #3 with a longer name", 6, new BigDecimal("37.99"),
                                "USD", "sku-33333")
                };*/

        PayPalItem[] items;

        if(PlacedOrder.getOrder_type().equals("1"))
        {
            items = new PayPalItem[ModifiedCart.size()];

            for(int i=0; i < ModifiedCart.size(); i++)
            {
                items[i] = new PayPalItem(ModifiedCart.get(i).getTitle_en(), 1, new BigDecimal(ModifiedCart.get(i).getPrice()), ModifiedCart.get(i).getCurrency(), ModifiedCart.get(i).getCatrt_id());
            }
        }
        else
        {
            items = new PayPalItem[ModifiedAuctionCart.size()];

            for(int i=0; i < ModifiedAuctionCart.size(); i++)
            {
                items[i] = new PayPalItem(ModifiedAuctionCart.get(i).getTitle_en(), 1, new BigDecimal(ModifiedAuctionCart.get(i).getPrice()), ModifiedAuctionCart.get(i).getCurrency(), ModifiedAuctionCart.get(i).getId());

                System.out.println(" Currency : >"+ModifiedAuctionCart.get(i).getCurrency());
            }
        }

        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal(PlacedOrder.getShipping_cost());
        BigDecimal tax = new BigDecimal(TAX);
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment;

        if(PlacedOrder.getOrder_type().equals("1"))
        {
            payment = new PayPalPayment(amount, getArguments().getString(GeneralConstants.SELECTED_CURRENCY), "Cart Items", paymentIntent);
        }
        else
        {
            payment = new PayPalPayment(amount, getArguments().getString(GeneralConstants.SELECTED_CURRENCY), "Auction Item/s", paymentIntent);

            System.out.println(" Currency2 : >"+getArguments().getString(GeneralConstants.SELECTED_CURRENCY));
        }

        payment.items(items).paymentDetails(paymentDetails);

        //--- set other optional fields like invoice_number, custom field, and soft_descriptor
        payment.custom("This is text that will be associated with the payment that the app can use.");

        return payment;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));

                        if(confirm.toJSONObject().getJSONObject("response").getString("state").equals("approved"))
                        {
                            PaypalSuccess(
                                    confirm.toJSONObject().getJSONObject("response").getString("id"),
                                    confirm.getPayment().toJSONObject()
                            );
                        }
                        else
                        {
                            PayPalNotSuccess(
                                    confirm.toJSONObject().getJSONObject("response"),
                                    confirm.getPayment().toJSONObject()
                            );
                        }

                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        //displayResultText("PaymentConfirmation info received from PayPal");


                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }else{
                    System.out.println("Null Found");
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }
}
