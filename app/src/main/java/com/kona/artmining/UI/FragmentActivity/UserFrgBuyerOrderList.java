package com.kona.artmining.UI.FragmentActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.kona.artmining.Adapter.UserFrgBuyerOrderListAdapter_T1;
import com.kona.artmining.Helper.Orders;
import com.kona.artmining.Interface.UserActivity_FrgMng_Interface;
import com.kona.artmining.Interface.UserFrgMngCommunicator;
import com.kona.artmining.Model.Order_T1;
import com.kona.artmining.R;
import com.kona.artmining.UI.FragmentController.UserActivity_FragmentsManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class UserFrgBuyerOrderList extends Fragment implements UserFrgMngCommunicator {

    Button showOrderList;

    UserActivity_FrgMng_Interface mCallback;

    ProgressDialog progress;

    ObservableRecyclerView recyclerView;
    UserFrgBuyerOrderListAdapter_T1 rvListAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    List<Order_T1> OrderContents = new ArrayList<Order_T1>();

    Activity activity;
    RelativeLayout recycler_viewPlaceHolder;



    LinearLayout orderDetailsPage, detailsBackBtt, detailsPage_TopSuccessfulMessage, orderDetailsPage_paymentItemShippingOnCash_holder;
    TextView detailsPage_TopSuccessfulMessage_title, detailsPage_TopSuccessfulMessage_Subtitle, detailsPage_TopSuccessfulMessage_Subtitle2, orderDetailsPage_date, orderDetailsPage_orderNumber, orderDetailsPage_featuredTitle, orderDetailsPage_ArtistName, orderDetailsPage_amount, orderDetailsPage_paymentItemShippingOnCash;

    TextView orderDetailsPage_shippingFullName, orderDetailsPage_shippingPhoneNumber, orderDetailsPage_shippingAddress, orderDetailsPage_paymentMethod, orderDetailsPage_paymentItem, orderDetailsPage_paymentItemTotals, orderDetailsPage_paymentItemShipping, orderDetailsPage_paymentItemTotal;

    ImageView orderDetailsPage_featuredimage, orderDetailsPage_ifAuctioned;

    public UserFrgBuyerOrderList() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.user_frg_buyer_order_list, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (UserActivity_FrgMng_Interface) activity;
            UserActivity_FragmentsManager DA = (UserActivity_FragmentsManager) getActivity();
            DA.frg_listener = this;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement UserActivity_FrgMng_Interface");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStage(view);
        RefreshPage();
    }

    void prepareStage(View view) {
        recyclerView = (ObservableRecyclerView) view.findViewById(R.id.recycler_view);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        /*layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(position == 0 || position == 1)
                {
                    return 2;
                }else{
                    return 1;
                }
            }
        });*/
        recyclerView.setLayoutManager(layoutManager);

        recycler_viewPlaceHolder = (RelativeLayout) view.findViewById(R.id.recycler_viewPlaceHolder);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        rvListAdapter = new UserFrgBuyerOrderListAdapter_T1(OrderContents, new UserFrgBuyerOrderListAdapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(Order_T1 item, int position) {
                ShowDetailsPage(item);
            }

            @Override
            public void updateStatus(Order_T1 item, int orderStatus) {
                _updateStatus(
                        (item.getOrder_id()+""),
                        (orderStatus+"")
                );
            }
        });


        recyclerView.setAdapter(rvListAdapter);

        swipeRefreshLayout.setProgressViewOffset(false, 100, 200);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                RefreshPage();
            }
        });

        view.findViewById(R.id._backBtt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        orderDetailsPage = (LinearLayout) view.findViewById(R.id.orderDetailsPage);
        orderDetailsPage.setVisibility(View.GONE);

        detailsBackBtt = (LinearLayout) view.findViewById(R.id.detailsBackBtt);
        detailsBackBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderDetailsPage.setVisibility(View.GONE);
            }
        });

        orderDetailsPage = (LinearLayout) view.findViewById(R.id.orderDetailsPage);
        orderDetailsPage.setVisibility(View.GONE);

        detailsPage_TopSuccessfulMessage = (LinearLayout) view.findViewById(R.id.detailsPage_TopSuccessfulMessage);
        detailsPage_TopSuccessfulMessage.setVisibility(View.GONE);

        orderDetailsPage_paymentItemShippingOnCash_holder = (LinearLayout) view.findViewById(R.id.orderDetailsPage_paymentItemShippingOnCash_holder);
        orderDetailsPage_paymentItemShippingOnCash_holder.setVisibility(View.GONE);

        orderDetailsPage_paymentItemShippingOnCash = (TextView) view.findViewById(R.id.orderDetailsPage_paymentItemShippingOnCash);

        detailsPage_TopSuccessfulMessage_title = (TextView) view.findViewById(R.id.detailsPage_TopSuccessfulMessage_title);
        detailsPage_TopSuccessfulMessage_Subtitle = (TextView) view.findViewById(R.id.detailsPage_TopSuccessfulMessage_Subtitle);
        detailsPage_TopSuccessfulMessage_Subtitle2 = (TextView) view.findViewById(R.id.detailsPage_TopSuccessfulMessage_Subtitle2);

        orderDetailsPage_date = (TextView) view.findViewById(R.id.orderDetailsPage_date);
        orderDetailsPage_orderNumber = (TextView) view.findViewById(R.id.orderDetailsPage_orderNumber);
        orderDetailsPage_featuredimage = (ImageView) view.findViewById(R.id.orderDetailsPage_featuredimage);
        orderDetailsPage_ifAuctioned = (ImageView) view.findViewById(R.id.orderDetailsPage_ifAuctioned);
        orderDetailsPage_ArtistName = (TextView) view.findViewById(R.id.orderDetailsPage_ArtistName);
        orderDetailsPage_featuredTitle = (TextView) view.findViewById(R.id.orderDetailsPage_featuredTitle);
        orderDetailsPage_amount = (TextView) view.findViewById(R.id.orderDetailsPage_amount);


        orderDetailsPage_shippingFullName = (TextView) view.findViewById(R.id.orderDetailsPage_shippingFullName);
        orderDetailsPage_shippingPhoneNumber = (TextView) view.findViewById(R.id.orderDetailsPage_shippingPhoneNumber);
        orderDetailsPage_shippingAddress = (TextView) view.findViewById(R.id.orderDetailsPage_shippingAddress);
        orderDetailsPage_paymentMethod = (TextView) view.findViewById(R.id.orderDetailsPage_paymentMethod);
        orderDetailsPage_paymentItem = (TextView) view.findViewById(R.id.orderDetailsPage_paymentItem);
        orderDetailsPage_paymentItemTotals = (TextView) view.findViewById(R.id.orderDetailsPage_paymentItemTotals);
        orderDetailsPage_paymentItemShipping = (TextView) view.findViewById(R.id.orderDetailsPage_paymentItemShipping);
        orderDetailsPage_paymentItemTotal = (TextView) view.findViewById(R.id.orderDetailsPage_paymentItemTotal);

        showOrderList = (Button) view.findViewById(R.id.showOrderList);
        showOrderList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderDetailsPage.setVisibility(View.GONE);
            }
        });
    }

    void ShowDetailsPage(Order_T1 orderDetails)
    {
        orderDetailsPage.setVisibility(View.VISIBLE);

        orderDetailsPage_date.setText(orderDetails.getOrderDate());
        orderDetailsPage_orderNumber.setText("(Order Num : "+orderDetails.getTransaction_id()+")");

        Picasso.with(getContext())
                .load(orderDetails.getFeaturedItem().getMain_image_thumb())
                .placeholder(R.drawable.transparent)
                .error(R.drawable.transparent)
                .fit()
                .into(orderDetailsPage_featuredimage);

        orderDetailsPage_featuredTitle.setText(orderDetails.getFeaturedItem().getTitle_en());
        orderDetailsPage_ArtistName.setText(orderDetails.getFeaturedItem().getArtistName_en());
        orderDetailsPage_amount.setText(orderDetails.getFeaturedItem().getPrice()+" "+orderDetails.getFeaturedItem().getCurrency());

        System.out.println(orderDetails.getShippingDetails().getFullName());

        orderDetailsPage_shippingFullName.setText(orderDetails.getShippingDetails().getFullName());
        orderDetailsPage_shippingPhoneNumber.setText(orderDetails.getShippingDetails().getExtension()+"-"+orderDetails.getShippingDetails().getPhoneNumber());
        orderDetailsPage_shippingAddress.setText(orderDetails.getShippingDetails().getAddressLine1() + " \n " +orderDetails.getShippingDetails().getAddressLine2());
        orderDetailsPage_paymentMethod.setText(orderDetails.getGateway_used());

        orderDetailsPage_paymentItem.setText("Items ("+orderDetails.getItems().size()+")");
        orderDetailsPage_paymentItemTotals.setText(orderDetails.getCurrency()+" "+orderDetails.getItem_total());
        orderDetailsPage_paymentItemShipping.setText(orderDetails.getCurrency()+" "+orderDetails.getShipping_cost());
        orderDetailsPage_paymentItemTotal.setText(orderDetails.getCurrency()+" "+orderDetails.getPay_total());

        orderDetailsPage_paymentItemShippingOnCash.setText(orderDetails.getCurrency()+" "+orderDetails.getFeaturedItem().getShip_cost_cash());

        if(orderDetails.getFeaturedItem().getShip_cost_cash() !=null)
        {
            if(Double.valueOf(orderDetails.getFeaturedItem().getShip_cost_cash())>0)
            {
                orderDetailsPage_paymentItemShippingOnCash_holder.setVisibility(View.VISIBLE);
            }
            else
            {
                orderDetailsPage_paymentItemShippingOnCash_holder.setVisibility(View.GONE);
            }
        }
        else
        {
            orderDetailsPage_paymentItemShippingOnCash_holder.setVisibility(View.GONE);
        }

        if(orderDetails.getOrder_type().equals("2"))
        {
            orderDetailsPage_ifAuctioned.setVisibility(View.VISIBLE);
        }
        else
        {
            orderDetailsPage_ifAuctioned.setVisibility(View.GONE);
        }
    }

    void RefreshPage() {
        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

        Orders.RetrieveAllBuyerOrders(new Orders.RetrieveAllOrders_response() {
            @Override
            public void Success(List<Order_T1> orders) {
                progress.dismiss();
                clearInboxRVData();
                OrderContents.addAll(orders);
                rvListAdapter.notifyDataSetChanged();
                if(swipeRefreshLayout != null)
                {
                    swipeRefreshLayout.setRefreshing(false);
                }
                if(OrderContents.size()>0)
                {
                    recycler_viewPlaceHolder.setVisibility(View.GONE);
                }
            }

            @Override
            public void TokenRefreshRequired() {
                Orders.RetrieveAllBuyerOrders(new Orders.RetrieveAllOrders_response() {
                    @Override
                    public void Success(List<Order_T1> orders) {
                        progress.dismiss();
                        clearInboxRVData();
                        OrderContents.addAll(orders);
                        rvListAdapter.notifyDataSetChanged();
                        if(swipeRefreshLayout != null)
                        {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                        if(OrderContents.size()>0)
                        {
                            recycler_viewPlaceHolder.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        if(swipeRefreshLayout != null)
                        {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                if(swipeRefreshLayout != null)
                {
                    swipeRefreshLayout.setRefreshing(false);
                }
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void _updateStatus(final String order_id, final String status_id)
    {
        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

        Orders.UpdateOrderStatus(order_id, status_id, new Orders._response() {
            @Override
            public void Success(String order_id) {
                progress.dismiss();
                RefreshPage();
            }

            @Override
            public void TokenRefreshRequired() {
                Orders.UpdateOrderStatus(order_id, status_id, new Orders._response() {
                    @Override
                    public void Success(String order_id) {
                        progress.dismiss();
                        RefreshPage();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void clearInboxRVData()
    {
        int size = OrderContents.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                OrderContents.remove(0);
            }
            rvListAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void _DevicebackPressed() {
        orderDetailsPage.setVisibility(View.GONE);
    }

    @Override
    public boolean ifDetailsPageHiden() {
        if(orderDetailsPage.getVisibility() == View.GONE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}