package com.kona.artmining.UI.FragmentActivity;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.kona.artmining.Adapter.ImageListingAdapter_T2;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Helper.Art;
import com.kona.artmining.Interface.UserActivity_FrgMng_Interface;
import com.kona.artmining.Interface.UserFrgMngCommunicator;
import com.kona.artmining.Model.Art_T1;
import com.kona.artmining.R;
import com.kona.artmining.UI.Activity.ArtDetailsActivity;
import com.kona.artmining.UI.Activity.CreateEditAnArt_Activity;
import com.kona.artmining.UI.Activity.UserActivity;
import com.kona.artmining.UI.FragmentController.UserActivity_FragmentsManager;

import java.util.ArrayList;
import java.util.List;


public class UserFrgMyArtworks extends Fragment implements UserFrgMngCommunicator {

    UserActivity_FrgMng_Interface mCallback;
    ObservableRecyclerView recyclerView;
    ImageListingAdapter_T2 rvListAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    List<Art_T1> Content = new ArrayList<Art_T1>();
    ProgressDialog progress;
    LinearLayout uploadArtBtt;
    ImageView delete_selected;

    static final int ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE = 400;

    public UserFrgMyArtworks() {}

    public static UserFrgMyArtworks newInstance(int param1) {
        UserFrgMyArtworks fragment = new UserFrgMyArtworks();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            mCallback = (UserActivity_FrgMng_Interface) activity;
            UserActivity_FragmentsManager DA = (UserActivity_FragmentsManager) getActivity();
            DA.frg_listener = this;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement UserActivity_FrgMng_Interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.user_frg_my_artworks, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStage(view);
    }

    void prepareStage(View view)
    {
        delete_selected = (ImageView) view.findViewById(R.id.delete_selected);
        delete_selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String removeItems = "";
                int ll = 0;

                for (int i = 0; i < Content.size(); i++) {
                    if(Content.get(i).isSelected())
                    {
                        if(ll==0)
                        {
                            removeItems = removeItems.concat(""+Content.get(i).getArt_id());
                        }else{
                            removeItems = removeItems.concat(","+Content.get(i).getArt_id());
                        }
                        System.out.println("We have to delete it: "+Content.get(i).getArt_id());
                        ll = ll + 1;
                    }

                }

                if(!TextUtils.isEmpty(removeItems))
                {
                    RemoveArtWorks(removeItems);
                }else{
                    Toast.makeText(getContext(), "Please select an item.", Toast.LENGTH_LONG).show();
                }
            }
        });

        view.findViewById(R.id.backBtt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                getActivity().finish();
            }
        });

        view.findViewById(R.id.uploadArtBtt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getActivity(), CreateEditAnArt_Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivityForResult(intent, 555);
            }
        });

        recyclerView = (ObservableRecyclerView) view.findViewById(R.id.recycler_view);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        /*layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(position == 0 || position == 1)
                {
                    return 2;
                }else{
                    return 1;
                }
            }
        });*/
        recyclerView.setLayoutManager(layoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        rvListAdapter = new ImageListingAdapter_T2(Content, new ImageListingAdapter_T2.OnItemClickListener() {
            @Override
            public void onItemClick(Art_T1 item)
            {
                /*
                Intent intent = new Intent(getActivity(), CreateEditAnArt_Activity.class);
                intent.putExtra(GeneralConstants.ID, item.getArt_id());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                */
                /*Intent intent = new Intent(getActivity(), ArtDetailsActivity.class);
                intent.putExtra(GeneralConstants.ID, item.getArt_id());
                getActivity().startActivityForResult(intent, ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE);*/
                mCallback.showArtDetailsPage(item.getArt_id());
            }

            @Override
            public void onItemSelected(Art_T1 item, int position, boolean isSelected)
            {

                for (int i = 0; i < Content.size(); i++) {
                    Content.get(i).setSelected(false);
                }

                Content.get(position).setSelected(isSelected);
                Update_DeleteSelectedButtonStatus();
                rvListAdapter.notifyDataSetChanged();
            }
        });
        recyclerView.setAdapter(rvListAdapter);

        swipeRefreshLayout.setProgressViewOffset(false,100,200);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(){
                RefreshPage();
            }
        });

        RefreshPage();

        /*

         */
    }

    void Update_DeleteSelectedButtonStatus()
    {
        boolean showBtt = false;

        for (int i = 0; i < Content.size(); i++) {
            if(Content.get(i).isSelected())
            {
                showBtt = true;
                break;
            }
        }

        /*if(showBtt){
            deleteSelected.setVisibility(View.VISIBLE);
        }else{
            deleteSelected.setVisibility(View.GONE);
        }*/
    }

    void RefreshPage()
    {
        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

        Art.GetArtistArtsWorks(new Art.RetrieveArts() {
            @Override
            public void Success(List<Art_T1> items) {
                progress.dismiss();
                clearRVData();
                Content.addAll(items);

                if(Content.size()>0)
                {
                    getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.GONE);
                }else{
                    getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.VISIBLE);
                }

                rvListAdapter.notifyDataSetChanged();

                if(swipeRefreshLayout != null)
                {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }


            @Override
            public void NotFound() {
                progress.dismiss();
            }

            @Override
            public void TokenRefreshRequired() {
                Art.GetArtistArtsWorks(new Art.RetrieveArts() {
                    @Override
                    public void Success(List<Art_T1> items) {
                        progress.dismiss();
                        clearRVData();
                        Content.addAll(items);

                        if(Content.size()>0)
                        {
                            getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.GONE);
                        }else{
                            getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.VISIBLE);
                        }

                        rvListAdapter.notifyDataSetChanged();

                        if(swipeRefreshLayout != null)
                        {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void NotFound() {
                        progress.dismiss();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void RemoveArtWorks(final String artIds)
    {
        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Updating details data.", "Please wait for a while.", true);

        Art.RemoveArts(artIds, new Art.response() {
            @Override
            public void Success() {
                progress.dismiss();
                RefreshPage();
            }

            @Override
            public void NotFound() {
                progress.dismiss();
            }

            @Override
            public void TokenRefreshRequired() {
                Art.RemoveArts(artIds, new Art.response() {
                    @Override
                    public void Success() {
                        progress.dismiss();
                        RefreshPage();
                    }

                    @Override
                    public void NotFound() {
                        progress.dismiss();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void clearRVData()
    {
        int size = Content.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Content.remove(0);
            }
            rvListAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void _DevicebackPressed() {

    }

    @Override
    public boolean ifDetailsPageHiden() {
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent_data)
    {
        System.out.println(requestCode + " ----------> " + resultCode);

        if(requestCode == 555)
        {
            if(resultCode == -1)
            {
                RefreshPage();
            }
        }
        if(requestCode == ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                Bundle data_ = intent_data.getExtras();
                mCallback.showArtDetailsPage(data_.getInt(GeneralConstants.ID));
            }
        }

    }
}
