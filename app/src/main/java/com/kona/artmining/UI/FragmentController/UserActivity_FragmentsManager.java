package com.kona.artmining.UI.FragmentController;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Interface.UserActivity_FrgMng_Interface;
import com.kona.artmining.Interface.UserFrgMngCommunicator;
import com.kona.artmining.R;
import com.kona.artmining.UI.Activity.CheckoutActivity;
import com.kona.artmining.UI.FragmentActivity.TheWebView;
import com.kona.artmining.UI.FragmentActivity.UserFrgAuctionList;
import com.kona.artmining.UI.FragmentActivity.UserFrgBuyerOrderList;
import com.kona.artmining.UI.FragmentActivity.UserFrgEditProfile;
import com.kona.artmining.UI.FragmentActivity.UserFrgMyArtReviews;
import com.kona.artmining.UI.FragmentActivity.UserFrgMyArtworks;
import com.kona.artmining.UI.FragmentActivity.UserFrgSellerOrderList;
import com.kona.artmining.UI.FragmentActivity.UserFrgShoppingCart;

public class UserActivity_FragmentsManager extends AppCompatActivity implements UserActivity_FrgMng_Interface {

    public static final String TAG = UserActivity_FragmentsManager.class.getSimpleName();

    private String fragmentName;
    private String URL;

    boolean ShowWebView = false;

    public static final int ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE = 400;

    public UserFrgMngCommunicator frg_listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_fragment_manager);
        initMessageFromBundle();
        addFragmentToThisLayout();
    }

    private void addFragmentToThisLayout()
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if(ShowWebView)
        {
            Fragment f = createScreen(fragmentName);
            Bundle args = new Bundle();
            args.putString(GeneralConstants.URL, URL);
            f.setArguments(args);
            fragmentTransaction.replace(R.id.content, f);
        }
        else
        {
            fragmentTransaction.replace(R.id.content, createScreen(fragmentName));
        }
        fragmentTransaction.commit();
    }

    private void initMessageFromBundle() {
        Bundle data = getIntent().getExtras();
        if (data != null) {
            fragmentName = data.getString(GeneralConstants.FRAGMENT_NAME);
            if(data.containsKey(GeneralConstants.URL))
            {
                URL = data.getString(GeneralConstants.URL);
                ShowWebView = true;
            }
            Log.i(TAG, "fragmentName: " + fragmentName);
        }
    }

    public Fragment createScreen(String fragmentName) {

        Fragment fragment = null;

        if (fragmentName.equals(UserFrgEditProfile.class.getName())) {
            fragment = new UserFrgEditProfile();
        } else if (fragmentName.equals(UserFrgShoppingCart.class.getName())) {
            fragment = new UserFrgShoppingCart();
        }else if(fragmentName.equals(UserFrgAuctionList.class.getName())) {
            fragment = new UserFrgAuctionList();
        }else if(fragmentName.equals(UserFrgBuyerOrderList.class.getName())) {
            fragment = new UserFrgBuyerOrderList();
        }else if(fragmentName.equals(UserFrgSellerOrderList.class.getName())) {
            fragment = new UserFrgSellerOrderList();
        }else if(fragmentName.equals(UserFrgMyArtworks.class.getName())) {
            fragment = new UserFrgMyArtworks();
        }else if(fragmentName.equals(UserFrgMyArtReviews.class.getName())) {
            fragment = new UserFrgMyArtReviews();
        }else if(fragmentName.equals(TheWebView.class.getName()))
        {
            fragment = new TheWebView();
        }

        return fragment;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void ShowPaymentProcessing(String tc_type, String curr_type) {
        Intent intent = new Intent(this, CheckoutActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(GeneralConstants.TC_TYPE, tc_type);
        intent.putExtra(GeneralConstants.SELECTED_CURRENCY, curr_type);
        System.out.println("----------"+curr_type);
        startActivity(intent);
    }

    @Override
    public void finishActivity()
    {
        finish();
    }

    @Override
    public void onBackPressed()
    {
        if(!frg_listener.ifDetailsPageHiden())
        {
            frg_listener._DevicebackPressed();
        }
        else
        {
            finish();
        }
    }

    @Override
    public void doLogout() {

        Intent resultIntent = new Intent();
        setResult(9, resultIntent);
        finish();
    }

    @Override
    public void updateProfileDetails()
    {
        Intent resultIntent = new Intent();
        setResult(8, resultIntent);
        finish();
    }

    @Override
    public void showArtDetailsPage(int artID) {
        setResult(ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE,new Intent().putExtra(GeneralConstants.ID, artID));
        finish();
    }
}
