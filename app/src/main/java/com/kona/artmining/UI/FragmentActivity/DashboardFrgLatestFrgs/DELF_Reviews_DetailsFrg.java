package com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.DashboardFrgLatest;
import com.kona.artmining.Helper.User;
import com.kona.artmining.Helper.UserAuthentication;
import com.kona.artmining.Interface.ActivityCommunicator;
import com.kona.artmining.Model.DashboardFrgLatest_Reviews_Model_T1;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.Model.User_T1;
import com.kona.artmining.R;
import com.kona.artmining.UI.FragmentActivity.UserFrgEditProfile;
import com.kona.artmining.custom_components.CommentRow_T1;
import com.kona.artmining.custom_popup_box.ConfirmationBox;
import com.kona.artmining.custom_popup_box.OptionListing_T1;
import com.kona.artmining.model.Comment_T1;
import com.kona.artmining.model.RadioButtonInfo;
import com.kona.artmining.utils.PicassoRoundTransform;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class DELF_Reviews_DetailsFrg extends Fragment
{
    int len = 300;

    FrameLayout see_more_less_button_review_container;

    Button exhibitions_layout_close_button;

    LinearLayout PreviewPage, EditPage, SelectUserLayout, PreviewPageBackBg, EditPage_backBtt, PreviewPage_detailReviewHolder, Pg_seeLessBtt, Pg_seeMoreBtt, PreviewPage_EditDelete_ButtonHolder, PreviewPage_CommentHolder, PreviewPage_ExistingCommentContainer, PreviewPage_doComment, showCommentScreen;
    TextView PreviewPage_title, PreviewPage_no_OfLike, PreviewPage_detailReview, PreviewPage_NoOfComments;
    ImageView PreviewPage_img, PreviewPage_DoALike, PreviewPage_Share;
    Button PreviewPage_editBtt, PreviewPage_deleteBtt;

    ActivityCommunicator ac;

    ImagePicker imagePicker;
    LinearLayout EditPage_SelectImage;
    ImageView EditPage_SelectImageContainer;

    EditText EditPage_Title, EditPage_Details, PreviewPage_CommentText;
    Button EditPage_Done;

    File imageTobeUpload;

    TextView SelectSupporter;
    Spinner SelectSupporterList;

    boolean NeedToWriteNewReview = false;
    boolean ifAdmin = false;
    boolean allowedToEditDelete = false;

    List<User_T1> SelectedUsers;

    ProgressDialog progress;

    int talkDetailsPg_aboutHolder_H;

    CircularImageView ProfileIcon;

    public DELF_Reviews_DetailsFrg() {}

    public static DELF_Reviews_DetailsFrg newInstance(int param1)
    {
        DELF_Reviews_DetailsFrg fragment = new DELF_Reviews_DetailsFrg();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ui_dashboard_frgmnts_latest_frg_reviews_details, container, false);
        PrepareStage(view);
        return view;
    }

    void CheckIfCurrentUserIsAdmin()
    {
        UserAuthentication.CheckIfCurrentUserIsAdmin(new UserAuthentication.checkIfAdmin() {
            @Override
            public void Yes() {
                ifAdmin = true;
                allowedToEditDelete = true;
                SelectUserLayout.setVisibility(View.VISIBLE);
                LoadUserList("1", true);
            }

            @Override
            public void No() {
                ifAdmin = false;
                SelectUserLayout.setVisibility(View.GONE);
            }

            @Override
            public void TokenRefresh() {
                UserAuthentication.CheckIfCurrentUserIsAdmin(new UserAuthentication.checkIfAdmin() {
                    @Override
                    public void Yes() {
                        ifAdmin = true;
                        allowedToEditDelete = true;
                        SelectUserLayout.setVisibility(View.VISIBLE);
                        LoadUserList("1", true);
                    }

                    @Override
                    public void No() {
                        ifAdmin = false;
                        SelectUserLayout.setVisibility(View.GONE);
                    }

                    @Override
                    public void TokenRefresh() {
                        ifAdmin = false;
                        SelectUserLayout.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    void LoadUserList(String UserType, final boolean showLoader)
    {
        User.RetrieveUserList(null, UserType, null, new User.userListRetrieve() {
            @Override
            public void Success(List<User_T1> users) {
                SelectedUsers = null;
                SelectedUsers = users;
                PopulateLoadUserListSpinner();
                HideProgressDialogue();
            }

            @Override
            public void TokenRefreshRequired()
            {

            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                HideProgressDialogue();
            }
        });
    }

    void PopulateLoadUserListSpinner()
    {
        List<String> spinnerArray =  new ArrayList<String>();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, spinnerArray);

        for (User_T1 Ut1 : SelectedUsers)
        {
            spinnerArray.add(Ut1.getName_en());
        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        SelectSupporterList.setAdapter(adapter);
    }

    void PrepareStage(View view)
    {

        see_more_less_button_review_container = (FrameLayout) view.findViewById(R.id.see_more_less_button_review_container);

        exhibitions_layout_close_button = (Button) view.findViewById(R.id.exhibitions_layout_close_button);
        exhibitions_layout_close_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ac.CloseMe();
            }
        });

        PreviewPage_EditDelete_ButtonHolder = (LinearLayout) view.findViewById(R.id.PreviewPage_EditDelete_ButtonHolder);
        PreviewPage_EditDelete_ButtonHolder.setVisibility(View.GONE);
        PreviewPage_CommentHolder = (LinearLayout) view.findViewById(R.id.PreviewPage_CommentHolder);
        PreviewPage_CommentHolder.setVisibility(View.GONE);
        PreviewPage_ExistingCommentContainer = (LinearLayout) view.findViewById(R.id.PreviewPage_ExistingCommentContainer);
        PreviewPage_NoOfComments = (TextView) view.findViewById(R.id.PreviewPage_NoOfComments);

        SelectUserLayout = (LinearLayout) view.findViewById(R.id.SelectUser);
        SelectUserLayout.setVisibility(View.GONE);

        showCommentScreen = (LinearLayout) view.findViewById(R.id.showCommentScreen);

        PreviewPage_detailReviewHolder = (LinearLayout) view.findViewById(R.id.PreviewPage_detailReviewHolder);

        PreviewPage = (LinearLayout) view.findViewById(R.id.PreviewPage);
        PreviewPage.setVisibility(View.GONE);
        EditPage = (LinearLayout) view.findViewById(R.id.EditPage);
        EditPage.setVisibility(View.GONE);
        PreviewPageBackBg = (LinearLayout) view.findViewById(R.id.PreviewPageBackBg);
        PreviewPageBackBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ac.CloseMe();
            }
        });

        PreviewPage_title = (TextView) view.findViewById(R.id.PreviewPage_title);
        PreviewPage_no_OfLike = (TextView) view.findViewById(R.id.PreviewPage_no_OfLike);
        PreviewPage_detailReview = (TextView) view.findViewById(R.id.PreviewPage_detailReview);

        PreviewPage_img = (ImageView) view.findViewById(R.id.PreviewPage_img);

        PreviewPage_editBtt = (Button) view.findViewById(R.id.PreviewPage_editBtt);
        PreviewPage_editBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                EditPage.setVisibility(View.VISIBLE);
                //IfAllowedToEdit();
            }
        });

        PreviewPage_deleteBtt = (Button) view.findViewById(R.id.PreviewPage_deleteBtt);
        PreviewPage_deleteBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                //IfAllowedToDelete();
                ConfirmationBox cb = new ConfirmationBox(getActivity().getWindow().getContext());
                cb.Setup("Remove Your Review", "Are you sure you want to delete it ?", "Delete", "Cancel");
                cb.setCancelable(false);
                cb.setListener(new ConfirmationBox.Confirmed() {
                    @Override
                    public void Yes() {
                        RemoveAReview();
                    }

                    @Override
                    public void No() {

                    }
                });
                cb.show();
            }
        });

        if(!Prefs.contains(UserConstants.AccessToken)) {
            PreviewPage_editBtt.setVisibility(View.GONE);
            PreviewPage_deleteBtt.setVisibility(View.GONE);
        }

        PreviewPage_DoALike = (ImageView) view.findViewById(R.id.PreviewPage_DoALike);
        PreviewPage_DoALike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                DoALike();
            }
        });

        PreviewPage_Share = (ImageView) view.findViewById(R.id.PreviewPage_Share);
        PreviewPage_Share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ShareThePage();
            }
        });


        EditPage_backBtt = (LinearLayout) view.findViewById(R.id.EditPage_backBtt);
        EditPage_backBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(NeedToWriteNewReview)
                {
                    ac.CloseMe();
                }
                else
                {
                    EditPage.setVisibility(View.GONE);
                }
            }
        });


        EditPage_SelectImage = (LinearLayout) view.findViewById(R.id.EditPage_SelectImage);
        EditPage_SelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickAnImage();
            }
        });

        EditPage_SelectImageContainer = (ImageView) view.findViewById(R.id.EditPage_SelectImageContainer);
        EditPage_Title = (EditText) view.findViewById(R.id.EditPage_Title);
        EditPage_Details = (EditText) view.findViewById(R.id.EditPage_Details);

        EditPage_Done = (Button) view.findViewById(R.id.EditPage_Done);
        EditPage_Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Validate())
                {
                    InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    if(NeedToWriteNewReview)
                    {
                        SendAReview();
                    }
                    else
                    {
                        UpdateAReview();
                    }
                }
            }
        });

        SelectSupporter = (TextView) view.findViewById(R.id.SelectSupporter);
        SelectSupporter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowUserTypeSelection();
            }
        });

        SelectSupporterList = (Spinner) view.findViewById(R.id.SelectSupporterList);
        /*SelectSupporterList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowUserTypeSelection();
            }
        });*/

        if(getArguments() !=null)
        {
            Populate();
            NeedToWriteNewReview = false;
        }else{
            NeedToWriteNewReview = true;
            EditPage.setVisibility(View.VISIBLE);
            CheckIfCurrentUserIsAdmin();
        }


        talkDetailsPg_aboutHolder_H = PreviewPage_detailReviewHolder.getLayoutParams().height;
        Pg_seeMoreBtt =  (LinearLayout) view.findViewById(R.id.Pg_seeMoreBtt);
        //Pg_seeMoreBtt.setVisibility(View.GONE);
        Pg_seeMoreBtt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                PreviewPage_detailReviewHolder.getLayoutParams().height = PreviewPage_detailReview.getLayout().getHeight()+50;
                PreviewPage_detailReviewHolder.requestLayout();
                Pg_seeMoreBtt.setVisibility(View.GONE);
                Pg_seeLessBtt.setVisibility(View.VISIBLE);
            }
        });

        Pg_seeLessBtt  =  (LinearLayout) view.findViewById(R.id.Pg_seeLessBtt);
        Pg_seeLessBtt.setVisibility(View.GONE);
        Pg_seeLessBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreviewPage_detailReviewHolder.getLayoutParams().height = talkDetailsPg_aboutHolder_H + 10;
                PreviewPage_detailReviewHolder.requestLayout();
                Pg_seeMoreBtt.setVisibility(View.VISIBLE);
                Pg_seeLessBtt.setVisibility(View.GONE);
            }
        });


        PreviewPage_CommentText = (EditText) view.findViewById(R.id.PreviewPage_CommentText);
        PreviewPage_doComment = (LinearLayout) view.findViewById(R.id.PreviewPage_doComment);
        ProfileIcon = (CircularImageView) view.findViewById(R.id.ProfileIcon);
        PreviewPage_doComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ValidateComment())
                {
                    InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    CreateAnComment();
                    if(PreviewPage_CommentHolder.getVisibility() == View.VISIBLE){
                        PreviewPage_CommentHolder.setVisibility(View.GONE);
                    }
                }
            }
        });

        if(Prefs.contains(UserConstants.AccessToken))
        {
            //PreviewPage_CommentHolder.setVisibility(View.VISIBLE);
            Picasso.with(getContext())
                    .load(new File(Prefs.getString(UserConstants.ProfilePicture, null)))
                    .placeholder(R.drawable.icon_profile)
                    .error(R.drawable.icon_profile)
                    .into(ProfileIcon);
        }



        showCommentScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Prefs.contains(UserConstants.AccessToken))
                {
                    PreviewPage_CommentHolder.setVisibility(View.VISIBLE);
                }
                else
                {
                    ac.CloseFragmentAndOpenLoginScreen();
                }
            }
        });
    }

    void CreateAnComment()
    {
       ShowProgressDialogue();
        DashboardFrgLatest.CreateANewReviewComment(getArguments().getString(GeneralConstants.ID), PreviewPage_CommentText.getText().toString(), new DashboardFrgLatest.response() {
            @Override
            public void Success() {
                HideProgressDialogue();
                Toast.makeText(getContext(), "Comment Posted Successfully.", Toast.LENGTH_LONG).show();
                LoadReviewComments();
                PreviewPage_CommentText.setText(null);
            }

            @Override
            public void TokenRefreshRequired() {
                DashboardFrgLatest.CreateANewReviewComment(getArguments().getString(GeneralConstants.ID), PreviewPage_CommentText.getText().toString(), new DashboardFrgLatest.response() {
                    @Override
                    public void Success() {
                        HideProgressDialogue();
                        Toast.makeText(getContext(), "Comment Posted Successfully.", Toast.LENGTH_LONG).show();
                        LoadReviewComments();
                        PreviewPage_CommentText.setText(null);
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        HideProgressDialogue();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                HideProgressDialogue();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    boolean ValidateComment()
    {
        if(TextUtils.isEmpty(PreviewPage_CommentText.getText().toString()))
        {
            Toast.makeText(getContext(), "Please enter some comment", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;

    }

    public void ShowUserTypeSelection()
    {
        OptionListing_T1 t = new OptionListing_T1(getActivity().getWindow().getContext());
        List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();
        final String[] theList = new String[]{"Regular", "Supporter", "Artist", "Curator"};
        for (int i = 0; i < theList.length; i++) {
            RadioButtonInfo io1=new RadioButtonInfo();
            io1.setID(i);
            io1.setText(theList[i]);
            btns.add(io1);
        }
        t.setup("User", btns, SelectSupporter.getText().toString());
        t.setListener(new OptionListing_T1.Success() {
            @Override
            public void Done(RadioButtonInfo value) {
                SelectSupporter.setText(value.getText());
                if(value.getText().equals("Regular"))
                {
                    LoadUserList("1", true);
                }
                else if(value.getText().equals("Supporter"))
                {
                    LoadUserList("4", true);
                }
                else if(value.getText().equals("Artist"))
                {
                    LoadUserList("2", true);
                }
                else
                {
                    LoadUserList("3", true);
                }
            }
        });
        t.show();
    }

    void IfAllowedToEdit()
    {
        if (Prefs.contains(UserConstants.AccessToken)) {

            DashboardFrgLatest.IfAllowedToEditReview(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.response() {
                @Override
                public void Success() {
                    EditPage.setVisibility(View.VISIBLE);
                    allowedToEditDelete = true;
                    progress.dismiss();
                }

                @Override
                public void TokenRefreshRequired() {
                    DashboardFrgLatest.IfAllowedToEditReview(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.response() {
                        @Override
                        public void Success() {
                            EditPage.setVisibility(View.VISIBLE);
                            allowedToEditDelete = true;
                            progress.dismiss();
                        }

                        @Override
                        public void TokenRefreshRequired() {

                        }

                        @Override
                        public void Error(String message) {
                            progress.dismiss();
                            allowedToEditDelete = false;
                            Toast.makeText(getContext(), "Your are not allowed to edit this review", Toast.LENGTH_LONG).show();
                        }
                    });
                }

                @Override
                public void Error(String message) {
                    progress.dismiss();
                    allowedToEditDelete = false;
                    Toast.makeText(getContext(), "Your are not allowed to edit this review", Toast.LENGTH_LONG).show();
                }
            });

        }else {
            //ac.openLoginPage();
        }
    }

    boolean Validate()
    {

        if(imageTobeUpload == null)
        {
            if(!allowedToEditDelete)
            {
                Toast.makeText(getContext(), "Please select an image", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(TextUtils.isEmpty(EditPage_Title.getText().toString()))
        {
            Toast.makeText(getContext(), "Please enter an title", Toast.LENGTH_LONG).show();
            return false;
        }

        if(TextUtils.isEmpty(EditPage_Details.getText().toString()))
        {
            Toast.makeText(getContext(), "Please enter some details", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    void IfAllowedToDelete()
    {
        if (Prefs.contains(UserConstants.AccessToken)) {
            DashboardFrgLatest.IfAllowedToEditReview(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.response() {
                @Override
                public void Success() {
                    allowedToEditDelete = true;
                    //RemoveAReview();
                    PreviewPage_EditDelete_ButtonHolder.setVisibility(View.VISIBLE);
                }

                @Override
                public void TokenRefreshRequired() {
                    DashboardFrgLatest.IfAllowedToEditReview(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.response() {
                        @Override
                        public void Success() {
                            allowedToEditDelete = true;
                            PreviewPage_EditDelete_ButtonHolder.setVisibility(View.VISIBLE);
                            //RemoveAReview();
                        }

                        @Override
                        public void TokenRefreshRequired() {

                        }

                        @Override
                        public void Error(String message) {
                            allowedToEditDelete = false;
                        }
                    });
                }

                @Override
                public void Error(String message) {
                    allowedToEditDelete = false;
                }
            });

        }else {
            //ac.openLoginPage();
        }
    }

    void RemoveAReview()
    {
        if (Prefs.contains(UserConstants.AccessToken)) {

            progress = ProgressDialog.show(getContext(), "Retrieving details.", "Please wait for a while.", true);
            DashboardFrgLatest.RemoveAReview(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.response() {
                @Override
                public void Success() {
                    progress.dismiss();
                    Toast.makeText(getContext(), "Removed", Toast.LENGTH_LONG).show();
                    ac.CloseMe();
                }

                @Override
                public void TokenRefreshRequired() {
                    DashboardFrgLatest.RemoveAReview(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.response() {
                        @Override
                        public void Success() {
                            progress.dismiss();
                            Toast.makeText(getContext(), "Removed", Toast.LENGTH_LONG).show();
                            ac.CloseMe();
                        }

                        @Override
                        public void TokenRefreshRequired() {

                        }

                        @Override
                        public void Error(String message) {
                            progress.dismiss();
                            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                            ac.CloseMe();
                        }
                    });
                }

                @Override
                public void Error(String message) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    progress.dismiss();
                    ac.CloseMe();
                }
            });


        }else {
            ac.openLoginPage();
        }
    }

    void UpdateAReview()
    {
        if (Prefs.contains(UserConstants.AccessToken)) {

            progress = ProgressDialog.show(getContext(), "Retrieving details.", "Please wait for a while.", true);

            DashboardFrgLatest.UpdateSelectedReviews(imageTobeUpload, (SelectedUsers!=null?String.valueOf(SelectedUsers.get(SelectSupporterList.getSelectedItemPosition()).getUser_id()):null),getArguments().getString(GeneralConstants.ID), EditPage_Title.getText().toString(), EditPage_Details.getText().toString(), new DashboardFrgLatest.response() {
                @Override
                public void Success() {
                    Toast.makeText(getContext(), "Review Updated!", Toast.LENGTH_LONG).show();
                    progress.dismiss();
                    //ac.GoToGalleryTab();
                    ac.CloseMe();
                }

                @Override
                public void TokenRefreshRequired() {
                    DashboardFrgLatest.UpdateSelectedReviews(imageTobeUpload, (SelectedUsers!=null?String.valueOf(SelectedUsers.get(SelectSupporterList.getSelectedItemPosition()).getUser_id()):null),getArguments().getString(GeneralConstants.ID),  EditPage_Title.getText().toString(), EditPage_Details.getText().toString(), new DashboardFrgLatest.response() {
                        @Override
                        public void Success() {
                            Toast.makeText(getContext(), "Review Updated!", Toast.LENGTH_LONG).show();
                            progress.dismiss();
                            //ac.GoToGalleryTab();
                            ac.CloseMe();
                        }

                        @Override
                        public void TokenRefreshRequired() {

                        }

                        @Override
                        public void Error(String message) {
                            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                            progress.dismiss();
                            ac.CloseMe();
                        }
                    });
                }

                @Override
                public void Error(String message) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    progress.dismiss();
                    ac.CloseMe();
                }
            });

        }else {
            ac.openLoginPage();
        }
    }

    void LoadReviewComments()
    {
        DashboardFrgLatest.RetrieveSelectedReviewComments(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.SelectedExhibitionCommentsRetrived() {
            @Override
            public void Success(List<Comment_T1> comments) {

                PreviewPage_NoOfComments.setText(comments.size()+"");
                PreviewPage.setVisibility(View.VISIBLE);
                if(PreviewPage_ExistingCommentContainer.getChildCount()>0) {
                    PreviewPage_ExistingCommentContainer.removeAllViews();
                }

                for (Comment_T1 comment : comments)
                {
                    CommentRow_T1 cr_t1 = new CommentRow_T1(getContext());
                    //cr_t1.setup(comment);
                    cr_t1.setup2(
                            (comment.getCommentor_english_name()+" / "+comment.getCommentor_korean_name()),
                            comment.getComment_details(),
                            comment.getComment_date_time(),
                            comment.getProfile_image_thumb()
                    );
                    PreviewPage_ExistingCommentContainer.addView(cr_t1);
                }

            }

            @Override
            public void Error(String message) {
                if(!message.contains("No comment found"))
                {
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    ac.CloseMe();
                }
            }
        });
    }

    void SendAReview()
    {
        if(Prefs.contains(UserConstants.AccessToken)) {

            progress = ProgressDialog.show(getContext(), "Retrieving details.", "Please wait for a while.", true);

            DashboardFrgLatest.CreateANewReview(imageTobeUpload, (SelectedUsers!=null?String.valueOf(SelectedUsers.get(SelectSupporterList.getSelectedItemPosition()).getUser_id()):null), EditPage_Title.getText().toString(), EditPage_Details.getText().toString(), new DashboardFrgLatest.response() {
                @Override
                public void Success() {
                    Toast.makeText(getContext(), "Review Updated!", Toast.LENGTH_LONG).show();
                    progress.dismiss();
                    ac.CloseMe();
                }

                @Override
                public void TokenRefreshRequired() {
                    DashboardFrgLatest.CreateANewReview(imageTobeUpload, (SelectedUsers!=null?String.valueOf(SelectedUsers.get(SelectSupporterList.getSelectedItemPosition()).getUser_id()):null), EditPage_Title.getText().toString(), EditPage_Details.getText().toString(), new DashboardFrgLatest.response() {
                        @Override
                        public void Success() {
                            Toast.makeText(getContext(), "Review Updated!", Toast.LENGTH_LONG).show();
                            progress.dismiss();
                            ac.CloseMe();
                        }

                        @Override
                        public void TokenRefreshRequired() {

                        }

                        @Override
                        public void Error(String message) {
                            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                            progress.dismiss();
                            ac.CloseMe();
                        }
                    });
                }

                @Override
                public void Error(String message) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    progress.dismiss();
                    ac.CloseMe();
                }
            });

        }else {
            ac.openLoginPage();
        }
    }

    void PickAnImage()
    {
        imagePicker = new ImagePicker(getActivity());
        imagePicker.setImagePickerCallback(new ImagePickerCallback(){
                                               @Override
                                               public void onImagesChosen(List<ChosenImage> images) {
                                                   System.out.println(images.toString());
                                                   System.out.println(images.get(0).toString());
                                                   System.out.println(images.get(0).getOriginalPath());
                                                   /*
                                                   //Picasso.with(getWindow().getContext()).load(new File(images.get(0).getOriginalPath())).into(profilePicture);
                                                   SelectedProfileImage = new File(images.get(0).getOriginalPath());

                                                   */

                                                   imageTobeUpload = new File(images.get(0).getOriginalPath());

                                                   Picasso.with(getActivity().getWindow().getContext())
                                                           .load(new File(images.get(0).getOriginalPath()))
                                                           .placeholder(R.drawable.img_photo_02)
                                                           .error(R.drawable.img_photo_02)
                                                           .transform(new PicassoRoundTransform())
                                                           .into(EditPage_SelectImageContainer);
                                               }

                                               @Override
                                               public void onError(String message) {
                                                   // Do error handling
                                               }
                                           }
        );
        // imagePicker.allowMultiple(); // Default is false
        // imagePicker.shouldGenerateMetadata(false); // Default is true
        // imagePicker.shouldGenerateThumbnails(false); // Default is true
        imagePicker.pickImage();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent_data) {
        if (requestCode == Picker.PICK_IMAGE_DEVICE) {
            System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
                imagePicker.submit(intent_data);
            }
        }
    }

    void ShareThePage()
    {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.putExtra(Intent.EXTRA_SUBJECT, (ServerConstants.BASE_URL+"latest/review_details?review_id="+getArguments().getString(GeneralConstants.ID)));
        share.putExtra(Intent.EXTRA_TEXT, (ServerConstants.BASE_URL+"latest/review_details?review_id="+getArguments().getString(GeneralConstants.ID)));
        startActivity(Intent.createChooser(share, "Share link!"));
    }

    void DoALike()
    {

        if (Prefs.contains(UserConstants.AccessToken)) {
            ShowProgressDialogue();
            DashboardFrgLatest.LikeUnlikeAReviews(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.response() {
                @Override
                public void Success() {
                    HideProgressDialogue();
                    //ac.CloseMe();
                    Populate();
                }

                @Override
                public void TokenRefreshRequired() {
                    DashboardFrgLatest.LikeUnlikeAReviews(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.response() {
                        @Override
                        public void Success() {
                            HideProgressDialogue();
                            //ac.CloseMe();
                            Populate();
                        }

                        @Override
                        public void TokenRefreshRequired() {

                        }

                        @Override
                        public void Error(String message) {
                            HideProgressDialogue();
                            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                        }
                    });
                }

                @Override
                public void Error(String message) {
                    HideProgressDialogue();
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                }
            });

        } else {
            ac.openLoginPage();
        }

    }

    void Populate()
    {
        ShowProgressDialogue();

        DashboardFrgLatest.RetrieveSelectedReviews(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.Review_Retrieved() {
            @Override
            public void Success(DashboardFrgLatest_Reviews_Model_T1 data) {

                PreviewPage_no_OfLike.setText(data.getLike_count());
                PreviewPage_title.setText(data.getReview_title());
                PreviewPage_detailReview.setText(data.getDetails_info());

                if(data.getDetails_info() != null){
                    if(data.getDetails_info().length() > 300){
                        see_more_less_button_review_container.setVisibility(View.VISIBLE);
                    }
                    else {
                        see_more_less_button_review_container.setVisibility(View.GONE);
                    }
                }
                else {
                    see_more_less_button_review_container.setVisibility(View.GONE);
                }

                Picasso.with(getContext())
                        .load(data.getImage())
                        .placeholder(R.drawable.transparent)
                        .error(R.drawable.transparent)
                        .into(PreviewPage_img);

                Picasso.with(getContext())
                        .load(data.getImage())
                        .placeholder(R.drawable.img_photo_02)
                        .error(R.drawable.img_photo_02)
                        .transform(new PicassoRoundTransform())
                        .into(EditPage_SelectImageContainer);

                EditPage_Title.setText(data.getReview_title());
                EditPage_Details.setText(data.getDetails_info());

                PreviewPage_title.setText(data.getReview_creator_name_en()+",\n"+data.getReview_title());




                //see_more_less_button_review_container

                progress.dismiss();
                PreviewPage.setVisibility(View.VISIBLE);

                LoadReviewComments();
                //LoadReviewedUserText(data);
                IfAllowedToDelete();
            }

            @Override
            public void Error(String message) {
                PreviewPage.setVisibility(View.VISIBLE);
            }
        });


    }

    void LoadReviewedUserText(final DashboardFrgLatest_Reviews_Model_T1 reviewDetails)
    {
        User.RetrieveUserList(null, null, reviewDetails.getUser_id(), new User.userListRetrieve() {
            @Override
            public void Success(List<User_T1> users) {


                //EditPage_Title.setText(users.get(0).getType_name()+", "+users.get(0).getName_en()+", "+reviewDetails.getReview_title());
                if(users.size()>0)
                {
                    PreviewPage_title.setText(users.get(0).getType_name()+", "+users.get(0).getName_en()+",\n"+reviewDetails.getReview_title());
                }
                else
                {
                    PreviewPage_title.setText("User Account Removed."+",\n"+reviewDetails.getReview_title());
                }
                LoadUserList("1", true);
            }

            @Override
            public void TokenRefreshRequired() {
                LoadUserList("1", true);
            }

            @Override
            public void Error(String message) {
                LoadUserList("1", true);
            }
        });
    }


    public void ShowProgressDialogue()
    {
        if(progress == null)
        {
            progress = ProgressDialog.show(getContext(), "Retrieving details data.", "Please wait for a while.", true);
            progress.setCancelable(true);
        }
    }


    public void HideProgressDialogue()
    {
        if(progress != null)
        {
            progress.dismiss();
            progress = null;
        }
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        ac = (ActivityCommunicator) getActivity();
    }
}
