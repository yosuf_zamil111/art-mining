package com.kona.artmining.UI.FragmentActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.google.gson.Gson;
import com.kona.artmining.Adapter.UserArtScrapbookAdapter_T1;
import com.kona.artmining.Adapter.UserCartAdapter_T1;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Cart;
import com.kona.artmining.Helper.User;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Interface.UserActivity_FrgMng_Interface;
import com.kona.artmining.Interface.UserFrgMngCommunicator;
import com.kona.artmining.Model.ArtScrapbookItemList_T1;
import com.kona.artmining.Model.CartItem_T1;
import com.kona.artmining.R;
import com.kona.artmining.UI.FragmentController.UserActivity_FragmentsManager;
import com.kona.artmining.custom_popup_box.ConfirmationBox;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;


public class UserFrgShoppingCart extends Fragment implements UserFrgMngCommunicator
{
    TextView shopping_cart_item_count,cart_list_terms_and_conditions;
    LinearLayout shopping_cart_select_all_btt, shopping_cart_delete_btt;
    Button shopping_cart_order_btt;

    ProgressDialog progress;

    ObservableRecyclerView recyclerView;
    UserCartAdapter_T1 rvListAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    List<CartItem_T1> Content = new ArrayList<CartItem_T1>();

    boolean selectAll = false;
    boolean allowToOrder = true;
    UserActivity_FrgMng_Interface mCallback;

    String SelectedCurrency = "";

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.user_frg_shopping_cart, container, false);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            mCallback = (UserActivity_FrgMng_Interface) activity;
            UserActivity_FragmentsManager DA = (UserActivity_FragmentsManager) getActivity();
            DA.frg_listener = this;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement UserActivity_FrgMng_Interface");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStage(view);
    }

    void prepareStage(View view)
    {
        cart_list_terms_and_conditions = (TextView) view.findViewById(R.id.cart_list_terms_and_conditions);
        cart_list_terms_and_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(ServerConstants.BASE_URL+"user/terms_and_conditions/7?toc_on=1"));
                startActivity(browserIntent);
            }
        });


        shopping_cart_item_count = (TextView) view.findViewById(R.id.shopping_cart_item_count);
        shopping_cart_select_all_btt = (LinearLayout) view.findViewById(R.id.shopping_cart_select_all_btt);
        shopping_cart_delete_btt = (LinearLayout) view.findViewById(R.id.shopping_cart_delete_btt);
        shopping_cart_order_btt = (Button) view.findViewById(R.id.shopping_cart_order_btt);

        recyclerView = (ObservableRecyclerView) view.findViewById(R.id.recycler_view);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        /*layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(position == 0 || position == 1)
                {
                    return 2;
                }else{
                    return 1;
                }
            }
        });*/
        recyclerView.setLayoutManager(layoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        rvListAdapter = new UserCartAdapter_T1(Content, new UserCartAdapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(CartItem_T1 item, int position, boolean isChecked)
            {
                Content.get(position).setChecked(isChecked);
                Update_OrderButtonStatus();
            }

            @Override
            public void doOrder(CartItem_T1 item) {
                SelectedCurrency = item.getCurrency();
                mCallback.ShowPaymentProcessing("cart", SelectedCurrency);
                SaveSingleItemModifiedCart(item);
                getActivity().finish();
            }

            @Override
            public void remove(CartItem_T1 item)
            {
                RemoveItem(item.getCatrt_id(), new TheResponse() {
                    @Override
                    public void success(String response) {
                        RefreshPage();
                    }

                    @Override
                    public void failed() {

                    }
                });
            }
        });
        recyclerView.setAdapter(rvListAdapter);

        swipeRefreshLayout.setProgressViewOffset(false,100,200);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(){
                RefreshPage();
            }
        });

        shopping_cart_select_all_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(selectAll)
                {
                    selectAll = false;
                }else
                {
                    selectAll = true;
                }

                for (int i = 0; i < Content.size(); i++) {
                    Content.get(i).setChecked(selectAll);
                }
                Update_OrderButtonStatus();
                FillUpRV(Content, true);
            }
        });

        view.findViewById(R.id.shopping_cart_backBtt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        shopping_cart_delete_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String removeItems = "";

                for (int i = 0; i < Content.size(); i++) {
                    if(Content.get(i).isChecked())
                    {
                        if(i==0)
                        {
                            removeItems = removeItems.concat(Content.get(i).getCatrt_id());
                        }else{
                            removeItems = removeItems.concat(","+Content.get(i).getCatrt_id());
                        }
                        System.out.println("We have to delete it: "+Content.get(i).getCatrt_id());
                    }
                }

                if(!TextUtils.isEmpty(removeItems))
                {
                    RemoveItem(removeItems, new TheResponse() {
                        @Override
                        public void success(String response) {
                            RefreshPage();
                        }

                        @Override
                        public void failed() {

                        }
                    });
                }
                else
                {
                    Toast.makeText(getContext(), "Please select some items.", Toast.LENGTH_LONG).show();
                }
            }
        });

        shopping_cart_order_btt.setEnabled(false);
        shopping_cart_order_btt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                SaveModifiedCart();
                if(allowToOrder)
                {
                    mCallback.ShowPaymentProcessing("cart", SelectedCurrency);
                    getActivity().finish();
                }
                else
                {
                    Toast.makeText(getContext(), "Same Currency Please.", Toast.LENGTH_LONG).show();
                }
            }
        });

        RefreshPage();
    }

    void LogOut()
    {
        Prefs.remove(UserConstants.AccessToken);
        Prefs.remove(UserConstants.RefreshToken);
        Prefs.remove(UserConstants.DO_AUTO_LOGIN);
        Prefs.remove(UserConstants.ProfilePicture);
        getActivity().finish();
    }

    void SaveModifiedCart()
    {
        List<CartItem_T1> ModifiedCart = new ArrayList<CartItem_T1>();

        SelectedCurrency = "";
        int Pointer = 0;

        for (int i = 0; i < Content.size(); i++) {
            if (!Content.get(i).isSold_out()) {
                if (Content.get(i).isChecked()) {
                    ModifiedCart.add(Content.get(i));
                    if(Pointer == 0)
                    {
                        SelectedCurrency = Content.get(i).getCurrency();
                        Pointer = Pointer + 1;
                    }
                    else
                    {
                        if(!SelectedCurrency.equals(Content.get(i).getCurrency()))
                        {
                            allowToOrder = false;
                            break;
                        }
                        else
                        {
                            allowToOrder = true;
                            SelectedCurrency = Content.get(i).getCurrency();
                        }
                    }
                }
            }
        }

        Prefs.putString(GeneralConstants.MODIFIED_CART_JSON, new Gson().toJson(ModifiedCart));
    }

    void SaveSingleItemModifiedCart(CartItem_T1 CI_T1)
    {
        List<CartItem_T1> ModifiedCart = new ArrayList<CartItem_T1>();
        ModifiedCart.add(CI_T1);
        Prefs.putString(GeneralConstants.MODIFIED_CART_JSON, new Gson().toJson(ModifiedCart));
    }

    void Update_OrderButtonStatus()
    {
        boolean showBtt = false;

        for (int i = 0; i < Content.size(); i++) {
            if(!Content.get(i).isSold_out())
            {
                if(Content.get(i).isChecked())
                {
                    showBtt = true;
                    break;
                }
            }
        }

        shopping_cart_order_btt.setEnabled(showBtt);
        if(Content.size() < 1)
        {
            shopping_cart_order_btt.setVisibility(View.GONE);
        }
    }

    void RemoveItem(final String itemId, final TheResponse resp)
    {
        ConfirmationBox cb = new ConfirmationBox(getActivity().getWindow().getContext());
        cb.Setup("Remove Cart Item", "Are you sure you want to delete it ?", "Delete", "Cancel");
        cb.setCancelable(false);
        cb.setListener(new ConfirmationBox.Confirmed() {
            @Override
            public void Yes() {

                Cart.RemoveItem(itemId, new Cart.CartDataRemove() {
                    @Override
                    public void Success() {
                        resp.success("");
                    }

                    @Override
                    public void NotFound() {
                        resp.failed();
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        Cart.RemoveItem(itemId, new Cart.CartDataRemove() {
                            @Override
                            public void Success() {
                                resp.success("");
                            }

                            @Override
                            public void NotFound() {
                                resp.failed();
                            }

                            @Override
                            public void TokenRefreshRequired() {
                                resp.failed();
                            }

                            @Override
                            public void Error(String message) {
                                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                                resp.failed();
                            }
                        });
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                        resp.failed();
                    }
                });

            }

            @Override
            public void No() {

            }
        });
        cb.show();
    }

    void RefreshPage()
    {
        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

        Cart.GetDetails(getContext(), new Cart.userCartDataRetrieve() {
            @Override
            public void Success(List<CartItem_T1> scrapBookData) {
                FillUpRV(scrapBookData, false);
            }

            @Override
            public void TokenRefreshRequired() {
                Cart.GetDetails(getContext(), new Cart.userCartDataRetrieve() {
                    @Override
                    public void Success(List<CartItem_T1> scrapBookData) {
                        FillUpRV(scrapBookData, false);
                        progress.dismiss();
                    }

                    @Override
                    public void TokenRefreshRequired()
                    {
                        progress.dismiss();
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                        progress.dismiss();
                        if(message.contentEquals("Not Authorized!"))
                        {
                            LogOut();
                        }
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                progress.dismiss();
                if(message.contentEquals("Not Authorized!"))
                {
                    LogOut();
                }
            }
        });
    }

    public void FillUpRV(List<CartItem_T1> CartItem, boolean refresh)
    {
        if(!refresh)
        {
            clearRVData();
            Content.addAll(CartItem);
        }
        shopping_cart_item_count.setText(""+Content.size());

        if(Content.size()>0)
        {
            getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.GONE);
        }else{
            getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.VISIBLE);
        }

        rvListAdapter.notifyDataSetChanged();

        if(swipeRefreshLayout != null)
        {
            swipeRefreshLayout.setRefreshing(false);
        }

        progress.dismiss();
        Update_OrderButtonStatus();
    }

    public void clearRVData()
    {
        int size = Content.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Content.remove(0);
            }
            rvListAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*
        if (requestCode == FILE_CODE && resultCode == Activity.RESULT_OK)
        {

        }
        */
    }

    @Override
    public void _DevicebackPressed() {

    }

    @Override
    public boolean ifDetailsPageHiden() {
        return true;
    }
}
