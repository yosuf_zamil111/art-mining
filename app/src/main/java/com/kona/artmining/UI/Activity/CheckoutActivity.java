package com.kona.artmining.UI.Activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Interface.CheckoutActivity_Interface;
import com.kona.artmining.Interface.FragmentCommunicator;
import com.kona.artmining.R;
import com.kona.artmining.UI.FragmentActivity.Checkout.ShippingAddress_PaymentMethod;
import com.kona.artmining.UI.FragmentActivity.Checkout.TermsConditions;
import com.kona.artmining.UI.FragmentActivity.UserFrgAuctionList;
import com.kona.artmining.UI.FragmentActivity.UserFrgShoppingCart;
import com.kona.artmining.UI.FragmentController.UserActivity_FragmentsManager;
import com.paypal.android.sdk.payments.PayPalService;
import com.pixplicity.easyprefs.library.Prefs;

public class CheckoutActivity extends AppCompatActivity implements CheckoutActivity_Interface
{

    String tc_type;
    String SelectedCurrency = "";

    boolean ShowShippingMethod = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_fragment_manager);
        initMessageFromBundle();

        /*
        args.putString(GeneralConstants.TC_TYPE, tc_type);
        FragmentReplace(new TermsConditions(), args);
        */

        Bundle args = new Bundle();
        args.putString(GeneralConstants.TC_TYPE, tc_type);
        args.putString(GeneralConstants.SELECTED_CURRENCY, SelectedCurrency);
        FragmentReplace(new ShippingAddress_PaymentMethod(), args);
        ShowShippingMethod = true;
    }

    private void FragmentReplace(Fragment fragment, Bundle arg)
    {
        fragment.setArguments(arg);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, fragment);
        fragmentTransaction.commit();
    }

    private void initMessageFromBundle() {
        Bundle data = getIntent().getExtras();
        if (data != null)
        {
            tc_type = data.getString(GeneralConstants.TC_TYPE);
            SelectedCurrency  = data.getString(GeneralConstants.SELECTED_CURRENCY);
            Prefs.putString(GeneralConstants.TC_TYPE, tc_type);
        }
    }

    @Override
    public void onBackPressed()
    {
        if(ShowShippingMethod)
        {
            /*Bundle args = new Bundle();
            args.putString(GeneralConstants.TC_TYPE, tc_type);
            FragmentReplace(new TermsConditions(), args);
            ShowShippingMethod = false;*/
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
            if(fragment instanceof FragmentCommunicator){
                ((FragmentCommunicator)fragment)._DevicebackPressed();
            }
        }
        else
        {
            __OpenFragmentForm_UAFM(Prefs.getString(GeneralConstants.TC_TYPE, ""));
            finish();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void ShowShippingMethods() {
        Bundle args = new Bundle();
        args.putString(GeneralConstants.TC_TYPE, tc_type);
        args.putString(GeneralConstants.SELECTED_CURRENCY, SelectedCurrency);
        FragmentReplace(new ShippingAddress_PaymentMethod(), args);
        ShowShippingMethod = true;
    }

    @Override
    public void OpenFragmentForm_UAFM(String className)
    {
        Intent intent = new Intent(this, UserActivity_FragmentsManager.class);
        intent.putExtra(GeneralConstants.FRAGMENT_NAME, className);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void ShowTermsConditions() {

        /*Bundle args = new Bundle();
        args.putString(GeneralConstants.TC_TYPE, tc_type);
        FragmentReplace(new TermsConditions(), args);
        ShowShippingMethod = false;*/
        __OpenFragmentForm_UAFM(Prefs.getString(GeneralConstants.TC_TYPE, ""));
        finish();
    }

    @Override
    public void ShowOrderDetailsPage(String OrderID) {
        // if OrderID is null, no order summary need to show, just show order list. else show details over id.
        Intent myIntent = new Intent(CheckoutActivity.this, OrderActivity.class);
        myIntent.putExtra(GeneralConstants.ORDER_ID, OrderID);
        CheckoutActivity.this.startActivity(myIntent);
        finish();
    }

    void __OpenFragmentForm_UAFM(String typ)
    {
        Intent intent = new Intent(this, UserActivity_FragmentsManager.class);
        if(typ.equals("auc"))
        {
            intent.putExtra(GeneralConstants.FRAGMENT_NAME, UserFrgAuctionList.class.getName());
        }
        else
        {
            intent.putExtra(GeneralConstants.FRAGMENT_NAME, UserFrgShoppingCart.class.getName());
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("------------------------------------->------------------------------>");
        System.out.println("-------------------------------------"+requestCode+"------------------------------>");
        System.out.println("-------------------------------------"+resultCode+"------------------------------>");
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        fragment.onActivityResult(requestCode, resultCode, data);
    }
}
