package com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Helper.DashboardFrgLatest;
import com.kona.artmining.Interface.ActivityCommunicator;
import com.kona.artmining.Model.DashboardFrgLatest_Event_Model_T1;
import com.kona.artmining.Model.DashboardFrgLatest_Exhibition_Model_T1;
import com.kona.artmining.R;
import com.squareup.picasso.Picasso;

public class DELF_Event_DetailsFrg extends Fragment
{
    LinearLayout backBtt,PreviewPage;
    TextView title, details;
    ImageView featuredImage;

    ProgressDialog progress;

    ActivityCommunicator ac;

    public DELF_Event_DetailsFrg() {
        // Required empty public constructor
    }

    public static DELF_Event_DetailsFrg newInstance(int param1) {
        DELF_Event_DetailsFrg fragment = new DELF_Event_DetailsFrg();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ui_dashboard_frgmnts_latest_frg_event_details, container, false);
        PrepareStage(view);
        //exhibition();
        return view;
    }

    void PrepareStage(View view)
    {

        PreviewPage = (LinearLayout) view.findViewById(R.id.PreviewPage);
        PreviewPage.setVisibility(View.GONE);
        progress = ProgressDialog.show(getContext(), "Retrieving details.", "Please wait for a while.", true);

        backBtt = (LinearLayout) view.findViewById(R.id.backBtt);
        backBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ac.CloseMe();
            }
        });

        title = (TextView) view.findViewById(R.id.title);
        details = (TextView) view.findViewById(R.id.details);

        featuredImage = (ImageView) view.findViewById(R.id.featuredImage);

        DashboardFrgLatest.RetrieveSelectedEventDetails(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.SelectedEventRetrived() {
            @Override
            public void Success(DashboardFrgLatest_Event_Model_T1 data) {
                title.setText(data.getTitle());
                details.setText(data.getDetails_info());

                Picasso.with(getContext())
                        .load(data.getImage())
                        .placeholder(R.drawable.transparent)
                        .error(R.drawable.transparent)
                        .fit()
                        .into(featuredImage);

                progress.dismiss();
                PreviewPage.setVisibility(View.VISIBLE);
            }

            @Override
            public void Error(String message) {
                PreviewPage.setVisibility(View.VISIBLE);
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                ac.CloseMe();
            }
        });
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        ac = (ActivityCommunicator) getActivity();
    }
}
