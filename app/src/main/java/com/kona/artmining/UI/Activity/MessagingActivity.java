package com.kona.artmining.UI.Activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.kona.artmining.Adapter.MessageAdapter_T1;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Messaging;
import com.kona.artmining.Interface.MessagingActivity_Interface;
import com.kona.artmining.Model.Message;
import com.kona.artmining.R;
import com.kona.artmining.UI.FragmentActivity.Message.ReadMail;
import com.kona.artmining.UI.FragmentActivity.Message.ReplyMail;
import com.kona.artmining.UI.FragmentActivity.Message.WriteMail;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;


public class MessagingActivity extends AppCompatActivity implements MessagingActivity_Interface {

    LinearLayout deleteSelectedItems, backBtt, ShowsentMail, ShowtrashMail, inboxMail;
    LinearLayout trash_email_move_and_delete_button;

    // Inbox
    ObservableRecyclerView InboxRecyclerView;
    MessageAdapter_T1 Inbox_rvListAdapter;
    SwipeRefreshLayout Inbox_swipeRefreshLayout;
    List<Message> InboxContent = new ArrayList<Message>();
    TextView inboxUnreadMessageCount, totalUnreadMessage;
    LinearLayout inml_ShowTrashMails, inml_ShowSentMails;

    // Sent
    MessageAdapter_T1 Sent_rvListAdapter;
    SwipeRefreshLayout Sent_swipeRefreshLayout;
    ObservableRecyclerView SentRecyclerView;
    List<Message> SentContent = new ArrayList<Message>();
    LinearLayout sntml_inbox, sntml_trash;

    // Trash
    MessageAdapter_T1 Trash_rvListAdapter;
    SwipeRefreshLayout Trash_swipeRefreshLayout;
    ObservableRecyclerView TrashRecyclerView;
    List<Message> TrashContent = new ArrayList<Message>();
    LinearLayout trsml_inbox, trsml_sent, trsml_deleteMessage, trsml_move_to_inbox;

    // Fragment
    boolean readMailHitted = false;
    boolean writeMailHitted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging);
        initMessageFromBundle();

        prepareInbox();
        prepareSent();
        prepareTrash();

        backBtt  = (LinearLayout) findViewById(R.id.backBtt);
        backBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        deleteSelectedItems  = (LinearLayout) findViewById(R.id.deleteSelectedItems);
        deleteSelectedItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(inboxMail.getVisibility() == View.VISIBLE)
                {
                    // Remove inbox element
                    String ric = "";
                    for(int i1 = 0; i1 < InboxContent.size(); ++i1) {
                        if(InboxContent.get(i1).isChecked())
                        {
                            if(i1==0)
                            {
                                ric = ric.concat(InboxContent.get(i1).getId()+"");
                            }
                            else
                            {
                                ric = ric.concat(","+InboxContent.get(i1).getId()+"");
                            }
                        }
                    }
                    TrashElement(ric, UserConstants.MESSAGE_TYPE_INBOX);
                }
                else if(ShowsentMail.getVisibility() == View.VISIBLE)
                {
                    // Remove sent element
                    String ric = "";
                    for(int i2 = 0; i2 < SentContent.size(); ++i2) {
                        if(SentContent.get(i2).isChecked())
                        {
                            if(i2==0)
                            {
                                ric = ric.concat(SentContent.get(i2).getId()+"");
                            }
                            else
                            {
                                ric = ric.concat(","+SentContent.get(i2).getId()+"");
                            }
                        }
                    }
                    RemoveElement(ric, UserConstants.MESSAGE_TYPE_SENT);
                }
                else
                {
                    // Remove trash element
                    String ric = "";
                    for(int i3 = 0; i3 < TrashContent.size(); ++i3) {
                        if(TrashContent.get(i3).isChecked())
                        {
                            if(i3==0)
                            {
                                ric = ric.concat(TrashContent.get(i3).getId()+"");
                            }
                            else
                            {
                                ric = ric.concat(","+TrashContent.get(i3).getId()+"");
                            }
                        }
                    }
                    TrashElement(ric,UserConstants.MESSAGE_TYPE_TRASH);
                }
            }
        });
    }

    void TrashElement(final String messageID, final int type)
    {
        if(TextUtils.isEmpty(messageID))
        {
            Toast.makeText(getWindow().getContext(), "Select some item", Toast.LENGTH_LONG).show();
            return;
        }

        Messaging.TrashMessage(messageID, new Messaging.MessageRemove() {
            @Override
            public void Success() {
                if(type==UserConstants.MESSAGE_TYPE_INBOX)
                {
                    refreshInbox();
                    refreshTrash();
                }
                else
                {
                    refreshSent();
                    refreshTrash();
                }
            }

            @Override
            public void NotFound()
            {

            }

            @Override
            public void TokenRefreshRequired() {
                Messaging.TrashMessage(messageID, new Messaging.MessageRemove() {
                    @Override
                    public void Success()
                    {
                        if(type==UserConstants.MESSAGE_TYPE_INBOX)
                        {
                            refreshInbox();
                            refreshTrash();
                        }
                        else
                        {
                            refreshSent();
                            refreshTrash();
                        }
                    }

                    @Override
                    public void NotFound()
                    {

                    }

                    @Override
                    public void TokenRefreshRequired()
                    {

                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void MoveToInbox(final String messageID)
    {
        if(TextUtils.isEmpty(messageID))
        {
            Toast.makeText(getWindow().getContext(), "Select some item", Toast.LENGTH_LONG).show();
            return;
        }

        Messaging.MoveToInbox(messageID, new Messaging.response() {
            @Override
            public void Success() {
                refreshTrash();
            }

            @Override
            public void TokenRefreshRequired() {
                Messaging.MoveToInbox(messageID, new Messaging.response() {
                    @Override
                    public void Success()
                    {
                        refreshTrash();
                    }

                    @Override
                    public void TokenRefreshRequired()
                    {

                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void RemoveElement(final String messageID, final int type)
    {
        if(TextUtils.isEmpty(messageID))
        {
            Toast.makeText(getWindow().getContext(), "Select some item", Toast.LENGTH_LONG).show();
            return;
        }

        Messaging.RemoveMessage(messageID, new Messaging.MessageRemove() {
            @Override
            public void Success() {
                if(type==UserConstants.MESSAGE_TYPE_INBOX)
                {
                    refreshInbox();
                }
                else if(type==UserConstants.MESSAGE_TYPE_SENT)
                {
                    refreshSent();
                }
                else
                {
                    refreshTrash();
                }
            }

            @Override
            public void NotFound()
            {

            }

            @Override
            public void TokenRefreshRequired() {
                Messaging.RemoveMessage(messageID, new Messaging.MessageRemove() {
                    @Override
                    public void Success()
                    {
                        if(type==UserConstants.MESSAGE_TYPE_INBOX)
                        {
                            refreshInbox();
                        }
                        else if(type==UserConstants.MESSAGE_TYPE_SENT)
                        {
                            refreshSent();
                        }
                        else
                        {
                            refreshTrash();
                        }
                    }

                    @Override
                    public void NotFound()
                    {

                    }

                    @Override
                    public void TokenRefreshRequired()
                    {

                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void prepareInbox()
    {
        inboxMail = (LinearLayout) findViewById(R.id.inboxMail);
        totalUnreadMessage = (TextView) findViewById(R.id.totalUnreadMessage);
        inboxUnreadMessageCount = (TextView) findViewById(R.id.inboxUnreadMessageCount);
        InboxRecyclerView = (ObservableRecyclerView) findViewById(R.id.InboxRecyclerView);
        ObservableRecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        InboxRecyclerView.setLayoutManager(mLayoutManager);
        Inbox_rvListAdapter = new MessageAdapter_T1(InboxContent, new MessageAdapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(Message item)
            {
                Bundle args = new Bundle();
                args.putLong(GeneralConstants.MESSAGE_ID, item.getId());
                args.putInt(UserConstants.MESSAGE_TYPE, UserConstants.MESSAGE_TYPE_INBOX);
                readMailHitted = true;
                FragmentReplace(new ReadMail(), args);
            }

            @Override
            public void onItemChecked(Message item, int position, boolean ifChecked)
            {
                InboxContent.get(position).setChecked(ifChecked);
                UpdateTotalSelectedMessage(InboxContent);
            }
        });
        InboxRecyclerView.setAdapter(Inbox_rvListAdapter);

        Inbox_swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.Inbox_swipeRefreshLayout);
        Inbox_swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshInbox();
            }
        });


        inml_ShowTrashMails = (LinearLayout) findViewById(R.id.inml_ShowTrashMails);
        inml_ShowTrashMails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inboxMail.setVisibility(View.GONE);
                ShowsentMail.setVisibility(View.GONE);
                ShowtrashMail.setVisibility(View.VISIBLE);
                deleteSelectedItems.setVisibility(View.GONE);
                UpdateTotalSelectedMessage(InboxContent);
            }
        });
        inml_ShowSentMails = (LinearLayout) findViewById(R.id.inml_ShowSentMails);
        inml_ShowSentMails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inboxMail.setVisibility(View.GONE);
                ShowsentMail.setVisibility(View.VISIBLE);
                ShowtrashMail.setVisibility(View.GONE);
                deleteSelectedItems.setVisibility(View.VISIBLE);
                UpdateTotalSelectedMessage(InboxContent);
            }
        });

        refreshInbox();
    }

    void refreshInbox()
    {
        Messaging.RetrieveInboxMessage(new Messaging.MessageList_Retrieved() {
            @Override
            public void Success(List<Message> message) {
                clearInboxRVData();
                InboxContent.addAll(message);
                Inbox_rvListAdapter.notifyDataSetChanged();
                TotalUnreadInboxMessage();

                if(Inbox_swipeRefreshLayout != null)
                {
                    Inbox_swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void TokenRefreshRequired() {
                Messaging.RetrieveInboxMessage(new Messaging.MessageList_Retrieved() {
                    @Override
                    public void Success(List<Message> message) {
                        clearInboxRVData();
                        InboxContent.addAll(message);
                        Inbox_rvListAdapter.notifyDataSetChanged();
                        TotalUnreadInboxMessage();

                        if(Inbox_swipeRefreshLayout != null)
                        {
                            Inbox_swipeRefreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void TotalUnreadInboxMessage()
    {
        int i2 = 0;
        for(int i = 0; i < InboxContent.size(); ++i) {
            if(InboxContent.get(i).getUnread_info()==1)
            {
                i2=i2+1;
            }
        }
        inboxUnreadMessageCount.setText(i2+"");
    }

    void UpdateTotalSelectedMessage(List<Message> messages_)
    {
        int i2 = 0;
        for(int i = 0; i < messages_.size(); ++i) {
            if(messages_.get(i).isChecked())
            {
                i2=i2+1;
            }
        }

        totalUnreadMessage.setText(i2+"");
    }

    void prepareSent()
    {
        ShowsentMail = (LinearLayout) findViewById(R.id.sentMail);
        ShowsentMail.setVisibility(View.GONE);
        sntml_inbox = (LinearLayout) findViewById(R.id.sntml_inbox);
        sntml_inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inboxMail.setVisibility(View.VISIBLE);
                ShowsentMail.setVisibility(View.GONE);
                ShowtrashMail.setVisibility(View.GONE);
                deleteSelectedItems.setVisibility(View.VISIBLE);
                UpdateTotalSelectedMessage(SentContent);
            }
        });
        sntml_trash = (LinearLayout) findViewById(R.id.sntml_trash);
        sntml_trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inboxMail.setVisibility(View.GONE);
                ShowsentMail.setVisibility(View.GONE);
                ShowtrashMail.setVisibility(View.VISIBLE);
                deleteSelectedItems.setVisibility(View.GONE);
                UpdateTotalSelectedMessage(SentContent);
            }
        });

        SentRecyclerView = (ObservableRecyclerView) findViewById(R.id.SentRecyclerView);
        ObservableRecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        SentRecyclerView.setLayoutManager(mLayoutManager);
        Sent_rvListAdapter = new MessageAdapter_T1(SentContent, new MessageAdapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(Message item) {
                Bundle args = new Bundle();
                args.putLong(GeneralConstants.MESSAGE_ID, item.getId());
                args.putInt(UserConstants.MESSAGE_TYPE, UserConstants.MESSAGE_TYPE_SENT);
                FragmentReplace(new ReadMail(), args);
                readMailHitted = true;
            }

            @Override
            public void onItemChecked(Message item, int position, boolean ifChecked)
            {
                SentContent.get(position).setChecked(ifChecked);
                UpdateTotalSelectedMessage(SentContent);
            }
        });
        SentRecyclerView.setAdapter(Sent_rvListAdapter);

        Sent_swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.Sent_swipeRefreshLayout);
        Sent_swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshSent();
            }
        });

        refreshSent();
    }

    void refreshSent()
    {
        Messaging.RetrieveSentMessage(new Messaging.MessageList_Retrieved() {
            @Override
            public void Success(List<Message> message) {
                clearSentRVData();
                SentContent.addAll(message);
                Sent_rvListAdapter.notifyDataSetChanged();

                if(Sent_swipeRefreshLayout != null)
                {
                    Sent_swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void TokenRefreshRequired() {
                Messaging.RetrieveInboxMessage(new Messaging.MessageList_Retrieved() {
                    @Override
                    public void Success(List<Message> message) {
                        clearSentRVData();
                        SentContent.addAll(message);
                        Sent_rvListAdapter.notifyDataSetChanged();

                        if(Sent_swipeRefreshLayout != null)
                        {
                            Sent_swipeRefreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void prepareTrash()
    {
        ShowtrashMail = (LinearLayout) findViewById(R.id.trashMail);
        ShowtrashMail.setVisibility(View.GONE);

        trash_email_move_and_delete_button = (LinearLayout) findViewById(R.id.trash_email_move_and_delete_button);
        trash_email_move_and_delete_button.setVisibility(View.GONE);

        trsml_inbox = (LinearLayout) findViewById(R.id.trsml_inbox);
        trsml_inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inboxMail.setVisibility(View.VISIBLE);
                ShowsentMail.setVisibility(View.GONE);
                ShowtrashMail.setVisibility(View.GONE);
                deleteSelectedItems.setVisibility(View.VISIBLE);
                UpdateTotalSelectedMessage(TrashContent);
            }
        });
        trsml_sent = (LinearLayout) findViewById(R.id.trsml_sent);
        trsml_sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inboxMail.setVisibility(View.GONE);
                ShowsentMail.setVisibility(View.VISIBLE);
                ShowtrashMail.setVisibility(View.GONE);
                deleteSelectedItems.setVisibility(View.VISIBLE);
                UpdateTotalSelectedMessage(TrashContent);
            }
        });
        trsml_deleteMessage = (LinearLayout) findViewById(R.id.trsml_deleteMessage);
        trsml_deleteMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Remove trash element
                String ric = "";
                for(int i3 = 0; i3 < TrashContent.size(); ++i3) {
                    if(TrashContent.get(i3).isChecked())
                    {
                        if(i3==0)
                        {
                            ric = ric.concat(TrashContent.get(i3).getId()+"");
                        }
                        else
                        {
                            ric = ric.concat(","+TrashContent.get(i3).getId()+"");
                        }
                    }
                }
                RemoveElement(ric,UserConstants.MESSAGE_TYPE_TRASH);
            }
        });

        trsml_move_to_inbox = (LinearLayout) findViewById(R.id.trsml_move_to_inbox);
        trsml_move_to_inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ric = "";
                for(int i3 = 0; i3 < TrashContent.size(); ++i3) {
                    if(TrashContent.get(i3).isChecked())
                    {
                        if(i3==0)
                        {
                            ric = ric.concat(TrashContent.get(i3).getId()+"");
                        }
                        else
                        {
                            ric = ric.concat(","+TrashContent.get(i3).getId()+"");
                        }
                    }
                }
                MoveToInbox(ric);
            }
        });

        TrashRecyclerView = (ObservableRecyclerView) findViewById(R.id.TrashRecyclerView);
        ObservableRecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        TrashRecyclerView.setLayoutManager(mLayoutManager);
        Trash_rvListAdapter = new MessageAdapter_T1(TrashContent, new MessageAdapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(Message item) {
                Bundle args = new Bundle();
                args.putLong(GeneralConstants.MESSAGE_ID, item.getId());
                args.putInt(UserConstants.MESSAGE_TYPE, UserConstants.MESSAGE_TYPE_TRASH);
                FragmentReplace(new ReadMail(), args);
                readMailHitted = true;
            }

            @Override
            public void onItemChecked(Message item, int position, boolean ifChecked)
            {
                TrashContent.get(position).setChecked(ifChecked);
                UpdateTotalSelectedMessage(TrashContent);
            }
        });
        TrashRecyclerView.setAdapter(Trash_rvListAdapter);

        Trash_swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.Trash_swipeRefreshLayout);
        Trash_swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshTrash();
            }
        });

        refreshTrash();
    }

    void refreshTrash()
    {
        Messaging.RetrieveTrashMessage(new Messaging.MessageList_Retrieved() {
            @Override
            public void Success(List<Message> message) {
                clearTrashRVData();
                TrashContent.addAll(message);
                Trash_rvListAdapter.notifyDataSetChanged();

                if(TrashContent.size()>0){
                    findViewById(R.id.trash_email_move_and_delete_button).setVisibility(View.VISIBLE);
                }


                if(Trash_swipeRefreshLayout != null)
                {
                    Trash_swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void TokenRefreshRequired() {
                Messaging.RetrieveTrashMessage(new Messaging.MessageList_Retrieved() {
                    @Override
                    public void Success(List<Message> message) {
                        clearTrashRVData();
                        TrashContent.addAll(message);
                        Trash_rvListAdapter.notifyDataSetChanged();

                        if(Trash_swipeRefreshLayout != null)
                        {
                            Trash_swipeRefreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void FragmentReplace(Fragment fragment, Bundle arg)
    {
        findViewById(R.id.frag_content).setVisibility(View.VISIBLE);
        fragment.setArguments(arg);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frag_content, fragment);
        fragmentTransaction.commit();
    }

    int ArtistId;
    String ArtistName;

    private void initMessageFromBundle() {
        Bundle data = getIntent().getExtras();
        if(data != null)
        {
            ArtistName = data.getString(GeneralConstants.RECEIVER_NAME);
            ArtistId = data.getInt(GeneralConstants.RECEIVER_ID);
            writeMailHitted = true;
            Bundle args = new Bundle();
            args.putLong(GeneralConstants.RECEIVER_ID, ArtistId);
            args.putString(GeneralConstants.RECEIVER_NAME, ArtistName);
            FragmentReplace(new WriteMail(), args);
        }
    }

    @Override
    public void onBackPressed()
    {
        /*if(readMailHitted)
        {
            readMailHitted = false;
            findViewById(R.id.frag_content).setVisibility(View.GONE);
        }else{
            if(writeMailHitted)
            {
                writeMailHitted = false;
                findViewById(R.id.frag_content).setVisibility(View.GONE);
            }else{
                finish();
            }
        }*/
        finish();
    }

    public void clearInboxRVData()
    {
        int size = InboxContent.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                InboxContent.remove(0);
            }
            Inbox_rvListAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    public void clearSentRVData()
    {
        int size = SentContent.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                SentContent.remove(0);
            }
            Sent_rvListAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    public void clearTrashRVData()
    {
        int size = TrashContent.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                TrashContent.remove(0);
            }
            Trash_rvListAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void updateSentMails() {
        refreshSent();
    }

    @Override
    public void updateInbox() {
        refreshInbox();
    }

    @Override
    public void replyAgainstMessage(long message_id, int type)
    {
        Bundle args = new Bundle();
        args.putLong(GeneralConstants.MESSAGE_ID, message_id);
        args.putInt(UserConstants.MESSAGE_TYPE, type);
        FragmentReplace(new ReplyMail(), args);
    }

    @Override
    public void trashMessage(long message_id, int type)
    {
        TrashElement(String.valueOf(message_id), type);
    }

    @Override
    public void removeMessage(long message_id, int type) {
        RemoveElement(String.valueOf(message_id), type);
    }

    @Override
    public void readMail_topBackPressed() {
        findViewById(R.id.frag_content).setVisibility(View.GONE);
    }

    @Override
    public void writeMail_topBackPressed() {
        findViewById(R.id.frag_content).setVisibility(View.GONE);
    }

    @Override
    public void exit() {
        finish();
    }
}
