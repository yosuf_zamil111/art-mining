package com.kona.artmining.UI.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Art;
import com.kona.artmining.Interface.CreateEditArtActivity_Interface;
import com.kona.artmining.Model.ArtImages_T1;
import com.kona.artmining.Model.Art_T1;
import com.kona.artmining.Model.SearchListItem;
import com.kona.artmining.R;
import com.kona.artmining.custom_popup_box.ScrapBook_Picker;
import com.kona.artmining.slide_show_builder.Animations.DescriptionAnimation;
import com.kona.artmining.slide_show_builder.SliderLayout;
import com.kona.artmining.slide_show_builder.SliderTypes.BaseSliderView;
import com.kona.artmining.slide_show_builder.SliderTypes.TextSliderView;
import com.kona.artmining.slide_show_builder.Tricks.ViewPagerEx;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;

import static com.kona.artmining.slide_show_builder.Indicators.PagerIndicator.IndicatorVisibility.Invisible;

public class ArtDetailsActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, CreateEditArtActivity_Interface {

    LinearLayout item_details_Btt;

    private SliderLayout mDemoSlider;
    TextView sliderCurrentStatus;
    ImageView shareBtt, addToScrapBtt, singleImageSlider;
    int ExistingArtID = 0;
    ProgressDialog progress;
    Art_T1 ExistingArt;
    Button moreBtt;
    int bp = 0;
    int currentSliderPosition = 0;

    String TAG = ArtDetailsActivity.class.getName();

    ScrapBook_Picker SP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);


        item_details_Btt = (LinearLayout) findViewById(R.id.item_details_Btt);
        item_details_Btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getWindow().getContext(), "Back", Toast.LENGTH_LONG).show();
                finish();
            }
        });

        initMessageFromBundle();

        mDemoSlider = (SliderLayout)findViewById(R.id.slider);
        sliderCurrentStatus = (TextView) findViewById(R.id.sliderCurrentStatus);
        singleImageSlider = (ImageView) findViewById(R.id.singleImageSlider);
        moreBtt = (Button) findViewById(R.id.moreBtt);

        shareBtt = (ImageView)findViewById(R.id.shareBtt);
        addToScrapBtt = (ImageView)findViewById(R.id.addToScrapBtt);

        mDemoSlider.setIndicatorVisibility(Invisible);
        //mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        //mDemoSlider.setDuration(4000);
        mDemoSlider.stopAutoCycle();
        mDemoSlider.addOnPageChangeListener(this);

        shareBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ShareUrl();
            }
        });

        if(!Prefs.contains(UserConstants.AccessToken)){
            addToScrapBtt.setVisibility(View.GONE);
        }

        addToScrapBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(ExistingArt!=null)
                {
                    if(ExistingArt.getImages().size()>0)
                    {
                        SP = new ScrapBook_Picker(getWindow().getContext());
                        SP.setImage(ExistingArt.getImages().get(currentSliderPosition).getMain_image());
                        SP.setListener(new ScrapBook_Picker.task() {
                            @Override
                            public void done() {
                                AddArtToScrapBook();
                            }
                        });
                        SP.show();
                    }
                }
            }
        });

        moreBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mDemoSlider.stopAutoCycle();

                setResult(Activity.RESULT_OK,new Intent().putExtra(GeneralConstants.POSITION, bp).putExtra(GeneralConstants.ID, ExistingArtID));
                finish();
                System.out.println("------> Hi");
            }
        });
    }

    private void initMessageFromBundle()
    {
        Bundle data = getIntent().getExtras();
        if (data != null)
        {
            ExistingArtID = data.getInt(GeneralConstants.ID);
            bp = data.getInt(GeneralConstants.POSITION);
            LoadTheDetails();
        }
    }

    void LoadTheDetails()
    {
        ShowProgressDialogue();
        Art.GetArtDetails(ExistingArtID + "", new Art.ArtDetailsReceived() {
            @Override
            public void Success(Art_T1 itemData) {
                HideProgressDialogue();
                ExistingArt = itemData;
                prepareSlider();
            }

            @Override
            public void NotFound() {
                HideProgressDialogue();
            }

            @Override
            public void Error(String message) {
                HideProgressDialogue();
                Log.e(TAG, message);
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void PickedArtist(SearchListItem selectedArtist) {

    }

    @Override
    public void CloseFragment() {

    }

    @Override
    public void ShowProgressDialogue()
    {
        if(progress == null)
        {
            progress = ProgressDialog.show(getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);
        }
    }

    @Override
    public void HideProgressDialogue()
    {
        if(progress != null)
        {
            progress.dismiss();
            progress = null;
        }
    }

    void ShareUrl()
    {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL");
        i.putExtra(Intent.EXTRA_TEXT, (ServerConstants.BASE_URL+"art/details/"+ExistingArt.getArt_id()));
        startActivity(Intent.createChooser(i, "Share URL"));
    }

    void AddArtToScrapBook()
    {
        ShowProgressDialogue();
        Art.AddArtToScrapBook(ExistingArt.getImages().get(currentSliderPosition).getArt_image_id(), new Art.response() {
            @Override
            public void Success() {
                HideProgressDialogue();
                Toast.makeText(getWindow().getContext(), "Art Image Scraped!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void NotFound() {
                HideProgressDialogue();
            }

            @Override
            public void TokenRefreshRequired() {
                Art.AddArtToScrapBook(ExistingArt.getImages().get(currentSliderPosition).getArt_image_id(), new Art.response() {
                    @Override
                    public void Success() {
                        HideProgressDialogue();
                        Toast.makeText(getWindow().getContext(), "Art Image Scraped!", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void NotFound() {
                        HideProgressDialogue();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message)
                    {
                        HideProgressDialogue();
                        Log.e(TAG, message);
                        Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                HideProgressDialogue();
                Log.e(TAG, message);
                Toast.makeText(getWindow().getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void prepareSlider()
    {

        if(ExistingArt.getImages().size()<2)
        {
            sliderCurrentStatus.setVisibility(View.GONE);

            mDemoSlider.setVisibility(View.GONE);
            singleImageSlider.setVisibility(View.VISIBLE);

            ShowProgressDialogue();
            Picasso.with(getWindow().getContext())
                    .load(ExistingArt.getImages().get(0).getMain_image())
                    .placeholder(R.drawable.transparent)
                    .error(R.drawable.transparent)
                    .fit().centerInside()
                    .into(singleImageSlider, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            HideProgressDialogue();
                        }

                        @Override
                        public void onError() {
                            HideProgressDialogue();
                        }
                    });
        }
        else
        {
            sliderCurrentStatus.setVisibility(View.VISIBLE);
            mDemoSlider.setVisibility(View.VISIBLE);
            singleImageSlider.setVisibility(View.GONE);
        }

        for(ArtImages_T1 artImages_t1 : ExistingArt.getImages()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView.image(artImages_t1.getMain_image_watermarked()).setScaleType(BaseSliderView.ScaleType.FitCenterCrop).setOnSliderClickListener(this);

            System.out.println("-> "+artImages_t1.getMain_image());

            mDemoSlider.addSlider(textSliderView);
        }


        /*
        HashMap<String,String> url_maps = new HashMap<String, String>();
        url_maps.put("Hannibal", "https://s3-us-west-2.amazonaws.com/art-mining/582db6f8d2a55.png");
        url_maps.put("Big Bang Theory", "https://s3-us-west-2.amazonaws.com/art-mining/582db1806fc63.png");
        url_maps.put("House of Cards", "https://s3-us-west-2.amazonaws.com/art-mining/582dafdb411fb.jpg");
        url_maps.put("Game of Thrones", "https://s3-us-west-2.amazonaws.com/art-mining/582dade766365.jpg");

        for(String name : url_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }
        */
    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider)
    {
        //Toast.makeText(this,slider.getBundle().get("extra") + "",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
        sliderCurrentStatus.setText((position+1)+"/"+mDemoSlider.getTotalSliderNumber());
        currentSliderPosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {}
}
