package com.kona.artmining.UI.FragmentActivity;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ObservableWebView;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.kona.artmining.Adapter.ImageListingAdapter_T1;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Dashboard;
import com.kona.artmining.Interface.ActivityCommunicator;
import com.kona.artmining.Interface.FragmentCommunicator;
import com.kona.artmining.Interface.TheJavascriptInterface;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Listener.OnVerticalScrollListener;
import com.kona.artmining.Model.ImageList_T1;
import com.kona.artmining.R;
import com.kona.artmining.UI.Activity.CreateEditAnArt_Activity;
import com.kona.artmining.UI.Activity.DashboardActivity;
import com.kona.artmining.custom_components.Spinner_T3;
import com.kona.artmining.custom_components.Spinner_T4;
import com.kona.artmining.model.RadioButtonInfo;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

public class DashboardFrgGalleryActivity extends Fragment implements FragmentCommunicator
{

    public static final String ARG_INITIAL_POSITION = "ARG_INITIAL_POSITION";

    FragmentActivity me;
    ActivityCommunicator ac;
    ObservableRecyclerView recyclerView;
    ImageListingAdapter_T1 rvListAdapter;
    private List<ImageList_T1> Content = new ArrayList<ImageList_T1>();
    boolean hideHeader = false;
    TheJavascriptInterface JI;

    LinearLayout htmlLayoutContainer;
    ObservableWebView htmlLayout;

    SwipeRefreshLayout swipeRefreshLayout;

    int _ArtistId, _ThemeID, _SortType, _include_soldout;

    public DashboardFrgGalleryActivity() {}

    public static DashboardFrgGalleryActivity newInstance(int param1) {
        DashboardFrgGalleryActivity fragment = new DashboardFrgGalleryActivity();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        me=getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ui_dashboard_frgmnts_gallery, container, false);
        PrepareStage(view);
        UpdatePage(_ArtistId,_ThemeID, _SortType, _include_soldout);

        return view;
    }

    void PrepareStage(View container)
    {
        _ArtistId = 0;
        _ThemeID = 0;

        JI = new TheJavascriptInterface(getActivity());
        htmlLayout = (ObservableWebView) container.findViewById(R.id.webview_frg_v2);
        htmlLayoutContainer  = (LinearLayout) container.findViewById(R.id.webview_frg_v2_container);
        htmlLayoutContainer.setVisibility(View.GONE);
        WebSettings webSettings = htmlLayout.getSettings();
        webSettings.setJavaScriptEnabled(true);
        htmlLayout.addJavascriptInterface(JI, "Android");
        htmlLayout.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //htmlLayout.addJavascriptInterface(JI, "Android");
                view.loadUrl(url);
                return false;
            }
        });
        htmlLayout.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                ac.ShowProgressDialogue();

                System.out.println("Progress status: "+progress);

                if(progress == 100)
                {
                    ac.HideProgressDialogue();
                    //htmlLayout.clearCache(true);
                }
            }
        });
        JI.setListener(new TheJavascriptInterface.listener() {
            @Override
            public void message(String m) {
                if(m.equals("closeWebView"))
                {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            htmlLayoutContainer.setVisibility(View.GONE);
                        }
                    });
                }
                else if(m.equals("updateTheCart"))
                {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ac.UpdateTheCart();
                        }
                    });
                }
            }

            @Override
            public void showDetailsPage(String art_id) {

            }
        });

        recyclerView = (ObservableRecyclerView) container.findViewById(R.id.recycler_view);
        GridLayoutManager layoutManager = new GridLayoutManager(me.getWindow().getContext(), 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(position == 0 || position == 1)
                {
                    return 2;
                }else{
                    return 1;
                }
            }
        });
        recyclerView.setLayoutManager(layoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) container.findViewById(R.id.swipeRefreshLayout);

        rvListAdapter = new ImageListingAdapter_T1(Content, true, new ImageListingAdapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(ImageList_T1 item) {

                //htmlLayoutContainer.setVisibility(View.VISIBLE);
                //htmlLayout.loadUrl(ServerConstants.BASE_URL + "art/infos/" + item.getArt_id() + "?mobile_view=1&access_token=" + Prefs.getString(UserConstants.AccessToken, null));
                //recyclerView.scrollVerticallyToPosition(0);

                //ac.loadWebPage(ServerConstants.BASE_URL + "art/infos/" + item.getArt_id() + "?mobile_view=1&access_token=" + Prefs.getString(UserConstants.AccessToken, null));
                ac.showArtDetailsPage(item.getArt_id());

            }
        }, new ImageListingAdapter_T1.OnSpinnerSelectListener() {
            @Override
            public void onItemClick(int ArtistId, int ThemeID, int type, int sortType, boolean ifSoldOutChecked)
            {
                _ArtistId = ArtistId;
                _ThemeID = ThemeID;
                _SortType = sortType;
                if(ifSoldOutChecked)
                {
                    _include_soldout = 1;
                }
                else
                {
                    _include_soldout = 0;
                }

                UpdatePage(_ArtistId,_ThemeID, _SortType, _include_soldout);

                /*Dashboard.RetrieveGalleryData((ArtistId+""), (ThemeID+""), new Dashboard.ImageList_Retrieved() {
                    @Override
                    public void Success(List<ImageList_T1> data) {
                        clearRVData();
                        Content.addAll(data);
                        if(Content.size() < 10)
                        {
                            AddBlankDataToContent(10 - Content.size());
                        }
                        rvListAdapter.notifyDataSetChanged();
                        //https://github.com/cymcsg/UltimateRecyclerView/issues/233
                        //recyclerView.getLayoutManager().scrollToPosition(50);
                        //FillUpData();
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getContext(),message, Toast.LENGTH_LONG).show();
                    }
                });*/
            }

            @Override
            public void showArtUploader() {
                Intent intent = new Intent(getActivity(), CreateEditAnArt_Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivityForResult(intent, 555);
            }
        });
        recyclerView.setAdapter(rvListAdapter);

        Activity parentActivity = getActivity();
        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            // Scroll to the specified offset after layout

            Bundle args = getArguments();
            if (args != null && args.containsKey(ARG_INITIAL_POSITION)) {

                final int initialPosition = args.getInt(ARG_INITIAL_POSITION, 0);

                if(initialPosition > 0)
                {
                    hideHeader = true;
                }

                System.out.println("args ----------"+initialPosition+"--------------- >"+args.size());
                /*ScrollUtils.addOnGlobalLayoutListener(recyclerView, new Runnable() {
                    @Override
                    public void run() {
                        recyclerView.scrollVerticallyToPosition(initialPosition);
                    }
                });*/
            }

            /*ScrollUtils.addOnGlobalLayoutListener(recyclerView, new Runnable() {
                @Override
                public void run() {
                    recyclerView.scrollVerticallyToPosition(1);
                }
            });*/

            recyclerView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.findViewById(R.id.root));
            recyclerView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }


        swipeRefreshLayout.setProgressViewOffset(false,100,200);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                UpdatePage(_ArtistId,_ThemeID, _SortType, _include_soldout);
            }
        });





    }

    void UpdatePage(int artistId, int themeId, int sortType, int include_soldout)
    {
        Dashboard.RetrieveGalleryData((""+artistId), (""+themeId), sortType, include_soldout, new Dashboard.ImageList_Retrieved() {
            @Override
            public void Success(List<ImageList_T1> data) {
                clearRVData();
                Content.addAll(data);
                if(Content.size() < 6)
                {
                    AddBlankDataToContent(6 - Content.size());
                }
                rvListAdapter.notifyDataSetChanged();

                if(swipeRefreshLayout != null)
                {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(),message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void AddBlankDataToContent(int number)
    {
        for(int x = 0; x < number; x = x+1) {

            ImageList_T1 IT1 = new ImageList_T1();

            IT1.setClickable(false);
            Content.add(IT1);
        }
    }

    public void clearRVData()
    {
        int size = Content.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Content.remove(0);
            }
            rvListAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void passDataToFragment(String someValue) {
        System.out.println("Frag Data "+someValue);

        if(Integer.valueOf(someValue) > 0)
        {
            ScrollUtils.addOnGlobalLayoutListener(recyclerView, new Runnable() {
                @Override
                public void run() {
                    recyclerView.scrollVerticallyToPosition(1);
                }
            });
        }
    }

    @Override
    public void reloadThePage() {
        rvListAdapter.notifyDataSetChanged();
    }

    @Override
    public void _DevicebackPressed()
    {
        htmlLayout.loadUrl("about:blank");
        htmlLayoutContainer.setVisibility(View.GONE);
    }

    @Override
    public boolean ifDetailsPageHiden() {

        if(htmlLayoutContainer.getVisibility() == View.VISIBLE)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    @Override
    public void showDetails(int id)
    {
        htmlLayoutContainer.setVisibility(View.VISIBLE);
        htmlLayout.loadUrl(ServerConstants.BASE_URL + "art/details/" + id + "?mobile_view=1&access_token=" + Prefs.getString(UserConstants.AccessToken, null));
        recyclerView.scrollVerticallyToPosition(0);
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        DashboardActivity DA = (DashboardActivity) getActivity();
        DA.fragmentCommunicator = this;
        ac = (ActivityCommunicator) getActivity();
    }
}
