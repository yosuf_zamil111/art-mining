package com.kona.artmining.UI.FragmentActivity;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kona.artmining.Adapter.DashboardSearchListingAdapter;
import com.kona.artmining.Helper.Dashboard;
import com.kona.artmining.Interface.CreateEditArtActivity_Interface;
import com.kona.artmining.Model.SearchListItem;
import com.kona.artmining.R;

import java.util.ArrayList;
import java.util.List;


public class frg_CreateEditAnArt_Activity_SearchArtist extends Fragment {

    CreateEditArtActivity_Interface mCallback;
    RecyclerView SearchResultPanelContents;
    List<SearchListItem> artistSearchResult;
    DashboardSearchListingAdapter mDashboardSearchListingAdapter;

    LinearLayout backMe;
    EditText SearchBar;

    public frg_CreateEditAnArt_Activity_SearchArtist(){}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ui_create_edit_art_activity__artist_search_page, container, false);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try {
            mCallback = (CreateEditArtActivity_Interface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement CreateEditArtActivity_Interface");
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStage(view);
    }

    void prepareStage(View view)
    {
        artistSearchResult = new ArrayList<>();

        backMe = (LinearLayout) view.findViewById(R.id.backMe);
        backMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.CloseFragment();
            }
        });

        SearchBar = (EditText) view.findViewById(R.id.SearchBar);
        SearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Search(s.toString());
            }
        });

        SearchResultPanelContents = (RecyclerView) view.findViewById(R.id.SearchResultPanelContents);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        SearchResultPanelContents.setLayoutManager(mLayoutManager);

        mDashboardSearchListingAdapter = new DashboardSearchListingAdapter(artistSearchResult, new DashboardSearchListingAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final SearchListItem item) {
                /*
                tsbb_t1.resetSearchBox();
                Toast.makeText(DashboardActivity.this.getWindow().getContext(),"Wait for a while....", Toast.LENGTH_SHORT).show();
                UserAuthentication.CheckIfAdmin(new UserAuthentication.message() {
                    @Override
                    public void Success(String message) {
                        Intent intent = new Intent(DashboardActivity.this, ArtistActivity.class);
                        intent.putExtra("artist_id", item.getID());
                        DashboardActivity.this.startActivityForResult(intent, ACTIVITY_ARTIST);
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(DashboardActivity.this.getWindow().getContext(),message, Toast.LENGTH_LONG).show();
                    }
                });
                */
                mCallback.PickedArtist(item);
            }
        });

        SearchResultPanelContents.setAdapter(mDashboardSearchListingAdapter);

        Dashboard.SearchArtist("a", new Dashboard.SearchResult_Retrieved() {
            @Override
            public void Success(List<SearchListItem> dta) {
                clearDashboardSearchList();
                artistSearchResult.addAll(dta);
                mDashboardSearchListingAdapter.notifyDataSetChanged();
                mCallback.HideProgressDialogue();
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(),message, Toast.LENGTH_LONG).show();

                clearDashboardSearchList();
                mDashboardSearchListingAdapter.notifyDataSetChanged();
                mCallback.HideProgressDialogue();
            }

            @Override
            public void EmptyKeywordFound() {
                clearDashboardSearchList();
                mDashboardSearchListingAdapter.notifyDataSetChanged();
                mCallback.HideProgressDialogue();
            }
        });
    }

    void Search(String keyword)
    {
        mCallback.ShowProgressDialogue();
        Dashboard.SearchArtist(keyword, new Dashboard.SearchResult_Retrieved() {
            @Override
            public void Success(List<SearchListItem> dta) {
                clearDashboardSearchList();
                artistSearchResult.addAll(dta);
                mDashboardSearchListingAdapter.notifyDataSetChanged();
                mCallback.HideProgressDialogue();
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(),message, Toast.LENGTH_LONG).show();

                clearDashboardSearchList();
                mDashboardSearchListingAdapter.notifyDataSetChanged();
                mCallback.HideProgressDialogue();
            }

            @Override
            public void EmptyKeywordFound() {
                clearDashboardSearchList();
                mDashboardSearchListingAdapter.notifyDataSetChanged();
                mCallback.HideProgressDialogue();
            }
        });
    }

    public void clearDashboardSearchList()
    {
        int size = artistSearchResult.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                artistSearchResult.remove(0);
            }
            mDashboardSearchListingAdapter.notifyItemRangeRemoved(0, size);
        }
    }
}
