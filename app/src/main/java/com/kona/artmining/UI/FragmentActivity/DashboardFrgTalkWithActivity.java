package com.kona.artmining.UI.FragmentActivity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.kona.artmining.Adapter.ImageListingWithDetailsAdapter_T2;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Dashboard;
import com.kona.artmining.Interface.ActivityCommunicator;
import com.kona.artmining.Interface.FragmentCommunicator;
import com.kona.artmining.Interface.FragmentCommunicator_TalkWith;
import com.kona.artmining.Interface.TheJavascriptInterface;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.Artist_T1;
import com.kona.artmining.Model.ImageList_T1;
import com.kona.artmining.Model.SearchListItem;
import com.kona.artmining.R;
import com.kona.artmining.UI.Activity.ArtDetailsActivity;
import com.kona.artmining.UI.Activity.DashboardActivity;
import com.kona.artmining.UI.Activity.MessagingActivity;
import com.pixplicity.easyprefs.library.Prefs;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DashboardFrgTalkWithActivity extends Fragment implements FragmentCommunicator_TalkWith {

    public static final String ARG_INITIAL_POSITION = "ARG_INITIAL_POSITION";

    FrameLayout see_more_less_button_container;

    FragmentActivity me;
    ActivityCommunicator ac;
    ObservableRecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    ImageListingWithDetailsAdapter_T2 rvListAdapter;
    private List<ImageList_T1> Content = new ArrayList<ImageList_T1>();
    boolean hideHeader = false;

    LinearLayout htmlLayoutContainer,talkWithDetailsPage;
    WebView htmlLayout;
    ObservableScrollView talkWithDetailsPageScroll;

    int _ArtistId, _ThemeID;

    // Details Page
    ImageView talkDetailsPg_BgImage, talkDetailsPg_doMessage;
    CircularImageView artistImage;
    TextView talkDetailsPg_about, talkDetailsPg_profileDetails, talkDetailsPg_ArtistName, talkDetailsPg_ArtistEmail;
    LinearLayout talkDetailsPg_seeMoreBtt, talkDetailsPg_seeLessBtt, talkDetailsPg_aboutHolder;
    Artist_T1 SelectedArtist;
    ImageView talkDetailsPg_img1, talkDetailsPg_img2, talkDetailsPg_img3, talkDetailsPg_img4;

    int talkDetailsPg_aboutHolder_H;

    static final int ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE = 400;

    public DashboardFrgTalkWithActivity() {
        // Required empty public constructor
    }

    public static DashboardFrgTalkWithActivity newInstance(int param1) {
        DashboardFrgTalkWithActivity fragment = new DashboardFrgTalkWithActivity();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ui_dashboard_frgmnts_talk_with, container, false);
        PrepareStage(view);
        PrepareDetailsPage(view);

        UpdatePage(_ArtistId,_ThemeID);

        return view;
    }

    void PrepareStage(View container)
    {
        see_more_less_button_container = (FrameLayout) container.findViewById(R.id.see_more_less_button_container);


        _ArtistId = 0;
        _ThemeID = 0;

        TheJavascriptInterface JI = new TheJavascriptInterface(getActivity());
        htmlLayout = (WebView) container.findViewById(R.id.fullscreen_webview_container);
        htmlLayoutContainer  = (LinearLayout) container.findViewById(R.id.fullscreen_webview);
        htmlLayout.addJavascriptInterface(JI, "Android");
        WebSettings webSettings = htmlLayout.getSettings();
        webSettings.setJavaScriptEnabled(true);

        container.findViewById(R.id.BackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                htmlLayoutContainer.setVisibility(View.GONE);
            }
        });

        recyclerView = (ObservableRecyclerView) container.findViewById(R.id.recycler_view);
        ObservableRecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        swipeRefreshLayout = (SwipeRefreshLayout) container.findViewById(R.id.swipeRefreshLayout);

        rvListAdapter = new ImageListingWithDetailsAdapter_T2(Content, true, new ImageListingWithDetailsAdapter_T2.OnItemClickListener() {
            @Override
            public void onItemClick(ImageList_T1 item) {
                talkWithDetailsPage.setVisibility(View.VISIBLE);
                ac.ShowParentToolbar();
                LoadDetailsPage(item.getArtist_id()+"");
            }

            @Override
            public void sendMessage(ImageList_T1 item) {
                if (Prefs.contains(UserConstants.AccessToken)) {
                    SendAMessage(item.getArtist_id(), item.getTitle_en());
                } else {
                    ac.openLoginPage();
                }
            }
        }, new ImageListingWithDetailsAdapter_T2.OnSpinnerSelectListener() {
            @Override
            public void onItemClick(int ArtistId, int ThemeID, int type) {
                _ArtistId = ArtistId;
                _ThemeID = ThemeID;
                UpdatePage(_ArtistId,_ThemeID);
            }
        });
        recyclerView.setAdapter(rvListAdapter);

        Activity parentActivity = getActivity();
        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            // Scroll to the specified offset after layout

            Bundle args = getArguments();
            if (args != null && args.containsKey(ARG_INITIAL_POSITION)) {

                final int initialPosition = args.getInt(ARG_INITIAL_POSITION, 0);

                if(initialPosition > 0)
                {
                    hideHeader = true;
                }

                System.out.println("args ----------"+initialPosition+"--------------- >"+args.size());
            }

            recyclerView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.findViewById(R.id.root));
            recyclerView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }

        swipeRefreshLayout.setProgressViewOffset(false,100,200);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                UpdatePage(_ArtistId,_ThemeID);
            }
        });
    }

    void PrepareDetailsPage(View container)
    {
        talkWithDetailsPage = (LinearLayout) container.findViewById(R.id.talkWithDetailsPage);
        talkWithDetailsPage.setVisibility(View.GONE);

        talkDetailsPg_BgImage =  (ImageView) container.findViewById(R.id.talkDetailsPg_BgImage);
        talkDetailsPg_doMessage =  (ImageView) container.findViewById(R.id.talkDetailsPg_doMessage);
        talkDetailsPg_doMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Prefs.contains(UserConstants.AccessToken)) {
                    SendAMessage(SelectedArtist.getId(), SelectedArtist.getName_en());
                } else {
                    ac.openLoginPage();
                }
            }
        });
        artistImage = (CircularImageView) container.findViewById(R.id.artistImage);

        talkDetailsPg_about =  (TextView) container.findViewById(R.id.talkDetailsPg_about);
        talkDetailsPg_profileDetails =  (TextView) container.findViewById(R.id.talkDetailsPg_profileDetails);

        talkDetailsPg_ArtistName =  (TextView) container.findViewById(R.id.talkDetailsPg_ArtistName);
        talkDetailsPg_ArtistEmail =  (TextView) container.findViewById(R.id.talkDetailsPg_ArtistEmail);

        talkDetailsPg_aboutHolder = (LinearLayout) container.findViewById(R.id.talkDetailsPg_aboutHolder);

        talkDetailsPg_aboutHolder_H = talkDetailsPg_aboutHolder.getLayoutParams().height;
        talkDetailsPg_seeMoreBtt =  (LinearLayout) container.findViewById(R.id.talkDetailsPg_seeMoreBtt);
        talkDetailsPg_seeMoreBtt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                talkDetailsPg_aboutHolder.getLayoutParams().height = talkDetailsPg_about.getLayout().getHeight()+50;
                talkDetailsPg_aboutHolder.requestLayout();
                talkDetailsPg_seeMoreBtt.setVisibility(View.GONE);
                talkDetailsPg_seeLessBtt.setVisibility(View.VISIBLE);
            }
        });

        talkDetailsPg_seeLessBtt  =  (LinearLayout) container.findViewById(R.id.talkDetailsPg_seeLessBtt);
        talkDetailsPg_seeLessBtt.setVisibility(View.GONE);
        talkDetailsPg_seeLessBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                talkDetailsPg_aboutHolder.getLayoutParams().height = talkDetailsPg_aboutHolder_H + 10;
                talkDetailsPg_aboutHolder.requestLayout();
                talkDetailsPg_seeMoreBtt.setVisibility(View.VISIBLE);
                talkDetailsPg_seeLessBtt.setVisibility(View.GONE);
            }
        });

        talkWithDetailsPageScroll =  (ObservableScrollView) container.findViewById(R.id.talkWithDetailsPageScroll);

        talkDetailsPg_img1 =  (ImageView) container.findViewById(R.id.talkDetailsPg_img1);
        talkDetailsPg_img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedArtist.getLatest_art().size()>0)
                {
                    /*Intent intent = new Intent(getActivity(), ArtDetailsActivity.class);
                    intent.putExtra(GeneralConstants.ID, SelectedArtist.getLatest_art_id().get(0));
                    intent.putExtra(GeneralConstants.POSITION, 0);
                    getActivity().startActivityForResult(intent, ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE);*/

                    talkWithDetailsPage.setVisibility(View.GONE);
                    ac.showArtDetailsPageWV(SelectedArtist.getLatest_art_id().get(0));
                }
            }
        });
        talkDetailsPg_img2 =  (ImageView) container.findViewById(R.id.talkDetailsPg_img2);
        talkDetailsPg_img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedArtist.getLatest_art().size()>1)
                {
                    /*Intent intent = new Intent(getActivity(), ArtDetailsActivity.class);
                    intent.putExtra(GeneralConstants.ID, SelectedArtist.getLatest_art_id().get(1));
                    intent.putExtra(GeneralConstants.POSITION, 0);
                    getActivity().startActivityForResult(intent, ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE);*/

                    talkWithDetailsPage.setVisibility(View.GONE);
                    ac.showArtDetailsPageWV(SelectedArtist.getLatest_art_id().get(1));
                }
            }
        });
        talkDetailsPg_img3 =  (ImageView) container.findViewById(R.id.talkDetailsPg_img3);
        talkDetailsPg_img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedArtist.getLatest_art().size()>2)
                {
                    /*
                    Intent intent = new Intent(getActivity(), ArtDetailsActivity.class);
                    intent.putExtra(GeneralConstants.ID, SelectedArtist.getLatest_art_id().get(2));
                    intent.putExtra(GeneralConstants.POSITION, 0);
                    getActivity().startActivityForResult(intent, ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE);
                    */
                    talkWithDetailsPage.setVisibility(View.GONE);
                    ac.showArtDetailsPageWV(SelectedArtist.getLatest_art_id().get(2));
                }
            }
        });
        talkDetailsPg_img4 =  (ImageView) container.findViewById(R.id.talkDetailsPg_img4);
        talkDetailsPg_img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedArtist.getLatest_art().size()>3)
                {
                    /*Intent intent = new Intent(getActivity(), ArtDetailsActivity.class);
                    intent.putExtra(GeneralConstants.ID, SelectedArtist.getLatest_art_id().get(3));
                    intent.putExtra(GeneralConstants.POSITION, 3);
                    getActivity().startActivityForResult(intent, ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE);*/
                    talkWithDetailsPage.setVisibility(View.GONE);
                    ac.showArtDetailsPageWV(SelectedArtist.getLatest_art_id().get(3));
                }
            }
        });

        Activity parentActivity = getActivity();
        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            // Scroll to the specified offset after layout
            talkWithDetailsPageScroll.setTouchInterceptionViewGroup((ViewGroup) parentActivity.findViewById(R.id.root));
            talkWithDetailsPageScroll.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }
    }

    void LoadDetailsPage(String artistID)
    {
        ac.ShowProgressDialogue();
        Dashboard.GetArtistDetails(artistID, new Dashboard.ArtistDetails_Retrieved() {
            @Override
            public void Success(Artist_T1 artist) {
                SelectedArtist = artist;
                PopulateDetailsPage();
            }

            @Override
            public void Error(String message)
            {

            }
        });
    }

    void PopulateDetailsPage()
    {

        talkDetailsPg_img1.setImageResource(R.drawable.img_photo_01_list);
        talkDetailsPg_img2.setImageResource(R.drawable.img_photo_01_list);
        talkDetailsPg_img3.setImageResource(R.drawable.img_photo_01_list);
        talkDetailsPg_img4.setImageResource(R.drawable.img_photo_01_list);

        if(SelectedArtist.getCover_image() != null)
        {
            Picasso.with(getContext())
                    .load(SelectedArtist.getCover_image())
                    .placeholder(R.drawable.transparent)
                    .error(R.drawable.transparent)
                    .into(talkDetailsPg_BgImage);
        }else{
            if(SelectedArtist.getLatest_art().size()>0)
            {
                Picasso.with(getContext())
                        .load(SelectedArtist.getLatest_art().get(0))
                        .placeholder(R.drawable.transparent)
                        .error(R.drawable.transparent)
                        .into(talkDetailsPg_BgImage);
            }
            else
            {
                Picasso.with(getContext())
                        .load(R.drawable.transparent)
                        .placeholder(R.drawable.transparent)
                        .error(R.drawable.transparent)
                        .into(talkDetailsPg_BgImage);
            }
        }

        if(SelectedArtist.getLatest_art().size()>0)
        {
            Picasso.with(getContext())
                    .load(SelectedArtist.getLatest_art().get(0))
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .fit()
                    .into(talkDetailsPg_img1);
        }

        if(SelectedArtist.getLatest_art().size()>1)
        {
            Picasso.with(getContext())
                    .load(SelectedArtist.getLatest_art().get(1))
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .fit()
                    .into(talkDetailsPg_img2);
        }

        if(SelectedArtist.getLatest_art().size()>2)
        {
            Picasso.with(getContext())
                    .load(SelectedArtist.getLatest_art().get(2))
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .fit()
                    .into(talkDetailsPg_img3);
        }

        if(SelectedArtist.getLatest_art().size()>3)
        {
            Picasso.with(getContext())
                    .load(SelectedArtist.getLatest_art().get(3))
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .fit()
                    .into(talkDetailsPg_img4);
        }

        Picasso.with(getContext())
                .load(SelectedArtist.getProfilePicture())
                .placeholder(R.drawable.icon_profile_large)
                .error(R.drawable.icon_profile_large)
                .fit()
                .into(artistImage);

        talkDetailsPg_ArtistName.setText(SelectedArtist.getName_en());
        talkDetailsPg_ArtistEmail.setText(SelectedArtist.getEmail());
        talkDetailsPg_about.setText(SelectedArtist.getAbout_en());

        if(SelectedArtist.getAbout_en() != null){
            if(SelectedArtist.getAbout_en().length() > 300){
                see_more_less_button_container.setVisibility(View.VISIBLE);
            }
            else {
                see_more_less_button_container.setVisibility(View.GONE);
            }
        }else {
            see_more_less_button_container.setVisibility(View.GONE);
        }

        talkDetailsPg_profileDetails.setText(SelectedArtist.getProfile_en());

        talkDetailsPg_aboutHolder_H = talkDetailsPg_aboutHolder.getLayoutParams().height;
        talkDetailsPg_seeMoreBtt.setVisibility(View.VISIBLE);
        talkDetailsPg_seeLessBtt.setVisibility(View.GONE);
        talkDetailsPg_aboutHolder.getLayoutParams().height = talkDetailsPg_aboutHolder_H + 10;
        talkDetailsPg_aboutHolder.requestLayout();

        ac.HideProgressDialogue();
    }



    void SendAMessage(int ArtistId, String ArtistName)
    {
        Intent myIntent = new Intent(getActivity(), MessagingActivity.class);
        myIntent.putExtra(GeneralConstants.RECEIVER_ID, ArtistId);
        myIntent.putExtra(GeneralConstants.RECEIVER_NAME, ArtistName);
        getActivity().startActivity(myIntent);
    }

    void UpdatePage(int artistId, int themeId)
    {
        Dashboard.RetrieveTalkWithData(String.valueOf(themeId), new Dashboard.ImageList_Retrieved() {
            @Override
            public void Success(List<ImageList_T1> data) {
                clearRVData();
                Content.addAll(data);
                if(Content.size() < 10)
                {
                    AddBlankDataToContent(10 - Content.size());
                }
                rvListAdapter.notifyDataSetChanged();

                if(swipeRefreshLayout != null)
                {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(),message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void AddBlankDataToContent(int number)
    {
        for(int x = 0; x < number; x = x+1) {

            ImageList_T1 IT1 = new ImageList_T1();

            IT1.setClickable(false);
            Content.add(IT1);
        }
    }

    public void clearRVData()
    {
        int size = Content.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Content.remove(0);
            }
            rvListAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        me=getActivity();
    }

    @Override
    public void passDataToFragment(String someValue) {
        System.out.println("Frag2 Data "+someValue);

        if(Integer.valueOf(someValue) > 0)
        {
            recyclerView.scrollVerticallyToPosition(1);
            ScrollUtils.addOnGlobalLayoutListener(recyclerView, new Runnable() {
                @Override
                public void run() {
                    recyclerView.scrollVerticallyToPosition(1);
                }
            });
        }
    }

    @Override
    public void _DevicebackPressed()
    {
        talkWithDetailsPage.setVisibility(View.GONE);
    }

    @Override
    public boolean ifDetailsPageHiden() {

        if(talkWithDetailsPage.getVisibility() == View.GONE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public void ShowUpArtist(String artistID) {
        talkWithDetailsPage.setVisibility(View.VISIBLE);
        ac.ShowParentToolbar();
        LoadDetailsPage(artistID);
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        DashboardActivity DA = (DashboardActivity) getActivity();
        DA.fc_talkWith = this;
        ac = (ActivityCommunicator) getActivity();
    }
}
