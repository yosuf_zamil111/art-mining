package com.kona.artmining.UI.FragmentActivity;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.kona.artmining.Adapter.DashboardFrgLatest_Reviews_Adapter_T1;
import com.kona.artmining.Helper.*;
import com.kona.artmining.Interface.UserActivity_FrgMng_Interface;
import com.kona.artmining.Interface.UserFrgMngCommunicator;
import com.kona.artmining.Model.DashboardFrgLatest_Reviews_Model_T1;
import com.kona.artmining.R;
import com.kona.artmining.UI.FragmentController.UserActivity_FragmentsManager;

import java.util.ArrayList;
import java.util.List;


public class UserFrgMyArtReviews extends Fragment implements UserFrgMngCommunicator {

    UserActivity_FrgMng_Interface mCallback;

    SwipeRefreshLayout swipeRefreshLayout;
    ObservableRecyclerView recycler_view;

    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    DashboardFrgLatest_Reviews_Adapter_T1 reviewsAdapter;
    private List<DashboardFrgLatest_Reviews_Model_T1> Content = new ArrayList<DashboardFrgLatest_Reviews_Model_T1>();

    ProgressDialog progress;

    public UserFrgMyArtReviews() {}

    public static UserFrgMyArtReviews newInstance(int param1) {
        UserFrgMyArtReviews fragment = new UserFrgMyArtReviews();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            mCallback = (UserActivity_FrgMng_Interface) activity;
            UserActivity_FragmentsManager DA = (UserActivity_FragmentsManager) getActivity();
            DA.frg_listener = this;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement UserActivity_FrgMng_Interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.user_frg_my_art_reviews, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStage(view);
    }

    void prepareStage(View view)
    {
        view.findViewById(R.id.backBtt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        recycler_view = (ObservableRecyclerView) view.findViewById(R.id.recycler_view);

        ObservableRecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(mLayoutManager);


        recycler_view.setHasFixedSize(true);
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recycler_view.setLayoutManager(gaggeredGridLayoutManager);

        reviewsAdapter = new DashboardFrgLatest_Reviews_Adapter_T1(Content, new DashboardFrgLatest_Reviews_Adapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(DashboardFrgLatest_Reviews_Model_T1 item)
            {

            }
        });

        recycler_view.setAdapter(reviewsAdapter);

        reviewsAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Populate();
            }
        });

        Populate();
    }

    void Populate()
    {
        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

        Artist.RetrieveArtistReviews(new Artist.artistReviewRetrieve() {
            @Override
            public void Success(List<DashboardFrgLatest_Reviews_Model_T1> reviews) {
                progress.dismiss();
                clearRVData();
                Content.addAll(reviews);

                reviewsAdapter.notifyDataSetChanged();

                if(swipeRefreshLayout != null)
                {
                    swipeRefreshLayout.setRefreshing(false);
                }

                if(Content.size()>0)
                {
                    getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.GONE);
                }else{
                    getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void TokenRefreshRequired() {
                Artist.RetrieveArtistReviews(new Artist.artistReviewRetrieve() {
                    @Override
                    public void Success(List<DashboardFrgLatest_Reviews_Model_T1> reviews) {
                        progress.dismiss();
                        clearRVData();
                        Content.addAll(reviews);

                        reviewsAdapter.notifyDataSetChanged();

                        if(swipeRefreshLayout != null)
                        {
                            swipeRefreshLayout.setRefreshing(false);
                        }

                        if(Content.size()>0)
                        {
                            getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.GONE);
                        }else{
                            getActivity().findViewById(R.id.recycler_viewPlaceHolder).setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void clearRVData()
    {
        int size = Content.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Content.remove(0);
            }
            reviewsAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void _DevicebackPressed() {

    }

    @Override
    public boolean ifDetailsPageHiden() {
        return true;
    }
}
