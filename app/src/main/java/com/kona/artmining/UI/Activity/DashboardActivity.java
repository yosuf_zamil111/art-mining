package com.kona.artmining.UI.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.github.ksoichiro.android.observablescrollview.Scrollable;
import com.google.gson.Gson;
import com.kona.artmining.Adapter.DashboardSearchListingAdapter;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Dashboard;
import com.kona.artmining.Helper.User;
import com.kona.artmining.Interface.ActivityCommunicator;
import com.kona.artmining.Interface.FragmentCommunicator;
import com.kona.artmining.Interface.FragmentCommunicator_TalkWith;
import com.kona.artmining.Interface.TheJavascriptInterface;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.SearchListItem;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.R;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgAuctionActivity;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgGalleryActivity;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgLatest;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs.DELF_Event_DetailsFrg;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs.DELF_Exhibitions_DetailsFrg;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs.DELF_Reviews_DetailsFrg;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgShopActivity;
import com.kona.artmining.UI.FragmentActivity.DashboardFrgTalkWithActivity;
import com.kona.artmining.UI.FragmentActivity.UserFrgShoppingCart;
import com.kona.artmining.UI.FragmentController.UserActivity_FragmentsManager;
import com.kona.artmining.custom_components.PagerSlidingTabStrip;
import com.kona.artmining.custom_components.TopSearchBarWithButtons_T1;
import com.kona.artmining.utils.KeyboardUtil;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;
import com.pixplicity.easyprefs.library.Prefs;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;
import net.hockeyapp.android.metrics.MetricsManager;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity implements ObservableScrollViewCallbacks, ActivityCommunicator {

    PagerSlidingTabStrip dashboard_pager_sts;
    ViewPager dashboard_pager;
    DashboardPagerAdapter adapter;
    TopSearchBarWithButtons_T1 tsbb_t1;
    FrameLayout frag_content;

    public FragmentCommunicator fragmentCommunicator;
    public FragmentCommunicator fragmentCommunicator2;
    public FragmentCommunicator fragmentCommunicator3;
    public FragmentCommunicator_TalkWith fc_talkWith;
    public FragmentCommunicator fc_latest;

    LinearLayout SearchResultPanel;
    RecyclerView SearchResultPanelContents;
    DashboardSearchListingAdapter mDashboardSearchListingAdapter;

    View mHeaderView;
    int mBaseTranslationY;

    String[] TabNames = { "Gallery", "Shop", "Auction", "Talk With", "Latest" };

    static final int ACTIVITY_USER_AUTHENTICATION = 100;
    static final int ACTIVITY_USER = 200;
    static final int ACTIVITY_ARTIST = 300;
    static final int ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE = 400;

    LinearLayout htmlLayoutContainer;
    LinearLayout htmlLayoutContainerTopHolder;
    WebView htmlLayout;
    TextView CartItemsCounter33;

    int screenTouched = 0;

    UserProfile currentUserProfile;

    ProgressDialog progress;

    List<SearchListItem> artistSearchResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        //HockeyApp();

        artistSearchResult = new ArrayList<>();

        frag_content = (FrameLayout) findViewById(R.id.frag_content);

        TheJavascriptInterface JI = new TheJavascriptInterface(this);
        htmlLayout = (WebView) findViewById(R.id.fullscreen_webview_container);
        htmlLayoutContainer  = (LinearLayout) findViewById(R.id.fullscreen_webview);
        htmlLayout.addJavascriptInterface(JI, "Android");
        WebSettings webSettings = htmlLayout.getSettings();
        webSettings.setJavaScriptEnabled(true);
        htmlLayout.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
            @Override
            public void onLoadResource(WebView view, String url) {
                // Notice Here.
                //ShowProgressDialogue();
                view.clearHistory();
                super.onLoadResource(view, url);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                // And Here.
                //HideProgressDialogue();
                view.clearHistory();
                super.onPageFinished(view,url);
            }
        });
        htmlLayout.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                ShowProgressDialogue();

                System.out.println("Progress status: "+progress);

                if(progress == 100)
                {
                    HideProgressDialogue();
                    //htmlLayout.clearCache(true);
                }
            }
        });
        JI.setListener(new TheJavascriptInterface.listener() {
            @Override
            public void message(String m) {
                if(m.equals("closeWebView"))
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            htmlLayoutContainer.setVisibility(View.GONE);
                            htmlLayoutContainerTopHolder.setVisibility(View.VISIBLE);
                            Intent myIntent = new Intent(DashboardActivity.this, UserAuthenticationActivity.class);
                            DashboardActivity.this.startActivityForResult(myIntent, ACTIVITY_USER_AUTHENTICATION);
                        }
                    });
                }
            }

            @Override
            public void showDetailsPage(String art_id) {

            }
        });

        htmlLayoutContainerTopHolder = (LinearLayout) findViewById(R.id.fullscreen_webviewTopHolder);

        CartItemsCounter33 = (TextView) findViewById(R.id.CartItemsCounter33);

        findViewById(R.id.BackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                htmlLayout.loadUrl("about:blank");
                htmlLayoutContainer.setVisibility(View.GONE);
                htmlLayoutContainerTopHolder.setVisibility(View.VISIBLE);
                UpdateCart();
            }
        });

        LinearLayout showCart = (LinearLayout) findViewById(R.id.showTheCart);
        showCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowTheCart();
            }
        });

        dashboard_pager_sts = (PagerSlidingTabStrip) findViewById(R.id.dashboard_pager_sts);
        dashboard_pager = (ViewPager) findViewById(R.id.dashboard_pager);

        //dashboard_pager.setOffscreenPageLimit(3);

        SearchResultPanel = (LinearLayout) findViewById(R.id.SearchResultPanel);
        SearchResultPanel.setVisibility(View.GONE);

        SearchResultPanelContents = (RecyclerView) findViewById(R.id.SearchResultPanelContents);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        SearchResultPanelContents.setLayoutManager(mLayoutManager);

        adapter = new DashboardPagerAdapter(getSupportFragmentManager());
        adapter.setTitles(TabNames);
        dashboard_pager.setAdapter(adapter);

        dashboard_pager_sts.setViewPager(dashboard_pager);

        mHeaderView = findViewById(R.id.header);

        tsbb_t1 = (TopSearchBarWithButtons_T1) findViewById(R.id.tsbb_t1);

        tsbb_t1.setListener(new TopSearchBarWithButtons_T1._clicked() {

            @Override
            public void ProfileIcon() {
                if(!Prefs.contains(UserConstants.AccessToken)){
                    Intent myIntent = new Intent(DashboardActivity.this, UserAuthenticationActivity.class);
                    DashboardActivity.this.startActivityForResult(myIntent, ACTIVITY_USER_AUTHENTICATION);
                }else{
                    Intent myIntent = new Intent(DashboardActivity.this, UserActivity.class);
                    DashboardActivity.this.startActivityForResult(myIntent, ACTIVITY_USER);
                }
            }

            @Override
            public void SearchBoxFocused() {
                SearchResultPanel.setVisibility(View.VISIBLE);
            }

            @Override
            public void SearchBoxUnFocused() {
                SearchResultPanel.setVisibility(View.GONE);
            }

            @Override
            public void SearchBoxText(String keyword) {

                if(SearchResultPanel.getVisibility() != View.VISIBLE)
                {
                    SearchResultPanel.setVisibility(View.VISIBLE);
                }

                Dashboard.SearchArtist(keyword, new Dashboard.SearchResult_Retrieved() {
                    @Override
                    public void Success(List<SearchListItem> dta) {
                        clearDashboardSearchList();
                        artistSearchResult.addAll(dta);
                        mDashboardSearchListingAdapter.notifyDataSetChanged();

                        //InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        //imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(DashboardActivity.this.getWindow().getContext(),message, Toast.LENGTH_LONG).show();

                        clearDashboardSearchList();
                        mDashboardSearchListingAdapter.notifyDataSetChanged();

                        //InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                       // imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }

                    @Override
                    public void EmptyKeywordFound() {
                        clearDashboardSearchList();
                        mDashboardSearchListingAdapter.notifyDataSetChanged();
                    }
                });

                System.out.println(keyword);
            }

            @Override
            public void BackFromSearch() {
                SearchResultPanel.setVisibility(View.GONE);
                KeyboardUtil.hideKeyboard(DashboardActivity.this);
            }

            @Override
            public void ShowCart() {
                ShowTheCart();
            }
        });

        mDashboardSearchListingAdapter = new DashboardSearchListingAdapter(artistSearchResult, new DashboardSearchListingAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final SearchListItem item) {
                /*tsbb_t1.resetSearchBox();
                Toast.makeText(DashboardActivity.this.getWindow().getContext(),"Wait for a while....", Toast.LENGTH_SHORT).show();
                UserAuthentication.CheckIfAdmin(new UserAuthentication.message() {
                    @Override
                    public void Success(String message) {
                        Intent intent = new Intent(DashboardActivity.this, ArtistActivity.class);
                        intent.putExtra("artist_id", item.getID());
                        DashboardActivity.this.startActivityForResult(intent, ACTIVITY_ARTIST);
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(DashboardActivity.this.getWindow().getContext(),message, Toast.LENGTH_LONG).show();
                    }
                });*/
                tsbb_t1.resetSearchBox2();
                SearchResultPanel.setVisibility(View.GONE);

                dashboard_pager.setCurrentItem(3);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                fc_talkWith.ShowUpArtist(item.getID()+"");
            }
        });
        SearchResultPanelContents.setAdapter(mDashboardSearchListingAdapter);



        dashboard_pager_sts.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                propagateToolbarState(toolbarIsShown());
                if(i == 4)
                {
                    showToolbar();
                }

                /*if(fc_latest!=null)
                {
                    fc_latest.passDataToFragment("I am back");
                }*/
            }
            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        propagateToolbarState(toolbarIsShown());
        UpdateCart();
        UpdateUserImage(false);

        /*new Timer().scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                UpdateCart();
            }
        },0,(10 * 1000));*/
        ExternalCallListener();
        LoadUserDetails();
    }

    public void clearDashboardSearchList()
    {
        int size = artistSearchResult.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                artistSearchResult.remove(0);
            }
            mDashboardSearchListingAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    private void FragmentReplace(Fragment fragment, Bundle arg)
    {
        frag_content.setVisibility(View.VISIBLE);
        fragment.setArguments(arg);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frag_content, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void ShowExhibitionsFrg(String _ID, boolean forceToEditScreen) {
        Bundle args = new Bundle();
        args.putString(GeneralConstants.ID, _ID);
        args.putBoolean(GeneralConstants.FORCE_TO_SHOW_EDIT_SCREEN, forceToEditScreen);
        FragmentReplace(new DELF_Exhibitions_DetailsFrg(), args);
    }

    @Override
    public void ShowReviewFrg(String _ID) {
        Bundle args = new Bundle();
        args.putString(GeneralConstants.ID, _ID);
        FragmentReplace(new DELF_Reviews_DetailsFrg(), args);
    }

    @Override
    public void ShowEventFrg(String _ID) {
        Bundle args = new Bundle();
        args.putString(GeneralConstants.ID, _ID);
        FragmentReplace(new DELF_Event_DetailsFrg(), args);
    }

    @Override
    public void ShowReviewFrgToWriteNew() {
        FragmentReplace(new DELF_Reviews_DetailsFrg(), null);
    }

    @Override
    public void UpdateTheCart() {
        UpdateCart();
    }

    @Override
    public void CloseMe() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        frag_content.setVisibility(View.GONE);
    }

    @Override
    public void GoToGalleryTab() {
       // dashboard_pager.setCurrentItem(0);
    }

    void ShowTheCart()
    {
        if(Prefs.contains(UserConstants.AccessToken)){
            Intent intent = new Intent(this, UserActivity_FragmentsManager.class);
            intent.putExtra(GeneralConstants.FRAGMENT_NAME, UserFrgShoppingCart.class.getName());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    private void ExternalCallListener()
    {
        Intent intent = getIntent();
        Uri data = intent.getData();

        if(data != null){
            if(data.getPathSegments().size()>1)
            {
                if(data.getPathSegments().get(1).equals("verifyemail"))
                {
                    htmlLayoutContainerTopHolder.setVisibility(View.GONE);
                    htmlLayoutContainer.setVisibility(View.VISIBLE);
                    htmlLayout.loadUrl(ServerConstants.BASE_URL+ TextUtils.join("/", data.getPathSegments())+"?"+data.getQuery()+"&mobile_view=1");
                    System.out.println(ServerConstants.BASE_URL+TextUtils.join("/", data.getPathSegments())+"?"+data.getQuery()+"&mobile_view=1");
                }
            }

            System.out.println("-------Received External Data-----------");
            System.out.println(data.getPathSegments().toString());
            System.out.println(data.getQuery());
            System.out.println(data.getPath());
            System.out.println(data.getEncodedQuery());
        }
    }

    void HockeyApp()
    {
        checkForUpdates();
        MetricsManager.register(this, getApplication());
    }

    void UpdateCart()
    {
        Dashboard.getCartInfo(new TheResponse() {
            @Override
            public void success(String response) {
                tsbb_t1.updateCartItem(response);
                System.out.println("------------getCartInfo---------------"+CartItemsCounter33.getText());
                CartItemsCounter33.setText(response);
            }

            @Override
            public void failed() {
                tsbb_t1.updateCartItem("0");
                CartItemsCounter33.setText("0");
            }
        });
    }

    void LoadUserDetails()
    {
        User.GetProfileDataProfile(getWindow().getContext(), new User.userProfileRetrieve() {
            @Override
            public void Success(UserProfile userProfile) {
                currentUserProfile = userProfile;
                UpdateUserImage(false);
                Prefs.putString(UserConstants.UserFullDetails, new Gson().toJson(userProfile));
            }

            @Override
            public void TokenRefreshRequired() {
                User.GetProfileDataProfile(getWindow().getContext(), new User.userProfileRetrieve() {
                    @Override
                    public void Success(UserProfile userProfile) {
                        currentUserProfile = userProfile;
                        UpdateUserImage(false);
                        Prefs.putString(UserConstants.UserFullDetails, new Gson().toJson(userProfile));
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        Toast.makeText(getApplicationContext(),"Token loop found.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void Error(String message) {
                        if(!message.contentEquals("Empty Access Token"))
                        {
                            Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
                        }
                        if(message.contentEquals("Not Authorized!"))
                        {
                            openLoginPage();
                        }
                    }
                });
            }

            @Override
            public void Error(String message) {
                if(!message.contentEquals("Empty Access Token"))
                {
                    Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();
                }
                if(message.contentEquals("Not Authorized!"))
                {
                    openLoginPage();
                }
            }
        });
    }

    void UpdateUserImage(boolean reset)
    {
        if(reset)
        {
            Prefs.remove(UserConstants.ProfilePicture);
            tsbb_t1.resetProfilePicture();
        }else{
            if(Prefs.getString(UserConstants.ProfilePicture, null) != null)
            {
                tsbb_t1.updateProfilePicture(Prefs.getString(UserConstants.ProfilePicture, null));
            }
        }

    }

    @Override
    public void onBackPressed()
    {
        if(frag_content.getVisibility() == View.VISIBLE)
        {
            frag_content.setVisibility(View.GONE);
        }
        else
        {
            if(htmlLayoutContainer.getVisibility() == View.VISIBLE)
            {
                htmlLayout.loadUrl("about:blank");
                htmlLayoutContainerTopHolder.setVisibility(View.VISIBLE);
                htmlLayoutContainer.setVisibility(View.GONE);
                UpdateCart();
            }
            else
            {
                if(SearchResultPanel.getVisibility() == View.VISIBLE)
                {
                    SearchResultPanel.setVisibility(View.GONE);
                    SearchResultPanel.clearFocus();
                    getWindow().getDecorView().clearFocus();
                }else{
                    /*if(dashboard_pager.getCurrentItem() > 0)
                    {
                        if(dashboard_pager.getCurrentItem()==3 && !fc_talkWith.ifDetailsPageHiden())
                        {
                            fc_talkWith._DevicebackPressed();
                        }
                        else
                        {
                            dashboard_pager.setCurrentItem(0);
                        }
                    }else{

                    }*/
                    if(dashboard_pager.getCurrentItem()==4)
                    {
                        dashboard_pager.setCurrentItem(0);
                    }
                    else if(dashboard_pager.getCurrentItem()==3)
                    {
                        if(!fc_talkWith.ifDetailsPageHiden())
                        {
                            fc_talkWith._DevicebackPressed();
                        }
                        else
                        {
                            dashboard_pager.setCurrentItem(0);
                        }
                    }
                    else if(dashboard_pager.getCurrentItem()==2)
                    {
                        if(!fragmentCommunicator3.ifDetailsPageHiden())
                        {
                            fragmentCommunicator3._DevicebackPressed();
                        }
                        else
                        {
                            dashboard_pager.setCurrentItem(0);
                        }
                    }
                    else if(dashboard_pager.getCurrentItem()==1)
                    {
                        if(!fragmentCommunicator2.ifDetailsPageHiden())
                        {
                            fragmentCommunicator2._DevicebackPressed();
                        }
                        else
                        {
                            dashboard_pager.setCurrentItem(0);
                        }
                    }
                    else if(dashboard_pager.getCurrentItem()==0)
                    {
                        if(!fragmentCommunicator.ifDetailsPageHiden())
                        {
                            fragmentCommunicator._DevicebackPressed();
                        }
                        else
                        {
                            if(!Prefs.getBoolean(UserConstants.DO_AUTO_LOGIN, false))
                            {
                                DoLogout();
                            }
                            finish();
                        }
                    }
                    else
                    {
                        if(!Prefs.getBoolean(UserConstants.DO_AUTO_LOGIN, false))
                        {
                            DoLogout();
                        }
                        finish();
                    }
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        // Check which request we're responding to

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frag_content);
        if(fragment != null)
        {
            fragment.onActivityResult(requestCode, resultCode, intent);
        }


        switch(requestCode) {
            case ACTIVITY_USER_AUTHENTICATION:
                if(resultCode == Activity.RESULT_OK)
                {
                    dashboard_pager.setCurrentItem(0);
                    UpdateCart();
                    LoadUserDetails();
                    UpdateUserImage(false);
                    RefreshFragmentDetails();
                }
                else if(resultCode == Activity.DEFAULT_KEYS_SEARCH_GLOBAL)
                {
                    // Asking to restart the activity.
                    Intent myIntent = new Intent(DashboardActivity.this, UserAuthenticationActivity.class);
                    DashboardActivity.this.startActivityForResult(myIntent, ACTIVITY_USER_AUTHENTICATION);
                }
                break;
            case ACTIVITY_USER:
                if(resultCode == UserActivity.USER_LOGGED_OUT)
                {
                    dashboard_pager.setCurrentItem(0);
                    UpdateCart();
                    UpdateUserImage(true);
                    RefreshFragmentDetails();
                }
                else if(resultCode == ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE)
                {
                    Bundle data = intent.getExtras();
                    dashboard_pager.setCurrentItem(data.getInt(GeneralConstants.POSITION));
                    System.out.println("Position: "+data.getInt(GeneralConstants.POSITION));
                    loadFragmentPageWebView_ArtDetails(data.getInt(GeneralConstants.ID), data.getInt(GeneralConstants.POSITION));
                }
                else
                {
                    UpdateUserImage(false);
                    RefreshFragmentDetails();
                }
                break;
            case ACTIVITY_ARTIST:

                break;
            case ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE:
                    if(resultCode == Activity.RESULT_OK)
                    {
                        Bundle data = intent.getExtras();
                        dashboard_pager.setCurrentItem(data.getInt(GeneralConstants.POSITION));
                        System.out.println("Position: "+data.getInt(GeneralConstants.POSITION));
                        loadFragmentPageWebView_ArtDetails(data.getInt(GeneralConstants.ID), data.getInt(GeneralConstants.POSITION));
                    }
                break;
            case 555:
                RefreshFragmentDetails();
                break;
            default:

                break;
        }
    }

    void loadFragmentPageWebView_ArtDetails(int id, int position)
    {
        if(fragmentCommunicator!=null && position == 0)
        {
            fragmentCommunicator.showDetails(id);
        }

        if(fragmentCommunicator2!=null && position == 1)
        {
            fragmentCommunicator2.showDetails(id);
        }

        if(fragmentCommunicator3!=null && position == 2)
        {
            fragmentCommunicator3.showDetails(id);
        }
    }

    void RefreshFragmentDetails()
    {
        if(fragmentCommunicator!=null)
        {
            fragmentCommunicator.reloadThePage();
        }

        if(fragmentCommunicator2!=null)
        {
            fragmentCommunicator2.reloadThePage();
        }

        if(fragmentCommunicator3!=null)
        {
            fragmentCommunicator3.reloadThePage();
        }

        if(fc_latest!=null)
        {
            fc_latest.reloadThePage();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        UpdateCart();
        // ... your own onResume implementation
        checkForCrashes();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    public void onDestroy()
    {
        if(!Prefs.getBoolean(UserConstants.DO_AUTO_LOGIN, false))
        {
            DoLogout();
        }
        System.out.println("Did Logout > "+Prefs.getBoolean(UserConstants.DO_AUTO_LOGIN, false)+" <");
        unregisterManagers();
        super.onDestroy();
    }

    public void DoLogout()
    {
        Prefs.remove(UserConstants.AccessToken);
        Prefs.remove(UserConstants.RefreshToken);
        Prefs.remove(UserConstants.DO_AUTO_LOGIN);
        Prefs.remove(UserConstants.ProfilePicture);
        System.out.println("Did Logout");
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        if(dragging)
        {
            int toolbarHeight = tsbb_t1.getHeight();
            float currentHeaderTranslationY = ViewHelper.getTranslationY(mHeaderView);
            if (firstScroll) {
                if (-toolbarHeight < currentHeaderTranslationY) {
                    mBaseTranslationY = scrollY;
                }
            }
            float headerTranslationY = ScrollUtils.getFloat(-(scrollY - mBaseTranslationY), -toolbarHeight, 0);
            ViewPropertyAnimator.animate(mHeaderView).cancel();
            ViewHelper.setTranslationY(mHeaderView, headerTranslationY);
        }else{
            if(screenTouched == 2)
            {
                if(tsbb_t1.getHeight()>scrollY)
                {
                    float headerTranslationY = ScrollUtils.getFloat(-(scrollY - mBaseTranslationY), -tsbb_t1.getHeight(), 0);
                    ViewPropertyAnimator.animate(mHeaderView).cancel();
                    ViewHelper.setTranslationY(mHeaderView, headerTranslationY);
                }
            }
        }
    }

    @Override
    public void onDownMotionEvent()
    {
        screenTouched = 1;
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        mBaseTranslationY = 0;
        screenTouched = 2;

        Fragment fragment = getCurrentFragment();
        if (fragment == null) {
            return;
        }
        View view = fragment.getView();
        if (view == null) {
            return;
        }

        // ObservableXxxViews have same API
        // but currently they don't have any common interfaces.
        adjustToolbar(scrollState, view);
    }

    private void adjustToolbar(ScrollState scrollState, View view) {
        int toolbarHeight = tsbb_t1.getHeight();
        final Scrollable scrollView = (Scrollable) view.findViewById(R.id.scroll);
        if (scrollView == null) {
            return;
        }
        int scrollY = scrollView.getCurrentScrollY();
        if (scrollState == ScrollState.DOWN) {
            showToolbar();
        } else if (scrollState == ScrollState.UP) {
            if (toolbarHeight <= scrollY) {
                hideToolbar();
            } else {
                showToolbar();
            }
        } else {
            // Even if onScrollChanged occurs without scrollY changing, toolbar should be adjusted
            if (toolbarIsShown() || toolbarIsHidden()) {
                // Toolbar is completely moved, so just keep its state
                // and propagate it to other pages
                propagateToolbarState(toolbarIsShown());
            } else {
                // Toolbar is moving but doesn't know which to move:
                // you can change this to hideToolbar()
                showToolbar();
            }
        }
    }

    private Fragment getCurrentFragment() {
        return adapter.getItem(dashboard_pager.getCurrentItem());
    }

    private void propagateToolbarState(boolean isShown) {
        int toolbarHeight = tsbb_t1.getHeight();

        // Set scrollY for the fragments that are not created yet
        adapter.setScrollY(isShown ? 0 : toolbarHeight);

        if(fragmentCommunicator!=null)
        {
            fragmentCommunicator.passDataToFragment(""+(isShown ? 0 : toolbarHeight));
        }

        if(fragmentCommunicator2!=null)
        {
            fragmentCommunicator2.passDataToFragment(""+(isShown ? 0 : toolbarHeight));
        }

        if(fragmentCommunicator3!=null)
        {
            fragmentCommunicator3.passDataToFragment(""+(isShown ? 0 : toolbarHeight));
        }

        if(fc_talkWith!=null)
        {
            fc_talkWith.passDataToFragment(""+(isShown ? 0 : toolbarHeight));
        }

        if(fc_latest!=null)
        {
            fc_latest.passDataToFragment(""+(isShown ? 0 : toolbarHeight));
        }

        // Set scrollY for the active fragments
        for (int i = 0; i < adapter.getCount(); i++) {
            // Skip current item
            if (i == dashboard_pager.getCurrentItem()) {
                continue;
            }

            // Skip destroyed or not created item
            Fragment f = adapter.getItem(i);
            if (f == null) {
                continue;
            }

            View view = f.getView();
            if (view == null) {
                continue;
            }
            propagateToolbarState(isShown, view, toolbarHeight);
        }
    }

    private void propagateToolbarState(boolean isShown, View view, int toolbarHeight) {
        Scrollable scrollView = (Scrollable) view.findViewById(R.id.scroll);
        if (scrollView == null) {
            return;
        }
        if (isShown) {
            // Scroll up
            if (0 < scrollView.getCurrentScrollY()) {
                scrollView.scrollVerticallyTo(0);
            }
        } else {
            // Scroll down (to hide padding)
            if (scrollView.getCurrentScrollY() < toolbarHeight) {
                scrollView.scrollVerticallyTo(toolbarHeight);
            }
        }
    }

    private boolean toolbarIsShown() {
        return ViewHelper.getTranslationY(mHeaderView) == 0;
    }

    private boolean toolbarIsHidden() {
        return ViewHelper.getTranslationY(mHeaderView) == -tsbb_t1.getHeight();
    }

    private void showToolbar() {
        float headerTranslationY = ViewHelper.getTranslationY(mHeaderView);
        if (headerTranslationY != 0) {
            ViewPropertyAnimator.animate(mHeaderView).cancel();
            ViewPropertyAnimator.animate(mHeaderView).translationY(0).setDuration(200).start();
        }
        propagateToolbarState(true);
    }

    private void hideToolbar() {
        float headerTranslationY = ViewHelper.getTranslationY(mHeaderView);
        int toolbarHeight = tsbb_t1.getHeight();
        if (headerTranslationY != -toolbarHeight) {
            ViewPropertyAnimator.animate(mHeaderView).cancel();
            ViewPropertyAnimator.animate(mHeaderView).translationY(-toolbarHeight).setDuration(200).start();
        }
        propagateToolbarState(false);
    }

    @Override
    public void passDataToActivity(String someValue) {

    }

    @Override
    public void openLoginPage() {
        DoLogout();
        Intent myIntent = new Intent(DashboardActivity.this, UserAuthenticationActivity.class);
        DashboardActivity.this.startActivityForResult(myIntent, ACTIVITY_USER_AUTHENTICATION);
    }

    @Override
    public void CloseFragmentAndOpenLoginScreen()
    {
        frag_content.setVisibility(View.GONE);
        Intent myIntent = new Intent(DashboardActivity.this, UserAuthenticationActivity.class);
        DashboardActivity.this.startActivityForResult(myIntent, ACTIVITY_USER_AUTHENTICATION);
    }

    @Override
    public void loadWebPage(String url) {
        System.out.println(url);
        htmlLayoutContainer.setVisibility(View.VISIBLE);
        htmlLayoutContainerTopHolder.setVisibility(View.VISIBLE);
        htmlLayout.loadUrl(url);
    }

    @Override
    public void showArtDetailsPage(int ID)
    {
        Intent intent = new Intent(DashboardActivity.this, ArtDetailsActivity.class);
        intent.putExtra(GeneralConstants.ID, ID);
        intent.putExtra(GeneralConstants.POSITION, dashboard_pager.getCurrentItem());
        DashboardActivity.this.startActivityForResult(intent, ACTIVITY_ART_SLIDESHOW_DETAILS_PAGE);
    }

    @Override
    public void showArtDetailsPageWV(int ID) {
        dashboard_pager.setCurrentItem(0);
        loadFragmentPageWebView_ArtDetails(ID, 0);
    }

    @Override
    public void ShowProgressDialogue()
    {
        if(progress == null)
        {
            progress = ProgressDialog.show(getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);
            progress.setCancelable(true);
        }
    }

    @Override
    public void HideProgressDialogue()
    {
        if(progress != null)
        {
            progress.dismiss();
            progress = null;
        }
    }

    @Override
    public void ShowParentToolbar() {
        showToolbar();
    }

    @Override
    public void HideParentToolbar() {

    }

    public class DashboardPagerAdapter extends FragmentPagerAdapter {

        private String[] TITLES = { };

        private int mScrollY;

        public void setScrollY(int scrollY) {
            mScrollY = scrollY;
        }

        public DashboardPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        public void setTitles(String[] TITLES)
        {
            this.TITLES = TITLES;
        }

        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return DashboardFrgGalleryActivity.newInstance(position);
                case 1:
                    return DashboardFrgShopActivity.newInstance(position);
                case 2:
                    return DashboardFrgAuctionActivity.newInstance(position);
                case 3:
                    return DashboardFrgTalkWithActivity.newInstance(position);
                case 4:
                    return DashboardFrgLatest.newInstance(position);
                default:
                    return null;
            }
        }

        /*@Override
        protected Fragment createItem(int position) {
            // Initialize fragments.
            // Please be sure to pass scroll position to each fragments using setArguments.
            Fragment f;
            switch (position) {
                case 0: {
                    f = new DashboardFrgGalleryActivity();
                    if (0 < mScrollY) {
                        Bundle args = new Bundle();
                        args.putInt(DashboardFrgGalleryActivity.ARG_INITIAL_POSITION, 1);
                        f.setArguments(args);
                    }
                    break;
                }
                case 1: {
                    f = new DashboardFrgAuctionActivity();
                    if (0 < mScrollY) {
                        Bundle args = new Bundle();
                        args.putInt(DashboardFrgAuctionActivity.ARG_INITIAL_POSITION, 1);
                        f.setArguments(args);
                    }
                    break;
                }
                case 2: {
                    f = new DashboardFrgShopActivity();
                    if (0 < mScrollY) {
                        Bundle args = new Bundle();
                        args.putInt(DashboardFrgShopActivity.ARG_INITIAL_POSITION, 1);
                        f.setArguments(args);
                    }
                    break;
                }
                default: {
                    f = new DashboardFrgGalleryActivity();
                    if (0 < mScrollY) {
                        Bundle args = new Bundle();
                        args.putInt(DashboardFrgGalleryActivity.ARG_INITIAL_POSITION, 1);
                        f.setArguments(args);
                    }
                    break;
                }
            }
            return f;
        }*/
    }
}
