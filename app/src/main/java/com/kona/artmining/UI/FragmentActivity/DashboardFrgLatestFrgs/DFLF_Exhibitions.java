package com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kona.artmining.Adapter.DashboardFrgLatest_Exhibition_Adapter_T1;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.DashboardFrgLatest;
import com.kona.artmining.Interface.ActivityCommunicator;
import com.kona.artmining.Model.DashboardFrgLatest_Exhibition_Model_T1;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.R;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;


public class DFLF_Exhibitions extends Fragment {

    SwipeRefreshLayout swipeRefreshLayout;
    ObservableRecyclerView recycler_view;
    View ifNoUserLogIn_View;
    DashboardFrgLatest_Exhibition_Adapter_T1 latestAdapter;
    int img_id, img;
    LinearLayout UploadNewExhibitionHolder, UploadNewExhibition;
    ActivityCommunicator ac;
    UserProfile currentUserProfile;

    private List<DashboardFrgLatest_Exhibition_Model_T1> Content = new ArrayList<DashboardFrgLatest_Exhibition_Model_T1>();


    public DFLF_Exhibitions() {
        // Required empty public constructor
    }

    public static DFLF_Exhibitions newInstance(int param1) {
        DFLF_Exhibitions fragment = new DFLF_Exhibitions();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ui_dashboard_frgmnts_latest_frg_exhibitions, container, false);
        PrepareStage(view);

        exhibition();

        return view;
    }

    void PrepareStage(View view)
    {
        //ifNoUserLogIn_View = (View) view.findViewById(R.id.ifNoUserLogIn_View);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        recycler_view = (ObservableRecyclerView) view.findViewById(R.id.recycler_view);
        UploadNewExhibition = (LinearLayout) view.findViewById(R.id.UploadNewExhibition);
        UploadNewExhibitionHolder = (LinearLayout) view.findViewById(R.id.UploadNewExhibitionHolder);


        latestAdapter = new DashboardFrgLatest_Exhibition_Adapter_T1(Content, new DashboardFrgLatest_Exhibition_Adapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(DashboardFrgLatest_Exhibition_Model_T1 item)
            {
                ac.ShowExhibitionsFrg(item.getExb_id()+"", false);
            }
        });

        recycler_view.setAdapter(latestAdapter);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        recycler_view.setLayoutManager(layoutManager);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                exhibition();
            }
        });

        UploadNewExhibition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentUserProfile = new Gson().fromJson(Prefs.getString(UserConstants.UserFullDetails, "{}"),  new TypeToken<UserProfile>(){}.getType());

                System.out.println("Clicked >");
                System.out.println("Clicked User Type > "+currentUserProfile.getType());

                if(currentUserProfile.getType()!=1)
                {
                    // Show Upload Page
                    ac.ShowExhibitionsFrg("", true);
                }
                else {
                    //Toast.makeText(,"Token loop found.", Toast.LENGTH_LONG).show();
                    Toast.makeText(getContext(), "Your are not allowed to upload this.", Toast.LENGTH_LONG).show();
                }
            }
        });

        if (Prefs.contains(UserConstants.AccessToken)) {
            currentUserProfile = new Gson().fromJson(Prefs.getString(UserConstants.UserFullDetails, "{}"),  new TypeToken<UserProfile>(){}.getType());
            if(currentUserProfile.getType()!=1)
            {
                UploadNewExhibitionHolder.setVisibility(View.VISIBLE);
                //ifNoUserLogIn_View.setVisibility(View.GONE);
            }
            else
            {
                UploadNewExhibitionHolder.setVisibility(View.GONE);
                //ifNoUserLogIn_View.setVisibility(View.GONE);
            }
        } else {
            UploadNewExhibitionHolder.setVisibility(View.GONE);
            //ifNoUserLogIn_View.setVisibility(View.GONE);
        }
    }

    void  exhibition(){
        DashboardFrgLatest.RetrieveExhibitionData(new DashboardFrgLatest.Exhibition_Retrieved(){
            @Override
            public void Success(List<DashboardFrgLatest_Exhibition_Model_T1> data) {
                clearRVData();
                Content.addAll(data);
                latestAdapter.notifyDataSetChanged();

                if(swipeRefreshLayout != null)
                {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(),message, Toast.LENGTH_LONG).show();
            }
        });

    }

    public void clearRVData()
    {
        int size = Content.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Content.remove(0);
            }
            latestAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ac = (ActivityCommunicator) getActivity();
    }

}
