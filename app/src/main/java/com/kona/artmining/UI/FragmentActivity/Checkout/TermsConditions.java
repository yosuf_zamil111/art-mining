package com.kona.artmining.UI.FragmentActivity.Checkout;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.kona.artmining.Adapter.TermsConditionListingAdapter;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Helper.UserAuthentication;
import com.kona.artmining.Interface.CheckoutActivity_Interface;
import com.kona.artmining.Model.TermsCondition_T1;
import com.kona.artmining.R;
import com.kona.artmining.UI.FragmentActivity.UserFrgAuctionList;
import com.kona.artmining.UI.FragmentActivity.UserFrgShoppingCart;
import com.kona.artmining.custom_components.OvalButton_T1;

import java.util.ArrayList;
import java.util.List;

public class TermsConditions extends Fragment {

    ObservableRecyclerView recyclerView;
    TermsConditionListingAdapter rvListAdapter;
    List<TermsCondition_T1> Content = new ArrayList<TermsCondition_T1>();

    ProgressDialog progress;

    RelativeLayout ui_user_authentication__terms_conditions_details_page;
    TextView ui_user_authentication__terms_conditions_details_page_title, ui_user_authentication__terms_conditions_details_page_details;
    Button ui_user_authentication__terms_conditions_details_page_back_btt;
    OvalButton_T1 ui_user_terms_conditions_back_button, ui_user_terms_conditions_next_button, ui_user_terms_conditions_agree_all_button;

    CheckoutActivity_Interface mCallback;

    public TermsConditions() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            mCallback = (CheckoutActivity_Interface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement CheckoutActivity_Interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ui_checkout_frg_terms_conditions, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStage(view);
    }

    void prepareStage(View view)
    {
        progress = ProgressDialog.show(getContext(), "Retrieving terms & conditions.", "Please wait for a while.", true);

        recyclerView = (ObservableRecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        rvListAdapter = new TermsConditionListingAdapter(Content, new TermsConditionListingAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TermsCondition_T1 tc) {
                ui_user_authentication__terms_conditions_details_page.setVisibility(View.VISIBLE);
                ui_user_authentication__terms_conditions_details_page_title.setText(tc.getSectionTitle());
                ui_user_authentication__terms_conditions_details_page_details.setText(tc.getSectionDescription());
            }

            @Override
            public void checkUncheck(TermsCondition_T1 tc, int position, boolean ifChecked) {
                Content.get(position).setChecked(ifChecked);

                if(tc.isSection_required() && !ifChecked)
                {
                    ui_user_terms_conditions_agree_all_button.BackBackground(getResources().getDrawable(R.drawable.resetbuttonshape));
                }
                UpdateNextbuttonView();
            }
        });
        ui_user_terms_conditions_agree_all_button = (OvalButton_T1) view.findViewById(R.id.ui_user_terms_conditions_agree_all_button);
        ui_user_terms_conditions_agree_all_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {

                for (int i = 0; i < Content.size(); i++) {
                    Content.get(i).setChecked(true);
                }
                rvListAdapter.notifyDataSetChanged();
                UpdateNextbuttonView();

                ui_user_terms_conditions_agree_all_button.ChangeLayoutBackground(getResources().getDrawable(R.drawable.registrationbuttonshape3));
            }
        });
        recyclerView.setAdapter(rvListAdapter);

        UserAuthentication.RetrieveTermsCondition(new UserAuthentication.termsConditions() {
            @Override
            public void Success(List<TermsCondition_T1> tc) {
                Content.addAll(tc);
                rvListAdapter.notifyDataSetChanged();
                progress.dismiss();
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                progress.dismiss();
            }
        }, getArguments().getString(GeneralConstants.TC_TYPE));

        ui_user_authentication__terms_conditions_details_page = (RelativeLayout) view.findViewById(R.id.ui_user_authentication__terms_conditions_details_page);
        ui_user_authentication__terms_conditions_details_page_title = (TextView) view.findViewById(R.id.ui_user_authentication__terms_conditions_details_page_title);
        ui_user_authentication__terms_conditions_details_page_details = (TextView) view.findViewById(R.id.ui_user_authentication__terms_conditions_details_page_details);

        ui_user_authentication__terms_conditions_details_page_back_btt = (Button) view.findViewById(R.id.ui_user_authentication__terms_conditions_details_page_back_btt);
        ui_user_authentication__terms_conditions_details_page_back_btt.setClickable(true);
        ui_user_authentication__terms_conditions_details_page_back_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ui_user_authentication__terms_conditions_details_page.setVisibility(View.GONE);
            }
        });

        ui_user_terms_conditions_back_button = (OvalButton_T1) view.findViewById(R.id.ui_user_terms_conditions_back_button);
        ui_user_terms_conditions_back_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {
                if(getArguments().getString(GeneralConstants.TC_TYPE).equals("auc"))
                {
                    mCallback.OpenFragmentForm_UAFM(UserFrgAuctionList.class.getName());
                }else{
                    mCallback.OpenFragmentForm_UAFM(UserFrgShoppingCart.class.getName());
                }
                getActivity().finish();
            }
        });
        ui_user_terms_conditions_next_button = (OvalButton_T1) view.findViewById(R.id.ui_user_terms_conditions_next_button);
        ui_user_terms_conditions_next_button.ChangeLayoutBackground(getResources().getDrawable(R.drawable.disable_button_shape_2));
        ui_user_terms_conditions_next_button.ChangeLabelColor(Color.parseColor("#ffffff"));
        ui_user_terms_conditions_next_button._onClick(new OvalButton_T1._click() {
            @Override
            public void done()
            {

                boolean agreed = true;

                for (int i = 0; i < Content.size(); i++) {
                    if(!Content.get(i).isChecked() && Content.get(i).isSection_required())
                    {
                        agreed = false;
                        break;
                    }
                }

                if(agreed){
                    mCallback.ShowShippingMethods();
                }else{
                    Toast.makeText(getContext(), "You need to agree on required Terms & Conditions.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void UpdateNextbuttonView()
    {
        //ui_user_terms_conditions_next_button.

        int p = 0;

        for (int i = 0; i < Content.size(); i++) {

            if(Content.get(i).isSection_required())
            {
                p = p + 1;
            }

            if(Content.get(i).isSection_required() && Content.get(i).isChecked())
            {
                p = p - 1;
            }
        }

        System.out.println(p);

        if(p==0)
        {
            ui_user_terms_conditions_next_button.makeEnable(true);
            ui_user_terms_conditions_next_button.ChangeLayoutBackground(getResources().getDrawable(R.drawable.registrationbuttonshape2));
            ui_user_terms_conditions_next_button.ChangeLabelColor(Color.parseColor("#000000"));
        }else{
            ui_user_terms_conditions_next_button.makeEnable(false);
            ui_user_terms_conditions_next_button.ChangeLayoutBackground(getResources().getDrawable(R.drawable.disable_button_shape_2));
            ui_user_terms_conditions_next_button.ChangeLabelColor(Color.parseColor("#ffffff"));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*
        if (requestCode == FILE_CODE && resultCode == Activity.RESULT_OK)
        {

        }
        */
    }
}
