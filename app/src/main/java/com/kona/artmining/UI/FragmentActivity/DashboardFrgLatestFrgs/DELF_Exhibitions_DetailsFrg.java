package com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.DashboardFrgLatest;
import com.kona.artmining.Interface.ActivityCommunicator;
import com.kona.artmining.Model.Comment_T2;
import com.kona.artmining.Model.DashboardFrgLatest_Exhibition_Model_T1;
import com.kona.artmining.R;
import com.kona.artmining.custom_components.CommentRow_T1;
import com.kona.artmining.model.Comment_T1;
import com.kona.artmining.utils.PicassoRoundTransform;
import com.pixplicity.easyprefs.library.Prefs;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class DELF_Exhibitions_DetailsFrg extends Fragment
{
    int len = 300;
    Button exhibitions_layout_close_button;
    LinearLayout backBtt,PreviewPage, EditPage, EditPage_backBtt, PreviewPage_CommentHolder, PreviewPage_ExistingCommentContainer, showCommentScreen;
    TextView title, details, PreviewPage_NoOfComments;
    ImageView featuredImage;
    CircularImageView ProfileIcon;

    ProgressDialog progress;

    ActivityCommunicator ac;

    ImagePicker imagePicker;
    LinearLayout EditPage_SelectImage;
    ImageView EditPage_SelectImageContainer;

    EditText EditPage_Title, EditPage_Details, PreviewPage_CommentText;
    LinearLayout PreviewPage_doComment;
    Button EditPage_Done;

    File imageTobeUpload;

    public DELF_Exhibitions_DetailsFrg() {
        // Required empty public constructor
    }

    public static DELF_Exhibitions_DetailsFrg newInstance(int param1) {
        DELF_Exhibitions_DetailsFrg fragment = new DELF_Exhibitions_DetailsFrg();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ui_dashboard_frgmnts_latest_frg_exhibitions_details, container, false);
        PrepareStage(view);
        //exhibition();
        return view;
    }

    void PrepareStage(View view)
    {
        exhibitions_layout_close_button = (Button) view.findViewById(R.id.exhibitions_layout_close_button);
        exhibitions_layout_close_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ac.CloseMe();
            }
        });

        PreviewPage = (LinearLayout) view.findViewById(R.id.PreviewPage);
        PreviewPage.setVisibility(View.GONE);
        EditPage = (LinearLayout) view.findViewById(R.id.EditPage);
        EditPage.setVisibility(View.GONE);
        PreviewPage_CommentHolder = (LinearLayout) view.findViewById(R.id.PreviewPage_CommentHolder);
        PreviewPage_CommentHolder.setVisibility(View.GONE);
        PreviewPage_ExistingCommentContainer = (LinearLayout) view.findViewById(R.id.PreviewPage_ExistingCommentContainer);
        PreviewPage_NoOfComments = (TextView) view.findViewById(R.id.PreviewPage_NoOfComments);

        showCommentScreen = (LinearLayout) view.findViewById(R.id.showCommentScreen);

        backBtt = (LinearLayout) view.findViewById(R.id.backBtt);
        backBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ac.CloseMe();
            }
        });

        title = (TextView) view.findViewById(R.id.title);
        details = (TextView) view.findViewById(R.id.details);

        featuredImage = (ImageView) view.findViewById(R.id.featuredImage);

        EditPage_backBtt = (LinearLayout) view.findViewById(R.id.EditPage_backBtt);
        EditPage_backBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ac.CloseMe();
            }
        });


        EditPage_SelectImage = (LinearLayout) view.findViewById(R.id.EditPage_SelectImage);
        EditPage_SelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickAnImage();
            }
        });

        EditPage_SelectImageContainer = (ImageView) view.findViewById(R.id.EditPage_SelectImageContainer);
        EditPage_Title = (EditText) view.findViewById(R.id.EditPage_Title);

        /*EditPage_Title.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if(EditPage_Title.getText().toString().length()>= len) {
                    //Show toast here
                    Toast.makeText(getContext(), "You crossed the limit of characters here.", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });*/


        EditPage_Details = (EditText) view.findViewById(R.id.EditPage_Details);

        EditPage_Done = (Button) view.findViewById(R.id.EditPage_Done);
        EditPage_Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Validate())
                {
                    InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    UploadAnExibition();
                }
            }
        });
        PreviewPage_CommentText = (EditText) view.findViewById(R.id.PreviewPage_CommentText);
        PreviewPage_CommentText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                PreviewPage_CommentText.setFilters(new InputFilter[] {new InputFilter.LengthFilter(300)});
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(i >= 299){
                    Toast.makeText(getActivity(),"You crossed the limit of characters here.", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        PreviewPage_doComment = (LinearLayout) view.findViewById(R.id.PreviewPage_doComment);
        ProfileIcon = (CircularImageView) view.findViewById(R.id.ProfileIcon);
        PreviewPage_doComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ValidateComment())
                {
                    InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    CreateAnComment();
                    if(PreviewPage_CommentHolder.getVisibility() == View.VISIBLE){
                        PreviewPage_CommentHolder.setVisibility(View.GONE);
                    }
                }
            }
        });

        if(Prefs.contains(UserConstants.AccessToken))
        {
            Picasso.with(getContext())
                    .load(new File(Prefs.getString(UserConstants.ProfilePicture, null)))
                    .placeholder(R.drawable.icon_profile)
                    .error(R.drawable.icon_profile)
                    .into(ProfileIcon);
        }


        if(getArguments().getBoolean(GeneralConstants.FORCE_TO_SHOW_EDIT_SCREEN))
        {
            EditPage.setVisibility(View.VISIBLE);
        }
        else
        {
            progress = ProgressDialog.show(getContext(), "Retrieving details.", "Please wait for a while.", true);
            DashboardFrgLatest.RetrieveSelectedExhibitionsDetils(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.SelectedExhibitionRetrived() {
                @Override
                public void Success(DashboardFrgLatest_Exhibition_Model_T1 data) {

                    title.setText(data.getTitle());
                    details.setText(data.getDetails_info());

                    Picasso.with(getContext())
                            .load(data.getImage())
                            .placeholder(R.drawable.transparent)
                            .error(R.drawable.transparent)
                            .fit()
                            .into(featuredImage);
                    LoadExibitionComments();
                }

                @Override
                public void Error(String message) {
                    PreviewPage.setVisibility(View.VISIBLE);
                    progress.dismiss();
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    ac.CloseMe();
                }
            });
        }

        showCommentScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Prefs.contains(UserConstants.AccessToken))
                {
                    PreviewPage_CommentHolder.setVisibility(View.VISIBLE);
                }
                else
                {
                    ac.CloseFragmentAndOpenLoginScreen();
                }
            }
        });
    }

    void LoadExibitionComments()
    {
        DashboardFrgLatest.RetrieveSelectedExhibitionsComments(getArguments().getString(GeneralConstants.ID), new DashboardFrgLatest.SelectedExhibitionCommentsRetrived() {
            @Override
            public void Success(List<Comment_T1> comments) {

                PreviewPage_NoOfComments.setText(comments.size()+"");
                progress.dismiss();
                PreviewPage.setVisibility(View.VISIBLE);
                if(PreviewPage_ExistingCommentContainer.getChildCount()>0) {
                    PreviewPage_ExistingCommentContainer.removeAllViews();
                }

                for (Comment_T1 comment : comments)
                {
                    CommentRow_T1 cr_t1 = new CommentRow_T1(getContext());
                    //cr_t1.setup(comment);
                    cr_t1.setup2(
                            (comment.getCommentor_english_name()+" / "+comment.getCommentor_korean_name()),
                            comment.getComment_details(),
                            comment.getComment_date_time(),
                            comment.getProfile_image_thumb()
                    );
                    PreviewPage_ExistingCommentContainer.addView(cr_t1);
                }

            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                if(!message.contains("No comment found"))
                {
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    ac.CloseMe();
                }
            }
        });
    }

    boolean Validate()
    {

        if(imageTobeUpload == null)
        {
            Toast.makeText(getContext(), "Please select an image", Toast.LENGTH_LONG).show();
            return false;
        }

        if(TextUtils.isEmpty(EditPage_Title.getText().toString()))
        {
            Toast.makeText(getContext(), "Please enter an title", Toast.LENGTH_LONG).show();
            return false;
        }

        if(TextUtils.isEmpty(EditPage_Details.getText().toString()))
        {
            Toast.makeText(getContext(), "Please enter some details", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    boolean ValidateComment()
    {
        if(TextUtils.isEmpty(PreviewPage_CommentText.getText().toString()))
        {
            Toast.makeText(getContext(), "Please enter some comment", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;

    }

    void PickAnImage()
    {
        imagePicker = new ImagePicker(getActivity());
        imagePicker.setImagePickerCallback(new ImagePickerCallback(){
                                               @Override
                                               public void onImagesChosen(List<ChosenImage> images) {
                                                   System.out.println(images.toString());
                                                   System.out.println(images.get(0).toString());
                                                   System.out.println(images.get(0).getOriginalPath());
                                                   /*
                                                   //Picasso.with(getWindow().getContext()).load(new File(images.get(0).getOriginalPath())).into(profilePicture);
                                                   SelectedProfileImage = new File(images.get(0).getOriginalPath());

                                                   */

                                                   imageTobeUpload = new File(images.get(0).getOriginalPath());

                                                   Picasso.with(getActivity().getWindow().getContext())
                                                           .load(new File(images.get(0).getOriginalPath()))
                                                           .placeholder(R.drawable.img_photo_02)
                                                           .error(R.drawable.img_photo_02)
                                                           .transform(new PicassoRoundTransform())
                                                           .into(EditPage_SelectImageContainer);
                                               }

                                               @Override
                                               public void onError(String message) {
                                                   // Do error handling
                                               }
                                           }
        );
        // imagePicker.allowMultiple(); // Default is false
        // imagePicker.shouldGenerateMetadata(false); // Default is true
        // imagePicker.shouldGenerateThumbnails(false); // Default is true
        imagePicker.pickImage();
    }

    void UploadAnExibition()
    {
        progress = ProgressDialog.show(getContext(), "Retrieving details.", "Please wait for a while.", true);
        DashboardFrgLatest.CreateANewExibition(imageTobeUpload, EditPage_Title.getText().toString(), EditPage_Details.getText().toString(), new DashboardFrgLatest.response() {
            @Override
            public void Success() {
                progress.dismiss();
                Toast.makeText(getContext(), "Exhibition Created", Toast.LENGTH_LONG).show();
                //ac.GoToGalleryTab();
                ac.CloseMe();
            }

            @Override
            public void TokenRefreshRequired() {
                DashboardFrgLatest.CreateANewExibition(imageTobeUpload, EditPage_Title.getText().toString(), EditPage_Details.getText().toString(), new DashboardFrgLatest.response() {
                    @Override
                    public void Success() {
                        progress.dismiss();
                        Toast.makeText(getContext(), "Exhibition Created", Toast.LENGTH_LONG).show();
                        //ac.GoToGalleryTab();
                        ac.CloseMe();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                        ac.CloseMe();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                ac.CloseMe();
            }
        });
    }

    void CreateAnComment()
    {
        progress = ProgressDialog.show(getContext(), "Retrieving details.", "Please wait for a while.", true);
        DashboardFrgLatest.CreateANewExibitionComment(getArguments().getString(GeneralConstants.ID), PreviewPage_CommentText.getText().toString(), new DashboardFrgLatest.response() {
            @Override
            public void Success() {
                LoadExibitionComments();
                PreviewPage_CommentText.setText(null);
                Toast.makeText(getContext(), "Comment Posted Successfully.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void TokenRefreshRequired() {
                DashboardFrgLatest.CreateANewExibitionComment(getArguments().getString(GeneralConstants.ID), PreviewPage_CommentText.getText().toString(), new DashboardFrgLatest.response() {
                    @Override
                    public void Success() {
                        LoadExibitionComments();
                        PreviewPage_CommentText.setText(null);
                        Toast.makeText(getContext(), "Comment Posted Successfully.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent_data) {
        if (requestCode == Picker.PICK_IMAGE_DEVICE) {
            System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
                imagePicker.submit(intent_data);
            }
        }
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        ac = (ActivityCommunicator) getActivity();
    }
}
