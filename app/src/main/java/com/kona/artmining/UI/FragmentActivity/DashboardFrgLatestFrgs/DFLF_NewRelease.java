package com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.kona.artmining.Adapter.DashboardFrgLatest_NewRelease_Page_Adapter_T1;
import com.kona.artmining.Model.DashboardFrgLatest_NewRelease_Page_Model_T1;
import com.kona.artmining.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DFLF_NewRelease extends Fragment {


    ObservableRecyclerView recycler_view;
    DashboardFrgLatest_NewRelease_Page_Adapter_T1 releaseAdapter;
    List<DashboardFrgLatest_NewRelease_Page_Model_T1> Content = new ArrayList<>();


    public DFLF_NewRelease() {
        // Required empty public constructor
    }


    public static DFLF_NewRelease newInstance(int param1) {
        DFLF_NewRelease fragment = new DFLF_NewRelease();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ui_dashboard_frgmnts_latest_frg_new_release, container, false);
        PrepareStage(view);

        return view;
    }

    void PrepareStage(View view)
    {

        recycler_view = (ObservableRecyclerView) view.findViewById(R.id.recycler_view);
        ObservableRecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(mLayoutManager);
        releaseAdapter = new DashboardFrgLatest_NewRelease_Page_Adapter_T1(Content);
        recycler_view.setAdapter(releaseAdapter);


        com.kona.artmining.Helper.DashboardFrgLatest.RetrieveLatest(new com.kona.artmining.Helper.DashboardFrgLatest.Release_Retrieved() {
            @Override
            public void Success(List<DashboardFrgLatest_NewRelease_Page_Model_T1> data) {
                Content.addAll(data);
                releaseAdapter.notifyDataSetChanged();
            }

            @Override
            public void Error(String message) {

            }
        });


        /*recycler_view1 = (ObservableRecyclerView) view.findViewById(R.id.recycler_view1);


        ObservableRecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view1.setLayoutManager(mLayoutManager1);


        newReleaseAdapter1 = new DashboardFrgLatest_NewRelease1_Adapter_T1(Content1);

        for(int i=0; i<7; i++){

            DashboardFrgLatest_NewRelease_Model_T1 nrm = new DashboardFrgLatest_NewRelease_Model_T1();
            nrm.setArtNewsName("Art news for art mining application");
            nrm.setArtNewsDate("10-09-2019");
            Content.add(nrm);

            DashboardFrgLatest_NewRelease_Model_T1 nrm1 = new DashboardFrgLatest_NewRelease_Model_T1();
            nrm1.setMagazineNews("Magazine for the monthly news");
            nrm1.setMagazineDate("10-09-2019");
            Content1.add(nrm1);
        }


        recycler_view1.setAdapter(newReleaseAdapter1);*/

    }

    public void clearRVData()
    {
        int size = Content.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Content.remove(0);
            }
            releaseAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

}
