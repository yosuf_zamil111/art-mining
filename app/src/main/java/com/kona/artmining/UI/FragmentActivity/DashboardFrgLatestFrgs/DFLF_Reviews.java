package com.kona.artmining.UI.FragmentActivity.DashboardFrgLatestFrgs;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kona.artmining.Adapter.DashboardFrgLatest_Reviews_Adapter_T1;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.DashboardFrgLatest;
import com.kona.artmining.Interface.ActivityCommunicator;
import com.kona.artmining.Model.DashboardFrgLatest_Reviews_Model_T1;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.R;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DFLF_Reviews extends Fragment {
    SwipeRefreshLayout swipeRefreshLayout;
    ObservableRecyclerView recycler_view;

    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    UserProfile currentUserProfile;


    LinearLayout addNewReview, addNewReview_bttContainer;
    View ifNoUserLogIn_View;
    DashboardFrgLatest_Reviews_Adapter_T1 reviewsAdapter;
    private List<DashboardFrgLatest_Reviews_Model_T1> Content = new ArrayList<DashboardFrgLatest_Reviews_Model_T1>();
    ActivityCommunicator ac;


    public DFLF_Reviews() {
        // Required empty public constructor
    }


    public static DFLF_Reviews newInstance(int param1) {
        DFLF_Reviews fragment = new DFLF_Reviews();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.ui_dashboard_frgmnts_latest_frg_reviews, container, false);
        PrepareStage(view);

        return view;
    }

    void PrepareStage(View view)
    {

        ifNoUserLogIn_View = (View) view.findViewById(R.id.ifNoUserLogIn_View);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        recycler_view = (ObservableRecyclerView) view.findViewById(R.id.recycler_view);

        ObservableRecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recycler_view.setLayoutManager(mLayoutManager);


        recycler_view.setHasFixedSize(true);
        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        recycler_view.setLayoutManager(gaggeredGridLayoutManager);

        reviewsAdapter = new DashboardFrgLatest_Reviews_Adapter_T1(Content, new DashboardFrgLatest_Reviews_Adapter_T1.OnItemClickListener() {
            @Override
            public void onItemClick(DashboardFrgLatest_Reviews_Model_T1 item)
            {
                ac.ShowReviewFrg(item.getReview_id());
            }
        });

        recycler_view.setAdapter(reviewsAdapter);

        reviewsAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Populate();
            }
        });

        addNewReview = (LinearLayout) view.findViewById(R.id.addNewReview);
        addNewReview_bttContainer = (LinearLayout) view.findViewById(R.id.addNewReview_bttContainer);
        addNewReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Prefs.contains(UserConstants.AccessToken)) {
                    CheckUserProfile();
                    if(currentUserProfile.getType()!=1)
                    {
                        ac.ShowReviewFrgToWriteNew();
                    }
                    else
                    {
                        Toast.makeText(getContext(),"You are not authorized for this", Toast.LENGTH_LONG).show();
                    }
                }else{
                    ac.openLoginPage();
                }
            }
        });
        
        Populate();

        if (Prefs.contains(UserConstants.AccessToken)) {
            CheckUserProfile();
            if(currentUserProfile.getType()!=1)
            {
                addNewReview_bttContainer.setVisibility(View.VISIBLE);
                ifNoUserLogIn_View.setVisibility(View.GONE);
            }
            else
            {
                addNewReview_bttContainer.setVisibility(View.GONE);
                ifNoUserLogIn_View.setVisibility(View.GONE);
            }
        } else {
            addNewReview_bttContainer.setVisibility(View.GONE);
            ifNoUserLogIn_View.setVisibility(View.GONE);
        }
    }

    void CheckUserProfile()
    {
        currentUserProfile = new Gson().fromJson(Prefs.getString(UserConstants.UserFullDetails, "{}"),  new TypeToken<UserProfile>(){}.getType());
    }

    void Populate()
    {
        DashboardFrgLatest.RetrieveAllReviews(new DashboardFrgLatest.Reviews_Retrieved() {
            @Override
            public void Success(List<DashboardFrgLatest_Reviews_Model_T1> data) {
                clearRVData();
                Content.addAll(data);

                reviewsAdapter.notifyDataSetChanged();

                if(swipeRefreshLayout != null)
                {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getContext(),message, Toast.LENGTH_LONG).show();
            }
        });
    }

    public void clearRVData()
    {
        int size = Content.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Content.remove(0);
            }
            reviewsAdapter.notifyItemRangeRemoved(0, size);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ac = (ActivityCommunicator) getActivity();
    }

}
