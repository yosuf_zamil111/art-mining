package com.kona.artmining.UI.FragmentActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.kona.artmining.Helper.User;
import com.kona.artmining.Helper.UserAuthentication;
import com.kona.artmining.Interface.UserActivity_FrgMng_Interface;
import com.kona.artmining.Interface.UserActivity_Interface;
import com.kona.artmining.Interface.UserFrgMngCommunicator;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.R;
import com.kona.artmining.UI.Activity.MainActivity;
import com.kona.artmining.UI.FragmentController.UserActivity_FragmentsManager;
import com.kona.artmining.custom_components.FormField_T1_TextBox;
import com.kona.artmining.custom_components.FormField_T2_TextBox;
import com.kona.artmining.custom_components.FormField_T3_TextBox_Spinner;
import com.kona.artmining.custom_components.FormField_T4_TextBox;
import com.kona.artmining.custom_components.Spinner_T2;
import com.kona.artmining.custom_components.Spinner_T5;
import com.kona.artmining.custom_components.SquareButton_T1;
import com.kona.artmining.custom_components.UserTypeRadioGroup;
import com.kona.artmining.custom_popup_box.CCImagePicker;
import com.kona.artmining.custom_popup_box.GenderSelectionPopUpBox;
import com.kona.artmining.custom_popup_box.OptionListing_T1;
import com.kona.artmining.custom_popup_box.SmsVerification;
import com.kona.artmining.decent_date_picker.DPicker;
import com.kona.artmining.model.RadioButtonInfo;
import com.kona.artmining.option_table.Table_T1;
import com.kona.artmining.option_table.modals.Col2RowDetails;
import com.kona.artmining.utils.PicassoRoundTransform;
import com.kona.artmining.utils.Utils_Helper;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class UserFrgEditProfile extends Fragment implements UserFrgMngCommunicator
{
    TextView ui_user_edit_profile_heading_name,ui_user_edit_profile_email;
    LinearLayout ui_user_edit_profile_change_picture,ui_user_registration_zip_search_window,ui_user_registration_zip_search_window_bckBtt,ui_user_edit_group_text_hide;
    UserTypeRadioGroup ui_user_edit_group;
    LinearLayout ui_user_reset_search_bckBtt,ui_user_edit_profile_check_text;
    UserProfile currentUserProfile;
    FormField_T1_TextBox ui_user_edit_profile_name,ui_user_edit_profile_password,ui_user_edit_profile_repassword,ui_user_edit_profile_birthdate,
            ui_user_edit_profile_gender;
    FormField_T2_TextBox ui_user_edit_profile_address_zip,ui_user_edit_profile_address2;
    FormField_T3_TextBox_Spinner ui_user_edit_profile_mobile_number;
    FormField_T4_TextBox ui_user_edit_profile_account_holder;
    Spinner_T5 ui_user_edit_profile_bank_name;
    EditText ui_user_edit_profile_account_number;
    SquareButton_T1 ui_user_edit_profile_done_button,ui_user_edit_profile_back_button;

    Table_T1 ui_user_registration_zip_search_window_street_st, ui_user_registration_zip_search_window_house_st;
    FormField_T2_TextBox ui_user_registration_address_zip,ui_user_registration_input_email;
    FormField_T2_TextBox ui_user_edit_profile_address1;
    Spinner_T2 ui_user_registration_zip_search_window_street_city_spinner, ui_user_registration_zip_search_window_street_city_province;

    CCImagePicker CCIP;
    CircularImageView profilePicture;
    File SelectedProfileImage;
    ProgressDialog progress;
    ImagePicker imagePicker;
    CameraImagePicker cameraPicker;
    UserActivity_FrgMng_Interface mCallback;

    FormField_T1_TextBox ui_user_edit_profile_name_kr, ui_user_edit_profile_about_en, ui_user_edit_profile_about_kr, ui_user_edit_profile_profile_en, ui_user_edit_profile_profile_kr;

    public static final int REQUEST_SELECT_FILE = 100;
    private final static int FILECHOOSER_RESULTCODE = 1;

    private ValueCallback<Uri> mUploadMessage;
    public ValueCallback<Uri[]> uploadMessage;

    Calendar calendar;

    private static final String FAKE_PASSWORD = "*****##******";

    public UserFrgEditProfile() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ui_user__edit_profile, container, false);
    }

    Activity activity;
    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        this.activity = activity;
        try {
            mCallback = (UserActivity_FrgMng_Interface) activity;
            UserActivity_FragmentsManager DA = (UserActivity_FragmentsManager) getActivity();
            DA.frg_listener = this;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement UserActivity_Interface");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        calendar = Calendar.getInstance();

        prepareEditProfileStage();

        prepareZipcode();

        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent intent_data) {

        System.out.println(requestCode+"---------->"+resultCode);

        if (requestCode == Picker.PICK_IMAGE_DEVICE) {
            System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
                imagePicker.submit(intent_data);
            }
        }else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
            System.out.println("PICK_IMAGE_CAMERA");
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("PICK_IMAGE_CAMERA  RESULT_OK");
                cameraPicker.submit(intent_data);
            }
        }else{
            if (resultCode == Activity.RESULT_OK) {
                System.out.println("PICK_IMAGE_DEVICE  RESULT_OK");
                imagePicker.submit(intent_data);
            }
        }

        /*if (resultCode == UserActivity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                imagePicker.submit(intent_data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                cameraPicker.submit(intent_data);
            }
        }else{

        }*/

    }

    void prepareEditProfileStage()
    {
        CCIP = new CCImagePicker(getActivity());
        CCIP.setListener(new CCImagePicker.task() {
            @Override
            public void fromCamera() {
                cameraPicker = new CameraImagePicker(getActivity());
                cameraPicker.setImagePickerCallback(new ImagePickerCallback(){
                                                        @Override
                                                        public void onImagesChosen(List<ChosenImage> images) {
                                                            // Display images
                                                            System.out.println(images.toString());
                                                            System.out.println(images.get(0).toString());
                                                            System.out.println(images.get(0).getOriginalPath());
                                                            SelectedProfileImage = new File(images.get(0).getOriginalPath());

                                                            Picasso.with(getActivity().getWindow().getContext())
                                                                    .load(SelectedProfileImage)
                                                                    .placeholder(R.drawable.transparent)
                                                                    .error(R.drawable.transparent)
                                                                    .transform(new PicassoRoundTransform())
                                                                    .into(profilePicture);
                                                        }

                                                        @Override
                                                        public void onError(String message) {
                                                            // Do error handling
                                                        }
                                                    }
                );
                // imagePicker.shouldGenerateMetadata(false); // Default is true
                // imagePicker.shouldGenerateThumbnails(false); // Default is true
                String outputPath = cameraPicker.pickImage();
            }

            @Override
            public void fromGallery() {
                imagePicker = new ImagePicker(UserFrgEditProfile.this);
                imagePicker.setImagePickerCallback(new ImagePickerCallback(){
                                                       @Override
                                                       public void onImagesChosen(List<ChosenImage> images) {
                                                           System.out.println(images.toString());
                                                           System.out.println(images.get(0).toString());
                                                           System.out.println(images.get(0).getOriginalPath());
                                                           //Picasso.with(getWindow().getContext()).load(new File(images.get(0).getOriginalPath())).into(profilePicture);
                                                           SelectedProfileImage = new File(images.get(0).getOriginalPath());

                                                           Picasso.with(getActivity().getWindow().getContext())
                                                                   .load(SelectedProfileImage)
                                                                   .placeholder(R.drawable.transparent)
                                                                   .error(R.drawable.transparent)
                                                                   .transform(new PicassoRoundTransform())
                                                                   .into(profilePicture);
                                                       }

                                                       @Override
                                                       public void onError(String message) {
                                                           // Do error handling
                                                       }
                                                   }
                );
                // imagePicker.allowMultiple(); // Default is false
                // imagePicker.shouldGenerateMetadata(false); // Default is true
                // imagePicker.shouldGenerateThumbnails(false); // Default is true
                imagePicker.pickImage();
            }
        });

        profilePicture = (CircularImageView) getActivity().findViewById(R.id.profilePicture);

        ui_user_edit_profile_heading_name = (TextView) getActivity().findViewById(R.id.ui_user_edit_profile_heading_name);
        ui_user_edit_profile_email = (TextView) getActivity().findViewById(R.id.ui_user_edit_profile_email);

        ui_user_edit_profile_change_picture = (LinearLayout) getActivity().findViewById(R.id.ui_user_edit_profile_change_picture);
        ui_user_edit_profile_change_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CCIP.show();
            }
        });

        ui_user_edit_profile_password = (FormField_T1_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_password);
        ui_user_edit_profile_repassword = (FormField_T1_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_repassword);
        ui_user_edit_profile_name = (FormField_T1_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_name);
        ui_user_edit_profile_name_kr = (FormField_T1_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_name_kr);
        ui_user_edit_profile_about_en = (FormField_T1_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_about_en);
        ui_user_edit_profile_about_kr = (FormField_T1_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_about_kr);
        ui_user_edit_profile_profile_en = (FormField_T1_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_profile_en);
        ui_user_edit_profile_profile_kr = (FormField_T1_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_profile_kr);


        ui_user_edit_profile_birthdate = (FormField_T1_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_birthdate);
        ui_user_edit_profile_birthdate.disableEditable();
        ui_user_edit_profile_birthdate._onClick(new FormField_T1_TextBox._click() {
            @Override
            public void done() {
                DPicker dp= new DPicker(getActivity().getWindow().getContext());
                dp.setTheTitle("Birthday");
                dp.setDoneBttBg(getResources().getDrawable(R.drawable.next_button));
                //dp.setCancelBttBg(getResources().getDrawable(R.drawable.loginbuttonshape));
                dp.setCancelBttBg(getResources().getDrawable(R.drawable.birthday_box_cancel));

                if(TextUtils.isEmpty(ui_user_edit_profile_birthdate.getText()))
                {
                    dp.setDefaultValue(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
                }else{

                    String i = ui_user_edit_profile_birthdate.getText();
                    System.out.println(i);
                    String[] _split = i.split(". ");
                    dp.setDefaultValue(Integer.valueOf(_split[2]), (Integer.valueOf(_split[1]) - 1), Integer.valueOf(_split[0]));
                }

                dp.setListener(new DPicker.Success() {
                    @Override
                    public void Done(Date _date) {
                        ui_user_edit_profile_birthdate.setText(_date.getYear()+". "+(_date.getMonth() + 1)+". "+_date.getDate());
                        ui_user_edit_profile_birthdate.setTheOtherValue((_date.getMonth() < 10 ? "0":"")+(_date.getMonth() + 1)+". "+(_date.getDate() < 10 ? "0":"")+_date.getDate()+". "+_date.getYear());
                    }
                });
                dp.show();
            }

            @Override
            public void Focused() {

            }

            @Override
            public void Unfocused() {

            }
        });

        ui_user_edit_profile_gender = (FormField_T1_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_gender);
        ui_user_edit_profile_gender.disableEditable();
        ui_user_edit_profile_gender._onClick(new FormField_T1_TextBox._click() {
            @Override
            public void done() {
                final GenderSelectionPopUpBox gp=new GenderSelectionPopUpBox(getActivity().getWindow().getContext());
                gp.setDoneBttBg(getResources().getDrawable(R.drawable.next_button));
                if(!TextUtils.isEmpty(ui_user_edit_profile_gender.getText()))
                {
                    gp.setDefaultValue(ui_user_edit_profile_gender.getText());
                }

                gp.setListener(new GenderSelectionPopUpBox.Success() {
                    @Override
                    public void Done(String value) {
                        ui_user_edit_profile_gender.setText(value);
                        ui_user_edit_profile_gender.setTheOtherValue(gp.get_value2());
                    }
                });
                gp.show();
            }

            @Override
            public void Focused() {

            }

            @Override
            public void Unfocused() {

            }
        });

        ui_user_edit_profile_mobile_number = (FormField_T3_TextBox_Spinner) getActivity().findViewById(R.id.ui_user_edit_profile_mobile_number);
        ui_user_edit_profile_mobile_number.setListener(new FormField_T3_TextBox_Spinner.Success() {
            @Override
            public void done() {
                OptionListing_T1 t = new OptionListing_T1(getActivity().getWindow().getContext());
                List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();
                final String[] theList = new String[]{"880", "32", "226", "359", "387", "1-246", "681", "590", "1-441", "673", "591", "973", "257", "229", "975", "1-876", "267", "685", "599", "55", "1-242", "44-1534", "375", "501", "7", "250", "381", "670", "262", "993", "992", "40", "690", "245", "1-671", "502", "30", "240", "590", "81", "592", "44-1481", "594", "995", "1-473", "44", "241", "503", "224", "220", "299", "350", "233", "968", "216", "962", "385", "509", "36", "852", "504", "58", "1-787","1-939", "970", "680", "351", "47", "595", "964", "507", "689", "675", "51", "92", "63", "870", "48", "508", "260", "212", "372", "20", "27", "593", "39", "84", "677", "251", "252", "263", "966", "34", "291", "382", "373", "261", "590", "212", "377", "998", "95", "223", "853", "976", "692", "389", "230", "356", "265", "960", "596", "1-670", "1-664", "222", "44-1624", "256", "255", "60", "52", "972", "33", "246", "290", "358", "679", "500", "691", "298", "505", "31", "47", "264", "678", "687", "227", "672", "234", "64", "977", "674", "683", "682", "225", "41", "57", "86", "237", "56", "61", "1", "242", "236", "243", "420", "357", "61", "506", "599", "238", "53", "268", "963", "599", "996", "254", "211", "597", "686", "855", "1-869", "269", "239", "421", "82", "386", "850", "965", "221", "378", "232", "248", "7", "1-345", "65", "46", "249", "1-809", "1-829", "1-767", "253", "45", "1-284", "49", "967", "213", "1", "598", "262", "1", "961", "1-758", "856", "688", "886", "1-868", "90", "94", "423", "371", "676", "370", "352", "231", "266", "66", "228", "235", "1-649", "218", "379", "1-784", "971", "376", "1-268", "93", "1-264", "1-340", "354", "98", "374", "355", "244", "1-684", "54", "61", "43", "297", "91", "358-18", "994", "353", "62", "380", "974", "258"};
                for (int i = 0; i < theList.length; i++) {
                    RadioButtonInfo io1=new RadioButtonInfo();
                    io1.setID(i);
                    io1.setText(theList[i]);
                    btns.add(io1);
                }
                t.setup2("Mobile", btns, ui_user_edit_profile_mobile_number.getSpinnerText());
                t.setListener(new OptionListing_T1.Success() {
                    @Override
                    public void Done(RadioButtonInfo value) {
                        ui_user_edit_profile_mobile_number.setSpinnerText(value.getText());
                    }
                });
                t.show();
            }

            @Override
            public void Focused() {

            }

            @Override
            public void Unfocused() {

            }
        });
        //ui_user_edit_profile_address_zip = (FormField_T2_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_address_zip);
        ui_user_registration_address_zip = (FormField_T2_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_address_zip);
        ui_user_edit_profile_address1 = (FormField_T2_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_address1);
        ui_user_edit_profile_address2 = (FormField_T2_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_address2);

        ui_user_edit_group_text_hide = (LinearLayout) getActivity().findViewById(R.id.ui_user_edit_group_text_hide);
        ui_user_edit_group_text_hide.setVisibility(View.GONE);
        ui_user_edit_group = (UserTypeRadioGroup) getActivity().findViewById(R.id.ui_user_edit_group);
        ui_user_edit_group.setElements();
        ui_user_edit_group.setListener(new UserTypeRadioGroup.Success() {
            @Override
            public void done(String id) {
                if(!id.contains("1")){
                    ui_user_edit_group_text_hide.setVisibility(View.VISIBLE);
                }else{
                    ui_user_edit_group_text_hide.setVisibility(View.GONE);
                }
            }
        });
        ui_user_edit_profile_account_holder = (FormField_T4_TextBox) getActivity().findViewById(R.id.ui_user_edit_profile_account_holder);
        ui_user_edit_profile_bank_name = (Spinner_T5) getActivity().findViewById(R.id.ui_user_edit_profile_bank_name);
        ui_user_edit_profile_account_number = (EditText) getActivity().findViewById(R.id.ui_user_edit_profile_account_number);
        ui_user_edit_profile_check_text = (LinearLayout) getActivity().findViewById(R.id.ui_user_edit_profile_check_text);

        ui_user_edit_profile_back_button= (SquareButton_T1)getActivity().findViewById(R.id.ui_user_edit_profile_back_button);

        ui_user_edit_profile_back_button._onClick(new SquareButton_T1._click() {
            @Override
            public void done() {

                getActivity().finish();
            }
        });
        ui_user_edit_profile_done_button = (SquareButton_T1)getActivity().findViewById(R.id.ui_user_edit_profile_done_button);
        ui_user_edit_profile_done_button._onClick(new SquareButton_T1._click() {
            @Override
            public void done() {
                if(validateEditPage())
                {
                    progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Sending data to server....", "Please wait for a while.", true);

                    User.UpdateProfile(SelectedProfileImage, currentUserProfile, new User.message() {
                        @Override
                        public void Success(String message) {
                            progress.dismiss();
                            mCallback.updateProfileDetails();
                            if(!TextUtils.isEmpty(message))
                            {
                                Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void ReqSMS() {
                            progress.dismiss();
                            System.out.println("The success found");
                            final SmsVerification smsV = new SmsVerification(getActivity().getWindow().getContext());
                            smsV.setDoneBttBg(getResources().getDrawable(R.drawable.next_button));
                            smsV.setCancelable(false);
                            smsV.setListener(new SmsVerification.Success() {
                                @Override
                                public void Done(String value) {

                                    UserAuthentication.VerifyBySMS(value, new UserAuthentication.message() {
                                        @Override
                                        public void Success(String message) {
                                            User.UpdateProfile(SelectedProfileImage, currentUserProfile, new User.message() {
                                                @Override
                                                public void Success(String message) {
                                                    mCallback.updateProfileDetails();
                                                    if(!TextUtils.isEmpty(message))
                                                    {
                                                        Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_LONG).show();
                                                    }
                                                }

                                                @Override
                                                public void ReqSMS() {

                                                }

                                                @Override
                                                public void TokenRefreshRequired() {

                                                    User.UpdateProfile(SelectedProfileImage, currentUserProfile, new User.message() {
                                                        @Override
                                                        public void Success(String message) {
                                                            progress.dismiss();
                                                            mCallback.updateProfileDetails();
                                                            if(!TextUtils.isEmpty(message))
                                                            {
                                                                Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_LONG).show();
                                                            }
                                                        }

                                                        @Override
                                                        public void ReqSMS() {

                                                        }

                                                        @Override
                                                        public void TokenRefreshRequired() {
                                                            progress.dismiss();
                                                            Toast.makeText(getActivity().getApplicationContext(),"Token loop found.", Toast.LENGTH_LONG).show();
                                                        }

                                                        @Override
                                                        public void Error(String message) {
                                                            progress.dismiss();
                                                            if(!TextUtils.isEmpty(message))
                                                            {
                                                                Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_LONG).show();
                                                            }
                                                            if(message.equals("Profile Updated and Ended current Access Token!"))
                                                            {
                                                                mCallback.doLogout();
                                                            }
                                                        }
                                                    });

                                                }

                                                @Override
                                                public void Error(String message) {
                                                    if(!TextUtils.isEmpty(message))
                                                    {
                                                        Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_LONG).show();
                                                    }
                                                    if(message.equals("Profile Updated and Ended current Access Token!"))
                                                    {
                                                        mCallback.doLogout();
                                                    }
                                                }
                                            });

                                            smsV.hideMe();
                                        }

                                        @Override
                                        public void Error(String message) {
                                            if(!TextUtils.isEmpty(message))
                                            {
                                                Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_LONG).show();
                                            }
                                            if(message.equals("Profile Updated and Ended current Access Token!"))
                                            {
                                                mCallback.doLogout();
                                            }
                                        }
                                    });
                                }

                                @Override
                                public void ResendSMS() {
                                    UserAuthentication.ResendSMS("+" + ui_user_edit_profile_mobile_number.getText(), new UserAuthentication.message() {
                                        @Override
                                        public void Success(String message) {
                                            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                                        }

                                        @Override
                                        public void Error(String message) {
                                            Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                            smsV.show();
                        }

                        @Override
                        public void TokenRefreshRequired() {
                            System.out.println("TokenRefreshRequired");

                            User.UpdateProfile(SelectedProfileImage, currentUserProfile, new User.message() {
                                @Override
                                public void Success(String message) {
                                    progress.dismiss();
                                    if(!TextUtils.isEmpty(message))
                                    {
                                        Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void ReqSMS() {

                                }

                                @Override
                                public void TokenRefreshRequired() {
                                    progress.dismiss();
                                    Toast.makeText(getActivity().getApplicationContext(),"Token loop found.", Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void Error(String message) {
                                    progress.dismiss();
                                    if(!TextUtils.isEmpty(message))
                                    {
                                        Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_LONG).show();
                                    }
                                    if(message.equals("Profile Updated and Ended current Access Token!"))
                                    {
                                        mCallback.doLogout();
                                    }
                                }
                            });
                        }
                        @Override
                        public void Error(String message) {
                            System.out.println("TokenRefreshRequired");
                            progress.dismiss();
                            if(!TextUtils.isEmpty(message))
                            {
                                Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_LONG).show();
                            }
                            if(message.equals("Profile Updated and Ended current Access Token!"))
                            {
                                mCallback.doLogout();
                            }
                        }
                    });
                }
            }
        });



        User.GetProfileDataProfile(getActivity().getWindow().getContext(), new User.userProfileRetrieve() {
            @Override
            public void Success(UserProfile userProfile) {
                currentUserProfile = userProfile;
                progress.dismiss();
                FillUp_EditProfilePage_WithData();
            }

            @Override
            public void TokenRefreshRequired() {
                User.GetProfileDataProfile(getActivity().getWindow().getContext(), new User.userProfileRetrieve() {
                    @Override
                    public void Success(UserProfile userProfile) {
                        currentUserProfile = userProfile;
                        progress.dismiss();
                        FillUp_EditProfilePage_WithData();
                    }

                    @Override
                    public void TokenRefreshRequired() {
                        Toast.makeText(getActivity().getApplicationContext(),"Token loop found.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_LONG).show();
            }
        });

        ui_user_reset_search_bckBtt = (LinearLayout) getActivity().findViewById(R.id.ui_user_reset_search_bckBtt);
        ui_user_reset_search_bckBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getActivity().findViewById(R.id.UserProfileEditPage).setVisibility(View.GONE);
                getActivity().finish();
            }
        });
    }

    void prepareZipcode(){
        ui_user_registration_zip_search_window = (LinearLayout) getActivity().findViewById(R.id.ui_user_registration_zip_search_window);
        ui_user_registration_zip_search_window_bckBtt = (LinearLayout) getActivity().findViewById(R.id.ui_user_registration_zip_search_window_bckBtt);

        getActivity().findViewById(R.id.ui_user_registration_zip_search_window_bckBtt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().findViewById(R.id.ui_user_registration_zip_search_window).setVisibility(View.GONE);
            }
        });


        ui_user_registration_zip_search_window_street_st = (Table_T1) getActivity().findViewById(R.id.ui_user_registration_zip_search_window_street_st);
        ui_user_registration_zip_search_window_street_st.setColumnNames(new String[]{"zipcode", "address"});
        List<Col2RowDetails> rows = new ArrayList<>();

        Col2RowDetails crd = new Col2RowDetails();
        crd.set_value("12345");
        crd.setCol1_Contents("12345");
        crd.setCol2_Contents("Place 1");
        rows.add(crd);

        Col2RowDetails crd22 = new Col2RowDetails();
        crd22.set_value("67890");
        crd22.setCol1_Contents("67890");
        crd22.setCol2_Contents("Place 2");
        rows.add(crd22);

        Col2RowDetails crd23 = new Col2RowDetails();
        crd23.set_value("69876");
        crd23.setCol1_Contents("69876");
        crd23.setCol2_Contents("Place 3");
        rows.add(crd23);

        Col2RowDetails crd24 = new Col2RowDetails();
        crd24.set_value("91937");
        crd24.setCol1_Contents("91937");
        crd24.setCol2_Contents("Place 4");
        rows.add(crd24);

        Col2RowDetails crd25 = new Col2RowDetails();
        crd25.set_value("50923");
        crd25.setCol1_Contents("50923");
        crd25.setCol2_Contents("Place 5");
        rows.add(crd25);

        ui_user_registration_zip_search_window_street_st.setContainerElements(rows, "8878");
        ui_user_registration_zip_search_window_street_st.setListener(new Table_T1.Success() {
            @Override
            public void done(Col2RowDetails _selected) {
                getActivity().findViewById(R.id.ui_user_registration_zip_search_window).setVisibility(View.GONE);
                ui_user_registration_address_zip.setText(_selected.get_value());
                ui_user_edit_profile_address1.setText(_selected.getCol2_Contents());
            }
        });

        ui_user_registration_zip_search_window_house_st = (Table_T1) getActivity().findViewById(R.id.ui_user_registration_zip_search_window_house_st);
        ui_user_registration_zip_search_window_house_st.setColumnNames(new String[]{"zipcode", "address"});
        List<Col2RowDetails> rows2 = new ArrayList<>();

        Col2RowDetails crd2 = new Col2RowDetails();
        crd2.set_value("88887");
        crd2.setCol1_Contents("88887");
        crd2.setCol2_Contents("Lishfh");
        rows2.add(crd2);

        Col2RowDetails crd3 = new Col2RowDetails();
        crd3.set_value("826843");
        crd3.setCol1_Contents("826843");
        crd3.setCol2_Contents("Kbaak");
        rows2.add(crd3);

        Col2RowDetails crd4 = new Col2RowDetails();
        crd4.set_value("90755");
        crd4.setCol1_Contents("90755");
        crd4.setCol2_Contents("Auorty");
        rows2.add(crd4);

        ui_user_registration_zip_search_window_house_st.setContainerElements(rows2, "8878");
        ui_user_registration_zip_search_window_house_st.setListener(new Table_T1.Success() {
            @Override
            public void done(Col2RowDetails _selected) {
                getActivity().findViewById(R.id.ui_user_registration_zip_search_window).setVisibility(View.GONE);
                ui_user_registration_address_zip.setText(_selected.get_value());
                ui_user_edit_profile_address1.setText(_selected.getCol2_Contents());
            }
        });


        ui_user_registration_zip_search_window_street_city_spinner = (Spinner_T2) getActivity().findViewById(R.id.ui_user_registration_zip_search_window_street_city_spinner);
        ui_user_registration_zip_search_window_street_city_spinner.setText("City");
        List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();
        RadioButtonInfo io1=new RadioButtonInfo();
        io1.setID(0);
        io1.setText("Haka");
        btns.add(io1);

        RadioButtonInfo io2=new RadioButtonInfo();
        io2.setID(1);
        io2.setText("Haka2");
        btns.add(io2);

        RadioButtonInfo io3=new RadioButtonInfo();
        io3.setID(2);
        io3.setText("Japan");
        btns.add(io3);

        RadioButtonInfo io4=new RadioButtonInfo();
        io4.setID(3);
        io4.setText("Shanhai");
        btns.add(io4);

        RadioButtonInfo io5=new RadioButtonInfo();
        io5.setID(4);
        io5.setText("Kung");
        btns.add(io5);

        RadioButtonInfo io6=new RadioButtonInfo();
        io6.setID(5);
        io6.setText("Alishohor");
        btns.add(io6);

        RadioButtonInfo io7=new RadioButtonInfo();
        io7.setID(6);
        io7.setText("Vill City");
        btns.add(io7);

        ui_user_registration_zip_search_window_street_city_spinner.setOptions("City", btns,"Haka2");


        ui_user_registration_zip_search_window_street_city_province = (Spinner_T2) getActivity().findViewById(R.id.ui_user_registration_zip_search_window_street_city_province);
        ui_user_registration_zip_search_window_street_city_province.setText("Province");
        List<RadioButtonInfo> btns2 = new ArrayList<RadioButtonInfo>();
        RadioButtonInfo io12=new RadioButtonInfo();
        io12.setID(0);
        io12.setText("Haksad ada");
        btns2.add(io12);

        RadioButtonInfo io22=new RadioButtonInfo();
        io22.setID(1);
        io22.setText("Haks adasdasda2");
        btns2.add(io22);

        RadioButtonInfo io23=new RadioButtonInfo();
        io23.setID(2);
        io23.setText("Aka Sukaj sdf");
        btns2.add(io23);

        RadioButtonInfo io24=new RadioButtonInfo();
        io24.setID(3);
        io24.setText("Aka Sukaj sdf");
        btns2.add(io24);

        RadioButtonInfo io25=new RadioButtonInfo();
        io25.setID(4);
        io25.setText("Aka Sukaj sdf");
        btns2.add(io25);

        RadioButtonInfo io26=new RadioButtonInfo();
        io26.setID(5);
        io26.setText("Swong Ki");
        btns2.add(io26);

        RadioButtonInfo io27=new RadioButtonInfo();
        io27.setID(6);
        io27.setText("Shanghai");
        btns2.add(io27);
        ui_user_registration_zip_search_window_street_city_province.setOptions("Province", btns2,"Aka Sukaj sdf");



        ui_user_registration_address_zip.setListener(new FormField_T2_TextBox.Success() {
            @Override
            public void done() {
                getActivity().findViewById(R.id.ui_user_registration_zip_search_window).setVisibility(View.VISIBLE);
                getActivity().findViewById(R.id.ui_user_registration_zip_search_window_street).setVisibility(View.VISIBLE);
                getActivity().findViewById(R.id.ui_user_registration_zip_search_window_house).setVisibility(View.GONE);
            }

            @Override
            public void Focused() {

            }

            @Override
            public void Unfocused() {

            }
        });

        getActivity().findViewById(R.id.ui_user_registration_zip_search_window_street_show_house).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().findViewById(R.id.ui_user_registration_zip_search_window_street).setVisibility(View.GONE);
                getActivity().findViewById(R.id.ui_user_registration_zip_search_window_house).setVisibility(View.VISIBLE);
            }
        });

        getActivity().findViewById(R.id.ui_user_registration_zip_search_window_house_show_street).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().findViewById(R.id.ui_user_registration_zip_search_window_street).setVisibility(View.VISIBLE);
                getActivity().findViewById(R.id.ui_user_registration_zip_search_window_house).setVisibility(View.GONE);
            }
        });

    }

    private boolean validateEditPage()
    {
        if(TextUtils.isEmpty(ui_user_edit_profile_password.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Password field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(TextUtils.isEmpty(ui_user_edit_profile_repassword.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Re-password field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }

        String password = ui_user_edit_profile_password.getText().toString();
        String repassword = ui_user_edit_profile_repassword.getText().toString();
        if(!password.equals(repassword)){
            Toast.makeText(getActivity().getWindow().getContext(), "Password field doesn't match.", Toast.LENGTH_LONG).show();
            return false;
        }
        currentUserProfile.setPassword(password);

        if(TextUtils.isEmpty(ui_user_edit_profile_name.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Name field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        currentUserProfile.setName(ui_user_edit_profile_name.getText());
        currentUserProfile.setName_kr(ui_user_edit_profile_name_kr.getText());


        if(TextUtils.isEmpty(ui_user_edit_profile_profile_en.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Profile field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        currentUserProfile.setProfile_en(ui_user_edit_profile_profile_en.getText());

        if(TextUtils.isEmpty(ui_user_edit_profile_profile_kr.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Profile Kr field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        currentUserProfile.setProfile_kr(ui_user_edit_profile_profile_kr.getText());

        if(TextUtils.isEmpty(ui_user_edit_profile_about_en.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "About field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        currentUserProfile.setAbout_en(ui_user_edit_profile_about_en.getText());

        if(TextUtils.isEmpty(ui_user_edit_profile_about_kr.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Aboutr KR field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        currentUserProfile.setAbout_kr(ui_user_edit_profile_about_kr.getText());


        if(TextUtils.isEmpty(ui_user_edit_profile_birthdate.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Birthday field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }

        String birthdate = ((ui_user_edit_profile_birthdate.getTheOtherValue() == null)?currentUserProfile.getBirth_date():ui_user_edit_profile_birthdate.getTheOtherValue()).replaceAll(". ", "/");

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(birthdate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if(Utils_Helper.getYearsBetweenDates(convertedDate, calendar.getTime()) < 14)
        {
            Toast.makeText(getContext(), "You need to be 14 years old minimum.", Toast.LENGTH_LONG).show();
            return false;
        }


        currentUserProfile.setBirth_date((ui_user_edit_profile_birthdate.getTheOtherValue() == null)?currentUserProfile.getBirth_date():ui_user_edit_profile_birthdate.getTheOtherValue());

        if(TextUtils.isEmpty(ui_user_edit_profile_gender.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Gender field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        currentUserProfile.setSex(ui_user_edit_profile_gender.getTheOtherValue());

        String phone = ui_user_edit_profile_mobile_number.getText();
        phone = phone.substring(3);
        if(TextUtils.isEmpty(phone))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Mobile number field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        currentUserProfile.setPhone(ui_user_edit_profile_mobile_number.getTextOnly());
        currentUserProfile.setPhone_ext(ui_user_edit_profile_mobile_number.getSpinnerText());

        if(TextUtils.isEmpty(ui_user_registration_address_zip.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Address zip field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        //currentUserProfile.setAddress(ui_user_registration_address_zip.getText());
        currentUserProfile.setZip_code(ui_user_registration_address_zip.getText());

        if(TextUtils.isEmpty(ui_user_edit_profile_address1.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Address 1 zip field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        currentUserProfile.setAddress(ui_user_edit_profile_address1.getText());
        currentUserProfile.setAddress2(ui_user_edit_profile_address2.getText());
        currentUserProfile.setType(Integer.valueOf(ui_user_edit_group.getValue()));



        //System.out.println(currentUserProfile.getZip_code()+"  "+currentUserProfile.getAddress()+"  "+currentUserProfile.getAddress2()+"  "+currentUserProfile.getB());

        //return false;

        /*if(TextUtils.isEmpty(ui_user_edit_profile_address2.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Address 2 zip field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }*/
        /*if(TextUtils.isEmpty(ui_user_edit_profile_account_holder.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Account holder field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(TextUtils.isEmpty(ui_user_edit_profile_account_number.getText()))
        {
            Toast.makeText(getActivity().getWindow().getContext(), "Account number field must be field up.", Toast.LENGTH_LONG).show();
            return false;
        }*/
        return true;
    }

    void FillUp_EditProfilePage_WithData()
    {
        //currentUserProfile.getName();
        ui_user_edit_profile_heading_name.setText(currentUserProfile.getName());
        ui_user_edit_profile_email.setText(currentUserProfile.getEmail());
        ui_user_edit_profile_name.setText(currentUserProfile.getName());
        ui_user_edit_profile_birthdate.setText(currentUserProfile.getBirth_date());
        String[] birthDate = currentUserProfile.getBirth_date().split(". ");
        ui_user_edit_profile_birthdate.setTheOtherValue(birthDate[1]+"/"+birthDate[2]+"/"+birthDate[0]);



        //up.setBirth_date(JO.getString(UserConstants.REG_FIELD_BIRTH_DATE).replaceAll("-", ". "));


        ui_user_edit_profile_gender.setTheOtherValue(currentUserProfile.getSex());

        if(ui_user_edit_profile_gender.getTheOtherValue().equals("M") || ui_user_edit_profile_gender.getTheOtherValue().equals("m") )
        {
            ui_user_edit_profile_gender.setText("Male");
        }else {
            ui_user_edit_profile_gender.setText("Female");
        }

        //ui_user_edit_profile_mobile_number.setText(currentUserProfile.getPhone());
        ui_user_edit_profile_mobile_number.setSpinnerText(currentUserProfile.getPhone_ext());
        ui_user_edit_profile_mobile_number.setText(currentUserProfile.getPhone());
        ui_user_registration_address_zip.setText(currentUserProfile.getZip_code());
        ui_user_edit_profile_address1.setText(currentUserProfile.getAddress());
        ui_user_edit_profile_address2.setText(currentUserProfile.getAddress2());

        Picasso.with(getActivity().getWindow().getContext())
                .load(currentUserProfile.getThumb_image())
                .placeholder(R.drawable.transparent)
                .error(R.drawable.transparent)
                .transform(new PicassoRoundTransform())
                .into(profilePicture);

        ui_user_edit_group.setValue(currentUserProfile.getType());
        ui_user_edit_profile_password.setText(FAKE_PASSWORD);
        ui_user_edit_profile_repassword.setText(FAKE_PASSWORD);

        ui_user_edit_profile_name_kr.setText(currentUserProfile.getName_kr());
        ui_user_edit_profile_about_en.setText(currentUserProfile.getAbout_en().replaceAll("null",""));
        ui_user_edit_profile_about_kr.setText(currentUserProfile.getAbout_kr().replaceAll("null",""));
        ui_user_edit_profile_profile_en.setText(currentUserProfile.getProfile_en().replaceAll("null",""));
        ui_user_edit_profile_profile_kr.setText(currentUserProfile.getProfile_kr().replaceAll("null",""));
    }

    @Override
    public void _DevicebackPressed() {

    }

    @Override
    public boolean ifDetailsPageHiden() {
        return true;
    }
}
