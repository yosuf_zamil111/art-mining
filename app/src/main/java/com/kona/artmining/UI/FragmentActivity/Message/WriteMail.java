package com.kona.artmining.UI.FragmentActivity.Message;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Messaging;
import com.kona.artmining.Interface.MessagingActivity_Interface;
import com.kona.artmining.Model.Message;
import com.kona.artmining.R;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

public class WriteMail extends Fragment {

    MessagingActivity_Interface mCallback;
    LinearLayout backBtt, sentMail;

    EditText receiverName, mailSubject, mailBody;

    ProgressDialog progress;
    Message theSelectedMessage;

    long ReceiverID;

    public WriteMail() {}

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            mCallback = (MessagingActivity_Interface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement MessagingActivity_Interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ui_message_frg_write_mail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStage(view);
    }

    void prepareStage(View view)
    {
        backBtt = (LinearLayout) view.findViewById(R.id.backBtt);
        backBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.exit();
            }
        });

        sentMail = (LinearLayout) view.findViewById(R.id.sentMail);
        sentMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ValidateFields())
                {
                    SendMail();
                }
            }
        });

        receiverName = (EditText) view.findViewById(R.id.receiverName);
        mailSubject = (EditText) view.findViewById(R.id.amailSubject);
        mailBody = (EditText) view.findViewById(R.id.mailBody);

        receiverName.setText(getArguments().getString(GeneralConstants.RECEIVER_NAME));
        ReceiverID = getArguments().getLong(GeneralConstants.RECEIVER_ID);
    }

    void SendMail()
    {
        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

        final RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("to_id",ReceiverID+"");
        params.add("subject",mailSubject.getText().toString());
        params.add("body",mailBody.getText().toString());

        Messaging.SendMail(params, new Messaging.response() {
            @Override
            public void Success() {
                progress.dismiss();
                mCallback.updateSentMails();
                mCallback.exit();
                Toast.makeText(getContext(), "Message Sent", Toast.LENGTH_LONG).show();
            }

            @Override
            public void TokenRefreshRequired() {
                Messaging.SendMail(params, new Messaging.response() {
                    @Override
                    public void Success() {
                        progress.dismiss();
                        mCallback.updateSentMails();
                        mCallback.exit();
                        Toast.makeText(getContext(), "Message Sent", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    boolean ValidateFields()
    {
        if(TextUtils.isEmpty(mailSubject.getText().toString()))
        {
            Toast.makeText(getContext(), "Add some subject", Toast.LENGTH_LONG).show();
            return false;
        }

        if(mailSubject.getText().toString().length() < 2)
        {
            Toast.makeText(getContext(), "Type something more, please.", Toast.LENGTH_LONG).show();
            return false;
        }

        if(TextUtils.isEmpty(mailBody.getText().toString()))
        {
            Toast.makeText(getContext(), "Add some text for message", Toast.LENGTH_LONG).show();
            return false;
        }

        if(mailBody.getText().toString().length() < 2)
        {
            Toast.makeText(getContext(), "Type something more for the message, please.", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

}
