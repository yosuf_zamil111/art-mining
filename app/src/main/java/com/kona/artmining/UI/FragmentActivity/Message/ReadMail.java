package com.kona.artmining.UI.FragmentActivity.Message;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Messaging;
import com.kona.artmining.Interface.MessagingActivity_Interface;
import com.kona.artmining.Model.Message;
import com.kona.artmining.R;

public class ReadMail extends Fragment {

    MessagingActivity_Interface mCallback;
    LinearLayout backBtt;
    ImageView replyMail, deleteMail;
    TextView theSender,mailSubject,mailTime,mailDetails, senderOrReceived;

    ProgressDialog progress;
    Message theSelectedMessage;

    public ReadMail(){}

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            mCallback = (MessagingActivity_Interface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement MessagingActivity_Interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ui_message_frg_read_mail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStage(view);
    }

    void prepareStage(View view)
    {
        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

        backBtt = (LinearLayout) view.findViewById(R.id.backBtt);
        backBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getArguments().getInt(UserConstants.MESSAGE_TYPE)==UserConstants.MESSAGE_TYPE_INBOX)
                {
                    mCallback.updateInbox();
                }
                else
                {
                    mCallback.updateSentMails();
                }
                mCallback.readMail_topBackPressed();
            }
        });

        replyMail = (ImageView) view.findViewById(R.id.replyMail);
        replyMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.replyAgainstMessage(theSelectedMessage.getId(), getArguments().getInt(UserConstants.MESSAGE_TYPE));
            }
        });

        deleteMail = (ImageView) view.findViewById(R.id.deleteMail);
        deleteMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getArguments().getInt(UserConstants.MESSAGE_TYPE)==UserConstants.MESSAGE_TYPE_TRASH)
                {
                    mCallback.removeMessage(theSelectedMessage.getId(), getArguments().getInt(UserConstants.MESSAGE_TYPE));
                }else{
                    mCallback.trashMessage(theSelectedMessage.getId(), getArguments().getInt(UserConstants.MESSAGE_TYPE));
                }
                mCallback.readMail_topBackPressed();
            }
        });

        theSender = (TextView) view.findViewById(R.id.theSender);
        mailSubject = (TextView) view.findViewById(R.id.mailSubject);
        mailTime = (TextView) view.findViewById(R.id.mailTime);
        mailDetails = (TextView) view.findViewById(R.id.mailDetails);
        senderOrReceived  = (TextView) view.findViewById(R.id.senderOrReceived);

        Messaging.GetMessageDetails(String.valueOf(getArguments().getLong(GeneralConstants.MESSAGE_ID)),new Messaging.Message_Retrieved() {
            @Override
            public void Success(Message _message)
            {
                theSelectedMessage = _message;
                fillUpMessagePanel();
            }

            @Override
            public void TokenRefreshRequired()
            {
                Messaging.GetMessageDetails(getArguments().getString(GeneralConstants.MESSAGE_ID),new Messaging.Message_Retrieved() {
                            @Override
                            public void Success(Message _message)
                            {
                                theSelectedMessage = _message;
                                fillUpMessagePanel();
                            }

                            @Override
                            public void TokenRefreshRequired()
                            {

                            }

                            @Override
                            public void NotFound() {

                            }

                            @Override
                            public void Error(String message) {
                                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                            }
                        });
            }

            @Override
            public void NotFound() {

            }

            @Override
            public void Error(String message)
            {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void fillUpMessagePanel()
    {
        if(getArguments().getInt(UserConstants.MESSAGE_TYPE)==UserConstants.MESSAGE_TYPE_INBOX)
        {
            theSender.setText(theSelectedMessage.getSender_name());
            senderOrReceived.setText("Sender");
        }else if(getArguments().getInt(UserConstants.MESSAGE_TYPE)==UserConstants.MESSAGE_TYPE_SENT)
        {
            theSender.setText(theSelectedMessage.getReceiver_name());
            senderOrReceived.setText("Received");
        }else{
            theSender.setText(theSelectedMessage.getSender_name());
            senderOrReceived.setText("Sender");
        }

        mailSubject.setText(theSelectedMessage.getSubject());
        mailTime.setText(theSelectedMessage.getDate());
        mailDetails.setText(theSelectedMessage.getMail_details());

        progress.dismiss();
    }

}
