package com.kona.artmining.UI.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.UserAuthentication;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.R;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.kona.artmining.custom_components.UserTypeRadioGroup;
import com.kona.artmining.custom_popup_box.BoxWithOptions;
import com.kona.artmining.custom_popup_box.CCImagePicker;
import com.kona.artmining.custom_popup_box.GenderSelectionPopUpBox;
import com.kona.artmining.custom_popup_box.OptionListing_T1;
import com.kona.artmining.custom_popup_box.SmsVerification;
import com.kona.artmining.decent_date_picker.DPicker;
import com.kona.artmining.model.RadioButtonInfo;
import com.kona.artmining.option_table.Table_T1;
import com.kona.artmining.option_table.modals.Col2RowDetails;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity{

    WebView htmlLayout;
    LinearLayout htmlLayoutContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        htmlLayoutContainer = (LinearLayout) findViewById(R.id.fullscreen_webview);
        htmlLayout = (WebView) findViewById(R.id.fullscreen_webview_container);
        WebSettings webSettings = htmlLayout.getSettings();
        webSettings.setJavaScriptEnabled(true);
        htmlLayout.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });


        /*mMonthSpinner = (NumberPicker) findViewById(R.id.month);
        mMonthSpinner.setMinValue(0);
        mMonthSpinner.setMaxValue(new DateFormatSymbols().getShortMonths().length - 1);
        mMonthSpinner.setDisplayedValues(new DateFormatSymbols().getShortMonths());
        mMonthSpinner.setOnLongPressUpdateInterval(200);
        mMonthSpinner.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

            }
        });*/
        //mMonthSpinnerInput = (EditText) mMonthSpinner.findViewById(R.id.numberpicker_input);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        UserTypeRadioGroup utrg = (UserTypeRadioGroup) findViewById(R.id.utrg);
        utrg.setElements();

        findViewById(R.id.Hitler).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SmsVerification smsV = new SmsVerification(getWindow().getContext());
                smsV.setDoneBttBg(getResources().getDrawable(R.drawable.next_button));
                smsV.setListener(new SmsVerification.Success() {
                    @Override
                    public void Done(String value) {
                        System.out.println();
                    }
                });
                smsV.show();


                GenderSelectionPopUpBox gp=new GenderSelectionPopUpBox(getWindow().getContext());
                gp.setDoneBttBg(getResources().getDrawable(R.drawable.next_button));
                gp.setListener(new GenderSelectionPopUpBox.Success() {
                    @Override
                    public void Done(String value) {
                        System.out.println("> "+value);
                    }
                });
                gp.show();

                OptionListing_T1 t = new OptionListing_T1(getWindow().getContext());

                List<RadioButtonInfo> btns = new ArrayList<RadioButtonInfo>();

                RadioButtonInfo io1=new RadioButtonInfo();
                io1.setID(0);
                io1.setText("Haka");
                btns.add(io1);

                RadioButtonInfo io2=new RadioButtonInfo();
                io2.setID(1);
                io2.setText("Haka2");
                btns.add(io2);

                t.setup("Mobile", btns, "1");
                t.setListener(new OptionListing_T1.Success() {
                    @Override
                    public void Done(String value) {
                        System.out.println("> "+value);
                    }
                });
                t.show();

            }
        });

        findViewById(R.id.UserProfilePage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent = new Intent(MainActivity.this, UserAuthenticationActivity.class);
                MainActivity.this.startActivity(myIntent);

                UserAuthentication.DoLogin(new TheResponse() {
                    @Override
                    public void success(String response) {
                        System.out.println("Success me");
                    }

                    @Override
                    public void failed() {
                        System.out.println("Why Success Failed");
                    }
                });

                BoxWithOptions xp=new BoxWithOptions(getWindow().getContext());
                xp.show();

            }
        });*/

        if(Prefs.contains(UserConstants.AccessToken)){
            findViewById(R.id.UserProfilePage).setVisibility(View.GONE);
            findViewById(R.id.UserLogout).setVisibility(View.VISIBLE);
            findViewById(R.id.UserThemeListing).setVisibility(View.VISIBLE);
            findViewById(R.id.UserUserListing).setVisibility(View.VISIBLE);
            findViewById(R.id.UserRejectedUserListing).setVisibility(View.VISIBLE);
            findViewById(R.id.UserApprovedUserListing).setVisibility(View.VISIBLE);
        }else{
            findViewById(R.id.UserProfilePage).setVisibility(View.VISIBLE);
            findViewById(R.id.UserLogout).setVisibility(View.GONE);
            findViewById(R.id.UserThemeListing).setVisibility(View.GONE);
            findViewById(R.id.UserUserListing).setVisibility(View.GONE);
            findViewById(R.id.UserRejectedUserListing).setVisibility(View.GONE);
            findViewById(R.id.UserApprovedUserListing).setVisibility(View.GONE);
        }

        findViewById(R.id.BackButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                htmlLayoutContainer.setVisibility(View.GONE);
            }
        });

        findViewById(R.id.UserThemeListing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestParams params=new RequestParams();
                params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));
                artmineRestClient.get("/theme/listing.json", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                        System.out.println("-------->"+response.toString());
                        try {
                            if(response.getBoolean("success")){
                                htmlLayoutContainer.setVisibility(View.VISIBLE);
                                htmlLayout.loadUrl(ServerConstants.THEME_LISTING+Prefs.getString(UserConstants.AccessToken,""));
                            }else{
                                Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                        Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
        findViewById(R.id.UserUserListing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestParams params=new RequestParams();
                params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));
                artmineRestClient.get("/theme/listing.json", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                        System.out.println("-------->"+response.toString());
                        try {
                            if(response.getBoolean("success")){
                                htmlLayoutContainer.setVisibility(View.VISIBLE);
                                htmlLayout.loadUrl(ServerConstants.USER_LISTING+Prefs.getString(UserConstants.AccessToken,""));
                            }else{
                                Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                        Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
        findViewById(R.id.UserRejectedUserListing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestParams params=new RequestParams();
                params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));
                artmineRestClient.get("/theme/listing.json", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                        System.out.println("-------->"+response.toString());
                        try {
                            if(response.getBoolean("success")){
                                htmlLayoutContainer.setVisibility(View.VISIBLE);
                                htmlLayout.loadUrl(ServerConstants.REJECTED_USER_LISTING+Prefs.getString(UserConstants.AccessToken,""));
                            }else{
                                Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                        Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
        findViewById(R.id.UserApprovedUserListing).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RequestParams params=new RequestParams();
                params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));
                artmineRestClient.get("/theme/listing.json", params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                        System.out.println("-------->"+response.toString());
                        try {
                            if(response.getBoolean("success")){
                                htmlLayoutContainer.setVisibility(View.VISIBLE);
                                htmlLayout.loadUrl(ServerConstants.APPROVED_USER_LISTING+Prefs.getString(UserConstants.AccessToken,""));
                            }else{
                                Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                        Toast.makeText(getApplicationContext(), "You Are not Authorized to view This Page.", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });



        findViewById(R.id.UserLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoLogout();
            }
        });

        findViewById(R.id.UserProfilePage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!Prefs.contains(UserConstants.AccessToken)){
                    Intent myIntent = new Intent(MainActivity.this, UserAuthenticationActivity.class);
                    MainActivity.this.startActivityForResult(myIntent, 1);
                }else{
                    Toast.makeText(getApplicationContext(), "You have already logged in", Toast.LENGTH_LONG).show();
                }

                /*Calendar calendar;
                calendar = Calendar.getInstance();

                DPicker dp= new DPicker(getWindow().getContext());
                dp.setTheTitle("Birthday");
                dp.setDoneBttBg(getResources().getDrawable(R.drawable.next_button));
                dp.setCancelBttBg(getResources().getDrawable(R.drawable.loginbuttonshape));
                dp.setDefaultValue(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
                dp.setListener(new DPicker.Success() {
                    @Override
                    public void Done(Date _date) {
                        System.out.println(_date.getYear());
                    }
                });
                dp.show();*/

                /*
                UserAuthentication.CheckIfEmailExists("a@a.a", new TheResponse() {
                    @Override
                    public void success(String response) {
                        System.out.println("Success");
                    }

                    @Override
                    public void failed() {
                        System.out.println("Failed");
                    }
                });
                */

            }
        });

        findViewById(R.id.KumarSir).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CCImagePicker smsV = new CCImagePicker(getWindow().getContext());
                smsV.setListener(new CCImagePicker.task() {
                    @Override
                    public void fromCamera() {
                        cameraPicker = new CameraImagePicker(MainActivity.this);
                        cameraPicker.setImagePickerCallback(new ImagePickerCallback(){
                                                               @Override
                                                               public void onImagesChosen(List<ChosenImage> images) {
                                                                   // Display images
                                                                   System.out.println("------------------------------------->sasCameradsad------------------------------>");
                                                                   System.out.println(images.toString());
                                                                   System.out.println(images.get(0).toString());
                                                                   System.out.println(images.get(0).getOriginalPath());
                                                               }

                                                               @Override
                                                               public void onError(String message) {
                                                                   // Do error handling
                                                               }
                                                           }
                        );
                        // imagePicker.shouldGenerateMetadata(false); // Default is true
                        // imagePicker.shouldGenerateThumbnails(false); // Default is true
                        String outputPath = cameraPicker.pickImage();
                    }

                    @Override
                    public void fromGallery() {
                        imagePicker = new ImagePicker(MainActivity.this);
                        imagePicker.setImagePickerCallback(new ImagePickerCallback(){
                                                               @Override
                                                               public void onImagesChosen(List<ChosenImage> images) {
                                                                   // Display images
                                                                   System.out.println("------------------------------------->sadsaGallerydsad------------------------------>");
                                                                   System.out.println(images.toString());
                                                                   System.out.println(images.get(0).toString());
                                                                   System.out.println(images.get(0).getOriginalPath());
                                                               }

                                                               @Override
                                                               public void onError(String message) {
                                                                   // Do error handling
                                                               }
                                                           }
                        );
                        // imagePicker.allowMultiple(); // Default is false
                        // imagePicker.shouldGenerateMetadata(false); // Default is true
                        // imagePicker.shouldGenerateThumbnails(false); // Default is true
                        imagePicker.pickImage();
                    }
                });
                smsV.show();
            }
        });
    }

    private ImagePicker imagePicker;
    private CameraImagePicker cameraPicker;
    private String pickerPath;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("------------------------------------->------------------------------>");
        System.out.println("-------------------------------------"+requestCode+"------------------------------>");
        System.out.println("-------------------------------------"+resultCode+"------------------------------>");

        if (requestCode == Picker.PICK_IMAGE_DEVICE) {
            System.out.println("-------------------------------------PICK_IMAGE_DEVICE------------------------------>");
            imagePicker.submit(data);
        } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
            System.out.println("-------------------------------------PICK_IMAGE_CAMERA------------------------------>");
            cameraPicker.submit(data);
        }
    }

    /*
    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        System.out.println("------------------------------------->------------------------------>");
        System.out.println(images.toString());
        System.out.println(images.get(0).toString());
        System.out.println(images.get(0).getOriginalPath());
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }*/

    public void DoLogout()
    {
        Prefs.remove(UserConstants.AccessToken);
        Prefs.remove(UserConstants.RefreshToken);
        Prefs.remove(UserConstants.DO_AUTO_LOGIN);

        findViewById(R.id.UserProfilePage).setVisibility(View.VISIBLE);
        findViewById(R.id.UserLogout).setVisibility(View.GONE);
        findViewById(R.id.UserThemeListing).setVisibility(View.GONE);
        findViewById(R.id.UserUserListing).setVisibility(View.GONE);
        findViewById(R.id.UserRejectedUserListing).setVisibility(View.GONE);
        findViewById(R.id.UserApprovedUserListing).setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed()
    {
        if(htmlLayoutContainer.getVisibility()==View.VISIBLE)
        {
            htmlLayoutContainer.setVisibility(View.GONE);
        }else{
            // If Autologin not allowed.
            if(!Prefs.getBoolean(UserConstants.DO_AUTO_LOGIN, false))
            {
                DoLogout();
            }

            finish();
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        System.out.println("Hit "+Prefs.getBoolean(UserConstants.DO_AUTO_LOGIN, false));
        if(!Prefs.getBoolean(UserConstants.DO_AUTO_LOGIN, false))
        {
            DoLogout();
        }
    }

    @Override
    public void onStop()
    {
        super.onStop();
        System.out.println("Hit "+Prefs.getBoolean(UserConstants.DO_AUTO_LOGIN, false));
        if(!Prefs.getBoolean(UserConstants.DO_AUTO_LOGIN, false))
        {
            DoLogout();
        }
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 1) {
            // Make sure the request was successful

            if(Prefs.contains(UserConstants.AccessToken)){
                findViewById(R.id.UserProfilePage).setVisibility(View.GONE);
                findViewById(R.id.UserLogout).setVisibility(View.VISIBLE);
                findViewById(R.id.UserThemeListing).setVisibility(View.VISIBLE);
                findViewById(R.id.UserUserListing).setVisibility(View.VISIBLE);
                findViewById(R.id.UserRejectedUserListing).setVisibility(View.VISIBLE);
                findViewById(R.id.UserApprovedUserListing).setVisibility(View.VISIBLE);
            }else{
                findViewById(R.id.UserProfilePage).setVisibility(View.VISIBLE);
                findViewById(R.id.UserLogout).setVisibility(View.GONE);
                findViewById(R.id.UserThemeListing).setVisibility(View.GONE);
                findViewById(R.id.UserUserListing).setVisibility(View.GONE);
                findViewById(R.id.UserRejectedUserListing).setVisibility(View.GONE);
                findViewById(R.id.UserApprovedUserListing).setVisibility(View.GONE);
            }

        }
    }*/

}
