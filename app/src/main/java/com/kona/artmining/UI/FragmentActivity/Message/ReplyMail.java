package com.kona.artmining.UI.FragmentActivity.Message;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Messaging;
import com.kona.artmining.Interface.MessagingActivity_Interface;
import com.kona.artmining.Model.Message;
import com.kona.artmining.R;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReplyMail extends Fragment
{
    MessagingActivity_Interface mCallback;
    LinearLayout backBtt, sentMail;;

    ProgressDialog progress;
    Message theSelectedMessage;
    TextView mailSubject, mailPreviousDetails;
    EditText mailDetails, mailSenderReceivedName;

    public ReplyMail(){}

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try {
            mCallback = (MessagingActivity_Interface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement MessagingActivity_Interface");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ui_message_frg_reply_mail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareStage(view);
    }

    void prepareStage(View view)
    {
        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

        backBtt = (LinearLayout) view.findViewById(R.id.backBtt);
        backBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.writeMail_topBackPressed();
            }
        });

        sentMail = (LinearLayout) view.findViewById(R.id.sentMail);
        sentMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendMail();
            }
        });

        mailSubject = (TextView) view.findViewById(R.id.mailSubject);
        mailPreviousDetails = (TextView) view.findViewById(R.id.mailPreviousDetails);
        mailDetails = (EditText) view.findViewById(R.id.mailDetailsd);
        mailSenderReceivedName  = (EditText) view.findViewById(R.id.mailSenderReceivedName);

        Messaging.GetMessageDetails(String.valueOf(getArguments().getLong(GeneralConstants.MESSAGE_ID)),new Messaging.Message_Retrieved() {
            @Override
            public void Success(Message _message)
            {
                theSelectedMessage = _message;
                fillUpMessagePanel();
            }

            @Override
            public void TokenRefreshRequired()
            {
                Messaging.GetMessageDetails(getArguments().getString(GeneralConstants.MESSAGE_ID),new Messaging.Message_Retrieved() {
                    @Override
                    public void Success(Message _message)
                    {
                        theSelectedMessage = _message;
                        fillUpMessagePanel();
                    }

                    @Override
                    public void TokenRefreshRequired()
                    {

                    }

                    @Override
                    public void NotFound() {

                    }

                    @Override
                    public void Error(String message) {
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void NotFound() {

            }

            @Override
            public void Error(String message)
            {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void SendMail()
    {
        progress = ProgressDialog.show(getActivity().getWindow().getContext(), "Retrieving details data.", "Please wait for a while.", true);

        final RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        if(getArguments().getInt(UserConstants.MESSAGE_TYPE)==UserConstants.MESSAGE_TYPE_SENT)
        {
            params.add("to_id",theSelectedMessage.getTo_id()+"");
        }else{
            params.add("to_id",theSelectedMessage.getFrom_id()+"");
        }

        params.add("subject",mailSubject.getText().toString());
        params.add("body",(mailDetails.getText().toString() +"\n"+  theSelectedMessage.getMail_details()));

        Messaging.SendMail(params, new Messaging.response() {
            @Override
            public void Success() {
                progress.dismiss();
                mCallback.updateSentMails();
                mCallback.writeMail_topBackPressed();
            }

            @Override
            public void TokenRefreshRequired() {
                Messaging.SendMail(params, new Messaging.response() {
                    @Override
                    public void Success() {
                        progress.dismiss();
                        mCallback.updateSentMails();
                        mCallback.writeMail_topBackPressed();
                    }

                    @Override
                    public void TokenRefreshRequired() {

                    }

                    @Override
                    public void Error(String message) {
                        progress.dismiss();
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void Error(String message) {
                progress.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    void fillUpMessagePanel()
    {
        mailPreviousDetails.setText(theSelectedMessage.getMail_details());
        mailSenderReceivedName.setText(theSelectedMessage.getReceiver_name());
        mailSubject.setText("Fwd: "+theSelectedMessage.getSubject());

        progress.dismiss();
    }

}
