package com.kona.artmining.Helper;


import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Interface.Success;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.TermsCondition_T1;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class UserAuthentication
{
    public static void DoLogin(String UserName, String Password, final TheResponse _resp)
    {
        RequestParams params=new RequestParams();
        params.add(ServerConstants.USER_AUTH_EMAIL, UserName);
        params.add(ServerConstants.USER_AUTH_PASSWORD, Password);
        params.add(ServerConstants.USER_AUTH_ACCESS_TYPE, "password");

        artmineRestClient.post(ServerConstants.LOGIN_PING, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){
                        Prefs.putString(UserConstants.AccessToken, response.getJSONObject("oAuth").getString(UserConstants.AccessToken));
                        Prefs.putString(UserConstants.RefreshToken, response.getJSONObject("oAuth").getString(UserConstants.RefreshToken));
                        Prefs.putInt(UserConstants.TokenValidity, response.getJSONObject("oAuth").getInt(UserConstants.TokenValidity));
                        _resp.success("");
                    }else{
                        _resp.failed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                if(response != null)
                {
                    System.out.println("-------->failed"+throwable.getMessage());
                    System.out.println(response.toString());
                    _resp.failed();
                }else{
                    _resp.failed();
                }
            }
        });
    }

    public static void DoRegistration(RequestParams params, final message _m)
    {
        artmineRestClient.post(ServerConstants.SIGNUP_PING, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());

                try {
                    if(!response.getBoolean("success")){
                        if(response.has("error"))
                        {
                            if(response.getString("error").contains("sms"))
                            {
                                _m.Success(response.getString("message"));
                            }else{
                                _m.Error(response.getString("message"));
                            }
                        }else{
                            _m.Error(response.getString("message"));
                        }
                    }else{
                        _m.Success(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                try {
                    _m.Error(response.getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void VerifyBySMS(String verification_code, final message _m)
    {
        RequestParams params=new RequestParams();
        params.add("verification_code", verification_code);

        artmineRestClient.post(ServerConstants.VERIFIED_SMS_PING, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){
                        _m.Success(response.getString("message"));
                    }else{
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                try {
                    _m.Error(response.getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void ResendSMS(String phoneNumber, final message _m)
    {
        RequestParams params=new RequestParams();
        params.add("phone", phoneNumber);

        artmineRestClient.post(ServerConstants.RESEND_SMS, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){
                        _m.Success(response.getString("message"));
                    }else{
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                try {
                    _m.Error(response.getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void CheckIfEmailExists(String email, final TheResponse _resp)
    {
        RequestParams params=new RequestParams();
        params.add(ServerConstants.USER_AUTH_EMAIL, email);

        artmineRestClient.post(ServerConstants.CHECK_EMAIL_ADDR, params, new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){
                        _resp.success("");
                    }else{
                        _resp.failed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                System.out.println(response.toString());
                _resp.failed();
            }
        });
    }

    public static void ResetPasswordMail(String email, final message _resp)
    {
        RequestParams params=new RequestParams();
        params.add(UserConstants.REG_FIELD_EMAIL,email);

        artmineRestClient.post(ServerConstants.FORGOT_PASSWORD_PING, params, new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){
                        _resp.Success(response.getString("message"));
                    }else{
                        _resp.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                if(response != null)
                {
                    System.out.println("-------->failed"+throwable.getMessage());
                    System.out.println(response.toString());
                    try {
                        _resp.Error(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        _resp.Error("JSON Error");
                    }
                }else{
                    _resp.Error("JSON Error");
                }
            }
        });
    }

    public static void CheckIfAdmin(final message _m)
    {
        RequestParams params=new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,null));

        artmineRestClient.get(ServerConstants.USER_LISTING_JSON, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("sucess")){
                        _m.Success("");
                    }else{
                        _m.Error("");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("json error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                try {
                    _m.Error(response.getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("json error");
                }
            }
        });
    }

    public static void RetrieveTermsCondition(final termsConditions _m, String type)
    {
        String url = "";
        if(type.equals("reg"))
        {
            url = ServerConstants.TERMS_AND_CONDITIONS_REGISTRATION;
        }else if(type.equals("auc"))
        {
            url = ServerConstants.TERMS_AND_CONDITIONS_AUCTION;
        }else{
            url = ServerConstants.TERMS_AND_CONDITIONS_CART;
        }

        artmineRestClient.get(url, new RequestParams(), new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){
                        JSONArray terms = response.getJSONArray("terms");

                        List<TermsCondition_T1> tc = new ArrayList<TermsCondition_T1>();

                        for (int i = 0; i < terms.length(); ++i) {
                            TermsCondition_T1 t= new TermsCondition_T1();
                            t.setSectionId(terms.getJSONObject(i).getInt("id"));
                            t.setSectionTitle(terms.getJSONObject(i).getString("section_title"));
                            t.setSectionDescription(terms.getJSONObject(i).getString("section_description"));
                            t.setChecked(false);
                            t.setSection_required(terms.getJSONObject(i).getBoolean("section_required"));

                            tc.add(t);
                        }

                        _m.Success(tc);
                    }else{
                        _m.Error("");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("json error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                if(response != null)
                {
                    System.out.println(response.toString());
                    try {
                        _m.Error(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        _m.Error("json error");
                    }
                }else{
                    _m.Error("json error");
                }
            }
        });
    }

    public static void CheckIfCurrentUserIsAdmin(final checkIfAdmin _response)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        artmineRestClient.post(ServerConstants.CHECK_IF_ADMIN_PING, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){
                        _response.Yes();
                    }else{
                        _response.No();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _response.No();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject err_response){
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _response.TokenRefresh();
                        }

                        @Override
                        public void failed() {
                            _response.No();
                        }
                    });
                }else{
                    _response.No();
                }
            }
        });
    }

    public interface message
    {
        void Success(String message);
        void Error(String message);
    }

    public interface termsConditions
    {
        void Success(List<TermsCondition_T1> tc);
        void Error(String message);
    }

    public interface checkIfAdmin
    {
        void Yes();
        void No();
        void TokenRefresh();
    }
}
