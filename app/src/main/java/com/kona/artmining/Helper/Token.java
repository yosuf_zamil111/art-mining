package com.kona.artmining.Helper;


import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Interface.Success;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class Token
{
    public static void RefreshAccessToken(final TheResponse success)
    {
        System.out.println("Refresh Token required-------->");
        RequestParams params=new RequestParams();
        params.add(UserConstants.RefreshToken, Prefs.getString(UserConstants.RefreshToken,""));
        params.add(ServerConstants.USER_AUTH_ACCESS_TYPE,"refresh");

        artmineRestClient.post(ServerConstants.UPDATE_ACCESS_TOKEN, params, new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){
                        Prefs.putString(UserConstants.AccessToken, response.getString(UserConstants.AccessToken));
                        Prefs.putString(UserConstants.RefreshToken, response.getString(UserConstants.RefreshToken));
                        Prefs.putInt(UserConstants.TokenValidity, response.getInt(UserConstants.TokenValidity));
                        success.success("");
                    }else{
                        success.failed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                System.out.println(response.toString());
                success.failed();
            }
        });
    }
}
