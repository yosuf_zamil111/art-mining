package com.kona.artmining.Helper;

import android.text.TextUtils;

import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Interface.Success;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.Artist_T1;
import com.kona.artmining.Model.ImageList_T1;
import com.kona.artmining.Model.SearchListItem;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.kona.artmining.model.RadioButtonInfo;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Dashboard {

    public static void RetrieveGalleryData(String artistId, String themeId, int SortType, int include_soldout, final ImageList_Retrieved ILR)
    {
        RequestParams params=new RequestParams();
        params.add(ServerConstants.THEME_ID, ((themeId.equals("0"))?null:themeId));
        params.add(ServerConstants.ARTIST_ID, ((artistId.equals("0"))?null:artistId));
        params.add(ServerConstants.SORT_TYPE, ((SortType == 0)?null:(SortType+"")));
        params.add(ServerConstants.INCLUDE_SOLDOUT, include_soldout+"");

        System.out.println("----GalleryData Called---->");

        final List<ImageList_T1> IL_T1 = new ArrayList<>();

        artmineRestClient.post(ServerConstants.ART_SEARCH, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("arts_info");

                        for (int i = 0; i < rec.length(); ++i) {
                            ImageList_T1 _T = new ImageList_T1();
                            _T.setArt_id(rec.getJSONObject(i).getInt("art_id"));
                            if(rec.getJSONObject(i).has("sold_out"))
                            {
                                if(rec.getJSONObject(i).getString("sold_out").contains("1"))
                                {
                                    _T.setSold_out("1");
                                }
                                else
                                {
                                    _T.setSold_out(null);
                                }
                            }
                            else
                            {
                                _T.setSold_out(null);
                            }
                            _T.setArt_type(rec.getJSONObject(i).getInt("art_type"));
                            _T.setMain_image(rec.getJSONObject(i).getString("main_image"));
                            _T.setMain_image_thumb(rec.getJSONObject(i).getString("main_image_thumb"));
                            _T.setMain_image_watermarked(rec.getJSONObject(i).getString("main_image_watermarked"));
                            IL_T1.add(_T);
                        }

                        ILR.Success(IL_T1);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    try {
                        ILR.Error(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }else{
                    ILR.Error("Connection Problem");
                }
            }
        });
    }


    public static void RetrieveAuctionData(String artistId, String themeId, int SortType, int include_soldout, final ImageList_Retrieved ILR)
    {
        RequestParams params=new RequestParams();
        params.add(ServerConstants.THEME_ID, ((themeId.equals("0"))?null:themeId));
        params.add(ServerConstants.ARTIST_ID, ((artistId.equals("0"))?null:artistId));
        params.add(ServerConstants.ART_TYPE, "2");
        params.add(ServerConstants.SORT_TYPE, ((SortType == 0)?null:(SortType+"")));
        params.add(ServerConstants.INCLUDE_SOLDOUT, include_soldout+"");

        System.out.println("----AuctionData Called---->");

        final List<ImageList_T1> IL_T1 = new ArrayList<>();

        artmineRestClient.post(ServerConstants.ART_SEARCH, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("arts_info");

                        for (int i = 0; i < rec.length(); ++i) {
                            ImageList_T1 _T = new ImageList_T1();
                            _T.setArt_id(rec.getJSONObject(i).getInt("art_id"));
                            if(rec.getJSONObject(i).has("sold_out"))
                            {
                                if(rec.getJSONObject(i).getString("sold_out").contains("1"))
                                {
                                    _T.setSold_out("1");
                                }
                                else
                                {
                                    _T.setSold_out(null);
                                }
                            }
                            else
                            {
                                _T.setSold_out(null);
                            }
                            _T.setArt_type(rec.getJSONObject(i).getInt("art_type"));
                            _T.setMain_image(rec.getJSONObject(i).getString("main_image"));
                            _T.setMain_image_thumb(rec.getJSONObject(i).getString("main_image_thumb"));
                            _T.setMain_image_watermarked(rec.getJSONObject(i).getString("main_image_watermarked"));
                            IL_T1.add(_T);
                        }

                        ILR.Success(IL_T1);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    try {
                        ILR.Error(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }else{
                    ILR.Error("Connection Problem");
                }
            }
        });
    }

    public static void RetrieveTalkWithData(String themeId, final ImageList_Retrieved ILR)
    {
        RequestParams params=new RequestParams();
        params.add("version", "v2");
        params.add(ServerConstants.THEME_ID, ((themeId.equals("0"))?null:themeId));

        System.out.println("----AuctionData Called---->");

        final List<ImageList_T1> IL_T1 = new ArrayList<>();

        artmineRestClient.get(ServerConstants.ARTIST_LIST, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("sucess")){

                        JSONArray rec = response.getJSONArray("artistList");

                        for (int i = 0; i < rec.length(); ++i) {
                            ImageList_T1 _T = new ImageList_T1();
                            _T.setArtist_id(rec.getJSONObject(i).getInt("user_id"));
                            _T.setTitle_en(rec.getJSONObject(i).getString("name_en"));
                            _T.setTitle_kr(rec.getJSONObject(i).getString("name_kr"));
                            _T.setEmail(rec.getJSONObject(i).getString("email"));

                            if(rec.getJSONObject(i).has("profile_image_thumb"))
                            {
                                _T.setProfile_image_thumb(rec.getJSONObject(i).getString("profile_image_thumb"));
                            }

                            if(rec.getJSONObject(i).has("cover_image"))
                            {
                                _T.setMain_image(rec.getJSONObject(i).getString("cover_image"));
                            }
                            else
                            {
                                if(rec.getJSONObject(i).has("latest_art_large"))
                                {
                                    _T.setMain_image(rec.getJSONObject(i).getString("latest_art_large"));
                                }
                            }

                            if(rec.getJSONObject(i).has("latest_art_thumb"))
                            {
                                _T.setMain_image_thumb(rec.getJSONObject(i).getString("latest_art_thumb"));
                            }

                            IL_T1.add(_T);
                        }

                        ILR.Success(IL_T1);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    try {
                        ILR.Error(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }else{
                    ILR.Error("Connection Problem");
                }
            }
        });
    }


    public static void RetrieveShopData(String artistId, String themeId, int SortType, int include_soldout, final ImageList_Retrieved ILR)
    {
        RequestParams params=new RequestParams();
        params.add(ServerConstants.THEME_ID, ((themeId.equals("0"))?null:themeId));
        params.add(ServerConstants.ARTIST_ID, ((artistId.equals("0"))?null:artistId));
        params.add(ServerConstants.ART_TYPE, "1");
        params.add(ServerConstants.SORT_TYPE, ((SortType == 0)?null:(SortType+"")));
        params.add(ServerConstants.INCLUDE_SOLDOUT, include_soldout+"");

        final List<ImageList_T1> IL_T1 = new ArrayList<>();

        artmineRestClient.post(ServerConstants.ART_SEARCH, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("arts_info");

                        for (int i = 0; i < rec.length(); ++i) {
                            ImageList_T1 imageList_t1 = new ImageList_T1();
                            imageList_t1.setArt_id(rec.getJSONObject(i).getInt("art_id"));
                            if(rec.getJSONObject(i).has("sold_out"))
                            {
                                if(rec.getJSONObject(i).getString("sold_out").contains("1"))
                                {
                                    imageList_t1.setSold_out("1");
                                }
                                else
                                {
                                    imageList_t1.setSold_out(null);
                                }
                            }
                            else
                            {
                                imageList_t1.setSold_out(null);
                            }
                            imageList_t1.setArt_type(rec.getJSONObject(i).getInt("art_type"));
                            imageList_t1.setMain_image(rec.getJSONObject(i).getString("main_image"));
                            imageList_t1.setMain_image_thumb(rec.getJSONObject(i).getString("main_image_thumb"));
                            imageList_t1.setMain_image_watermarked(rec.getJSONObject(i).getString("main_image_watermarked"));
                            IL_T1.add(imageList_t1);
                        }

                        ILR.Success(IL_T1);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    try {
                        ILR.Error(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }else{
                    ILR.Error("Connection Problem");
                }
            }
        });
    }


    public static void RetrieveThemeList(final RadioButtonInfo_Retrieved ILR)
    {
        RequestParams params=new RequestParams();
        //params.add(ServerConstants.USER_AUTH_EMAIL, UserName);

        final List<RadioButtonInfo> IL_T1 = new ArrayList<>();

        RadioButtonInfo _T = new RadioButtonInfo();

        // Default Value
        _T.setID(0);
        _T.setText("All");
        IL_T1.add(_T);

        artmineRestClient.get(ServerConstants.THEME_LIST, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("listThemes");

                        for (int i = 0; i < rec.length(); ++i) {
                            RadioButtonInfo _T = new RadioButtonInfo();

                            _T.setID(rec.getJSONObject(i).getInt("themes_id"));
                            _T.setText(rec.getJSONObject(i).getString("name"));

                            IL_T1.add(_T);
                        }

                        ILR.Success(IL_T1);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    try {
                        ILR.Error(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }else{
                    ILR.Error("Connection Problem");
                }
            }
        });
    }

    public static void RetrieveThemeList_v2(final RadioButtonInfo_Retrieved ILR)
    {
        RequestParams params=new RequestParams();
        //params.add(ServerConstants.USER_AUTH_EMAIL, UserName);

        final List<RadioButtonInfo> IL_T1 = new ArrayList<>();

        artmineRestClient.get(ServerConstants.THEME_LIST, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("listThemes");

                        for (int i = 0; i < rec.length(); ++i) {
                            RadioButtonInfo _T = new RadioButtonInfo();

                            _T.setID(rec.getJSONObject(i).getInt("themes_id"));
                            _T.setText(rec.getJSONObject(i).getString("name"));

                            IL_T1.add(_T);
                        }

                        ILR.Success(IL_T1);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    try {
                        ILR.Error(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }else{
                    ILR.Error("Connection Problem");
                }
            }
        });
    }

    public static void RetrieveArtistList(final RadioButtonInfo_Retrieved ILR)
    {
        RequestParams params=new RequestParams();
        //params.add(ServerConstants.USER_AUTH_EMAIL, UserName);

        final List<RadioButtonInfo> IL_T1 = new ArrayList<>();

        RadioButtonInfo _T = new RadioButtonInfo();

        // Default Value
        _T.setID(0);
        _T.setText("All");
        IL_T1.add(_T);

        artmineRestClient.get(ServerConstants.ARTIST_LIST, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("sucess")){

                        JSONArray rec = response.getJSONArray("artistList");

                        for (int i = 0; i < rec.length(); ++i) {
                            RadioButtonInfo _T = new RadioButtonInfo();

                            _T.setID(rec.getJSONObject(i).getInt("user_id"));
                            _T.setText(rec.getJSONObject(i).getString("name_en"));

                            IL_T1.add(_T);
                        }

                        ILR.Success(IL_T1);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    try {
                        ILR.Error(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }else{
                    ILR.Error("Connection Problem");
                }
            }
        });
    }

    public static void SearchArtist(String keyword, final SearchResult_Retrieved ILR)
    {
        if(!TextUtils.isEmpty(keyword))
        {
            RequestParams params=new RequestParams();
            params.add(ServerConstants.SEARCH_KEYWORD, keyword);
            params.add("version", "v2");

            final List<SearchListItem> IL_T1 = new ArrayList<>();

            artmineRestClient.get(ServerConstants.ARTIST_SEARCH, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                    System.out.println("-------->"+response.toString());
                    try {
                        if(response.getBoolean("sucess")){

                            JSONArray rec = response.getJSONArray("artist_list");

                            for (int i = 0; i < rec.length(); ++i) {
                                SearchListItem _T = new SearchListItem();

                                _T.setID(rec.getJSONObject(i).getInt("user_id"));
                                _T.setText(rec.getJSONObject(i).getString("name_en")+" / "+rec.getJSONObject(i).getString("name_kr"));
                                _T.setEmail(rec.getJSONObject(i).getString("email"));

                                if(rec.getJSONObject(i).has("profile_image_thumb"))
                                {
                                    _T.setProfileImg(rec.getJSONObject(i).getString("profile_image_thumb"));
                                }else{
                                    _T.setProfileImg(null);
                                }

                                List<String> portfolioLinks = new ArrayList<String>();

                                if(rec.getJSONObject(i).has("latest_art"))
                                {
                                    JSONArray latest_art = rec.getJSONObject(i).getJSONArray("latest_art");
                                    for (int ii = 0; ii < latest_art.length(); ++ii) {
                                        portfolioLinks.add(latest_art.getJSONObject(ii).getString("art_thumb"));
                                    }
                                }

                                _T.setPortfolioLinks(portfolioLinks);

                                IL_T1.add(_T);
                            }

                            if(rec.length() > 0)
                            {
                                ILR.Success(IL_T1);
                            }else{
                                ILR.Error("No Artist Found");
                            }
                        }else{
                            ILR.Error("Error data found");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        ILR.Error("JSON Error");
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                    System.out.println("-------->failed"+throwable.getMessage());
                    if(response != null)
                    {
                        try {
                            ILR.Error(response.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("JSON Error");
                            ILR.Error("JSON Error");
                        }
                    }else{
                        ILR.Error("Connection Problem");
                    }
                }
            });
        }else{
            ILR.EmptyKeywordFound();
        }
    }

    public static void GetArtistDetails(String artist_id, final ArtistDetails_Retrieved ILR)
    {
        RequestParams params=new RequestParams();
        params.add("artist_id", artist_id);
        params.add("version", "v2");

        final List<SearchListItem> IL_T1 = new ArrayList<>();

        artmineRestClient.get(ServerConstants.ARTIST_SEARCH, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("sucess")){

                        JSONArray rec = response.getJSONArray("artist_list");
                        JSONObject artist = rec.getJSONObject(0);

                        Artist_T1 artist_t1 = new Artist_T1();
                        artist_t1.setId(artist.getInt("user_id"));
                        artist_t1.setEmail(artist.getString("email"));
                        artist_t1.setName_en(artist.getString("name_en"));
                        artist_t1.setName_kr(artist.getString("name_kr"));

                        if(artist.has("profile_image_thumb"))
                        {
                            artist_t1.setProfilePicture(artist.getString("profile_image_thumb"));
                            artist_t1.setProfile_image_thumb(artist.getString("profile_image_thumb"));
                        }

                        if(artist.has("profile_image_main"))
                        {
                            artist_t1.setProfile_image_main(artist.getString("profile_image_main"));
                        }

                        if(artist.has("cover_image"))
                        {
                            artist_t1.setCover_image(artist.getString("cover_image"));
                        }

                        if(artist.has("profile_en"))
                        {
                            artist_t1.setProfile_en(artist.getString("profile_en"));
                        }
                        if(artist.has("profile_kr"))
                        {
                            artist_t1.setProfile_kr(artist.getString("profile_kr"));
                        }
                        if(artist.has("about_en"))
                        {
                            artist_t1.setAbout_en(artist.getString("about_en"));
                        }
                        if(artist.has("about_kr"))
                        {
                            artist_t1.setAbout_kr(artist.getString("about_kr"));
                        }

                        if(artist.has("latest_art"))
                        {
                            JSONArray LA = artist.getJSONArray("latest_art");
                            List<String> LLAA=new ArrayList<String>();

                            List<Integer> LLAA_ID=new ArrayList<Integer>();

                            for (int i = 0; i < LA.length(); ++i) {
                                LLAA.add(LA.getJSONObject(i).getString("art_thumb"));
                                LLAA_ID.add(LA.getJSONObject(i).getInt("art_id"));
                            }

                            artist_t1.setLatest_art(LLAA);
                            artist_t1.setLatest_art_id(LLAA_ID);
                        }

                        ILR.Success(artist_t1);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    try {
                        ILR.Error(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }else{
                    ILR.Error("Connection Problem");
                }
            }
        });
    }

    public static void getCartInfo(final TheResponse _s)
    {
        RequestParams params=new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));

        artmineRestClient.get(ServerConstants.CART_INFO, params, new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){
                        _s.success(response.getString("total_items"));
                    }else{
                        _s.failed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    System.out.println(response.toString());
                }
                _s.failed();
            }
        });
    }

    public static void RetrieveCountryList(final countryListRetrived ILR)
    {
        RequestParams params=new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        final List<RadioButtonInfo> IL_T1 = new ArrayList<>();

        RadioButtonInfo _T = new RadioButtonInfo();

        artmineRestClient.get(ServerConstants.COUNTRY_LIST, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("country_list");

                        for (int i = 0; i < rec.length(); ++i) {
                            RadioButtonInfo _T = new RadioButtonInfo();

                            _T.setID(i+1);
                            _T.setsID(rec.getJSONObject(i).getString("country_code"));
                            _T.setText(rec.getJSONObject(i).getString("country_name"));

                            IL_T1.add(_T);
                        }

                        ILR.Success(IL_T1);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response){
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    ILR.Success(IL_T1);
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            ILR.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                ILR.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                ILR.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        ILR.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }
            }
        });
    }

    public interface ImageList_Retrieved
    {
        void Success(List<ImageList_T1> message);
        void Error(String message);
    }

    public interface RadioButtonInfo_Retrieved
    {
        void Success(List<RadioButtonInfo> message);
        void Error(String message);
    }

    public interface SearchResult_Retrieved
    {
        void Success(List<SearchListItem> message);
        void Error(String message);
        void EmptyKeywordFound();
    }

    public interface ArtistDetails_Retrieved
    {
        void Success(Artist_T1 artist);
        void Error(String message);
    }

    public interface countryListRetrived {
        void Success(List<RadioButtonInfo> IL_T1);

        void TokenRefreshRequired();

        void Error(String message);
    }
}
