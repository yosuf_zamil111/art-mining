package com.kona.artmining.Helper;


import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Interface.Success;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.AuctionItem_T1;
import com.kona.artmining.Model.CartItem_T1;
import com.kona.artmining.Model.Order_T1;
import com.kona.artmining.Model.Shipping_T1;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Cart
{

    public static void GetDetails(final Context ctx, final userCartDataRetrieve _m) {

        RequestParams params = new RequestParams();
        final List<CartItem_T1> cartItemData = new ArrayList<CartItem_T1>();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        artmineRestClient.get(ServerConstants.CART_DETAILS, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->" + response.toString());
                try {
                    if (response.getBoolean("success")) {

                        JSONArray rec = response.getJSONArray("cart_items");

                        for (int ii = 0; ii < rec.length(); ++ii) {

                            CartItem_T1 asil = new CartItem_T1();
                            asil.setCatrt_id(rec.getJSONObject(ii).getString("catrt_id"));
                            asil.setArt_id(rec.getJSONObject(ii).getString("art_id"));
                            //asil.setShip_cost(GeneralConstants.SHIPPING_COST);
                            asil.setTitle_en(rec.getJSONObject(ii).getString("title_en"));
                            asil.setTitle_kr(rec.getJSONObject(ii).getString("title_kr"));

                            if(rec.getJSONObject(ii).get("currency").equals("WON")  || rec.getJSONObject(ii).get("currency").equals("KRW"))
                            {
                                asil.setCurrency("KR");
                            }else{
                                asil.setCurrency(rec.getJSONObject(ii).getString("currency"));
                            }

                            asil.setPrice(Double.valueOf(rec.getJSONObject(ii).getString("price")));
                            asil.setArtistName_en(rec.getJSONObject(ii).getString("name_en"));
                            asil.setArtistName_kr(rec.getJSONObject(ii).getString("name_kr"));
                            asil.setMain_image_watermarked(rec.getJSONObject(ii).getString("main_image_watermarked"));
                            asil.setMain_image_thumb(rec.getJSONObject(ii).getString("main_image_thumb"));

                            cartItemData.add(asil);
                        }

                        _m.Success(cartItemData);
                    } else {
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No cart item found.
                    _m.Success(cartItemData);
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _m.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _m.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void RemoveItem(String cart_item_id, final CartDataRemove _m)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("cart_item_id",cart_item_id);

        artmineRestClient.post(ServerConstants.CART_REMOVE_ITEM, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response)
            {
                _m.Success();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    _m.NotFound();
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _m.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _m.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void MakeAPayPalOrder(Order_T1 PlacedOrder, final orderPlaced op)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("shipping_information",new Gson().toJson(PlacedOrder.getShippingDetails()));

        if(PlacedOrder.getOrder_type().equals("1"))
        {
            params.add("product_information",new Gson().toJson(PlacedOrder.getItems()));
        }
        else
        {
            params.add("product_information",new Gson().toJson(PlacedOrder.getAuctionItem()));
        }

        params.add("item_total",PlacedOrder.getItem_total()+"");
        params.add("shipping_cost",PlacedOrder.getShipping_cost()+"");
        params.add("pay_total",PlacedOrder.getPay_total()+"");
        params.add("gateway_used",PlacedOrder.getGateway_used());
        params.add("order_type",PlacedOrder.getOrder_type());
        params.add("pay_id",PlacedOrder.getPay_id());
        params.add("currency",PlacedOrder.getCurrency());

        artmineRestClient.post(ServerConstants.CREATE_AN_ORDER, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response)
            {
                try {
                    if (response.getBoolean("success")) {
                        op.Success(response.getInt("order_id"));
                    }else{
                        op.Error("Not Succeed");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    op.Error("JSON Error");
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    op.NotFound();
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            op.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                op.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                op.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        op.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        op.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void GetShippingInfo(final shippingInfoRetrieved op)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        artmineRestClient.get(ServerConstants.RETRIEVE_DEFAULT_SHIPPING, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response)
            {
                System.out.println("-------->" + response.toString());
                try {
                    if (response.getBoolean("success")) {

                        JSONObject default_shipping = response.getJSONObject("default_shipping");
                        JSONObject shipping = default_shipping.getJSONObject("shipping_details");

                        Shipping_T1 ST= new Shipping_T1();
                        ST.setAddress_type(shipping.getString("address_type").contains("1")?"domestic":"overseas");
                        if(ST.getAddress_type().equals("overseas"))
                        {
                            ST.setOverseas(true);
                        }
                        else
                        {
                            ST.setOverseas(false);
                        }
                        ST.setFullName(shipping.getString("full_name"));
                        ST.setZipCode(shipping.getString("zipcode"));
                        ST.setAddressLine1(shipping.getString("address_1"));
                        ST.setAddressLine2(shipping.getString("address_2"));
                        ST.setPhoneNumber(shipping.getString("mobile_or_phone"));
                        ST.setCountryName(shipping.getString("country"));
                        ST.setCity(shipping.getString("city"));
                        ST.setState(shipping.getString("state"));
                        ST.setExtension(shipping.getString("extension"));

                        op.Success(ST);
                    }else{
                        op.Error("Not Succeed");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    op.Error("JSON Error");
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    op.NotFound();
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            op.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                op.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                op.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        op.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        op.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void SaveShippingInfo(Shipping_T1 shippingData, final response op)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("address_type", (shippingData.getAddress_type().equals("domestic")?"1":"2"));
        params.add("full_name", shippingData.getFullName());
        params.add("zipcode", shippingData.getZipCode());
        params.add("address_1", shippingData.getAddressLine1());
        params.add("address_2",  shippingData.getAddressLine2());
        params.add("mobile_or_phone",  shippingData.getPhoneNumber());
        params.add("country",  shippingData.getCountryName());
        params.add("city", shippingData.getCity());
        params.add("state", shippingData.getState());
        params.add("extension", shippingData.getExtension());

        artmineRestClient.post(ServerConstants.SAVE_SHIPPING_DETAILS, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response)
            {
                try {
                    if (response.getBoolean("success")) {
                        op.Success();
                    }else{
                        op.Error("Not Succeed");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    op.Error("JSON Error");
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    op.NotFound();
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            op.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                op.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                op.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        op.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        op.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void ProcessTheOrder(final Order_T1 PlacedOrder, final orderProcessed op)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("shipping_information",new Gson().toJson(PlacedOrder.getShippingDetails()));
        params.add("type",PlacedOrder.getOrder_type());

        if(PlacedOrder.getOrder_type().equals("1"))
        {
            params.add("product_information",new Gson().toJson(PlacedOrder.getItems()));

        }
        else
        {
            params.add("product_information",new Gson().toJson(PlacedOrder.getAuctionItem()));
        }


        //Log.d("Shipping country code"+params,"Eoor some");

        artmineRestClient.post(ServerConstants.PROCESS_ORDER_DETAILS, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response)
            {
                System.out.println("-------->" + response.toString());

                try {
                    if (response.getBoolean("success"))
                    {
                        if(PlacedOrder.getOrder_type().equals("1"))
                        {
                            PlacedOrder.setItems((List<CartItem_T1>) new Gson().fromJson(response.getString("product_info"), new TypeToken<List<CartItem_T1>>(){}.getType()));
                        }
                        else
                        {
                            PlacedOrder.setAuctionItem((List<AuctionItem_T1>) new Gson().fromJson(response.getString("product_info"), new TypeToken<List<AuctionItem_T1>>(){}.getType()));
                        }

                        PlacedOrder.setShipping_cost(response.getJSONObject("total_info").getDouble("ship_total"));
                        PlacedOrder.setItem_total(response.getJSONObject("total_info").getDouble("item_total"));
                        PlacedOrder.setPay_total(response.getJSONObject("total_info").getDouble("grand_total"));

                        op.Success(PlacedOrder);
                    }else{
                        op.Error("Not Succeed");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    op.Error("JSON Error");
                }

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    try {
                        op.NotFound(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        op.Error("JSON Error");
                    }
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            op.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                op.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                op.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        op.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        op.Error("JSON Error");
                    }
                }
            }
        });

    }

    public interface userCartDataRetrieve {
        void Success(List<CartItem_T1> scrapBookData);

        void TokenRefreshRequired();

        void Error(String message);
    }

    public interface CartDataRemove
    {
        void Success();
        void NotFound();
        void TokenRefreshRequired();
        void Error(String message);
    }

    public interface orderPlaced
    {
        void Success(int orderID);
        void NotFound();
        void TokenRefreshRequired();
        void Error(String message);
    }

    public interface orderProcessed
    {
        void Success(Order_T1 processedOrder);
        void NotFound(String message);
        void TokenRefreshRequired();
        void Error(String message);
    }


    public interface shippingInfoRetrieved
    {
        void Success(Shipping_T1 info);
        void NotFound();
        void TokenRefreshRequired();
        void Error(String message);
    }

    public interface response
    {
        void Success();
        void NotFound();
        void TokenRefreshRequired();
        void Error(String message);
    }
}
