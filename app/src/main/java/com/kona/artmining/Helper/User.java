package com.kona.artmining.Helper;

import android.content.Context;
import android.text.TextUtils;

import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Interface.Success;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.ArtScrapbookItemList_T1;
import com.kona.artmining.Model.CartItem_T1;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.Model.User_T1;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class User {
    public static void UpdateProfile(File profilePicture, final UserProfile userProfile, final message _message) {
        RequestParams params = new RequestParams();
        if (profilePicture != null) {
            try {
                params.put("fileToUpload", profilePicture);
            } catch (FileNotFoundException e) {

            }
        }

        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add(UserConstants.REG_FIELD_EMAIL, userProfile.getEmail());
        params.add(UserConstants.REG_FIELD_NAME_EN, userProfile.getName());
        params.add(UserConstants.REG_FIELD_NAME_KR, userProfile.getName_kr());
        params.add(UserConstants.REG_FIELD_PHONE_EXT, userProfile.getPhone_ext());
        params.add("about_en", userProfile.getAbout_en());
        params.add("about_kr", userProfile.getAbout_kr());
        params.add("profile_en", userProfile.getProfile_en());
        params.add("profile_kr", userProfile.getProfile_kr());
        params.add(UserConstants.REG_FIELD_BIRTHDATE, userProfile.getBirth_date().replaceAll(". ", "/"));
        params.add(UserConstants.REG_FIELD_SEX, userProfile.getSex());
        params.add(UserConstants.REG_FIELD_PHONE, userProfile.getPhone());
        params.add(UserConstants.REG_FIELD_PHONE_EXT, userProfile.getPhone_ext());
        params.add(UserConstants.REG_FIELD_ZIP, userProfile.getZip_code());
        params.add(UserConstants.REG_FIELD_ADDRESS, userProfile.getAddress());
        params.add(UserConstants.REG_FIELD_ADDRESS2, userProfile.getAddress2());
        //params.add(UserConstants.REG_FIELD_ADDRESS2, userProfile.getAddress2());
        params.add(UserConstants.REG_FIELD_USER_TYPE, Integer.toString(userProfile.getType()));
        params.add(UserConstants.REG_FIELD_PASSWORD, userProfile.getPassword());


        artmineRestClient.post(ServerConstants.USER_PROFILE_UPDATE, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("User:UpdateProfile-------->" + response.toString());
                try {
                    if(!response.getBoolean("success")){
                        if(response.has("error"))
                        {
                            if(response.getString("error").contains("sms"))
                            {
                                _message.ReqSMS();
                            }else{
                                _message.Error(response.getString("message"));
                            }
                        }else{
                            _message.Error(response.getString("message"));
                        }
                    }else{
                        _message.Success(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _message.Success("JSON Error");
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("User:UpdateProfile-------->" + err_response.toString());
                Token.RefreshAccessToken(new TheResponse() {
                    @Override
                    public void success(String response) {
                        _message.TokenRefreshRequired();
                    }

                    @Override
                    public void failed() {
                        try {
                            _message.Error(err_response.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("JSON Error");
                            _message.Error("JSON Error");
                        }
                    }
                });
            }
        });
    }

    public static void GetProfileDataProfile(final Context ctx, final userProfileRetrieve _m) {

        if(!TextUtils.isEmpty(Prefs.getString(UserConstants.AccessToken, null)))
        {
            RequestParams params = new RequestParams();
            final UserProfile up = new UserProfile();
            params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

            artmineRestClient.get(ServerConstants.USER_PROFILE_DETAILS, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                    System.out.println("-------->" + response.toString());
                    try {
                        if (response.getBoolean("success")) {

                            JSONArray rec = response.getJSONArray("profile_info");
                            JSONObject JO = rec.getJSONObject(0);

                            up.setEmail(JO.getString(UserConstants.REG_FIELD_EMAIL));
                            up.setId(JO.getInt("id"));
                            up.setName(JO.getString(UserConstants.REG_FIELD_NAME_EN));
                            up.setName_kr(JO.getString(UserConstants.REG_FIELD_NAME_KR));
                            up.setBirth_date(JO.getString(UserConstants.REG_FIELD_BIRTH_DATE).replaceAll("-", ". "));
                            up.setSex(JO.getString(UserConstants.REG_FIELD_SEX));

                            up.setPhone_ext(JO.getString(UserConstants.REG_FIELD_PHONE_EXT));

                            //String phone = JO.getString(UserConstants.REG_FIELD_PHONE);

                            //up.setPhone_country_code(phone.substring(0,3));
                            //up.setPhone_operator(phone.substring(3,6));
                            up.setPhone(JO.getString(UserConstants.REG_FIELD_PHONE));
                            up.setPhone_ext(JO.getString(UserConstants.REG_FIELD_PHONE_EXT));

                            //System.out.println(up.getPhone_country_code()+"----"+up.getPhone_operator()+"-------"+up.getPhone());

                            up.setZip_code(JO.getString(UserConstants.REG_FIELD_ZIP));
                            up.setAddress(JO.getString(UserConstants.REG_FIELD_ADDRESS));
                            up.setAddress2(JO.getString(UserConstants.REG_FIELD_ADDRESS2));
                            up.setAbout_en(JO.getString("about_en"));
                            up.setAbout_kr(JO.getString("about_kr"));
                            up.setProfile_en(JO.getString("profile_en"));
                            up.setProfile_kr(JO.getString("profile_kr"));
                            up.setType(JO.getInt(UserConstants.REG_FIELD_USER_TYPE));

                            up.setThumb_image(JO.getString("thumb_image"));
                            up.setMain_image(JO.getString("main_image"));

                            SaveUserProfilePicture(ctx, up.getThumb_image(), new Success() {
                                @Override
                                public void success(String response) {
                                    System.out.println("Data Saved");
                                    _m.Success(up);
                                }
                            });

                        } else {
                            _m.Error(response.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        _m.Error("JSON Error");
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                    System.out.println("-------->failed" + throwable.getMessage());
                    if(err_response != null)
                    {
                        System.out.println(err_response.toString());
                    }

                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _m.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }
            });

        }
        else
        {
            _m.Error("Empty Access Token");
        }
    }

    public static void GetUserScrapBookDataProfile(final Context ctx, final userScrapBookDataRetrieve _m)
    {

        RequestParams params = new RequestParams();
        final List<ArtScrapbookItemList_T1> scrapBookData = new ArrayList<ArtScrapbookItemList_T1>();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        // Add Empty Value
        scrapBookData.add(new ArtScrapbookItemList_T1());

        artmineRestClient.get(ServerConstants.ARTIST_SCRAPBOOK_DETAILS, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->" + response.toString());
                try {
                    if (response.getBoolean("success")) {

                        JSONArray rec = response.getJSONArray("scraps");

                        for (int ii = 0; ii < rec.length(); ++ii) {

                            ArtScrapbookItemList_T1 asil = new ArtScrapbookItemList_T1();
                            asil.setArt_id(rec.getJSONObject(ii).getInt("art_id"));
                            asil.setArt_image_id(rec.getJSONObject(ii).getInt("art_image_id"));
                            asil.setScrap_id(rec.getJSONObject(ii).getInt("scrap_id"));
                            asil.setAdded_on(rec.getJSONObject(ii).getString("added_on"));
                            asil.setMain_image(rec.getJSONObject(ii).getString("main_image"));
                            asil.setStatus(rec.getJSONObject(ii).getInt("status"));
                            asil.setMain_image_thumb(rec.getJSONObject(ii).getString("main_image_thumb"));
                            asil.setMain_image_watermarked(rec.getJSONObject(ii).getString("main_image_watermarked"));
                            asil.setSelected(false);
                            asil.setClickable(true);

                            scrapBookData.add(asil);
                        }

                        _m.Success(scrapBookData);
                    } else {
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(err_response != null)
                {
                    try {
                        if(err_response.getString("message").equals("No Scraps found!"))
                        {
                            _m.Success(scrapBookData);
                        }else{
                            Token.RefreshAccessToken(new TheResponse() {
                                @Override
                                public void success(String response) {
                                    _m.TokenRefreshRequired();
                                }

                                @Override
                                public void failed() {
                                    try {
                                        _m.Error(err_response.getString("message"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        System.out.println("JSON Error");
                                        _m.Error("JSON Error");
                                    }
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void UploadUserCoverPhoto(File _image, final response2 _m)
    {

        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        if (_image != null) {
            try {
                params.put("fileToUpload", _image);
            } catch (FileNotFoundException e) {

            }
        }

        artmineRestClient.post(ServerConstants.USER_COVER_IMAGE_UPLOAD, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->" + response.toString());
                _m.Success();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {

                System.out.println("-------->failed" + throwable.getMessage());

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _m.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }else if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    if(err_response != null)
                    {
                        try {
                            _m.Error(err_response.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("JSON Error");
                            _m.Error("JSON Error");
                        }
                    }
                }else{
                    System.out.println("JSON Error");
                    _m.Error("JSON Error");
                }
            }
        });
    }

    public static void RemoveScrapBookItem(String itemIds, final response2 _m)
    {

        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("scrap_id", itemIds);

        artmineRestClient.post(ServerConstants.ARTIST_SCRAPBOOK_ITEM_DELETE, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                _m.Success();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {

                System.out.println("-------->failed" + throwable.getMessage());

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _m.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }else if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    if(err_response != null)
                    {
                        try {
                            _m.Error(err_response.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("JSON Error");
                            _m.Error("JSON Error");
                        }
                    }
                }else{
                    System.out.println("JSON Error");
                    _m.Error("JSON Error");
                }
            }
        });
    }

    public static void SaveUserProfilePicture(final Context ctx, String path, final Success success)
    {
        artmineRestClient.getD(path, new RequestParams(), new FileAsyncHttpResponseHandler(ctx) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, File file)
            {

                System.out.println("SaveUserProfilePicture: "+file.getAbsolutePath());
                Prefs.putString(UserConstants.ProfilePicture, file.getAbsolutePath());
                success.success("");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file)
            {
                success.success("");
            }
        });
    }

    public static void RetrieveUserList(String keyword, String user_type, String artist_id, final userListRetrieve _userListRetrieve)
    {
        RequestParams params = new RequestParams();
        final List<User_T1> users = new ArrayList<User_T1>();
        params.add("keyword", keyword);
        params.add("version", "v2");
        params.add("user_type", user_type);
        params.add("artist_id", artist_id);

        // Add Empty Value
        artmineRestClient.get(ServerConstants.USER_SEARCH, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->" + response.toString());
                try {
                    if (response.getBoolean("sucess")) {

                        JSONArray rec = response.getJSONArray("artist_list");

                        for (int ii = 0; ii < rec.length(); ++ii) {

                            User_T1 asil = new User_T1();
                            asil.setId(rec.getJSONObject(ii).getInt("id"));
                            asil.setEmail(rec.getJSONObject(ii).getString("email"));
                            asil.setUser_type_id(rec.getJSONObject(ii).getString("user_type_id"));

                            if(asil.getUser_type_id().contentEquals("1"))
                            {
                                asil.setType_name("Regular");
                            }
                            else if(asil.getUser_type_id().contentEquals("2"))
                            {
                                asil.setType_name("Artist");
                            }
                            else if(asil.getUser_type_id().contentEquals("3"))
                            {
                                asil.setType_name("Curator");
                            }
                            else if(asil.getUser_type_id().contentEquals("4"))
                            {
                                asil.setType_name("Supporter");
                            }
                            else if(asil.getUser_type_id().contentEquals("7"))
                            {
                                asil.setType_name("Admin");
                            }
                            /*asil.setPhone(rec.getJSONObject(ii).getString("phone"));
                            asil.setCreated_date(rec.getJSONObject(ii).getString("created_date"));
                            asil.setStatus(rec.getJSONObject(ii).getString("status"));*/
                            asil.setUser_details_id(rec.getJSONObject(ii).getString("user_details_id"));
                            asil.setUser_id(rec.getJSONObject(ii).getInt("user_id"));
                            asil.setName_en(rec.getJSONObject(ii).getString("name_en"));
                            asil.setName_kr(rec.getJSONObject(ii).getString("name_kr"));
                            /*asil.setBirth_date(rec.getJSONObject(ii).getString("birth_date"));
                            asil.setSex(rec.getJSONObject(ii).getString("sex"));
                            asil.setAddress(rec.getJSONObject(ii).getString("address"));
                            asil.setAddress_2(rec.getJSONObject(ii).getString("address_2"));
                            asil.setModified_date(rec.getJSONObject(ii).getString("modified_date"));
                            if(rec.getJSONObject(ii).has("zip"))
                            {
                                asil.setZip(rec.getJSONObject(ii).getString("zip"));
                            }*/
                            /*asil.setProfile_image_thumb(rec.getJSONObject(ii).getString("profile_image_thumb"));
                            asil.setPerm_id(rec.getJSONObject(ii).getString("perm_id"));
                            asil.setType_name(rec.getJSONObject(ii).getString("type_name"));
                            asil.setUser_status(rec.getJSONObject(ii).getString("user_status"));*/
                            users.add(asil);
                        }

                        _userListRetrieve.Success(users);
                    } else {
                        _userListRetrieve.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _userListRetrieve.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                    try {
                        _userListRetrieve.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        _userListRetrieve.Error("json error");
                    }
                }else{
                    _userListRetrieve.Error("json error");
                }
            }
        });
    }


    public interface userProfileRetrieve {
        void Success(UserProfile userProfile);

        void TokenRefreshRequired();

        void Error(String message);
    }

    public interface userScrapBookDataRetrieve {
        void Success(List<ArtScrapbookItemList_T1> scrapBookData);

        void TokenRefreshRequired();

        void Error(String message);
    }

    public interface message {
        void Success(String message);

        void ReqSMS();

        void TokenRefreshRequired();

        void Error(String message);
    }

    public interface response2 {
        void Success();

        void TokenRefreshRequired();

        void Error(String message);
    }

    public interface userListRetrieve {
        void Success(List<User_T1> users);

        void TokenRefreshRequired();

        void Error(String message);
    }
}
