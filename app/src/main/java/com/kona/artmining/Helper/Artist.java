package com.kona.artmining.Helper;

import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.DashboardFrgLatest_Reviews_Model_T1;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Artist
{
    public static void UpdateArtistProfile(final UserProfile userProfile, final message _message) {
        RequestParams params = new RequestParams();

        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add(UserConstants.USER_ID, String.valueOf(userProfile.getId()));
        params.add("about_en", userProfile.getAbout_en());
        params.add("about_kr", userProfile.getAbout_kr());
        params.add("profile_en", userProfile.getProfile_en());
        params.add("profile_kr", userProfile.getProfile_kr());

        artmineRestClient.post(ServerConstants.USER_PROFILE_UPDATE, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("User:UpdateProfile-------->" + response.toString());
                try {
                    if(!response.getBoolean("success")){
                        if(response.has("error"))
                        {
                            if(response.getString("error").contains("sms"))
                            {
                                _message.Success(response.getString("message"));
                            }else{
                                _message.Error(response.getString("message"));
                            }
                        }else{
                            _message.Error(response.getString("message"));
                        }
                    }else{
                        _message.Success(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _message.Success("JSON Error");
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("User:UpdateProfile-------->" + err_response.toString());
                Token.RefreshAccessToken(new TheResponse() {
                    @Override
                    public void success(String response) {
                        _message.TokenRefreshRequired();
                    }

                    @Override
                    public void failed() {
                        try {
                            _message.Error(err_response.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("JSON Error");
                            _message.Error("JSON Error");
                        }
                    }
                });
            }
        });
    }

    public static void GetProfileDataProfile(String artistID, final artistProfileRetrieve _m)
    {
        final UserProfile up = new UserProfile();

        RequestParams params=new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,null));
        params.add(UserConstants.USER_ID, artistID);

        artmineRestClient.get(ServerConstants.USER_PROFILE_DETAILS, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("profile_info");
                        JSONObject JO = rec.getJSONObject(0);

                        /*up.setEmail(JO.getString(UserConstants.REG_FIELD_EMAIL));
                        up.setId(JO.getInt("id"));
                        up.setName(JO.getString(UserConstants.REG_FIELD_NAME_EN));
                        up.setBirth_date(JO.getString(UserConstants.REG_FIELD_BIRTH_DATE).replaceAll("-", ". "));
                        up.setSex(JO.getString(UserConstants.REG_FIELD_SEX));
                        up.setPhone(JO.getString(UserConstants.REG_FIELD_PHONE));
                        up.setZip_code(JO.getString(UserConstants.REG_FIELD_ZIP));
                        up.setAddress(JO.getString(UserConstants.REG_FIELD_ADDRESS));
                        up.setAddress2(JO.getString(UserConstants.REG_FIELD_ADDRESS2));
                        up.setAbout_en("about_en");
                        up.setAbout_kr("about_kr");
                        up.setProfile_en("profile_en");
                        up.setProfile_kr("profile_kr");
                        up.setType(JO.getInt(UserConstants.REG_FIELD_USER_TYPE));

                        up.setThumb_image(JO.getString("thumb_image"));
                        up.setMain_image(JO.getString("main_image"));*/

                        up.setEmail(JO.getString(UserConstants.REG_FIELD_EMAIL));
                        up.setId(JO.getInt("id"));
                        up.setName(JO.getString(UserConstants.REG_FIELD_NAME_EN));
                        up.setBirth_date(JO.getString(UserConstants.REG_FIELD_BIRTH_DATE).replaceAll("-", ". "));
                        up.setSex(JO.getString(UserConstants.REG_FIELD_SEX));

                        String phone = JO.getString(UserConstants.REG_FIELD_PHONE);

                        up.setPhone_country_code(phone.substring(0,3));
                        up.setPhone_operator(phone.substring(3,6));
                        up.setPhone(phone.substring(6));

                        //System.out.println(up.getPhone_country_code()+"----"+up.getPhone_operator()+"-------"+up.getPhone());

                        up.setZip_code(JO.getString(UserConstants.REG_FIELD_ZIP));
                        up.setAddress(JO.getString(UserConstants.REG_FIELD_ADDRESS));
                        up.setAddress2(JO.getString(UserConstants.REG_FIELD_ADDRESS2));
                        up.setAbout_en(JO.getString("about_en"));
                        up.setAbout_kr(JO.getString("about_kr"));
                        up.setProfile_en(JO.getString("profile_en"));
                        up.setProfile_kr(JO.getString("profile_kr"));
                        up.setType(JO.getInt(UserConstants.REG_FIELD_USER_TYPE));

                        up.setThumb_image(JO.getString("thumb_image"));
                        up.setMain_image(JO.getString("main_image"));

                        _m.Success(up);

                    }else{
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                Token.RefreshAccessToken(new TheResponse() {
                    @Override
                    public void success(String response) {
                        _m.TokenRefreshRequired();
                    }

                    @Override
                    public void failed() {
                        try {
                            _m.Error(err_response.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("JSON Error");
                            _m.Error("JSON Error");
                        }
                    }
                });
            }
        });
    }


    public static void RetrieveArtistReviews(final artistReviewRetrieve _m)
    {
        final List<DashboardFrgLatest_Reviews_Model_T1> reviews = new ArrayList<>();

        RequestParams params=new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,null));

        artmineRestClient.get(ServerConstants.ARTIST_REVIEWS, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("reviews");

                        for (int i = 0; i < rec.length(); ++i) {

                            DashboardFrgLatest_Reviews_Model_T1 TTRR = new DashboardFrgLatest_Reviews_Model_T1();

                            TTRR.setReview_id(rec.getJSONObject(i).getString("review_id"));
                            TTRR.setUser_id(rec.getJSONObject(i).getString("user_id"));
                            TTRR.setImage(rec.getJSONObject(i).getString("image"));
                            TTRR.setImage_thumb(rec.getJSONObject(i).getString("image_thumb"));
                            TTRR.setDetails_info(rec.getJSONObject(i).getString("details_info"));
                            TTRR.setShare_url(rec.getJSONObject(i).getString("share_url"));
                            TTRR.setPosted_by(rec.getJSONObject(i).getString("posted_by"));
                            TTRR.setDate(rec.getJSONObject(i).getString("date"));
                            TTRR.setReview_creator_name_en(rec.getJSONObject(i).getString("review_creator_name_en"));
                            TTRR.setReview_creator_name_kr(rec.getJSONObject(i).getString("review_creator_name_kr"));
                            TTRR.setLike_count(rec.getJSONObject(i).getString("review_like_count"));

                            reviews.add(TTRR);
                        }

                        _m.Success(reviews);

                    }else{
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response){
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    _m.Success(reviews);
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _m.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _m.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });
    }

    public interface artistReviewRetrieve
    {
        void Success(List<DashboardFrgLatest_Reviews_Model_T1> reviews);
        void TokenRefreshRequired();
        void Error(String message);
    }

    public interface artistProfileRetrieve
    {
        void Success(UserProfile userProfile);
        void TokenRefreshRequired();
        void Error(String message);
    }

    public interface message {
        void Success(String message);

        void TokenRefreshRequired();

        void Error(String message);
    }
}
