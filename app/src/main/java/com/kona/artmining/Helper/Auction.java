package com.kona.artmining.Helper;


import android.content.Context;
import android.util.Log;

import com.kona.artmining.Constants.GeneralConstants;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.AuctionItem_T1;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Auction
{

    public static void GetDetails(final Context ctx, final userAuctionDataRetrieve _m) {

        RequestParams params = new RequestParams();
        final List<AuctionItem_T1> cartItemData = new ArrayList<AuctionItem_T1>();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        artmineRestClient.get(ServerConstants.AUCTION_DETAILS, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->" + response.toString());
                try {
                    if (response.getBoolean("success")) {

                        JSONArray rec = response.getJSONArray("auction_items");

                        for (int ii = 0; ii < rec.length(); ++ii) {

                            AuctionItem_T1 asil = new AuctionItem_T1();
                            asil.setId(rec.getJSONObject(ii).getString("id"));
                            asil.setArt_id(rec.getJSONObject(ii).getString("art_id"));
                            //asil.setShip_cost(GeneralConstants.SHIPPING_COST);

                            asil.setTitle_en(rec.getJSONObject(ii).getString("title_en"));
                            asil.setTitle_kr(rec.getJSONObject(ii).getString("title_kr"));

                            if(rec.getJSONObject(ii).get("sold_out") instanceof Integer)
                            {
                                asil.setSold_out(true);
                            }else{
                                if(rec.getJSONObject(ii).getString("sold_out")!=null)
                                {
                                    if(rec.getJSONObject(ii).getString("sold_out").equals("1"))
                                    {
                                        asil.setSold_out(true);
                                    }else{
                                        asil.setSold_out(false);
                                    }
                                }else{
                                    asil.setSold_out(false);
                                }
                            }

                            if(rec.getJSONObject(ii).get("purchased") instanceof Integer)
                            {
                                asil.setPurchased(true);
                            }else{
                                if(rec.getJSONObject(ii).getString("purchased")!=null)
                                {
                                    if(rec.getJSONObject(ii).getString("purchased").equals("1"))
                                    {
                                        asil.setPurchased(true);
                                    }else{
                                        asil.setPurchased(false);
                                    }
                                }else{
                                    asil.setPurchased(false);
                                }
                            }

                            if(rec.getJSONObject(ii).get("currency").equals("WON")  || rec.getJSONObject(ii).get("currency").equals("KRW"))
                            {
                                asil.setCurrency("KR");
                            }else{
                                asil.setCurrency(rec.getJSONObject(ii).getString("currency"));
                            }
                            //asil.setCurrency(rec.getJSONObject(ii).getString("currency"));
                            asil.setPrice(Double.valueOf(rec.getJSONObject(ii).getString("bidding_amount")));
                            asil.setArtistName_en(rec.getJSONObject(ii).getString("name_en"));
                            asil.setArtistName_kr(rec.getJSONObject(ii).getString("name_kr"));
                            asil.setMain_image_watermarked(rec.getJSONObject(ii).getString("main_image_watermarked"));
                            asil.setMain_image_thumb(rec.getJSONObject(ii).getString("main_image_thumb"));

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            try {
                                Date date = dateFormat.parse(rec.getJSONObject(ii).getString("requested_time"));

                                asil.setTime(new SimpleDateFormat("hh:mm aa dd-MM-yy").format(date));

                            } catch (ParseException e) {
                                asil.setTime(rec.getJSONObject(ii).getString("requested_time"));
                            }

                            asil.setWinner(rec.getJSONObject(ii).getBoolean("winner"));

                            cartItemData.add(asil);
                        }

                        _m.Success(cartItemData);
                    } else {
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No cart item found.
                    _m.Success(cartItemData);
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _m.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _m.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });
    }


    public interface userAuctionDataRetrieve {
        void Success(List<AuctionItem_T1> auctionItemData);

        void TokenRefreshRequired();

        void Error(String message);
    }
}
