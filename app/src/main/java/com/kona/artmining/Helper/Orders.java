package com.kona.artmining.Helper;


import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.CartItem_T1;
import com.kona.artmining.Model.Order_T1;
import com.kona.artmining.Model.Shipping_T1;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Orders
{

    public static void RetrieveAllOrders(final RetrieveAllOrders_response _raor)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        final List<Order_T1> order_t1List = new ArrayList<>();

        artmineRestClient.get(ServerConstants.ORDER_LIST, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("Orders:RetrieveAllOrders-------->" + response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray orders = response.getJSONArray("orders");

                        for (int i = 0; i < orders.length(); ++i) {

                            Order_T1 _pp = new Order_T1();

                            JSONObject order = orders.getJSONObject(i);

                            _pp.setOrder_id(order.getInt("order_id"));
                            _pp.setOrder_type(order.getString("order_type"));
                            _pp.setUser_id(order.getInt("user_id"));
                            _pp.setTransaction_id(order.getString("transaction_id"));
                            _pp.setPay_total(order.getDouble("total"));
                            _pp.setItem_total(order.getDouble("sub_total"));
                            _pp.setCurrency(order.getString("currency"));
                            _pp.setShipping_cost(order.getDouble("shipping_cost"));
                            _pp.setOrderDate(order.getString("order_date").split(" ")[0].replaceAll("-","."));
                            _pp.setGateway_used(order.getString("pay_via"));

                            JSONArray product_info = order.getJSONArray("product_info");

                            List<CartItem_T1> BBCC = new ArrayList<CartItem_T1>();

                            for (int pl = 0; pl < product_info.length(); ++pl) {
                                BBCC.add(new CartItem_T1());
                            }

                            _pp.setItems(BBCC);

                            if(product_info.length()>0)
                            {
                                CartItem_T1 cartItem_t1 = new CartItem_T1();

                                if(product_info.getJSONObject(0).has("title_en"))
                                {
                                    cartItem_t1.setTitle_en(product_info.getJSONObject(0).getString("title_en"));
                                }
                                if(product_info.getJSONObject(0).has("title_kr"))
                                {
                                    cartItem_t1.setTitle_kr(product_info.getJSONObject(0).getString("title_kr"));
                                }

                                if(product_info.getJSONObject(0).has("artistName_en"))
                                {
                                    cartItem_t1.setArtistName_en(product_info.getJSONObject(0).getString("artistName_en"));
                                }
                                if(product_info.getJSONObject(0).has("artistName_kr"))
                                {
                                    cartItem_t1.setArtistName_kr(product_info.getJSONObject(0).getString("artistName_kr"));
                                }

                                if(product_info.getJSONObject(0).has("name_en"))
                                {
                                    cartItem_t1.setArtistName_en(product_info.getJSONObject(0).getString("name_en"));
                                }
                                if(product_info.getJSONObject(0).has("name_kr"))
                                {
                                    cartItem_t1.setArtistName_kr(product_info.getJSONObject(0).getString("name_kr"));
                                }

                                if(product_info.getJSONObject(0).has("currency"))
                                {
                                    cartItem_t1.setCurrency(product_info.getJSONObject(0).getString("currency"));
                                }
                                if(product_info.getJSONObject(0).has("price"))
                                {
                                    cartItem_t1.setPrice(product_info.getJSONObject(0).getDouble("price"));
                                }
                                if(product_info.getJSONObject(0).has("main_image_thumb"))
                                {
                                    cartItem_t1.setMain_image_thumb(product_info.getJSONObject(0).getString("main_image_thumb"));
                                }
                                if(product_info.getJSONObject(0).has("main_image_watermarked"))
                                {
                                    cartItem_t1.setMain_image_watermarked(product_info.getJSONObject(0).getString("main_image_watermarked"));
                                }
                                if(product_info.getJSONObject(0).has("ship_cost"))
                                {
                                    cartItem_t1.setShip_cost(product_info.getJSONObject(0).getString("ship_cost"));
                                }
                                if(product_info.getJSONObject(0).has("ship_cost_cash"))
                                {
                                    cartItem_t1.setShip_cost_cash(product_info.getJSONObject(0).getString("ship_cost_cash"));
                                }
                                _pp.setFeaturedItem(cartItem_t1);

                                if(order.has("shipping_address"))
                                {
                                    JSONObject shippingInfo = order.getJSONObject("shipping_address");

                                    Shipping_T1 ST1 = new Shipping_T1();
                                    ST1.setFullName(shippingInfo.getString("full_name"));
                                    if(shippingInfo.has("PhoneNumber"))
                                    {
                                        ST1.setPhoneNumber(shippingInfo.getString("PhoneNumber"));
                                    }
                                    else
                                    {
                                        ST1.setPhoneNumber(shippingInfo.getString("mobile_or_phone"));
                                    }

                                    ST1.setAddressLine1(shippingInfo.getString("address_1"));

                                    if(shippingInfo.has("extension"))
                                    {
                                        ST1.setExtension(shippingInfo.getString("extension"));
                                    }
                                    ST1.setAddressLine2(shippingInfo.getString("address_2"));

                                    _pp.setShippingDetails(ST1);

                                }

                            }

                            order_t1List.add(_pp);
                        }

                        _raor.Success(order_t1List);
                    }else{
                        _raor.Error("JSON Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _raor.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _raor.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _raor.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _raor.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _raor.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _raor.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void RetrieveAllBuyerOrders(final RetrieveAllOrders_response _raor)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        final List<Order_T1> _OT1 = new ArrayList<>();

        artmineRestClient.get(ServerConstants.ORDER_LIST, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("Orders:RetrieveAllOrders-------->" + response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray orders = response.getJSONArray("orders");

                        for (int i = 0; i < orders.length(); ++i) {

                            Order_T1 _pp = new Order_T1();

                            JSONObject order = orders.getJSONObject(i);

                            _pp.setOrder_id(order.getInt("order_id"));
                            _pp.setOrder_type(order.getString("order_type"));
                            _pp.setStatus(order.getInt("status"));
                            _pp.setUser_id(order.getInt("user_id"));
                            _pp.setTransaction_id(order.getString("transaction_id"));
                            _pp.setPay_total(order.getDouble("total"));
                            _pp.setItem_total(order.getDouble("sub_total"));
                            _pp.setCurrency(order.getString("currency"));
                            _pp.setShipping_cost(order.getDouble("shipping_cost"));
                            _pp.setOrderDate(order.getString("order_date").split(" ")[0].replaceAll("-","."));
                            _pp.setGateway_used(order.getString("pay_via"));

                            JSONArray product_info = order.getJSONArray("product_info");

                            List<CartItem_T1> BBCC = new ArrayList<CartItem_T1>();

                            for (int pl = 0; pl < product_info.length(); ++pl) {
                                BBCC.add(new CartItem_T1());
                            }

                            _pp.setItems(BBCC);

                            if(product_info.length()>0)
                            {
                                CartItem_T1 cartItem_t1 = new CartItem_T1();

                                if(product_info.getJSONObject(0).has("title_en"))
                                {
                                    cartItem_t1.setTitle_en(product_info.getJSONObject(0).getString("title_en"));
                                }
                                if(product_info.getJSONObject(0).has("title_kr"))
                                {
                                    cartItem_t1.setTitle_kr(product_info.getJSONObject(0).getString("title_kr"));
                                }

                                if(product_info.getJSONObject(0).has("artistName_en"))
                                {
                                    cartItem_t1.setArtistName_en(product_info.getJSONObject(0).getString("artistName_en"));
                                }
                                if(product_info.getJSONObject(0).has("artistName_kr"))
                                {
                                    cartItem_t1.setArtistName_kr(product_info.getJSONObject(0).getString("artistName_kr"));
                                }

                                if(product_info.getJSONObject(0).has("name_en"))
                                {
                                    cartItem_t1.setArtistName_en(product_info.getJSONObject(0).getString("name_en"));
                                }
                                if(product_info.getJSONObject(0).has("name_kr"))
                                {
                                    cartItem_t1.setArtistName_kr(product_info.getJSONObject(0).getString("name_kr"));
                                }

                                if(product_info.getJSONObject(0).has("currency"))
                                {
                                    cartItem_t1.setCurrency(product_info.getJSONObject(0).getString("currency"));
                                }
                                if(product_info.getJSONObject(0).has("price"))
                                {
                                    cartItem_t1.setPrice(product_info.getJSONObject(0).getDouble("price"));
                                }
                                if(product_info.getJSONObject(0).has("main_image_thumb"))
                                {
                                    cartItem_t1.setMain_image_thumb(product_info.getJSONObject(0).getString("main_image_thumb"));
                                }
                                if(product_info.getJSONObject(0).has("main_image_watermarked"))
                                {
                                    cartItem_t1.setMain_image_watermarked(product_info.getJSONObject(0).getString("main_image_watermarked"));
                                }
                                _pp.setFeaturedItem(cartItem_t1);

                                if(order.has("shipping_address"))
                                {
                                    JSONObject shippingInfo = order.getJSONObject("shipping_address");

                                    Shipping_T1 ST1 = new Shipping_T1();
                                    ST1.setFullName(shippingInfo.getString("full_name"));
                                    if(shippingInfo.has("PhoneNumber"))
                                    {
                                        ST1.setPhoneNumber(shippingInfo.getString("PhoneNumber"));
                                    }
                                    else
                                    {
                                        ST1.setPhoneNumber(shippingInfo.getString("mobile_or_phone"));
                                    }

                                    ST1.setAddressLine1(shippingInfo.getString("address_1"));

                                    if(shippingInfo.has("extension"))
                                    {
                                        ST1.setExtension(shippingInfo.getString("extension"));
                                    }
                                    ST1.setAddressLine2(shippingInfo.getString("address_2"));

                                    _pp.setShippingDetails(ST1);

                                }

                            }

                            _OT1.add(_pp);
                        }

                        _raor.Success(_OT1);
                    }else{
                        _raor.Error("JSON Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _raor.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _raor.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _raor.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _raor.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _raor.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _raor.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void UpdateOrderStatus(final String order_id, String status_id, final _response _raor) {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("order_id", order_id);
        params.add("status_id", status_id);

        artmineRestClient.post(ServerConstants.ORDER_UPDATE_STATUS, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response)
            {
                try
                {
                    if(response.getBoolean("success"))
                    {
                        _raor.Success(order_id);
                    }
                    else
                    {
                        _raor.Error("JSON Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _raor.Error("JSON Error");
                }
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _raor.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _raor.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _raor.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _raor.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _raor.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void RetrieveAllSellerOrders(final RetrieveAllOrders_response _raor)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        final List<Order_T1> _OT1 = new ArrayList<>();

        artmineRestClient.get(ServerConstants.ORDER_LIST_FOR_ARTIST, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("Orders:RetrieveAllOrders-------->" + response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray orders = response.getJSONArray("orders");

                        for (int i = 0; i < orders.length(); ++i) {

                            Order_T1 _pp = new Order_T1();

                            JSONObject order = orders.getJSONObject(i);

                            _pp.setOrder_id(order.getInt("order_id"));
                            _pp.setOrder_type(order.getString("order_type"));
                            _pp.setStatus(order.getInt("order_status"));
                            //_pp.setUser_id(order.getInt("user_id"));
                            //_pp.setTransaction_id(order.getString("transaction_id"));
                            _pp.setPay_total(order.getDouble("total_price"));
                            _pp.setItem_total(order.getDouble("item_total"));
                            _pp.setCurrency(order.getString("currency"));
                            _pp.setShipping_cost(order.getDouble("shipping_cost"));
                            _pp.setOrderDate(order.getString("order_date").split(" ")[0].replaceAll("-","."));
                            //_pp.setGateway_used(order.getString("pay_via"));

                            _pp.setTitle_en(order.getString("title_en"));
                            _pp.setMain_image_thumb(order.getString("main_image_thumb"));
                            _pp.setMain_image_watermarked(order.getString("main_image_watermarked"));
                            _pp.setBuyer_phone(order.getString("buyer_phone"));
                            _pp.setBuyer_name_en(order.getString("buyer_name_en"));

                            if(order.has("shipping_address"))
                            {
                                JSONObject shippingInfo = order.getJSONObject("shipping_address");

                                Shipping_T1 ST1 = new Shipping_T1();
                                ST1.setFullName(shippingInfo.getString("full_name"));
                                if(shippingInfo.has("PhoneNumber"))
                                {
                                    ST1.setPhoneNumber(shippingInfo.getString("PhoneNumber"));
                                }
                                else
                                {
                                    ST1.setPhoneNumber(shippingInfo.getString("mobile_or_phone"));
                                }

                                ST1.setAddressLine1(shippingInfo.getString("address_1"));

                                if(shippingInfo.has("extension"))
                                {
                                    ST1.setExtension(shippingInfo.getString("extension"));
                                }
                                ST1.setAddressLine2(shippingInfo.getString("address_2"));

                                _pp.setShippingDetails(ST1);

                            }

                            _OT1.add(_pp);
                        }

                        _raor.Success(_OT1);
                    }else{
                        _raor.Error("JSON Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _raor.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _raor.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _raor.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _raor.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _raor.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _raor.Error("JSON Error");
                    }
                }
            }
        });
    }

    public interface RetrieveAllOrders_response
    {
        void Success(List<Order_T1> orders);
        void TokenRefreshRequired();
        void Error(String message);
    }

    public interface _response
    {
        void Success(String order_id);
        void TokenRefreshRequired();
        void Error(String message);
    }
}
