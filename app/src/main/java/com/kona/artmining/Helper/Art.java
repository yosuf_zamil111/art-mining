package com.kona.artmining.Helper;


import android.content.Context;

import com.google.gson.JsonObject;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.ArtImages_T1;
import com.kona.artmining.Model.Art_T1;
import com.kona.artmining.Model.ShippingMethodNames;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Art
{

    public static void GetShippingMethods(final Context ctx, final ShippingServiceRetrived shippingServiceRetrived) {

        RequestParams params = new RequestParams();
        final List<ShippingMethodNames> itemData = new ArrayList<ShippingMethodNames>();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        artmineRestClient.get(ServerConstants.GET_SHIPPING_METHODS, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->" + response.toString());
                try {
                    if (response.getBoolean("success")) {

                        JSONArray rec = response.getJSONArray("message");

                        for (int ii = 0; ii < rec.length(); ++ii) {

                            ShippingMethodNames asil = new ShippingMethodNames();
                            asil.setGroup_id(rec.getJSONObject(ii).getInt("group_id"));
                            asil.setUser_id(rec.getJSONObject(ii).getInt("user_id"));
                            asil.setGroup_name(rec.getJSONObject(ii).getString("group_name"));

                            itemData.add(asil);
                        }

                        shippingServiceRetrived.Success(itemData);
                    } else {
                        shippingServiceRetrived.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    shippingServiceRetrived.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No cart item found.
                    shippingServiceRetrived.NotFound();
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            shippingServiceRetrived.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                shippingServiceRetrived.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                shippingServiceRetrived.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        shippingServiceRetrived.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        shippingServiceRetrived.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void GetArtDetails(String id, final ArtDetailsReceived _m) {

        RequestParams params = new RequestParams();
        final Art_T1 itemData = new Art_T1();

        artmineRestClient.get(ServerConstants.GET_ART_DETAILS.replaceAll("dddddd", id), params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->" + response.toString());
                try {
                    if (response.getBoolean("success")) {

                        JSONObject rec = response.getJSONObject("art_info");

                        itemData.setArt_id(rec.getInt("art_id"));
                        itemData.setTheme_id(rec.getInt("theme_id"));

                        JSONArray images = rec.getJSONArray("images");
                        List<ArtImages_T1> imagesT1List = new ArrayList<ArtImages_T1>();

                        for (int ii = 0; ii < images.length(); ++ii) {

                            ArtImages_T1 ai = new ArtImages_T1();
                            ai.setArt_id(images.getJSONObject(ii).getInt("art_id"));
                            ai.setArt_image_id(images.getJSONObject(ii).getInt("art_image_id"));
                            ai.setMain_image(images.getJSONObject(ii).getString("main_image"));
                            ai.setMain_image_watermarked(images.getJSONObject(ii).getString("main_image_watermarked"));
                            ai.setMain_image_thumb(images.getJSONObject(ii).getString("main_image_thumb"));
                            imagesT1List.add(ai);
                        }
                        itemData.setImages(imagesT1List);
                        itemData.setTitle_en(rec.getString("title_en"));
                        itemData.setTitle_kr(rec.getString("title_kr"));
                        itemData.setArt_type(rec.getInt("art_type"));
                        itemData.setStatus(rec.getInt("status"));
                        itemData.setPosted_by(rec.getInt("posted_by"));
                        if(rec.getString("sold_out")==null)
                        {
                            itemData.setSold_out(false);
                        }
                        else
                        {
                            itemData.setSold_out(true);
                        }
                        itemData.setShip_id(rec.getString("ship_id"));
                        itemData.setShip_type(rec.getString("ship_type"));

                        if(rec.getString("delivery_in_person")!="null")
                        {
                            itemData.setDelivery_in_person(rec.getInt("delivery_in_person"));
                        }

                        itemData.setNot_ship_to(rec.getString("not_ship_to"));
                        itemData.setArtist_id(rec.getInt("artist_id"));
                        itemData.setCurrency(rec.getString("currency"));
                        itemData.setMin_price(rec.getDouble("min_price"));
                        itemData.setMax_price(rec.getDouble("max_price"));
                        itemData.setPrice(rec.getDouble("price"));
                        itemData.setDetails_en(rec.getString("details_en"));
                        itemData.setDetails_kr(rec.getString("details_kr"));
                        itemData.setSize(rec.getString("size"));
                        itemData.setMaterial(rec.getString("material"));
                        itemData.setYear(rec.getString("year"));
                        itemData.setExpired_on(rec.getString("expired_on"));

                        _m.Success(itemData);
                    } else {
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No cart item found.
                    _m.NotFound();

                }else{
                    try {
                        _m.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void SaveAnArt(RequestParams params, final SaveTheArt _m) {


        artmineRestClient.SetTimeout(5000);
        artmineRestClient.post(ServerConstants.CREATE_AN_ART, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("----SaveAnArt---->" + response.toString());
                try {
                    if (response.getBoolean("success")) {
                        _m.Success();
                    } else {
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(throwable.getMessage() != null)
                {
                    if(throwable.getMessage().contains("Read timed out"))
                    {
                        _m.Success();
                    }
                }
                else {
                    _m.Success();
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    _m.NotFound();
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                if(err_response != null)
                                {
                                    _m.Error(err_response.getString("message"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        if(err_response != null)
                        {
                            _m.Error(err_response.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void GetArtistArtsWorks(final RetrieveArts _m)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        final List<Art_T1> itemData = new ArrayList<Art_T1>();


        artmineRestClient.get(ServerConstants.GET_ARTIST_POSTED_ART, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->" + response.toString());
                try {
                    if (response.getBoolean("success")) {

                        JSONArray aArray = response.getJSONArray("posted_arts");

                        for (int ii = 0; ii < aArray.length(); ++ii) {

                            JSONObject rec = aArray.getJSONObject(ii);


                            Art_T1 n = new Art_T1();
                            n.setArt_id(rec.getInt("art_id"));
                            n.setTheme_id(rec.getInt("theme_id"));
                            n.setTitle_en(rec.getString("title_en"));
                            n.setTitle_kr(rec.getString("title_kr"));
                            n.setArt_type(rec.getInt("art_type"));
                            n.setStatus(rec.getInt("status"));
                            n.setPosted_by(rec.getInt("posted_by"));

                            /*if(rec.getString("sold_out")==null)
                            {
                                n.setSold_out(false);
                            }
                            else
                            {
                                n.setSold_out(true);
                            }*/

                            if(rec.has("sold_out"))
                            {
                                if(rec.getString("sold_out").contains("1"))
                                {
                                    n.setSold_out(true);
                                }
                                else
                                {
                                    n.setSold_out(false);
                                }
                            }
                            else
                            {
                                n.setSold_out(false);
                            }

                            n.setShip_id(rec.getString("ship_id"));
                            n.setShip_type(rec.getString("ship_type"));
                            //n.setDelivery_in_person(rec.getInt("delivery_in_person"));
                            //n.setNot_ship_to(rec.getString("not_ship_to"));
                            n.setArtist_id(rec.getInt("artist_id"));
                            n.setCurrency(rec.getString("currency"));
                            n.setMin_price(rec.getDouble("min_price"));
                            n.setMax_price(rec.getDouble("max_price"));
                            n.setPrice(rec.getDouble("price"));
                            n.setDetails_en(rec.getString("details_en"));
                            n.setDetails_kr(rec.getString("details_kr"));
                            n.setSize(rec.getString("size"));
                            n.setMaterial(rec.getString("material"));
                            n.setYear(rec.getString("year"));
                            n.setExpired_on(rec.getString("expired_on"));


                            n.setMain_image_watermarked(rec.getString("main_image_watermarked"));
                            n.setMain_image(rec.getString("main_image"));
                            n.setMain_image_thumb(rec.getString("main_image_thumb"));

                            itemData.add(n);
                        }


                        _m.Success(itemData);
                    } else {
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No cart item found.
                    _m.NotFound();

                }else{
                    try {
                        _m.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void AddArtToScrapBook(int image_id, final response _m)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("image_id", image_id+"");

        artmineRestClient.post(ServerConstants.ARTIST_SCRAPBOOK_ITEM_ADD, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("----SaveAnArt---->" + response.toString());
                try {
                    if (response.getBoolean("success")) {
                        _m.Success();
                    } else {
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(throwable.getMessage() != null)
                {
                    if(throwable.getMessage().contains("Read timed out"))
                    {
                        _m.Success();
                    }
                }
                else {
                    _m.Success();
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    _m.NotFound();
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                if(err_response != null)
                                {
                                    _m.Error(err_response.getString("message"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        if(err_response != null)
                        {
                            _m.Error(err_response.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void RemoveArts(String art_ids, final response _m)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("art_id", art_ids);

        artmineRestClient.post(ServerConstants.REMOVE_AN_ART, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONArray response) {
                System.out.println("----RemoveAnArt---->" + response.toString());
                /*try {
                    if (response.getBoolean("success")) {
                        _m.Success();
                    } else {
                        _m.Error(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _m.Error("JSON Error");
                }*/
                //_m.Success();
                if(response.length()>0)
                {
                    try {
                        JSONObject singleResponse = response.getJSONObject(0);
                        if (singleResponse.getBoolean("success")) {
                            _m.Success();
                        } else {
                            _m.Error(singleResponse.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        _m.Error("JSON Parse Error 368");
                    }
                }
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("----RemoveAnArt---->" + response.toString());
                _m.Success();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(throwable.getMessage() != null)
                {
                    if(throwable.getMessage().contains("Read timed out"))
                    {
                        _m.Success();
                    }
                }
                else {
                    _m.Success();
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    _m.NotFound();
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                if(err_response != null)
                                {
                                    _m.Error(err_response.getString("message"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        if(err_response != null)
                        {
                            _m.Error(err_response.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });

    }

    public interface response
    {
        void Success();
        void NotFound();
        void TokenRefreshRequired();
        void Error(String message);
    }

    public interface ArtDetailsReceived
    {
        void Success(Art_T1 itemData);
        void NotFound();
        void Error(String message);
    }

    public interface ShippingServiceRetrived
    {
        void Success(List<ShippingMethodNames> itemData);
        void NotFound();
        void TokenRefreshRequired();
        void Error(String message);
    }

    public interface SaveTheArt
    {
        void Success();
        void NotFound();
        void TokenRefreshRequired();
        void Error(String message);
    }

    public interface RetrieveArts
    {
        void Success(List<Art_T1> items);
        void NotFound();
        void TokenRefreshRequired();
        void Error(String message);
    }
}
