package com.kona.artmining.Helper;


import android.text.TextUtils;

import com.kona.artmining.Constants.ExhibitionConstants;
import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.Comment_T2;
import com.kona.artmining.Model.DashboardFrgLatest_Event_Model_T1;
import com.kona.artmining.Model.DashboardFrgLatest_Exhibition_Model_T1;
import com.kona.artmining.Model.DashboardFrgLatest_NewRelease_Model_T1;
import com.kona.artmining.Model.DashboardFrgLatest_NewRelease_Page_Model_T1;
import com.kona.artmining.Model.DashboardFrgLatest_Reviews_Model_T1;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.kona.artmining.model.Comment_T1;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class DashboardFrgLatest
{

    public static void RetrieveExhibitionData( final Exhibition_Retrieved er){

        RequestParams params=new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));

        System.out.println("----Exhibition Data Called---->");


        final List<DashboardFrgLatest_Exhibition_Model_T1> latestModel = new ArrayList<>();

        artmineRestClient.get(ExhibitionConstants.EXHIBITION_SERVER_LINK, params, new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("exibitions");

                        for (int i = 0; i < rec.length(); ++i) {
                            DashboardFrgLatest_Exhibition_Model_T1 dashboardFrgLatest_exhibition_model_t1 = new DashboardFrgLatest_Exhibition_Model_T1();
                            dashboardFrgLatest_exhibition_model_t1.setExb_id(rec.getJSONObject(i).getInt("exibition_id"));
                            dashboardFrgLatest_exhibition_model_t1.setExb_image_thump(rec.getJSONObject(i).getString("image_thumb"));

                            latestModel.add(dashboardFrgLatest_exhibition_model_t1);
                        }
                        er.Success(latestModel);
                    }else{
                        er.Error("Some Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    System.out.println(response.toString());
                }
                er.Error("Some Error");
            }
        });

    }

    public static void RetrieveLatest( final Release_Retrieved er){

        RequestParams params=new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));

        final List<DashboardFrgLatest_NewRelease_Page_Model_T1> latestModel = new ArrayList<>();

        artmineRestClient.get(ServerConstants.RELEASE_LIST_ALL, params, new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("release_list");

                        for (int i = 0; i < rec.length(); ++i) {
                            //DashboardFrgLatest_Exhibition_Model_T1 _L = new DashboardFrgLatest_Exhibition_Model_T1();
                            //_L.setExb_id(rec.getJSONObject(i).getInt("exibition_id"));
                            //_L.setExb_image_thump(rec.getJSONObject(i).getString("image_thumb"));

                            DashboardFrgLatest_NewRelease_Page_Model_T1 DFLNR = new DashboardFrgLatest_NewRelease_Page_Model_T1();
                            DFLNR.setTitle(rec.getJSONObject(i).getString("release_section_name"));
                            DFLNR.setId(rec.getJSONObject(i).getInt("release_id"));

                            if(rec.getJSONObject(i).has("release_info"))
                            {
                                JSONArray release_info = rec.getJSONObject(i).getJSONArray("release_info");

                                List<DashboardFrgLatest_NewRelease_Model_T1> TheData = new ArrayList<DashboardFrgLatest_NewRelease_Model_T1>();

                                for (int i_release_info = 0; i_release_info < release_info.length(); ++i_release_info) {
                                    DashboardFrgLatest_NewRelease_Model_T1 release_model_t1 = new DashboardFrgLatest_NewRelease_Model_T1();

                                    release_model_t1.setId(release_info.getJSONObject(i_release_info).getInt("release_details_id"));
                                    release_model_t1.setDate(release_info.getJSONObject(i_release_info).getString("date"));
                                    release_model_t1.setDetails(release_info.getJSONObject(i_release_info).getString("release_details"));

                                    TheData.add(release_model_t1);
                                }

                                DFLNR.setData(TheData);
                            }
                            else
                            {
                                DFLNR.setData(new ArrayList<DashboardFrgLatest_NewRelease_Model_T1>());
                            }

                            latestModel.add(DFLNR);
                        }
                        er.Success(latestModel);
                    }else{
                        er.Error("Some Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    System.out.println(response.toString());
                }
                er.Error("Some Error");
            }
        });

    }

    public static void RetrieveAllReviews(final Reviews_Retrieved er) {

        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));

        final List<DashboardFrgLatest_Reviews_Model_T1> reviewModel = new ArrayList<>();

        artmineRestClient.get(ServerConstants.REVIEW_LIST_ALL, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("reviews");

                        for (int i = 0; i < rec.length(); ++i) {

                            DashboardFrgLatest_Reviews_Model_T1 TTRR = new DashboardFrgLatest_Reviews_Model_T1();

                            TTRR.setReview_id(rec.getJSONObject(i).getString("review_id"));
                            TTRR.setUser_id(rec.getJSONObject(i).getString("user_id"));
                            TTRR.setImage(rec.getJSONObject(i).getString("image"));
                            TTRR.setImage_thumb(rec.getJSONObject(i).getString("image_thumb"));
                            TTRR.setDetails_info(rec.getJSONObject(i).getString("details_info"));
                            TTRR.setShare_url(rec.getJSONObject(i).getString("share_url"));
                            TTRR.setPosted_by(rec.getJSONObject(i).getString("posted_by"));
                            TTRR.setDate(rec.getJSONObject(i).getString("date"));
                            TTRR.setReview_creator_name_en(rec.getJSONObject(i).getString("review_creator_name_en"));
                            TTRR.setReview_creator_name_kr(rec.getJSONObject(i).getString("review_creator_name_kr"));
                            TTRR.setLike_count(rec.getJSONObject(i).getString("review_like_count"));

                            reviewModel.add(TTRR);
                        }

                        er.Success(reviewModel);
                    }else{
                        er.Error("Some Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    System.out.println(response.toString());
                }
                er.Error("Some Error");
            }
        });

    }

    public static void RetrieveAllEvents(final Event_Retrieved er) {

        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));

        final List<DashboardFrgLatest_Event_Model_T1> eventModel = new ArrayList<>();

        artmineRestClient.get(ServerConstants.EVENT_LIST_ALL, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("events");

                        for (int i = 0; i < rec.length(); ++i) {

                            DashboardFrgLatest_Event_Model_T1 event_model_t1 = new DashboardFrgLatest_Event_Model_T1();

                            event_model_t1.setEvent_id(rec.getJSONObject(i).getInt("event_id"));
                            event_model_t1.setImage(rec.getJSONObject(i).getString("image"));
                            event_model_t1.setImage_thumb(rec.getJSONObject(i).getString("image_thumb"));
                            event_model_t1.setDetails_info(rec.getJSONObject(i).getString("details_info"));
                            event_model_t1.setDate(rec.getJSONObject(i).getString("date"));
                            event_model_t1.setTitle(rec.getJSONObject(i).getString("title"));

                            eventModel.add(event_model_t1);
                        }

                        er.Success(eventModel);
                    }else{
                        er.Error("Some Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    System.out.println(response.toString());
                }
                er.Error("Some Error");
            }
        });

    }

    public static void RetrieveSelectedReviews(String reviewID, final Review_Retrieved er) {

        RequestParams params = new RequestParams();
        params.add("review_id", reviewID);
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken,""));

        final DashboardFrgLatest_Reviews_Model_T1 reviews_model_t1 = new DashboardFrgLatest_Reviews_Model_T1();

        artmineRestClient.get(ServerConstants.REVIEW_DETAILS, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONObject rec = response.getJSONObject("review_details");

                        reviews_model_t1.setReview_id(rec.getString("review_id"));
                        reviews_model_t1.setUser_id(rec.getString("user_id"));
                        reviews_model_t1.setImage(rec.getString("image"));
                        reviews_model_t1.setReview_title(rec.getString("title"));
                        reviews_model_t1.setImage_thumb(rec.getString("image_thumb"));
                        reviews_model_t1.setDetails_info(rec.getString("details_info"));
                        reviews_model_t1.setShare_url(rec.getString("share_url"));
                        reviews_model_t1.setPosted_by(rec.getString("posted_by"));
                        reviews_model_t1.setDate(rec.getString("date"));
                        reviews_model_t1.setReview_creator_name_en(rec.getString("review_creator_name_en"));
                        reviews_model_t1.setReview_creator_name_kr(rec.getString("review_creator_name_kr"));
                        reviews_model_t1.setLike_count(rec.getString("like_count"));

                        er.Success(reviews_model_t1);
                    }else{
                        er.Error("Some Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    System.out.println(response.toString());
                }
                er.Error("Some Error");
            }
        });

    }

    public static void UpdateSelectedReviews(File review_image, String user_id, String review_id, String title, String details, final response er)
    {
        RequestParams params = new RequestParams();
        if (review_image != null) {
            try {
                params.put("review_image", review_image);
            } catch (FileNotFoundException e) {

            }
        }
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("review_id",review_id);

        /*if(user_id != null && !TextUtils.isEmpty(user_id))
        {
            params.add("user_id",user_id);
        }*/

        params.add("title",title);
        params.add("details",details);

        artmineRestClient.post(ServerConstants.REVIEW_EDIT, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                er.Success();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            er.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                er.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                er.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        er.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        er.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void CreateANewReview(File review_image, String user_id, String title, String details, final response er)
    {
        RequestParams params = new RequestParams();
        if (review_image != null) {
            try {
                params.put("review_image", review_image);
            } catch (FileNotFoundException e) {

            }
        }
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        if(user_id != null && !TextUtils.isEmpty(user_id))
        {
            params.add("user_id",user_id);
        }
        params.add("title",title);
        params.add("details",details);

        artmineRestClient.post(ServerConstants.REVIEW_CREATE, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                er.Success();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            er.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                er.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                er.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        er.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        er.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void CreateANewReviewComment(String review_id, String comment, final response er)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("review_id",review_id);
        params.add("comment",comment);

        artmineRestClient.post(ServerConstants.REVIEW_CREATE_COMMENT, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                er.Success();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            er.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                er.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                er.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        er.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        er.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void RetrieveSelectedReviewComments(String reviewID, final SelectedExhibitionCommentsRetrived er) {

        RequestParams params = new RequestParams();
        params.add("review_id", reviewID);

        final List<Comment_T1> theComments = new ArrayList<Comment_T1>();

        artmineRestClient.get(ServerConstants.REVIEW_RETRIEVE_COMMENT, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("comments");

                        for (int i = 0; i < rec.length(); ++i) {

                            Comment_T1 comment = new Comment_T1();
                            comment.setComment_id(rec.getJSONObject(i).getInt("comment_id"));
                            comment.setCommentor_id(rec.getJSONObject(i).getInt("commentor_id"));
                            comment.setComment_date_time(rec.getJSONObject(i).getString("comment_date_time"));
                            comment.setComment_details(rec.getJSONObject(i).getString("comment_details"));
                            comment.setCommentor_english_name(rec.getJSONObject(i).getString("commentor_english_name"));
                            comment.setCommentor_korean_name(rec.getJSONObject(i).getString("commentor_korean_name"));
                            comment.setProfile_image_thumb(rec.getJSONObject(i).getString("profile_image_thumb"));
                            comment.setComment_date_time(rec.getJSONObject(i).getString("comment_date_time"));


                            theComments.add(comment);
                        }
                    }

                    er.Success(theComments);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject err_response){
                System.out.println("-------->failed"+throwable.getMessage());

                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    er.Success(theComments);

                }else{
                    try {
                        er.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        er.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void IfAllowedToEditReview(String review_id, final response er)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("review_id",review_id);

        artmineRestClient.post(ServerConstants.IF_REVIEW_ALLOWED_TO_REMOVE, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){
                        er.Success();
                    }else{
                        er.Error("Not Allowed");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    er.Error("Not Allowed");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            er.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                er.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                er.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        er.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        er.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void RemoveAReview(String review_id, final response er)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("review_id",review_id);

        artmineRestClient.get(ServerConstants.REVIEW_REMOVE, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){
                        er.Success();
                    }else{
                        er.Error("Not Allowed");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    er.Error("Not Allowed");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            er.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                er.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                er.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        er.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        er.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void LikeUnlikeAReviews(String reviewID, final response er) {

        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("review_id", reviewID);

        final List<DashboardFrgLatest_Reviews_Model_T1> reviewModel = new ArrayList<>();

        artmineRestClient.post(ServerConstants.LIKE_UNLIKE_A_REVIEW, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        er.Success();
                    }else{
                        er.Error("Some Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response){
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            er.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                er.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                er.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        er.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        er.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void RetrieveSelectedExhibitionsDetils(String reviewID, final SelectedExhibitionRetrived er) {

        RequestParams params = new RequestParams();
        params.add("exhibition_id", reviewID);

        final DashboardFrgLatest_Exhibition_Model_T1 DFL_RM_T1 = new DashboardFrgLatest_Exhibition_Model_T1();

        artmineRestClient.get(ServerConstants.EXHIBITION_DETAILS, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONObject rec = response.getJSONObject("exibition_details");

                        DFL_RM_T1.setExb_id(rec.getInt("exibition_id"));
                        DFL_RM_T1.setTitle(rec.getString("title"));
                        DFL_RM_T1.setImage(rec.getString("image"));
                        DFL_RM_T1.setImage_thumb(rec.getString("image_thumb"));
                        DFL_RM_T1.setDate(rec.getString("date"));
                        DFL_RM_T1.setDetails_info(rec.getString("details_info"));

                        er.Success(DFL_RM_T1);
                    }else{
                        er.Error("Some Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    System.out.println(response.toString());
                }
                er.Error("Some Error");
            }
        });

    }

    public static void RetrieveSelectedExhibitionsComments(String reviewID, final SelectedExhibitionCommentsRetrived er) {

        RequestParams params = new RequestParams();
        params.add("exhibition_id", reviewID);

        final List<Comment_T1> theComments = new ArrayList<Comment_T1>();

        artmineRestClient.get(ServerConstants.EXHIBITION_RETRIEVE_COMMENTS, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("comments");

                        for (int i = 0; i < rec.length(); ++i) {

                            Comment_T1 comment = new Comment_T1();
                            comment.setComment_id(rec.getJSONObject(i).getInt("comment_id"));
                            comment.setCommentor_id(rec.getJSONObject(i).getInt("commentor_id"));
                            comment.setComment_date_time(rec.getJSONObject(i).getString("comment_date_time"));
                            comment.setComment_details(rec.getJSONObject(i).getString("comment_details"));
                            comment.setCommentor_english_name(rec.getJSONObject(i).getString("commentor_english_name"));
                            comment.setCommentor_korean_name(rec.getJSONObject(i).getString("commentor_korean_name"));
                            comment.setProfile_image_thumb(rec.getJSONObject(i).getString("profile_image_thumb"));
                            comment.setComment_date_time(rec.getJSONObject(i).getString("comment_date_time"));


                            theComments.add(comment);
                        }
                    }

                    er.Success(theComments);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject err_response){
                System.out.println("-------->failed"+throwable.getMessage());

                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    er.Success(theComments);

                }else{
                    try {
                        er.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        er.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void CreateANewExibition(File exibition_image, String title, String details, final response er)
    {
        RequestParams params = new RequestParams();
        if (exibition_image != null) {
            try {
                params.put("exibition_image", exibition_image);
            } catch (FileNotFoundException e) {

            }
        }
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("title",title);
        params.add("details",details);

        artmineRestClient.post(ServerConstants.EXHIBITION_CREATE, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                er.Success();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            er.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                er.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                er.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        er.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        er.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void CreateANewExibitionComment(String exhibition_id, String comment, final response er)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("exhibition_id",exhibition_id);
        params.add("comment",comment);

        artmineRestClient.post(ServerConstants.EXHIBITION_CREATE_COMMENT, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                er.Success();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response) {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            er.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                er.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                er.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        er.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        er.Error("JSON Error");
                    }
                }
            }
        });

    }

    public static void RetrieveSelectedEventDetails(String reviewID, final SelectedEventRetrived er) {

        RequestParams params = new RequestParams();
        params.add("event_id", reviewID);

        final DashboardFrgLatest_Event_Model_T1 DFL_RM_T1 = new DashboardFrgLatest_Event_Model_T1();

        artmineRestClient.get(ServerConstants.EVENT_DETAILS, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONObject rec = response.getJSONArray("event_details").getJSONObject(0);

                        DFL_RM_T1.setEvent_id(rec.getInt("event_id"));
                        DFL_RM_T1.setTitle(rec.getString("title"));
                        DFL_RM_T1.setImage(rec.getString("image"));
                        DFL_RM_T1.setImage_thumb(rec.getString("image_thumb"));
                        DFL_RM_T1.setDate(rec.getString("date"));
                        DFL_RM_T1.setDetails_info(rec.getString("details_info"));

                        er.Success(DFL_RM_T1);
                    }else{
                        er.Error("Some Error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                System.out.println("-------->failed"+throwable.getMessage());
                if(response != null)
                {
                    System.out.println(response.toString());
                }
                er.Error("Some Error");
            }
        });

    }

    public interface Release_Retrieved
    {
        void Success(List<DashboardFrgLatest_NewRelease_Page_Model_T1> message);
        void Error(String message);
    }

    public interface Exhibition_Retrieved
    {
        void Success(List<DashboardFrgLatest_Exhibition_Model_T1> message);
        void Error(String message);
    }

    public interface Reviews_Retrieved
    {
        void Success(List<DashboardFrgLatest_Reviews_Model_T1> message);
        void Error(String message);
    }

    public interface Event_Retrieved
    {
        void Success(List<DashboardFrgLatest_Event_Model_T1> message);
        void Error(String message);
    }

    public interface Review_Retrieved
    {
        void Success(DashboardFrgLatest_Reviews_Model_T1 data);
        void Error(String message);
    }

    public interface SelectedExhibitionRetrived
    {
        void Success(DashboardFrgLatest_Exhibition_Model_T1 data);
        void Error(String message);
    }

    public interface SelectedExhibitionCommentsRetrived
    {
        void Success(List<Comment_T1> comments);
        void Error(String message);
    }

    public interface SelectedEventRetrived
    {
        void Success(DashboardFrgLatest_Event_Model_T1 data);
        void Error(String message);
    }

    public interface response
    {
        void Success();
        void TokenRefreshRequired();
        void Error(String message);
    }
}
