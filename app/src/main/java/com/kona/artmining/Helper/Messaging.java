package com.kona.artmining.Helper;


import com.kona.artmining.Constants.ServerConstants;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Interface.TheResponse;
import com.kona.artmining.Model.Message;
import com.kona.artmining.RESTclient.artmineRestClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Messaging {

    public static void RetrieveInboxMessage(final MessageList_Retrieved ILR)
    {
        RequestParams params=new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        final List<Message> messageList = new ArrayList<>();

        artmineRestClient.get(ServerConstants.MESSAGE_INBOX_MESSAGE, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("inbox_messages");

                        for (int i = 0; i < rec.length(); ++i) {

                            JSONObject JO = rec.getJSONObject(i);

                            Message m = new Message();
                            m.setId(JO.getLong("message_id"));

                            m.setDate(JO.getString("date"));
                            m.setSubject(JO.getString("subject"));
                            m.setMail_details(JO.getString("mail_details"));
                            m.setUnread_info(JO.getInt("unread_info"));
                            m.setSender_name(JO.getString("sender_name"));
                            m.setReceiver_name(JO.getString("receiver_name"));

                            m.setMail_status(JO.getInt("to_mail_status"));
                            m.setFrom_id(JO.getInt("from_id"));
                            m.setTo_id(JO.getInt("to_id"));

                            m.setName(m.getSender_name());
                            m.setChecked(false);
                            m.setType(UserConstants.MESSAGE_TYPE_INBOX);

                            messageList.add(m);
                        }

                        ILR.Success(messageList);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                //System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            ILR.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                ILR.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                ILR.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        ILR.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void RetrieveSentMessage(final MessageList_Retrieved ILR)
    {
        RequestParams params=new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        final List<Message> messageArrayList = new ArrayList<>();

        artmineRestClient.get(ServerConstants.MESSAGE_SENT_MESSAGE, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("sent_messages");

                        for (int i = 0; i < rec.length(); ++i) {

                            JSONObject JO = rec.getJSONObject(i);

                            Message m = new Message();
                            m.setId(JO.getLong("message_id"));

                            m.setDate(JO.getString("date"));
                            m.setSubject(JO.getString("subject"));
                            m.setMail_details(JO.getString("mail_details"));
                            //m.setUnread_info(JO.getInt("unread_info"));
                            m.setUnread_info(0);
                            m.setSender_name(JO.getString("sender_name"));
                            m.setReceiver_name(JO.getString("receiver_name"));

                            m.setMail_status(JO.getInt("to_mail_status"));
                            m.setFrom_id(JO.getInt("from_id"));
                            m.setTo_id(JO.getInt("to_id"));

                            m.setName(m.getReceiver_name());
                            m.setChecked(false);
                            m.setType(UserConstants.MESSAGE_TYPE_SENT);

                            messageArrayList.add(m);
                        }

                        ILR.Success(messageArrayList);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            ILR.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                ILR.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                ILR.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        ILR.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }
            }
        });
    }


    public static void RetrieveTrashMessage(final MessageList_Retrieved ILR)
    {
        RequestParams params=new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));

        final List<Message> messageList = new ArrayList<>();

        artmineRestClient.get(ServerConstants.MESSAGE_TRASH_MESSAGE, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")){

                        JSONArray rec = response.getJSONArray("sent_messages");

                        for (int i = 0; i < rec.length(); ++i) {

                            JSONObject JO = rec.getJSONObject(i);

                            Message m = new Message();
                            m.setId(JO.getLong("message_id"));

                            m.setDate(JO.getString("date"));
                            m.setSubject(JO.getString("subject"));
                            m.setMail_details(JO.getString("mail_details"));
                            //m.setUnread_info(JO.getInt("unread_info"));
                            m.setUnread_info(0);
                            m.setSender_name(JO.getString("sender_name"));
                            m.setReceiver_name(JO.getString("receiver_name"));

                            m.setMail_status(JO.getInt("to_mail_status"));
                            m.setFrom_id(JO.getInt("from_id"));
                            m.setTo_id(JO.getInt("to_id"));

                            m.setName(m.getSender_name());
                            m.setChecked(false);
                            m.setType(UserConstants.MESSAGE_TYPE_TRASH);

                            messageList.add(m);
                        }

                        ILR.Success(messageList);

                    }else{
                        ILR.Error("Error data found");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    ILR.Error("JSON Error");
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            ILR.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                ILR.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                ILR.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        ILR.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        ILR.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void TrashMessage(String message_id, final MessageRemove _m)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("message_id",message_id);

        artmineRestClient.post(ServerConstants.MESSAGE_TRASH_A_MESSAGE, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response)
            {
                _m.Success();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    _m.NotFound();
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _m.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _m.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void RemoveMessage(String message_id, final MessageRemove _m)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("message_id",message_id);

        artmineRestClient.post(ServerConstants.MESSAGE_DELETE_MESSAGE, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response)
            {
                _m.Success();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND)
                {
                    // No item found.
                    _m.NotFound();
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _m.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _m.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _m.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _m.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _m.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void GetMessageDetails(String message_id, final Message_Retrieved _message_retrieved)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("message_id",message_id);

        artmineRestClient.get(ServerConstants.MESSAGE_SINGLE_MESSAGE_DETAILS, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response)
            {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")) {

                        JSONObject JO = response.getJSONObject("message");
                        Message m=new Message();
                        m.setId(JO.getLong("message_id"));

                        m.setDate(JO.getString("date"));
                        m.setSubject(JO.getString("subject"));
                        m.setMail_details(JO.getString("mail_details"));
                        m.setSender_name(JO.getString("sender_name"));
                        m.setReceiver_name(JO.getString("receiver_name"));

                        m.setMail_status(JO.getInt("to_mail_status"));
                        m.setFrom_id(JO.getInt("from_id"));
                        m.setTo_id(JO.getInt("to_id"));

                        _message_retrieved.Success(m);
                    }else{
                        _message_retrieved.NotFound();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _message_retrieved.NotFound();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_NOT_FOUND){
                    _message_retrieved.NotFound();
                }
                else if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _message_retrieved.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _message_retrieved.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _message_retrieved.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _message_retrieved.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _message_retrieved.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void SendMail(RequestParams parms, final response _response)
    {
        artmineRestClient.post(ServerConstants.MESSAGE_CREATE_MESSAGE, parms, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response)
            {
                System.out.println("-------->"+response.toString());
                try {
                    if(response.getBoolean("success")) {

                        _response.Success();
                    }else{
                        _response.Error("");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    _response.Error("");
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _response.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _response.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _response.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _response.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _response.Error("JSON Error");
                    }
                }
            }
        });
    }

    public static void MoveToInbox(String message_id, final response _response)
    {
        RequestParams params = new RequestParams();
        params.add(UserConstants.AccessToken, Prefs.getString(UserConstants.AccessToken, null));
        params.add("message_id",message_id);

        artmineRestClient.post(ServerConstants.MESSAGE_MOVE_TO_INBOX, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, final JSONObject response)
            {
                _response.Success();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject err_response)
            {
                System.out.println("-------->failed" + throwable.getMessage());
                if(err_response != null)
                {
                    System.out.println(err_response.toString());
                }

                if(statusCode == ServerConstants.SC_UNAUTHORIZED)
                {
                    // We need to refresh the token.
                    Token.RefreshAccessToken(new TheResponse() {
                        @Override
                        public void success(String response) {
                            _response.TokenRefreshRequired();
                        }

                        @Override
                        public void failed() {
                            try {
                                _response.Error(err_response.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.println("JSON Error");
                                _response.Error("JSON Error");
                            }
                        }
                    });
                }else{
                    try {
                        _response.Error(err_response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        System.out.println("JSON Error");
                        _response.Error("JSON Error");
                    }
                }
            }
        });
    }

    public interface response
    {
        void Success();
        void TokenRefreshRequired();
        void Error(String message);
    }

    public interface MessageList_Retrieved
    {
        void Success(List<Message> message);
        void TokenRefreshRequired();
        void Error(String message);
    }

    public interface Message_Retrieved
    {
        void Success(Message _message);
        void TokenRefreshRequired();
        void NotFound();
        void Error(String message);
    }

    public interface MessageRemove
    {
        void Success();
        void NotFound();
        void TokenRefreshRequired();
        void Error(String message);
    }
}
