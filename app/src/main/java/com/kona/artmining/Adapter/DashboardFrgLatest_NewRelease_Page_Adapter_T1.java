package com.kona.artmining.Adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.kona.artmining.Model.DashboardFrgLatest_NewRelease_Page_Model_T1;
import com.kona.artmining.R;

import java.util.ArrayList;
import java.util.List;

public class DashboardFrgLatest_NewRelease_Page_Adapter_T1 extends RecyclerView.Adapter<DashboardFrgLatest_NewRelease_Page_Adapter_T1.ViewHolder>
{
    List<DashboardFrgLatest_NewRelease_Page_Model_T1> content = new ArrayList<DashboardFrgLatest_NewRelease_Page_Model_T1>();


    public class ViewHolder extends RecyclerView.ViewHolder {

        private Context ctx;
        ObservableRecyclerView recycler_view;
        DashboardFrgLatest_NewRelease_Adapter_T1 adapter;
        TextView the_title;

        public ViewHolder(View itemView)
        {
            super(itemView);

            recycler_view = (ObservableRecyclerView) itemView.findViewById(R.id.recycler_view);
            ObservableRecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(itemView.getContext());
            recycler_view.setLayoutManager(mLayoutManager);
            the_title = (TextView) itemView.findViewById(R.id.the_title);
            ctx = itemView.getContext();
        }
    }

    public DashboardFrgLatest_NewRelease_Page_Adapter_T1(List<DashboardFrgLatest_NewRelease_Page_Model_T1> content){
        this.content = content;
    }

    @Override
    public DashboardFrgLatest_NewRelease_Page_Adapter_T1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dashboard_frg_latest_new_release_page_adapter_t1, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(DashboardFrgLatest_NewRelease_Page_Adapter_T1.ViewHolder holder, int position) {
        holder.adapter = new DashboardFrgLatest_NewRelease_Adapter_T1(content.get(holder.getAdapterPosition()).getData());
        holder.recycler_view.setAdapter(holder.adapter);
        holder.the_title.setText(content.get(holder.getAdapterPosition()).getTitle());
    }

    @Override
    public int getItemCount() {
        return content.size();
    }
}
