package com.kona.artmining.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.Model.SearchListItem;
import com.kona.artmining.Model.TermsCondition_T1;
import com.kona.artmining.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TermsConditionListingAdapter extends RecyclerView.Adapter<TermsConditionListingAdapter.ViewHolder> {
    private List<TermsCondition_T1> mDataset;
    private OnItemClickListener listener;
    private Context ctx;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView theConditionText;
        public ImageView item;
        public LinearLayout show_page_btt;

        public ViewHolder(View v) {
            super(v);
            theConditionText = (TextView) v.findViewById(R.id.theConditionText);
            item = (ImageView) v.findViewById(R.id.item);
            show_page_btt = (LinearLayout) v.findViewById(R.id.show_page_btt);

            ctx = v.getContext();
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TermsConditionListingAdapter(List<TermsCondition_T1> myDataset, OnItemClickListener listener) {
        mDataset = myDataset;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TermsConditionListingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                      int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.terms_condition_listing_adapter, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final TermsCondition_T1 i = mDataset.get(position);

        holder.theConditionText.setText(i.getSectionTitle());
        holder.item.setClickable(true);

        if(i.isChecked())
        {
            Picasso.with(ctx)
                    .load(R.drawable.btn_checkbox_sel)
                    .into(holder.item);
        }
        else
        {
            Picasso.with(ctx)
                    .load(R.drawable.btn_checkbox_nor)
                    .into(holder.item);
        }

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(i.isChecked())
                {
                    Picasso.with(ctx)
                            .load(R.drawable.btn_checkbox_nor)
                            .into(holder.item);
                    i.setChecked(false);
                }
                else
                {
                    Picasso.with(ctx)
                            .load(R.drawable.btn_checkbox_sel)
                            .into(holder.item);
                    i.setChecked(true);
                }
                listener.checkUncheck(i, holder.getAdapterPosition(), i.isChecked());
            }
        });

        holder.show_page_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(i);
            }
        });

        /*holder.textV.setText(mDataset.get(position).getText());
        holder.textEmail.setText(mDataset.get(position).getEmail());

        */
    }

    public interface OnItemClickListener {
        void onItemClick(TermsCondition_T1 tc);
        void checkUncheck(TermsCondition_T1 tc, int position, boolean ifChecked);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}