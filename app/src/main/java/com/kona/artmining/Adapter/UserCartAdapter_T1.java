package com.kona.artmining.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.Model.ArtScrapbookItemList_T1;
import com.kona.artmining.Model.CartItem_T1;
import com.kona.artmining.R;
import com.kona.artmining.custom_components.SquaredImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserCartAdapter_T1 extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private Context ctx;
    private OnItemClickListener listener;
    private List<CartItem_T1> TheList;

    public class TheViewHolder extends RecyclerView.ViewHolder
    {
        SquaredImageView itemImg;
        TextView itemName,artistName, artPrice;
        ImageView itemChecked, itemUnChecked, deleteItem;
        public LinearLayout item, itemSoldButton, itemOrder;

        public TheViewHolder(View view) {
            super(view);
            itemImg = (SquaredImageView) view.findViewById(R.id.itemImg);
            item = (LinearLayout) view.findViewById(R.id.item);

            itemName = (TextView) view.findViewById(R.id.itemName);
            artistName = (TextView) view.findViewById(R.id.artistName);
            artPrice = (TextView) view.findViewById(R.id.artPrice);

            itemSoldButton = (LinearLayout) view.findViewById(R.id.itemSoldButton);
            itemOrder = (LinearLayout) view.findViewById(R.id.itemOrder);
            deleteItem = (ImageView) view.findViewById(R.id.deleteItem);

            itemChecked = (ImageView) view.findViewById(R.id.itemChecked);
            itemUnChecked = (ImageView) view.findViewById(R.id.itemUnChecked);

            ctx = view.getContext();
        }
    }

    public UserCartAdapter_T1(List<CartItem_T1> ml, OnItemClickListener listener) {
        this.TheList = ml;
        this.listener=listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TheViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_cart_adapter_t1, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        addTheView((TheViewHolder) holder, position);
    }


    public void addTheView(final TheViewHolder holder, final int position) {

        final CartItem_T1 i = TheList.get(position);

        if(i.isSold_out())
        {
            holder.itemUnChecked.setVisibility(View.INVISIBLE);
            holder.itemSoldButton.setVisibility(View.VISIBLE);
            holder.itemOrder.setVisibility(View.INVISIBLE);
        }

        if(i.isChecked())
        {
            if(!i.isSold_out())
            {
                holder.itemChecked.setVisibility(View.VISIBLE);
                holder.itemUnChecked.setVisibility(View.INVISIBLE);
            }
        }else{
            if(!i.isSold_out())
            {
                holder.itemChecked.setVisibility(View.INVISIBLE);
                holder.itemUnChecked.setVisibility(View.VISIBLE);
            }
        }

        Picasso.with(ctx)
                .load(i.getMain_image_thumb())
                .placeholder(R.drawable.img_photo_01_list)
                .error(R.drawable.img_photo_01_list)
                .fit()
                .into(holder.itemImg);

        holder.item.setClickable(true);
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!i.isSold_out())
                {
                    if(holder.itemUnChecked.getVisibility() == View.VISIBLE)
                    {
                        // Checked
                        holder.itemChecked.setVisibility(View.VISIBLE);
                        holder.itemUnChecked.setVisibility(View.INVISIBLE);
                        listener.onItemClick(i, position, true);
                        i.setChecked(true);
                    }else{
                        // Un checked
                        holder.itemChecked.setVisibility(View.INVISIBLE);
                        holder.itemUnChecked.setVisibility(View.VISIBLE);
                        listener.onItemClick(i, position, false);
                        i.setChecked(false);
                    }
                }
            }
        });

        holder.deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.remove(i);
            }
        });

        holder.itemName.setText(i.getTitle_en());
        holder.artistName.setText(i.getArtistName_en()+" - "+i.getArtistName_kr());
        holder.artPrice.setText(i.getPrice()+" "+i.getCurrency());

        holder.itemOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!i.isSold_out())
                {
                    listener.doOrder(i);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return TheList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(CartItem_T1 item, int position, boolean isChecked);
        void doOrder(CartItem_T1 item);
        void remove(CartItem_T1 item);
    }
}
