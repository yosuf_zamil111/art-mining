package com.kona.artmining.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.Model.Order_T1;
import com.kona.artmining.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class OrderListItemAdapter_T1 extends RecyclerView.Adapter<OrderListItemAdapter_T1.ViewHolder>
{
    private Context ctx;
    private List<Order_T1> mDataset;
    private OnItemClickListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        public ImageView itemImg, ifAuctioned;
        public TextView _status, theTitle, artistName, theAmount, orderDate, orderNumber;
        LinearLayout order_item;

        public ViewHolder(View view) {
            super(view);
            itemImg = (ImageView) view.findViewById(R.id.itemImg);
            ifAuctioned = (ImageView) view.findViewById(R.id.ifAuctioned);

            _status = (TextView) view.findViewById(R.id._status);
            theTitle = (TextView) view.findViewById(R.id.theTitle);
            artistName = (TextView) view.findViewById(R.id.artistName);
            theAmount = (TextView) view.findViewById(R.id.theAmount);
            orderDate = (TextView) view.findViewById(R.id.orderDate);
            orderNumber = (TextView) view.findViewById(R.id.orderNumber);
            order_item = (LinearLayout) view.findViewById(R.id.order_item);
            ctx = view.getContext();
        }
    }

    public OrderListItemAdapter_T1(List<Order_T1> myDataset, OnItemClickListener listener) {
        mDataset = myDataset;
        this.listener = listener;
    }

    @Override
    public OrderListItemAdapter_T1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_list_ltem_adapter_t1, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position)
    {
        Order_T1 OT_1 = mDataset.get(position);

        Picasso.with(ctx)
                .load(OT_1.getFeaturedItem().getMain_image_thumb())
                .placeholder(R.drawable.deummy_data)
                .error(R.drawable.transparent)
                .fit()
                .into(holder.itemImg);

        holder._status.setText((OT_1.getStatus() == 1)?"In Transit":"In Preparation");
        holder.theTitle.setText(OT_1.getFeaturedItem().getTitle_en());
        holder.artistName.setText(OT_1.getFeaturedItem().getArtistName_en());
        holder.theAmount.setText(OT_1.getPay_total()+" "+OT_1.getFeaturedItem().getCurrency());
        holder.orderDate.setText(OT_1.getOrderDate());
        holder.orderNumber.setText("(Order Num : "+OT_1.getTransaction_id()+")");

        if(OT_1.getOrder_type().equals("2"))
        {
            holder.ifAuctioned.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.ifAuctioned.setVisibility(View.GONE);
        }

        holder.order_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(mDataset.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Order_T1 ot1);
    }
}
