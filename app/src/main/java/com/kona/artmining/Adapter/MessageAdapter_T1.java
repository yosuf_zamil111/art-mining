package com.kona.artmining.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.Model.Message;
import com.kona.artmining.R;

import java.util.List;

public class MessageAdapter_T1 extends RecyclerView.Adapter<MessageAdapter_T1.ViewHolder>
{
    private List<Message> mDataset;
    private OnItemClickListener listener;
    private Context ctx;

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        public TextView mailSubject, mailTime, mailSenderReceivedName, mailSummaryDetails;
        public LinearLayout item;
        public FrameLayout mailSelection;
        public ImageView mailUnChecked, mailChecked;

        public ViewHolder(View v) {
            super(v);

            mailSubject = (TextView) v.findViewById(R.id.mailSubject);
            mailTime = (TextView) v.findViewById(R.id.mailTime);
            mailSenderReceivedName = (TextView) v.findViewById(R.id.mailSenderReceivedName);
            mailSummaryDetails = (TextView) v.findViewById(R.id.mailSummaryDetails);

            item = (LinearLayout) v.findViewById(R.id.item);
            mailSelection = (FrameLayout) v.findViewById(R.id.mailSelection);

            mailUnChecked = (ImageView) v.findViewById(R.id.mailUnChecked);
            mailChecked = (ImageView) v.findViewById(R.id.mailChecked);

            ctx = v.getContext();
        }
    }

    public MessageAdapter_T1(List<Message> myDataset, OnItemClickListener listener) {
        mDataset = myDataset;
        this.listener = listener;
    }

    @Override
    public MessageAdapter_T1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.messaging_row_adapter, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final Message i = mDataset.get(holder.getAdapterPosition());

        holder.mailSenderReceivedName.setText(i.getName());
        holder.mailSubject.setText(i.getSubject());
        holder.mailTime.setText(i.getDate().split(" ")[1]+" "+i.getDate().split(" ")[0]);
        holder.mailSummaryDetails.setText(i.getMail_details());


        if(i.isChecked())
        {
            holder.mailChecked.setVisibility(View.VISIBLE);
            holder.mailUnChecked.setVisibility(View.GONE);
        }else{
            holder.mailChecked.setVisibility(View.GONE);
            holder.mailUnChecked.setVisibility(View.VISIBLE);
        }

        if(i.getUnread_info()==1)
        {
            holder.mailSenderReceivedName.setTypeface(null, Typeface.BOLD);
            holder.mailSubject.setTypeface(null, Typeface.BOLD);
        }
        else
        {
            holder.mailSenderReceivedName.setTypeface(null, Typeface.NORMAL);
            holder.mailSubject.setTypeface(null, Typeface.NORMAL);
        }

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                holder.mailSenderReceivedName.setTypeface(null, Typeface.NORMAL);
                holder.mailSubject.setTypeface(null, Typeface.NORMAL);
                listener.onItemClick(i);
            }
        });

        holder.mailSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(i.isChecked())
                {
                    i.setChecked(false);
                    holder.mailChecked.setVisibility(View.GONE);
                    holder.mailUnChecked.setVisibility(View.VISIBLE);
                    listener.onItemChecked(i, holder.getAdapterPosition(), false);
                }
                else
                {
                    i.setChecked(true);
                    holder.mailChecked.setVisibility(View.VISIBLE);
                    holder.mailUnChecked.setVisibility(View.GONE);
                    listener.onItemChecked(i, holder.getAdapterPosition(), true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Message item);
        void onItemChecked(Message item, int position, boolean ifChecked);
    }
}
