package com.kona.artmining.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kona.artmining.Constants.UserConstants;
import com.kona.artmining.Helper.Dashboard;
import com.kona.artmining.Model.ImageList_T1;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.R;
import com.kona.artmining.custom_components.Spinner_T3;
import com.kona.artmining.custom_components.Spinner_T4;
import com.kona.artmining.custom_components.SquaredImageView;
import com.kona.artmining.model.RadioButtonInfo;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

// dashboard gallery. auction and shop
public class ImageListingAdapter_T1 extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private OnItemClickListener listener;
    private OnSpinnerSelectListener spinnerListener;
    private java.util.List<ImageList_T1> TheList;

    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_HEADER2 = 1;
    private static final int VIEW_TYPE_ITEM = 2;

    private Context ctx;
    boolean addHeader;

    List<RadioButtonInfo> artistList = new ArrayList<RadioButtonInfo>();
    List<RadioButtonInfo> themeList = new ArrayList<RadioButtonInfo>();
    List<RadioButtonInfo> priceSortList = new ArrayList<RadioButtonInfo>();
    UserProfile currentUserProfile;


    public class TheViewHolder extends RecyclerView.ViewHolder
    {
        public SquaredImageView img;
        public ImageView ifAuctioned;
        public ImageView ifSoldOut,ifSoldOutAuction;
        public LinearLayout imgHover;

        public TheViewHolder(View view) {
            super(view);
            img = (SquaredImageView) view.findViewById(R.id.img);
            ifAuctioned = (ImageView) view.findViewById(R.id.ifAuctioned);
            ifSoldOut = (ImageView) view.findViewById(R.id.ifSoldOut);
            ifSoldOutAuction = (ImageView) view.findViewById(R.id.ifSoldOutAuction);
            imgHover = (LinearLayout) view.findViewById(R.id.imgHover);
            ctx = view.getContext();
        }
    }

    public class SpinnerViewHolder extends RecyclerView.ViewHolder
    {
        public Spinner_T3 ArtistFilter;
        public Spinner_T3 ThemeFilter;
        public Spinner_T4 FullFilter;
        public Spinner_T3 PriceFilter;
        LinearLayout uploadArtBttContainer, uploadArtBtt;

        public SpinnerViewHolder(View view) {
            super(view);
            ArtistFilter = (Spinner_T3) view.findViewById(R.id.ArtistFilter);
            ThemeFilter = (Spinner_T3) view.findViewById(R.id.ThemeFilter);
            FullFilter = (Spinner_T4) view.findViewById(R.id.FullFilter);
            PriceFilter = (Spinner_T3) view.findViewById(R.id.PriceFilter);
            uploadArtBttContainer  = (LinearLayout) view.findViewById(R.id.uploadArtBttContainer);
            uploadArtBtt  = (LinearLayout) view.findViewById(R.id.uploadArtBtt);

            ctx = view.getContext();
            PrepareStage();
        }

        void PrepareStage()
        {
            ArtistFilter.setText("Artist");
            ArtistFilter.SetSubtitleText("All");
            ArtistFilter.hideSoldOutCheckBox();
            ArtistFilter.setListener(new Spinner_T3.Success() {
                @Override
                public void done()
                {
                    spinnerListener.onItemClick(ArtistFilter.get_info().getID(), ThemeFilter.get_info().getID(), 0, PriceFilter.get_info().getID(), PriceFilter.ifSoldOutChecked());
                }

                @Override
                public void soldOutChecked(boolean ifChecked){}
            });

            ThemeFilter.setText("Theme");
            ThemeFilter.SetSubtitleText("All");
            ThemeFilter.hideSoldOutCheckBox();
            ThemeFilter.setListener(new Spinner_T3.Success()
            {
                @Override
                public void done()
                {
                    spinnerListener.onItemClick(ArtistFilter.get_info().getID(), ThemeFilter.get_info().getID(), 1, PriceFilter.get_info().getID(), PriceFilter.ifSoldOutChecked());
                }

                @Override
                public void soldOutChecked(boolean ifChecked) {}
            });

            PriceFilter.setText("Sort");
            PriceFilter.SetSubtitleText("All");
            /*String[] theList = new String[]{"All","Date oldest to newest", "Date newest to oldest", "Price lowest to highest", "Price highest to lowest","Polular post"};*/
            String[] theList = new String[]{"All","Old","New","Low Price","High Price","Popular"};
            for (int i = 0; i < theList.length; i++) {
                RadioButtonInfo io1=new RadioButtonInfo();
                io1.setID(i);
                io1.setText(theList[i]);
                priceSortList.add(io1);
            }
            PriceFilter.setOptions("Sort", priceSortList, "0");
            PriceFilter.setListener(new Spinner_T3.Success()
            {
                @Override
                public void done()
                {
                    spinnerListener.onItemClick(ArtistFilter.get_info().getID(), ThemeFilter.get_info().getID(), 1, PriceFilter.get_info().getID(), PriceFilter.ifSoldOutChecked());
                }

                @Override
                public void soldOutChecked(boolean ifChecked)
                {
                    spinnerListener.onItemClick(ArtistFilter.get_info().getID(), ThemeFilter.get_info().getID(), 1, PriceFilter.get_info().getID(), PriceFilter.ifSoldOutChecked());
                }
            });

            Dashboard.RetrieveThemeList(new Dashboard.RadioButtonInfo_Retrieved() {
                @Override
                public void Success(final List<RadioButtonInfo> message11) {
                    themeList.addAll(message11);
                    ThemeFilter.setOptions("Theme", themeList, "0");
                    Dashboard.RetrieveArtistList(new Dashboard.RadioButtonInfo_Retrieved() {
                        @Override
                        public void Success(List<RadioButtonInfo> message) {
                            artistList.addAll(message);
                            ArtistFilter.setOptions("Artist", artistList, "0");
                            FullFilter.setMultipleOptions(message,message11);
                        }

                        @Override
                        public void Error(String message) {

                        }
                    });
                }

                @Override
                public void Error(String message) {

                }
            });

            FullFilter.setText("Filter");
            FullFilter.EnableLevel2DialogueBox();
            FullFilter.SetSubtitleText("All");
            /*FullFilter.setListener(new Spinner_T4.Success() {
                @Override
                public void done() {
                    spinnerListener.onItemClick(FullFilter.get_info1().getID(), FullFilter.get_info2().getID(), 2);
                }
            });*/
            FullFilter.setThemeSelectedListener(new Spinner_T4.themeSelected() {
                @Override
                public void done(int id, String text) {
                    System.out.println(id+"<----Theme---->"+text);
                    //spinnerListener.onItemClick(FullFilter.get_info1().getID(), FullFilter.get_info2().getID(), 2);
                    FullFilter.SetSubtitleText("Applied");
                    //ThemeFilter.SetSubtitleText(FullFilter.get_info2().getText());
                    ThemeFilter.setOptions("Theme", themeList, FullFilter.get_info2().getID()+"");
                }
            });

            FullFilter.setArtistSelectedListener(new Spinner_T4.artistSelected() {
                @Override
                public void done(int id, String text) {
                    System.out.println(id+"<----Artist---->"+text);
                    //spinnerListener.onItemClick(FullFilter.get_info1().getID(), FullFilter.get_info2().getID(), 2);
                    FullFilter.SetSubtitleText("Applied");
                    //ArtistFilter.SetSubtitleText(FullFilter.get_info1().getText());
                    ArtistFilter.setOptions("Artist", artistList, (FullFilter.get_info1().getID()+""));
                }
            });

            FullFilter.setResetListener(new Spinner_T4.resetHitted() {
                @Override
                public void done() {
                    System.out.println("<----Just make a reset---->");
                    //spinnerListener.onItemClick(FullFilter.get_info1().getID(), FullFilter.get_info2().getID(), 2);
                    FullFilter.SetSubtitleText("All");
                    ArtistFilter.setOptions("Artist", artistList, (FullFilter.get_info2().getID()+""));
                    ThemeFilter.setOptions("Theme", themeList, FullFilter.get_info2().getID()+"");
                }
            });
        }
    }

    public class BlankViewHolder extends RecyclerView.ViewHolder
    {
        public BlankViewHolder(View view) {
            super(view);
            ctx = view.getContext();
        }
    }

    public ImageListingAdapter_T1(List<ImageList_T1> ml, boolean addHeader ,OnItemClickListener listener, OnSpinnerSelectListener spinnerListener) {
        this.TheList = ml;
        this.listener=listener;
        this.spinnerListener = spinnerListener;
        this.addHeader = addHeader;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == VIEW_TYPE_HEADER)
        {
            return new BlankViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.blank_view_1, parent, false));
        }
        else if(viewType == VIEW_TYPE_HEADER2)
        {
            return new SpinnerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.image_listing_adapter_t1_spinner, parent, false));
        }
        else
        {
            return new TheViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.image_listing_adapter_t1, parent, false));
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TheViewHolder) {
            addTheView((TheViewHolder) holder, position-2);
        }
        if (holder instanceof SpinnerViewHolder) {

            ImageListingAdapter_T1.SpinnerViewHolder holder2 = (ImageListingAdapter_T1.SpinnerViewHolder) holder;

            if (Prefs.contains(UserConstants.AccessToken)) {
                currentUserProfile = new Gson().fromJson(Prefs.getString(UserConstants.UserFullDetails, "{}"),  new TypeToken<UserProfile>(){}.getType());

                if(currentUserProfile.getType()!=1)
                {
                    holder2.uploadArtBttContainer.setVisibility(View.VISIBLE);
                }
                else
                {
                    holder2.uploadArtBttContainer.setVisibility(View.GONE);
                }

            }else{
                holder2.uploadArtBttContainer.setVisibility(View.GONE);
            }

            holder2.uploadArtBtt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    currentUserProfile = new Gson().fromJson(Prefs.getString(UserConstants.UserFullDetails, "{}"),  new TypeToken<UserProfile>(){}.getType());

                    System.out.println("Clicked >");
                    System.out.println("Clicked > "+currentUserProfile.getType());

                    if(currentUserProfile.getType()!=1)
                    {
                        spinnerListener.showArtUploader();
                    }
                    else {
                        //Toast.makeText(,"Token loop found.", Toast.LENGTH_LONG).show();
                        Toast.makeText(ctx, "Your are not allowed to upload here.", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    /*@Override
    public void onBindViewHolder(ImageListingAdapter_T1.TheViewHolder holder, int position) {

        if(addHeader)
        {
            if(position > 1)
            {
                addTheView(holder, position);
            }
        }else{
            addTheView(holder, position);
        }
    }*/

    void addTheView(final ImageListingAdapter_T1.TheViewHolder holder, int position)
    {
        final ImageList_T1 i = TheList.get(position);

        if(i.isClickable())
        {
            Picasso.with(ctx)
                    .load(i.getMain_image_thumb())
                    .placeholder(R.drawable.deummy_data)
                    .error(R.drawable.transparent)
                    .fit()
                    .into(holder.img);

            if(i.getArt_type() == 2)
            {
                holder.ifAuctioned.setVisibility(View.VISIBLE);
                holder.ifSoldOut.setVisibility(View.GONE);

                if(i.getSold_out() == null)
                {
                    holder.ifSoldOutAuction.setVisibility(View.GONE);
                }
                else
                {
                    holder.ifSoldOutAuction.setVisibility(View.VISIBLE);
                }

            }else{
                holder.ifAuctioned.setVisibility(View.GONE);
                holder.ifSoldOutAuction.setVisibility(View.GONE);

                if(i.getSold_out() == null)
                {
                    holder.ifSoldOut.setVisibility(View.GONE);
                }
                else
                {
                    holder.ifSoldOut.setVisibility(View.VISIBLE);
                }
            }





            holder.imgHover.setClickable(true);
            holder.imgHover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(i);
                }
            });

            /*
            holder.img.setOnTouchListener(new View.OnTouchListener() {

                private Rect rect;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_DOWN){
                        holder.img.setColorFilter(Color.argb(150, 0, 0, 0));
                        rect = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                    }
                    if(event.getAction() == MotionEvent.ACTION_UP){
                        holder.img.setColorFilter(Color.argb(0, 0, 0, 0));
                    }
                    if(event.getAction() == MotionEvent.ACTION_MOVE){
                        *//*if(!rect.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())){
                            holder.img.setColorFilter(Color.argb(0, 0, 0, 0));
                        }*//*
                        holder.img.setColorFilter(Color.argb(0, 0, 0, 0));
                    }
                    return false;
                }
            });
            */
        }
        else
        {
            Picasso.with(ctx)
                    .load(R.drawable.transparent)
                    .placeholder(R.drawable.transparent)
                    .error(R.drawable.transparent)
                    .fit()
                    .into(holder.img);

            if(i.getArt_type() == 2)
            {
                holder.ifAuctioned.setVisibility(View.VISIBLE);
            }else{
                holder.ifAuctioned.setVisibility(View.GONE);
            }

            holder.ifSoldOut.setVisibility(View.GONE);
            holder.ifSoldOutAuction.setVisibility(View.GONE);

            //ifSoldOutAuction
            //holder.ifSoldOutAuction.setVisibility(View.GONE);

            /*if(TextUtils.isEmpty(i.getSold_out()))
            {
                holder.ifSoldOut.setVisibility(View.GONE);
            }
            else
            {
                holder.ifSoldOut.setVisibility(View.VISIBLE);
            }*/

            holder.imgHover.setVisibility(View.GONE);
            holder.imgHover.setClickable(false);
            holder.imgHover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            /*
            holder.img.setOnTouchListener(null);
            */
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ImageList_T1 item);
    }

    public interface OnSpinnerSelectListener {
        void onItemClick(int ArtistId, int ThemeID, int type, int sortType, boolean ifSoldOutChecked);
        void showArtUploader();
    }

    @Override
    public int getItemViewType(int position) {
        //return (position == 0) ? VIEW_TYPE_HEADER : VIEW_TYPE_ITEM;
        if(position == 0)
        {
            return VIEW_TYPE_HEADER;
        }
        else if(position == 1)
        {
            return VIEW_TYPE_HEADER2;
        }
        else
        {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        if(addHeader)
        {
            return TheList.size()+2;
        }else{
            return TheList.size();
        }
    }
}

