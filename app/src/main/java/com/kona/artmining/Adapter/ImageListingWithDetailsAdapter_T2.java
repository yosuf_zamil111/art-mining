package com.kona.artmining.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.Helper.Dashboard;
import com.kona.artmining.Model.ImageList_T1;
import com.kona.artmining.R;
import com.kona.artmining.custom_components.Spinner_T3;
import com.kona.artmining.custom_components.Spinner_T4;
import com.kona.artmining.custom_components.SquaredImageView;
import com.kona.artmining.model.RadioButtonInfo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

// Dashboard Talk with
public class ImageListingWithDetailsAdapter_T2 extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private OnItemClickListener listener;
    private OnSpinnerSelectListener spinnerListener;
    private List<ImageList_T1> TheList;

    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_HEADER2 = 1;
    private static final int VIEW_TYPE_ITEM = 2;

    private Context ctx;
    boolean addHeader;

    List<RadioButtonInfo> artistList = new ArrayList<RadioButtonInfo>();
    List<RadioButtonInfo> themeList = new ArrayList<RadioButtonInfo>();


    public class TheViewHolder extends RecyclerView.ViewHolder
    {
        public SquaredImageView img;
        public ImageView bg, doMessage, artistImage;
        public LinearLayout bottomBar;
        public TextView name, email;

        public TheViewHolder(View view) {
            super(view);
            img = (SquaredImageView) view.findViewById(R.id.img);
            bg = (ImageView) view.findViewById(R.id.bg);
            doMessage = (ImageView) view.findViewById(R.id.doMessage);
            artistImage = (ImageView) view.findViewById(R.id.artistImage);

            name = (TextView) view.findViewById(R.id.name);
            email = (TextView) view.findViewById(R.id.email);
            bottomBar = (LinearLayout) view.findViewById(R.id.bottomBar);
            ctx = view.getContext();
        }
    }

    public class SpinnerViewHolder extends RecyclerView.ViewHolder
    {
        public Spinner_T3 ArtistFilter;
        public Spinner_T3 ThemeFilter;
        public Spinner_T4 FullFilter;
        LinearLayout uploadArtBttContainer;

        public SpinnerViewHolder(View view) {
            super(view);
            ArtistFilter = (Spinner_T3) view.findViewById(R.id.ArtistFilter);
            ThemeFilter = (Spinner_T3) view.findViewById(R.id.ThemeFilter);
            FullFilter = (Spinner_T4) view.findViewById(R.id.FullFilter);
            uploadArtBttContainer  = (LinearLayout) view.findViewById(R.id.uploadArtBttContainer);
            uploadArtBttContainer.setVisibility(View.GONE);
            ctx = view.getContext();
            
            view.findViewById(R.id.PriceFilter).setVisibility(View.GONE);

            PrepareStage();
        }

        void PrepareStage()
        {
            ArtistFilter.setText("Artist");
            ArtistFilter.SetSubtitleText("All");
            ArtistFilter.hideSoldOutCheckBox();
            ArtistFilter.setListener(new Spinner_T3.Success() {
                @Override
                public void done() {
                    spinnerListener.onItemClick(ArtistFilter.get_info().getID(), ThemeFilter.get_info().getID(), 0);
                }

                @Override
                public void soldOutChecked(boolean ifChecked) {

                }
            });
            ArtistFilter.setVisibility(View.GONE);

            ThemeFilter.setText("Theme");
            ThemeFilter.SetSubtitleText("All");
            ThemeFilter.hideSoldOutCheckBox();
            ThemeFilter.setListener(new Spinner_T3.Success() {
                @Override
                public void done() {
                    spinnerListener.onItemClick(ArtistFilter.get_info().getID(), ThemeFilter.get_info().getID(), 1);
                }

                @Override
                public void soldOutChecked(boolean ifChecked) {

                }
            });

            Dashboard.RetrieveThemeList(new Dashboard.RadioButtonInfo_Retrieved() {
                @Override
                public void Success(final List<RadioButtonInfo> message11) {
                    themeList.addAll(message11);
                    ThemeFilter.setOptions("Theme", themeList, "0");
                    Dashboard.RetrieveArtistList(new Dashboard.RadioButtonInfo_Retrieved() {
                        @Override
                        public void Success(List<RadioButtonInfo> message) {
                            artistList.addAll(message);
                            ArtistFilter.setOptions("Artist", artistList, "0");
                            FullFilter.setMultipleOptions(message,message11);
                        }

                        @Override
                        public void Error(String message) {

                        }
                    });
                }

                @Override
                public void Error(String message) {

                }
            });

            FullFilter.setText("Filter");
            FullFilter.HideTheme();
            FullFilter.EnableLevel2DialogueBox();
            FullFilter.SetSubtitleText("All");
            /*FullFilter.setListener(new Spinner_T4.Success() {
                @Override
                public void done() {
                    spinnerListener.onItemClick(FullFilter.get_info1().getID(), FullFilter.get_info2().getID(), 2);
                }
            });*/
            FullFilter.setThemeSelectedListener(new Spinner_T4.themeSelected() {
                @Override
                public void done(int id, String text) {
                    System.out.println(id+"<----Theme---->"+text);
                    //spinnerListener.onItemClick(FullFilter.get_info1().getID(), FullFilter.get_info2().getID(), 2);
                    FullFilter.SetSubtitleText("Filtered");
                    //ThemeFilter.SetSubtitleText(FullFilter.get_info2().getText());
                    ThemeFilter.setOptions("Theme", themeList, FullFilter.get_info2().getID()+"");
                }
            });

            FullFilter.setArtistSelectedListener(new Spinner_T4.artistSelected() {
                @Override
                public void done(int id, String text) {
                    System.out.println(id+"<----Artist---->"+text);
                    //spinnerListener.onItemClick(FullFilter.get_info1().getID(), FullFilter.get_info2().getID(), 2);
                    FullFilter.SetSubtitleText("Filtered");
                    //ArtistFilter.SetSubtitleText(FullFilter.get_info1().getText());
                    ArtistFilter.setOptions("Artist", artistList, (FullFilter.get_info1().getID()+""));
                }
            });

            FullFilter.setResetListener(new Spinner_T4.resetHitted() {
                @Override
                public void done() {
                    System.out.println("<----Just make a reset---->");
                    //spinnerListener.onItemClick(FullFilter.get_info1().getID(), FullFilter.get_info2().getID(), 2);
                    FullFilter.SetSubtitleText("All");
                    ArtistFilter.setOptions("Artist", artistList, (FullFilter.get_info2().getID()+""));
                    ThemeFilter.setOptions("Theme", themeList, FullFilter.get_info2().getID()+"");
                }
            });
        }
    }

    public class BlankViewHolder extends RecyclerView.ViewHolder
    {
        public BlankViewHolder(View view) {
            super(view);
            ctx = view.getContext();
        }
    }

    public ImageListingWithDetailsAdapter_T2(List<ImageList_T1> ml, boolean addHeader , OnItemClickListener listener, OnSpinnerSelectListener spinnerListener) {
        this.TheList = ml;
        this.listener=listener;
        this.spinnerListener = spinnerListener;
        this.addHeader = addHeader;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == VIEW_TYPE_HEADER)
        {
            return new BlankViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.blank_view_1, parent, false));
        }
        else if(viewType == VIEW_TYPE_HEADER2)
        {
            return new SpinnerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.image_listing_adapter_t1_spinner, parent, false));
        }
        else
        {
            return new TheViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.image_listing_adapter_t2, parent, false));
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TheViewHolder) {
            addTheView((TheViewHolder) holder, position-2);
        }
    }

    /*@Override
    public void onBindViewHolder(ImageListingAdapter_T1.TheViewHolder holder, int position) {

        if(addHeader)
        {
            if(position > 1)
            {
                addTheView(holder, position);
            }
        }else{
            addTheView(holder, position);
        }
    }*/

    void addTheView(final ImageListingWithDetailsAdapter_T2.TheViewHolder holder, int position)
    {
        final ImageList_T1 i = TheList.get(position);

        if(i.isClickable()) {
            if(!TextUtils.isEmpty(i.getMain_image()))
            {
                Picasso.with(ctx)
                        .load(i.getMain_image())
                        .placeholder(R.drawable.deummy_data)
                        .error(R.drawable.transparent)
                        .into(holder.bg);
            }
            else
            {
                Picasso.with(ctx)
                        .load(R.drawable.deummy_data)
                        .placeholder(R.drawable.deummy_data)
                        .error(R.drawable.transparent)
                        .into(holder.bg);
            }


            if(TextUtils.isEmpty(i.getProfile_image_thumb()))
            {
                Picasso.with(ctx)
                        .load(R.drawable.deummy_data)
                        .placeholder(R.drawable.icon_profile_large)
                        .error(R.drawable.icon_profile_large)
                        .fit()
                        .into(holder.artistImage);
            }
            else
            {
                Picasso.with(ctx)
                        .load(i.getProfile_image_thumb())
                        .placeholder(R.drawable.icon_profile_large)
                        .error(R.drawable.icon_profile_large)
                        .fit()
                        .into(holder.artistImage);
            }

            holder.name.setText(i.getTitle_en());
            holder.email.setText(i.getEmail());

            holder.bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(i);
                }
            });

            holder.bottomBar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(i);
                }
            });

            holder.doMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.sendMessage(i);
                }
            });

        }else {
            Picasso.with(ctx)
                    .load(R.drawable.transparent)
                    .placeholder(R.drawable.deummy_data)
                    .error(R.drawable.transparent)
                    .fit()
                    .into(holder.bg);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ImageList_T1 item);
        void sendMessage(ImageList_T1 item);
    }

    public interface OnSpinnerSelectListener {
        void onItemClick(int ArtistId, int ThemeID, int type);
    }

    @Override
    public int getItemViewType(int position) {
        //return (position == 0) ? VIEW_TYPE_HEADER : VIEW_TYPE_ITEM;
        if(position == 0)
        {
            return VIEW_TYPE_HEADER;
        }
        else if(position == 1)
        {
            return VIEW_TYPE_HEADER2;
        }
        else
        {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        if(addHeader)
        {
            return TheList.size()+2;
        }else{
            return TheList.size();
        }
    }
}

