package com.kona.artmining.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kona.artmining.Model.ArtScrapbookItemList_T1;
import com.kona.artmining.Model.UserProfile;
import com.kona.artmining.R;
import com.kona.artmining.custom_components.SquaredImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserArtScrapbookAdapter_T1 extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private Context ctx;
    private OnItemClickListener listener;
    private java.util.List<ArtScrapbookItemList_T1> TheList;

    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_ITEM = 1;

    UserProfile currentUserProfile;

    public class TheViewHolder extends RecyclerView.ViewHolder
    {
        SquaredImageView img;
        ImageView ifSelected;
        public LinearLayout imgHover;

        public TheViewHolder(View view) {
            super(view);
            img = (SquaredImageView) view.findViewById(R.id.img);
            imgHover = (LinearLayout) view.findViewById(R.id.imgHover);
            ifSelected = (ImageView) view.findViewById(R.id.ifSelected);
            ctx = view.getContext();
        }
    }

    public class BlankViewHolder extends RecyclerView.ViewHolder
    {
        public BlankViewHolder(View view) {
            super(view);
            ctx = view.getContext();
        }
    }

    public void SetCurrentUserProfile(UserProfile cup)
    {
        currentUserProfile = cup;
    }

    public UserArtScrapbookAdapter_T1(List<ArtScrapbookItemList_T1> ml, OnItemClickListener listener) {
        this.TheList = ml;
        this.listener=listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == VIEW_TYPE_HEADER)
        {
            if(currentUserProfile.getType() == 1)
            {
                return new BlankViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.blank_view_2, parent, false));
            }
            else
            {
                return new BlankViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.blank_view_3, parent, false));
            }
        }
        else
        {
            return new TheViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_art_scrapbook_adapter_t1, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TheViewHolder) {
            addTheView((TheViewHolder) holder, position);
        }
    }


    public void addTheView(final TheViewHolder holder, int position) {

        final ArtScrapbookItemList_T1 i = TheList.get(holder.getAdapterPosition());

        if(i.isClickable())
        {
            Picasso.with(ctx)
                    .load(i.getMain_image_thumb())
                    .placeholder(R.drawable.transparent)
                    .error(R.drawable.transparent)
                    .fit()
                    .into(holder.img);

            if(!i.isSelected())
            {
                holder.ifSelected.setImageResource(R.drawable.btn_checkbox_nor);
                holder.imgHover.setBackgroundResource(R.drawable.image_listing_adapter_t1_imgvw_bg);
            }
            else
            {
                holder.ifSelected.setImageResource(R.drawable.btn_checkbox02_sel);
                holder.imgHover.setBackgroundResource(R.drawable.image_listing_adapter_t1_imgvw_bg_50_opacity);
            }

            holder.ifSelected.setClickable(true);
            holder.ifSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(i.isSelected())
                    {
                        TheList.get(holder.getAdapterPosition()).setSelected(false);
                        holder.ifSelected.setImageResource(R.drawable.btn_checkbox_nor);
                        holder.imgHover.setBackgroundResource(R.drawable.image_listing_adapter_t1_imgvw_bg);
                    }
                    else
                    {
                        TheList.get(holder.getAdapterPosition()).setSelected(true);
                        holder.ifSelected.setImageResource(R.drawable.btn_checkbox02_sel);
                        holder.imgHover.setBackgroundResource(R.drawable.image_listing_adapter_t1_imgvw_bg_50_opacity);
                    }

                    listener.onItemClick(i, holder.getAdapterPosition(), i.isSelected());
                }
            });

            holder.imgHover.setClickable(true);
            holder.imgHover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.loadItemDetails(i);
                }
            });

            //ifSelected
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0) ? VIEW_TYPE_HEADER : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return TheList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(ArtScrapbookItemList_T1 item, int position, boolean isSelected);
        void loadItemDetails(ArtScrapbookItemList_T1 item);
    }
}
