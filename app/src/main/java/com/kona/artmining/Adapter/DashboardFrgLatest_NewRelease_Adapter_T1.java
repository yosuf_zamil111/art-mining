package com.kona.artmining.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kona.artmining.Model.DashboardFrgLatest_NewRelease_Model_T1;
import com.kona.artmining.R;

import java.util.List;

public class DashboardFrgLatest_NewRelease_Adapter_T1 extends RecyclerView.Adapter<DashboardFrgLatest_NewRelease_Adapter_T1.ViewHolder> {

    private List<DashboardFrgLatest_NewRelease_Model_T1> singleString;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private Context ctx;
        public TextView artNewsName,artNewsDate;

        public ViewHolder(View itemView) {
            super(itemView);

            artNewsName = (TextView) itemView.findViewById(R.id.artNewsName);
            artNewsDate = (TextView) itemView.findViewById(R.id.artNewsDate);

            ctx = itemView.getContext();

        }
    }
    public DashboardFrgLatest_NewRelease_Adapter_T1(List<DashboardFrgLatest_NewRelease_Model_T1> content){
        this.singleString = content;
        // this.ll=ll;
    }


    @Override
    public DashboardFrgLatest_NewRelease_Adapter_T1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.new_release_adapter_layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(DashboardFrgLatest_NewRelease_Adapter_T1.ViewHolder holder, int position) {

        //holder.artNewsName.setText(singleString.get(position).getArtNewsName());
        //holder.artNewsDate.setText(singleString.get(position).getArtNewsDate());
    }

    @Override
    public int getItemCount() {
        return singleString.size();
    }
}
