package com.kona.artmining.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kona.artmining.Model.DashboardFrgLatest_Reviews_Model_T1;
import com.kona.artmining.R;
import com.makeramen.roundedimageview.Corner;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

public class DashboardFrgLatest_Reviews_Adapter_T1 extends RecyclerView.Adapter<DashboardFrgLatest_Reviews_Adapter_T1.ViewHolder> {

    private Context ctx;
    private List<DashboardFrgLatest_Reviews_Model_T1> singleString;
    OnItemClickListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView reviews_img;
        public TextView reviews_title,reviews_date,reviews_details,reviews_like;

        public ViewHolder(View itemView) {
            super(itemView);

            reviews_img = (ImageView) itemView.findViewById(R.id.reviews_img);
            reviews_title = (TextView) itemView.findViewById(R.id.reviews_txt);
            reviews_date = (TextView) itemView.findViewById(R.id.reviews_date);
            reviews_details = (TextView) itemView.findViewById(R.id.reviews_txt1);
            reviews_like = (TextView) itemView.findViewById(R.id.reviews_like);

            ctx = itemView.getContext();
        }
    }
    public DashboardFrgLatest_Reviews_Adapter_T1(List<DashboardFrgLatest_Reviews_Model_T1> content, OnItemClickListener _listener){
        this.singleString = content;
        this.listener = _listener;
    }




    @Override
    public DashboardFrgLatest_Reviews_Adapter_T1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ui_dashboard_frgmnts_latest_frg_review_adapter, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final DashboardFrgLatest_Reviews_Adapter_T1.ViewHolder holder, int position) {

        /*Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0)
                .cornerRadiusDp(Corner.TOP_LEFT, 7)
                .cornerRadiusDp(Corner.TOP_RIGHT, 7)
                .oval(false)
                .build();*/
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0)
                .oval(false)
                .build();

        Picasso.with(ctx)
                .load(singleString.get(position).getImage())
                .placeholder(R.drawable.deummy_data)
                .error(R.drawable.transparent)
                .transform(transformation)
                .into(holder.reviews_img);

        holder.reviews_title.setText(singleString.get(position).getReview_creator_name_en());
        holder.reviews_date.setText(singleString.get(position).getDate().split(" ")[0]);
        holder.reviews_details.setText(singleString.get(position).getDetails_info());
        holder.reviews_like.setText(singleString.get(position).getLike_count());

        holder.reviews_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(singleString.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return singleString.size();
    }

    public interface OnItemClickListener {
        void onItemClick(DashboardFrgLatest_Reviews_Model_T1 item);
    }
}
