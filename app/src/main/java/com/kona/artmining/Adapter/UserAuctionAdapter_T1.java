package com.kona.artmining.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.Model.AuctionItem_T1;
import com.kona.artmining.Model.CartItem_T1;
import com.kona.artmining.R;
import com.kona.artmining.custom_components.SquaredImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserAuctionAdapter_T1 extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private Context ctx;
    private OnItemClickListener listener;
    private List<AuctionItem_T1> TheList;

    public class TheViewHolder extends RecyclerView.ViewHolder
    {
        SquaredImageView itemImg;
        TextView itemName,artistName, artPrice, time;
        ImageView itemChecked, itemUnChecked, itemLose, itemWon;
        public LinearLayout item, itemSoldButton, itemRunningButton, itemOrder, deleteItem, itemPurchased;
        FrameLayout checkButtonHolder;

        public TheViewHolder(View view) {
            super(view);
            itemImg = (SquaredImageView) view.findViewById(R.id.itemImg);
            item = (LinearLayout) view.findViewById(R.id.item);

            itemName = (TextView) view.findViewById(R.id.itemName);
            artistName = (TextView) view.findViewById(R.id.artistName);
            artPrice = (TextView) view.findViewById(R.id.artPrice);
            time = (TextView) view.findViewById(R.id.time);

            itemSoldButton = (LinearLayout) view.findViewById(R.id.itemSoldButton);
            itemRunningButton = (LinearLayout) view.findViewById(R.id.itemRunningButton);
            itemOrder = (LinearLayout) view.findViewById(R.id.itemOrder);
            deleteItem = (LinearLayout) view.findViewById(R.id.deleteItem);
            itemPurchased = (LinearLayout) view.findViewById(R.id.itemPurchased);

            itemChecked = (ImageView) view.findViewById(R.id.itemChecked);
            itemUnChecked = (ImageView) view.findViewById(R.id.itemUnChecked);

            itemLose = (ImageView) view.findViewById(R.id.itemLose);
            itemWon = (ImageView) view.findViewById(R.id.itemWon);

            checkButtonHolder = (FrameLayout) view.findViewById(R.id.checkButtonHolder);

            ctx = view.getContext();
        }
    }

    public UserAuctionAdapter_T1(List<AuctionItem_T1> ml, OnItemClickListener listener) {
        this.TheList = ml;
        this.listener=listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TheViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_auction_list_adapter_t1, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        addTheView((TheViewHolder) holder, position);
    }


    public void addTheView(final TheViewHolder holder, final int position) {

        final AuctionItem_T1 i = TheList.get(position);

        /*if(i.isSold_out())
        {
            holder.itemUnChecked.setVisibility(View.INVISIBLE);
        }

        */

        if(i.isChecked())
        {
            System.out.println("me checked :"+ position);
            if(i.isSold_out() && i.isWinner())
            {
                System.out.println("me checked final :"+ position);
                holder.itemChecked.setVisibility(View.VISIBLE);
                holder.itemUnChecked.setVisibility(View.INVISIBLE);
            }
        }else{
            if(i.isSold_out() && i.isWinner())
            {
                holder.itemChecked.setVisibility(View.INVISIBLE);
                holder.itemUnChecked.setVisibility(View.VISIBLE);
            }
        }

        Picasso.with(ctx)
                .load(i.getMain_image_thumb())
                .placeholder(R.drawable.img_photo_01_list)
                .error(R.drawable.img_photo_01_list)
                .fit()
                .into(holder.itemImg);

        holder.checkButtonHolder.setClickable(true);
        holder.checkButtonHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i.isSold_out() && i.isWinner() && !i.isPurchased())
                {
                    if(holder.itemUnChecked.getVisibility() == View.VISIBLE)
                    {
                        // Checked
                        holder.itemChecked.setVisibility(View.VISIBLE);
                        holder.itemUnChecked.setVisibility(View.INVISIBLE);
                        listener.onItemClick(i, position, true);
                        i.setChecked(true);
                    }else{
                        // Un checked
                        holder.itemChecked.setVisibility(View.INVISIBLE);
                        holder.itemUnChecked.setVisibility(View.VISIBLE);
                        listener.onItemClick(i, position, false);
                        i.setChecked(false);
                    }
                }
            }
        });

        holder.item.setClickable(true);
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.ShowDetails(i);
            }
        });

        holder.deleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //listener.remove(i);
            }
        });

        holder.itemName.setText(i.getTitle_en());
        holder.artistName.setText(i.getArtistName_en()+" - "+i.getArtistName_kr());
        holder.artPrice.setText(i.getPrice()+" "+i.getCurrency());
        holder.time.setText(i.getTime());

        if(i.isSold_out() && i.isWinner())
        {
            // User Win.
            holder.itemWon.setVisibility(View.VISIBLE);
            holder.itemLose.setVisibility(View.GONE);

            holder.itemSoldButton.setVisibility(View.INVISIBLE);
            holder.itemOrder.setVisibility(View.VISIBLE);
            holder.itemRunningButton.setVisibility(View.INVISIBLE);
            holder.itemPurchased.setVisibility(View.INVISIBLE);

            if(i.isPurchased())
            {
                holder.itemOrder.setVisibility(View.INVISIBLE);
                holder.itemPurchased.setVisibility(View.VISIBLE);

                holder.itemChecked.setVisibility(View.INVISIBLE);
                holder.itemUnChecked.setVisibility(View.INVISIBLE);
            }

        }else if(i.isSold_out() && !i.isWinner())
        {
            // User Lose.
            holder.itemWon.setVisibility(View.GONE);
            holder.itemLose.setVisibility(View.VISIBLE);

            holder.itemSoldButton.setVisibility(View.VISIBLE);
            holder.itemOrder.setVisibility(View.INVISIBLE);
            holder.itemRunningButton.setVisibility(View.INVISIBLE);
            holder.itemPurchased.setVisibility(View.INVISIBLE);


            holder.itemUnChecked.setVisibility(View.INVISIBLE);
        }else{
            holder.itemWon.setVisibility(View.GONE);
            holder.itemLose.setVisibility(View.GONE);

            holder.itemSoldButton.setVisibility(View.INVISIBLE);
            holder.itemRunningButton.setVisibility(View.VISIBLE);
            holder.itemPurchased.setVisibility(View.INVISIBLE);
            holder.itemOrder.setVisibility(View.INVISIBLE);

            holder.itemUnChecked.setVisibility(View.INVISIBLE);
        }

        holder.itemOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(i.isSold_out() && i.isWinner())
                {
                    if(!i.isPurchased()){
                        listener.doOrder(i);
                    }

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return TheList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(AuctionItem_T1 item, int position, boolean isChecked);
        void doOrder(AuctionItem_T1 item);
        void remove(AuctionItem_T1 item);
        void ShowDetails(AuctionItem_T1 item);
    }
}
