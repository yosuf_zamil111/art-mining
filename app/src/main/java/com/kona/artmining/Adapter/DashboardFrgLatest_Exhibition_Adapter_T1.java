package com.kona.artmining.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kona.artmining.Model.DashboardFrgLatest_Exhibition_Model_T1;
import com.kona.artmining.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DashboardFrgLatest_Exhibition_Adapter_T1 extends RecyclerView.Adapter<DashboardFrgLatest_Exhibition_Adapter_T1.ViewHolder>
{

    private List<DashboardFrgLatest_Exhibition_Model_T1> singleString;
    private Context ctx;
    OnItemClickListener listener;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView exhibitionImg1;

        public ViewHolder(View itemView) {
            super(itemView);

            exhibitionImg1 = (ImageView) itemView.findViewById(R.id.exhibitionImg1);

            ctx = itemView.getContext();
        }
    }
    public DashboardFrgLatest_Exhibition_Adapter_T1(List<DashboardFrgLatest_Exhibition_Model_T1> content, OnItemClickListener _listener){
        this.singleString = content;
        this.listener=_listener;
    }


    @Override
    public DashboardFrgLatest_Exhibition_Adapter_T1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ui_dashboard_frgmnts_latest_frg_exhibitions_adapter, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final DashboardFrgLatest_Exhibition_Adapter_T1.ViewHolder holder, int position) {
        Picasso.with(ctx)
                .load(singleString.get(position).getExb_image_thump())
                .placeholder(R.drawable.deummy_data)
                .error(R.drawable.transparent)
                .fit()
                .into(holder.exhibitionImg1);

        holder.exhibitionImg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(singleString.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return singleString.size();
    }

    public interface OnItemClickListener {
        void onItemClick(DashboardFrgLatest_Exhibition_Model_T1 item);
    }
}
