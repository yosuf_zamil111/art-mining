package com.kona.artmining.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.Model.ImageList_T1;
import com.kona.artmining.Model.SearchListItem;
import com.kona.artmining.R;
import com.squareup.picasso.Picasso;

import java.util.List;
// dashboard home page top search
public class DashboardSearchListingAdapter extends RecyclerView.Adapter<DashboardSearchListingAdapter.ViewHolder> {
    private List<SearchListItem> mDataset;
    private OnItemClickListener listener;
    private Context ctx;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textV, textEmail;
        public LinearLayout item;
        public ImageView profilePicture, portfolioPicture_1, portfolioPicture_2, portfolioPicture_3, portfolioPicture_4;

        public ViewHolder(View v) {
            super(v);
            textV = (TextView) v.findViewById(R.id.textV);
            textEmail = (TextView) v.findViewById(R.id.textEmail);
            item = (LinearLayout) v.findViewById(R.id.item);
            profilePicture = (ImageView) v.findViewById(R.id.profilePicture);

            portfolioPicture_1 = (ImageView) v.findViewById(R.id.portfolioPicture_1);
            portfolioPicture_2 = (ImageView) v.findViewById(R.id.portfolioPicture_2);
            portfolioPicture_3 = (ImageView) v.findViewById(R.id.portfolioPicture_3);
            portfolioPicture_4 = (ImageView) v.findViewById(R.id.portfolioPicture_4);

            ctx = v.getContext();
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public DashboardSearchListingAdapter(List<SearchListItem> myDataset, OnItemClickListener listener) {
        mDataset = myDataset;
        this.listener = listener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DashboardSearchListingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dashboard_search_listing_adapter, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        final SearchListItem i = mDataset.get(position);

        holder.textV.setText(mDataset.get(position).getText());
        holder.textEmail.setText(mDataset.get(position).getEmail());

        if(mDataset.get(position).getProfileImg() != null && !TextUtils.isEmpty(mDataset.get(position).getProfileImg()))
        {
            Picasso.with(ctx)
                    .load(mDataset.get(position).getProfileImg())
                    .placeholder(R.drawable.icon_profile_list)
                    .error(R.drawable.icon_profile_list)
                    .fit()
                    .into(holder.profilePicture);
        }else{
            Picasso.with(ctx)
                    .load(R.drawable.icon_profile_list)
                    .placeholder(R.drawable.icon_profile_list)
                    .error(R.drawable.icon_profile_list)
                    .fit()
                    .into(holder.profilePicture);
        }

        if(mDataset.get(position).getPortfolioLinks().size()>0)
        {
            Picasso.with(ctx)
                    .load(mDataset.get(position).getPortfolioLinks().get(0))
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .fit()
                    .into(holder.portfolioPicture_1);
        }

        if(mDataset.get(position).getPortfolioLinks().size()>1)
        {
            Picasso.with(ctx)
                    .load(mDataset.get(position).getPortfolioLinks().get(1))
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .fit()
                    .into(holder.portfolioPicture_2);
        }

        if(mDataset.get(position).getPortfolioLinks().size()>2)
        {
            Picasso.with(ctx)
                    .load(mDataset.get(position).getPortfolioLinks().get(2))
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .fit()
                    .into(holder.portfolioPicture_3);
        }

        if(mDataset.get(position).getPortfolioLinks().size()>3)
        {
            Picasso.with(ctx)
                    .load(mDataset.get(position).getPortfolioLinks().get(3))
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .fit()
                    .into(holder.portfolioPicture_4);
        }

        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(i.getID());
                System.out.println(i.getText());
                listener.onItemClick(i);
            }
        });
    }

    public interface OnItemClickListener {
        void onItemClick(SearchListItem item);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}