package com.kona.artmining.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.Model.Order_T1;
import com.kona.artmining.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserFrgSellerOrderListAdapter_T1 extends RecyclerView.Adapter<UserFrgSellerOrderListAdapter_T1.TheViewHolder>{

    private Context ctx;
    private OnItemClickListener listener;
    private List<Order_T1> TheList;

    public class TheViewHolder extends RecyclerView.ViewHolder
    {
        ImageView left_image;
        TextView buyer_date_time,buyer_order_number,buyer_status,buyer_title_name,buyer_full_name,buyer_phone_number,buyer_address;
        LinearLayout processOrderBtt, shipOrderBtt, confirmOrderBtt, completeOrderBtt, refundOrderBtt, cancelOrderBtt, requestRefundBtt, withdrawPurchaseBtt,order_list_item;
        public TheViewHolder(View view) {
            super(view);

            left_image = (ImageView) view.findViewById(R.id.left_image);
            buyer_date_time = (TextView) view.findViewById(R.id.buyer_date_time);
            buyer_order_number = (TextView) view.findViewById(R.id.buyer_order_number);
            buyer_status = (TextView) view.findViewById(R.id.buyer_status);
            buyer_title_name = (TextView) view.findViewById(R.id.buyer_title_name);
            buyer_full_name = (TextView) view.findViewById(R.id.buyer_full_name);
            buyer_phone_number = (TextView) view.findViewById(R.id.buyer_phone_number);
            buyer_address = (TextView) view.findViewById(R.id.buyer_address);

            processOrderBtt = (LinearLayout) view.findViewById(R.id.processOrderBtt);
            shipOrderBtt = (LinearLayout) view.findViewById(R.id.shipOrderBtt);
            confirmOrderBtt = (LinearLayout) view.findViewById(R.id.confirmOrderBtt);
            completeOrderBtt = (LinearLayout) view.findViewById(R.id.completeOrderBtt);
            refundOrderBtt = (LinearLayout) view.findViewById(R.id.refundOrderBtt);
            cancelOrderBtt = (LinearLayout) view.findViewById(R.id.cancelOrderBtt);
            requestRefundBtt = (LinearLayout) view.findViewById(R.id.requestRefundBtt);
            withdrawPurchaseBtt = (LinearLayout) view.findViewById(R.id.withdrawPurchaseBtt);
            order_list_item = (LinearLayout) view.findViewById(R.id.order_list_item);
        }
    }

    public UserFrgSellerOrderListAdapter_T1(List<Order_T1> ml, OnItemClickListener listener) {
        this.TheList = ml;
        this.listener=listener;
    }

    @Override
    public TheViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //return new TheViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_cart_adapter_t1, parent, false));
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_frg_seller_order_list_adapter, parent, false);

        TheViewHolder vh = new TheViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(TheViewHolder holder, int position) {
        addTheView(holder, position);
    }

    /*
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        addTheView((TheViewHolder) holder, position);
    }
    */

    public void addTheView(final TheViewHolder holder, final int position)
    {
        final Order_T1 i = TheList.get(position);

        if(!TextUtils.isEmpty(i.getMain_image_thumb()))
        {
            Picasso.with(ctx)
                    .load(i.getMain_image_thumb())
                    .placeholder(R.drawable.img_photo_01_list)
                    .error(R.drawable.img_photo_01_list)
                    .fit()
                    .into(holder.left_image);
        }

        holder.buyer_date_time.setText(i.getOrderDate());
        holder.buyer_order_number.setText("(Order Num : "+i.getOrder_id()+")");
        holder.buyer_status.setText(GetStatus(i.getStatus()));
        holder.buyer_title_name.setText(i.getTitle_en());
        holder.buyer_full_name.setText(i.getShippingDetails().getFullName());
        holder.buyer_phone_number.setText(i.getShippingDetails().getExtension()+i.getShippingDetails().getMobile_or_phone());
        holder.buyer_address.setText(i.getShippingDetails().getAddressLine1()+"\n"+i.getShippingDetails().getAddressLine2());
        /*
        holder.buyer_full_name.setText(i.getBuyer_name_en());
        holder.buyer_phone_number.setText(i.getBuyer_phone());
        holder.buyer_address.setText(i.getShippingDetails().getAddressLine1()+"\n"+i.getShippingDetails().getAddressLine2());
        */

        holder.processOrderBtt.setVisibility(View.GONE);
        holder.shipOrderBtt.setVisibility(View.GONE);
        holder.confirmOrderBtt.setVisibility(View.GONE);
        holder.completeOrderBtt.setVisibility(View.GONE);
        holder.refundOrderBtt.setVisibility(View.GONE);
        holder.cancelOrderBtt.setVisibility(View.GONE);
        holder.requestRefundBtt.setVisibility(View.GONE);
        holder.withdrawPurchaseBtt.setVisibility(View.GONE);

        holder.processOrderBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.updateStatus(i, 2);
            }
        });
        holder.shipOrderBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.updateStatus(i, 3);
            }
        });
        holder.confirmOrderBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        holder.completeOrderBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        holder.refundOrderBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.updateStatus(i, 10);
            }
        });
        holder.cancelOrderBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.updateStatus(i, 7);
            }
        });
        holder.requestRefundBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //listener.updateStatus(i, 10);
            }
        });

        holder.withdrawPurchaseBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        if(i.getStatus()==1)
        {
            holder.processOrderBtt.setVisibility(View.VISIBLE);
            holder.cancelOrderBtt.setVisibility(View.VISIBLE);
        }

        if(i.getStatus()==2)
        {
            holder.shipOrderBtt.setVisibility(View.VISIBLE);
            holder.cancelOrderBtt.setVisibility(View.VISIBLE);
        }

        if(i.getStatus()==9)
        {
            holder.refundOrderBtt.setVisibility(View.VISIBLE);
        }

        holder.order_list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(i,holder.getAdapterPosition());
            }
        });
    }

    String GetStatus(int statusID)
    {
        switch (statusID) {
            case 1:
                return "paid";
            /*case 2:
                return "processing";*/
            case 2:
                return "In Preparation";
            case 3:
                return "In Transit";
            case 4:
                return "Delivered";
            case 5:
                return "completed";
            case 6:
                return "refunded";
            case 7:
                return "cancel by Owner";
            case 8:
                return "canceled by Buyer";
            case 9:
                return "refund requested by buyer";
            case 10:
                return "refunded";
            default:
                return "";
        }
    }


    @Override
    public int getItemCount() {
        return TheList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Order_T1 item, int position);
        void updateStatus(Order_T1 item, int orderStatus);
    }
}