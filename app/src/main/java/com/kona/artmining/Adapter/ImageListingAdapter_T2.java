package com.kona.artmining.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kona.artmining.Helper.Dashboard;
import com.kona.artmining.Model.Art_T1;
import com.kona.artmining.R;
import com.kona.artmining.custom_components.Spinner_T3;
import com.kona.artmining.custom_components.Spinner_T4;
import com.kona.artmining.custom_components.SquaredImageView;
import com.kona.artmining.model.RadioButtonInfo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

//for my art works
public class ImageListingAdapter_T2 extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    private OnItemClickListener listener;
    private List<Art_T1> TheList;
    
    private static final int VIEW_TYPE_ITEM = 2;

    private Context ctx;


    public class TheViewHolder extends RecyclerView.ViewHolder
    {
        public SquaredImageView img;
        public ImageView ifAuctioned;
        public ImageView ifSoldOut;
        public ImageView ifSelected,ifSoldOutAuction;;
        public LinearLayout imgHover;

        public TheViewHolder(View view) {
            super(view);
            img = (SquaredImageView) view.findViewById(R.id.img);
            ifAuctioned = (ImageView) view.findViewById(R.id.ifAuctioned);
            ifSoldOut = (ImageView) view.findViewById(R.id.ifSoldOut);
            ifSoldOutAuction = (ImageView) view.findViewById(R.id.ifSoldOutAuction);
            imgHover = (LinearLayout) view.findViewById(R.id.imgHover);
            ifSelected = (ImageView) view.findViewById(R.id.ifSelected);
            ctx = view.getContext();
        }
    }

    public ImageListingAdapter_T2(List<Art_T1> ml, OnItemClickListener listener) {
        this.TheList = ml;
        this.listener=listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new TheViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.image_listing_adapter_t3, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TheViewHolder) {
            addTheView((TheViewHolder) holder, position);
        }
    }

    void addTheView(final ImageListingAdapter_T2.TheViewHolder holder, int position)
    {
        final Art_T1 i = TheList.get(position);

        Picasso.with(ctx)
                .load(i.getMain_image_thumb())
                .placeholder(R.drawable.deummy_data)
                .error(R.drawable.transparent)
                .fit()
                .into(holder.img);

        
        if(i.getArt_type() == 2)
        {
            holder.ifAuctioned.setVisibility(View.VISIBLE);


            if(!i.isSold_out())
            {
                holder.ifSoldOutAuction.setVisibility(View.GONE);
            }
            else
            {
                holder.ifSoldOutAuction.setVisibility(View.VISIBLE);
            }

        }else{
            holder.ifAuctioned.setVisibility(View.GONE);

            if(!i.isSold_out())
            {
                holder.ifSoldOut.setVisibility(View.GONE);
            }
            else
            {
                holder.ifSoldOut.setVisibility(View.VISIBLE);
            }
        }

        holder.imgHover.setClickable(true);
        holder.imgHover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(i);
            }
        });

        if(!i.isSelected())
        {
            holder.ifSelected.setImageResource(R.drawable.btn_checkbox_nor);
            holder.imgHover.setBackgroundResource(R.drawable.image_listing_adapter_t1_imgvw_bg);
        }
        else
        {
            holder.ifSelected.setImageResource(R.drawable.btn_checkbox02_sel);
            holder.imgHover.setBackgroundResource(R.drawable.image_listing_adapter_t1_imgvw_bg_50_opacity);
        }

        holder.ifSelected.setClickable(true);
        holder.ifSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(i.isSelected())
                {
                    TheList.get(holder.getAdapterPosition()).setSelected(false);
                    //holder.ifSelected.setImageResource(R.drawable.btn_checkbox_nor);
                    //holder.imgHover.setBackgroundResource(R.drawable.image_listing_adapter_t1_imgvw_bg);
                }
                else
                {
                    TheList.get(holder.getAdapterPosition()).setSelected(true);
                    //holder.ifSelected.setImageResource(R.drawable.btn_checkbox02_sel);
                    //holder.imgHover.setBackgroundResource(R.drawable.image_listing_adapter_t1_imgvw_bg_50_opacity);
                }

                listener.onItemSelected(i, holder.getAdapterPosition(), i.isSelected());
            }
        });
    }

    public interface OnItemClickListener {
        void onItemClick(Art_T1 item);
        void onItemSelected(Art_T1 item,  int position, boolean isSelected);
    }
    
    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return TheList.size();
    }
}

