package com.kona.artmining.RESTclient;

import com.kona.artmining.Constants.ServerConstants;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.easyprefs.library.Prefs;

public class artmineRestClient {

    private static final String BASE_URL = ServerConstants.BASE_URL;

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void SetTimeout(int Seconds)
    {
        client.setMaxRetriesAndTimeout(0,(Seconds * 1000));
    }

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        System.out.println("artmineRestClient:get "+getAbsoluteUrl(url));
        System.out.println("artmineRestClient:get "+params.toString());

        if(params !=null){

        }
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void getD(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        System.out.println("artmineRestClient:get "+url);
        System.out.println("artmineRestClient:get "+params.toString());

        if(params !=null){

        }
        client.get(url, params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        System.out.println("artmineRestClient:post "+getAbsoluteUrl(url));
        System.out.println("artmineRestClient:post "+params.toString());

        if(params !=null){

        }
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

}
