package com.kona.artmining.Constants;


public class ServerConstants {

    //public static final String BASE_URL = "http://nijosso.net/";
    public static final String BASE_URL = "http://52.43.83.218/";
    //public static final String BASE_URL = "http://192.168.0.251/art_minining/web/";

    public static final String LOGIN_PING ="user/logins.json";
    public static final String CHECK_IF_ADMIN_PING ="user/check_if_admins.json";
    public static final String SIGNUP_PING ="user/registers.json";

    public static final String TERMS_AND_CONDITIONS_AUCTION ="user/terms_with_conditions/7.json";
    public static final String TERMS_AND_CONDITIONS_CART ="user/terms_with_conditions/8.json";
    public static final String TERMS_AND_CONDITIONS_REGISTRATION ="user/terms_with_conditions/1.json";

    public static final String FORGOT_PASSWORD_PING ="user/forgotpasswords.json";
    public static final String VERIFIED_SMS_PING ="user/verifiedsms.json";
    public static final String RESEND_SMS ="user/resend_sms.json";
    public static final String CHECK_EMAIL_ADDR ="user/email_checkers.json";
    public static final String UPDATE_ACCESS_TOKEN ="user/refresh_tokens.json";
    public static final String CART_INFO ="cart/count.json";
    public static final String CART_DETAILS ="cart/cart_list.json";
    public static final String CART_REMOVE_ITEM ="cart/remove_cart_items.json";
    public static final String USER_PROFILE_DETAILS ="user/update_profile_view.json";
    public static final String USER_COVER_IMAGE_UPLOAD ="user/upload_cover_messages.json";
    public static final String ARTIST_REVIEWS ="latest/my_review_list.json";
    public static final String USER_PROFILE_UPDATE ="user/update_profile_infos.json";
    public static final String ART_SEARCH ="art/searches.json";
    public static final String THEME_LIST ="user/themes_list.json";
    public static final String COUNTRY_LIST ="shipping/country_list.json";
    public static final String ARTIST_LIST ="search/artist_list.json";
    public static final String ARTIST_SEARCH ="search/artist_search.json";
    public static final String USER_SEARCH ="search/user_search.json";

    public static final String ARTIST_SCRAPBOOK_DETAILS ="art/scrap_list.json";
    public static final String ARTIST_SCRAPBOOK_ITEM_DELETE ="art/scrap_deletes.json";
    public static final String ARTIST_SCRAPBOOK_ITEM_ADD ="art/add_scraps.json";

    public static final String CREATE_AN_ART = "art/creates.json";
    public static final String REMOVE_AN_ART = "art/delete_an_arts.json";
    public static final String GET_ART_DETAILS = "art/details/dddddd.json";
    public static final String GET_ARTIST_POSTED_ART = "art/list_posted_art.json";


    public static final String CREATE_AN_ORDER ="order/create_an_orders.json";
    public static final String ORDER_LIST ="order/order_list.json";
    public static final String ORDER_LIST_FOR_ARTIST ="/order/orders_for_artists.json";
    public static final String ORDER_UPDATE_STATUS ="/order/change_order_statuses.json";

    public static final String RETRIEVE_DEFAULT_SHIPPING ="order/default_shipping.json";
    public static final String SAVE_SHIPPING_DETAILS ="order/set_default_shipping_addresses.json";
    public static final String PROCESS_ORDER_DETAILS ="order/pocess_orders.json";

    public static final String RELEASE_LIST_ALL ="latest/all_releases.json";
    public static final String REVIEW_LIST_ALL ="latest/review_list.json";
    public static final String REVIEW_DETAILS ="latest/review_details.json";
    public static final String REVIEW_EDIT ="latest/review_edits.json";
    public static final String REVIEW_CREATE ="latest/review_creates.json";
    public static final String REVIEW_RETRIEVE_COMMENT ="latest/list_review_comments.json";
    public static final String REVIEW_CREATE_COMMENT ="latest/create_review_comments.json";
    public static final String IF_REVIEW_ALLOWED_TO_REMOVE ="latest/check_if_review_can_be_modified_or_removes.json";
    public static final String REVIEW_REMOVE ="/latest/remove_review.json";
    public static final String LIKE_UNLIKE_A_REVIEW ="/latest/add_or_remove_likes.json";


    public static final String EVENT_LIST_ALL ="/latest/event_list.json";

    public static final String EVENT_DETAILS ="latest/event_details.json";
    public static final String EXHIBITION_DETAILS ="latest/exhibition_details.json";
    public static final String EXHIBITION_CREATE ="latest/exhibition_creates.json";
    public static final String EXHIBITION_CREATE_COMMENT ="latest/create_exhibition_comments.json";
    public static final String EXHIBITION_RETRIEVE_COMMENTS ="latest/list_exhibition_comments.json";


    public static final String SHOP_SEARCH ="art/shop_list.json";
    public static final String AUCTION_SEARCH ="art/auction_list.json";

    public static final String THEME_LISTING =BASE_URL+"theme/listing?mobile_view=1&access_token=";
    public static final String USER_LISTING =BASE_URL+"admin/user/listing?mobile_view=1&access_token=";
    public static final String USER_LISTING_JSON ="admin/user/listing.json";
    public static final String REJECTED_USER_LISTING =BASE_URL+"admin/rejected/listing?mobile_view=1&access_token=";
    public static final String APPROVED_USER_LISTING =BASE_URL+"admin/approved/listing?mobile_view=1&access_token=";
    public static final String PROFILE_SETTINGS_OPTIONS = BASE_URL+"user/profile_settings_options?mobile_view=1&access_token=";
    public static final String POST_AN_ART = BASE_URL+"art/create?mobile_view=1&access_token=";
    public static final String POSTED_ART_LISTING = BASE_URL+"art/list_posted_art?mobile_view=1&access_token=";
    public static final String INIPAY_URL =BASE_URL+"order/koren_gateway?access_token=";


    public static final String GET_SHIPPING_METHODS ="shipping/group_list.json";

    public static final String AUCTION_DETAILS ="cart/auction_list.json";


    public static final String MESSAGE_CREATE_MESSAGE ="message/create_messages.json";
    public static final String MESSAGE_DELETE_MESSAGE ="message/delete_messages.json";
    public static final String MESSAGE_INBOX_MESSAGE ="message/inbox_messages.json";
    public static final String MESSAGE_SINGLE_MESSAGE_DETAILS ="message/message_details.json";
    public static final String MESSAGE_SENT_MESSAGE ="message/sent_messages.json";
    public static final String MESSAGE_TRASH_A_MESSAGE ="message/trash_messages.json";
    public static final String MESSAGE_TRASH_MESSAGE ="message/trashed_messages.json";
    public static final String MESSAGE_MOVE_TO_INBOX ="message/move_to_inboxes.json";

    public static final String USER_AUTH_EMAIL ="email";
    public static final String USER_AUTH_PASSWORD ="password";
    public static final String USER_AUTH_ACCESS_TYPE ="access_type";
    public static final String SEARCH_KEYWORD ="keyword";

    public static final String COUNTRY_CODE = "+88";

    public static final String ARTIST_ID = "artist_id";
    public static final String THEME_ID = "theme_id";
    public static final String ART_TYPE = "art_type";
    public static final String SORT_TYPE = "sort_by";
    public static final String INCLUDE_SOLDOUT = "include_soldout";


    public static final String URL_FORGOT_PASSWORD = BASE_URL+"user/forgotpassword";

    public static final int SC_NOT_FOUND = 404;
    public static final int SC_UNAUTHORIZED = 401;
}
