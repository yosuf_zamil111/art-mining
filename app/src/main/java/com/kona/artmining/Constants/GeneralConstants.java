package com.kona.artmining.Constants;

public class GeneralConstants
{
    public static final String FRAGMENT_NAME="fragmentName";
    public static final String TC_TYPE="fragmentName";
    public static final String SELECTED_CURRENCY="selectedCurrency";
    public static final String MESSAGE_ID="message_id";
    public static final String RECEIVER_ID="received_id";
    public static final String RECEIVER_NAME="received_name";


    public static final String URL="url";

    public static final String MODIFIED_CART_JSON="modified_cart_json";
    public static final String MODIFIED_AUCTION_CART_JSON="modified_auction_cart_json";


    public static final String ORDER_ID="orderId";

    public static final String ID="id";
    public static final String POSITION="position";
    public static final String FORCE_TO_SHOW_EDIT_SCREEN="forceShowEditScreen";

    public static final String SHIPPING_COST = "7.80";




    public static final String ART_DETAILS_PAGE = "ART_DETAILS_PAGE";
}
