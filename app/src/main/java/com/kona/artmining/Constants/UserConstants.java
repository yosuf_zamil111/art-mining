package com.kona.artmining.Constants;


public class UserConstants
{
    public static final String Key_LoggedUserID="loggedUserID";

    public static final String DO_AUTO_LOGIN="doAutoLogin";
    public static final String SET_DEFAULT_SHIPPING_ADDRESS = "set_default_shipping_address";

    public static final String AccessToken = "access_token";
    public static final String RefreshToken = "refresh_token";
    public static final String TokenValidity = "validity";
    public static final String ProfilePicture = "profilePicture";

    public static final String UserFullDetails = "userFullDetails";

    public static final String USER_ID="user_id";

    public static final String REG_FIELD_EMAIL = "email";
    public static final String REG_FIELD_PASSWORD = "password";
    public static final String REG_FIELD_PHONE = "phone";
    public static final String REG_FIELD_USER_TYPE = "user_type_id";
    public static final String REG_FIELD_NAME_EN = "name_en";
    public static final String REG_FIELD_NAME_KR = "name_kr";
    public static final String REG_FIELD_BIRTHDATE = "birthdate";
    public static final String REG_FIELD_BIRTH_DATE = "birth_date";
    public static final String REG_FIELD_SEX = "sex";
    public static final String REG_FIELD_PHONE_EXT = "phone_ext";
    public static final String REG_FIELD_ADDRESS = "address";
    public static final String REG_FIELD_ADDRESS2 = "address_2";
    public static final String REG_FIELD_ZIP = "zip";

    public static final String MESSAGE_TYPE = "message_type";
    public static final int MESSAGE_TYPE_INBOX = 1;
    public static final int MESSAGE_TYPE_SENT = 2;
    public static final int MESSAGE_TYPE_TRASH = 3;
}
