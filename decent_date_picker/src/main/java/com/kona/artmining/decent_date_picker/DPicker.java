package com.kona.artmining.decent_date_picker;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.custom_components.SquareButton_T1;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;

public class DPicker extends Dialog
{
    LinearLayout _Layout;
    SquareButton_T1 doneBtt;
    SquareButton_T1 cancelBtt;

    TextView Title;
    NumberPicker month;
    NumberPicker day;
    NumberPicker year;

    Success _success;
    String _value = null;

    Calendar calendar;

    public DPicker(Context context) {
        super(context);
        setContentView(R.layout.d_picker);
        setCancelable(false);

        calendar = Calendar.getInstance();

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        _Layout = (LinearLayout) this.findViewById(R.id._Layout);
        Title = (TextView) findViewById(R.id.Title);

        doneBtt = (SquareButton_T1) findViewById(R.id.doneBtt);
        doneBtt._onClick(new SquareButton_T1._click() {
            @Override
            public void done() {
                _success.Done(new Date(year.getValue(), month.getValue(), day.getValue()));
                dismiss();
            }
        });

        cancelBtt = (SquareButton_T1) findViewById(R.id.cancelBtt);
        cancelBtt._onClick(new SquareButton_T1._click() {
            @Override
            public void done() {
                dismiss();
            }
        });

        month = (NumberPicker) findViewById(R.id.month);
        month.setMaxValue(new DateFormatSymbols().getShortMonths().length - 1);
        month.setMinValue(0);
        String[] months = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
        month.setDisplayedValues(months);
        month.setFocusable(true);
        month.setFocusableInTouchMode(true);

        day = (NumberPicker) findViewById(R.id.day);
        day.setMaxValue(31);
        day.setMinValue(1);
        day.setFocusable(true);
        day.setFocusableInTouchMode(true);

        year = (NumberPicker) findViewById(R.id.year);
        year.setMaxValue(calendar.get(Calendar.YEAR));
        year.setMinValue(1960);
        year.setFocusable(true);
        year.setFocusableInTouchMode(true);

    }

    public void setTheTitle(String title)
    {
        Title.setText(title);
    }

    public void setDefaultValue(int Date, int Month, int Year)
    {
        day.setValue(Date);
        month.setValue(Month);
        year.setValue(Year);
    }

    public void setDoneBttBg(Drawable bg)
    {
        doneBtt.setBackground(bg);
    }

    public void setCancelBttBg(Drawable bg)
    {
        cancelBtt.setBackground(bg);
    }

    public void setListener(Success cmd)
    {
        this._success=cmd;
    }

    public interface Success
    {
        void Done(Date _date);
    }
}
