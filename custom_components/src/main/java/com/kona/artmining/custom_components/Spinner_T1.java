package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Spinner_T1 extends LinearLayout
{
    LinearLayout _Layout;
    TypedArray a;
    TextView Title;
    TextView SubTitle;
    Success _success;

    public Spinner_T1(Context context, AttributeSet attrs) {
        super(context, attrs);

        a = context.obtainStyledAttributes(attrs, R.styleable.Spinner_T1, 0, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.spinner_t1, this, true);

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);
        _Layout.setClickable(true);
        _Layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(_success != null)
                {
                    _success.done();
                }
            }
        });

        Title = (TextView) main.findViewById(R.id.Title);
        SubTitle = (TextView) main.findViewById(R.id.SubTitle);

        Title.setText(a.getString(R.styleable.Spinner_T1_SPT1_Text));

        if(a.hasValue(R.styleable.Spinner_T1_SPT1_BottomBorder))
        {
            _Layout.setBackground(context.getResources().getDrawable(R.drawable.inputborder));
        }

        main.findViewById(R.id.ifSubtitle).setVisibility(View.GONE);
        main.findViewById(R.id.ifNoSubtitle).setVisibility(View.VISIBLE);

        if(a.hasValue(R.styleable.Spinner_T1_SPT1_Subtitle))
        {
            main.findViewById(R.id.ifSubtitle).setVisibility(View.VISIBLE);
            main.findViewById(R.id.ifNoSubtitle).setVisibility(View.GONE);
            SubTitle.setVisibility(View.VISIBLE);
        }
    }

    public String getValue()
    {
        return Title.getText().toString();
    }

    public void setText(String txt)
    {
        Title.setText(txt);
    }

    public void SetSubtitleText(String txt)
    {
        SubTitle.setText(txt);
    }

    public void setListener(Success _success)
    {
        this._success = _success;
    }

    public interface Success
    {
        void done();
    }
}
