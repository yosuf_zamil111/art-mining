package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class OvalButton_T1 extends LinearLayout
{
    LinearLayout _Layout;
    TypedArray a;
    Boolean status = true;
    TextView LabelText;
    ImageView LabelImg;

    public OvalButton_T1(Context context, AttributeSet attrs) {
        super(context, attrs);

        a = context.obtainStyledAttributes(attrs, R.styleable.OvalButton_T1, 0, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.oval_button_t1, this, true);

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);

        if(a.hasValue(R.styleable.OvalButton_T1__background)){
            _Layout.setBackground(a.getDrawable(R.styleable.OvalButton_T1__background));
        }

        LabelText = (TextView) main.findViewById(R.id.TEXT);

        if(a.hasValue(R.styleable.OvalButton_T1__labelText)){
            LabelText.setText(a.getString(R.styleable.OvalButton_T1__labelText));
        }

        if(a.hasValue(R.styleable.OvalButton_T1__labelTextColor)){
            LabelText.setTextColor(a.getColor(R.styleable.OvalButton_T1__labelTextColor, getResources().getColor(android.R.color.black)));
        }

        if(a.hasValue(R.styleable.OvalButton_T1__labelTextSize)){
            LabelText.setTextSize(TypedValue.COMPLEX_UNIT_PX, a.getDimension(R.styleable.OvalButton_T1__labelTextSize, getResources().getDimension(R.dimen.loginbuttonfontsize)));
        }

        if(a.hasValue(R.styleable.OvalButton_T1__labelTextStyle)){
            LabelText.setTypeface(null, Typeface.BOLD);
        }else{
            LabelText.setTypeface(null, Typeface.NORMAL);
        }

        LabelImg = (ImageView) main.findViewById(R.id.IMG);

        if(a.hasValue(R.styleable.OvalButton_T1__labelImg)){
            LabelImg.setImageDrawable(a.getDrawable(R.styleable.OvalButton_T1__labelImg));
        }else{
            LabelImg.setVisibility(View.GONE);
        }

        a.recycle();
    }

    public void setLabel(String txt)
    {
        LabelText.setText(txt);
    }

    public void _onClick(final _click clk)
    {
        _Layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status){
                    clk.done();
                }
            }
        });
    }

    public void ChangeLayoutBackground(Drawable drawable)
    {
        _Layout.setBackground(drawable);
        LabelText.setTextColor(getResources().getColor(android.R.color.black));
        LabelImg.setImageDrawable(getResources().getDrawable(R.drawable.icon_check_sel));
    }

    public void BackBackground(Drawable drawable)
    {
        _Layout.setBackground(drawable);
        LabelText.setTextColor(getResources().getColor(android.R.color.black));
        LabelImg.setImageDrawable(getResources().getDrawable(R.drawable.icon_check_nor));
    }

    public void ChangeLabelColor(int color)
    {
        LabelText.setTextColor(color);
    }

    public void makeEnable(boolean status)
    {
        this.status=status;
        doEnable();
    }

    private void doEnable()
    {
        if(this.status){
            _Layout.setBackground(a.getDrawable(R.styleable.OvalButton_T1__background));
        }else{
            System.out.println("Disabled.");
            if(a.hasValue(R.styleable.OvalButton_T1__background_on_disable)){
                System.out.println("Disabled sdfsdf sdf.");
                _Layout.setBackground(a.getDrawable(R.styleable.OvalButton_T1__background_on_disable));
                LabelText.setTextColor(getResources().getColor(R.color.white));
            }
        }
    }

    public void setA_Background(Drawable drb, int col)
    {
        _Layout.setBackground(drb);
        LabelText.setTextColor(col);
    }


    public interface _click
    {
        void done();
    }
}
