package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.File;

public class TopSearchBarWithButtons_T1 extends LinearLayout
{
    LinearLayout _Layout;
    TypedArray a;
    _clicked c;

    CircularImageView ProfileIcon;
    EditText SearchBar;
    TextView CartItemsCounter;

    Context ctx;

    ImageView topBackButton;
    LinearLayout showCart;

    public TopSearchBarWithButtons_T1(Context context, AttributeSet attrs) {
        super(context, attrs);

        a = context.obtainStyledAttributes(attrs, R.styleable.CustomComponent_Attributes, 0, 0);

        this.ctx = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.top_search_bar_with_buttons_t1, this, true);

        CartItemsCounter = (TextView) main.findViewById(R.id.CartItemsCounter);
        showCart = (LinearLayout) main.findViewById(R.id.showCart);

        ProfileIcon = (CircularImageView) main.findViewById(R.id.ProfileIcon);
        ProfileIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(c != null)
                {
                    c.ProfileIcon();
                }
            }
        });

        topBackButton = (ImageView) main.findViewById(R.id.topBackButton);

        SearchBar = (EditText) main.findViewById(R.id.SearchBar);
        SearchBar.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(c != null)
                {
                    if(hasFocus)
                    {

                        topBackButton.setVisibility(View.VISIBLE);
                        ProfileIcon.setVisibility(View.GONE);
                        c.SearchBoxFocused();

                    }else{
                        topBackButton.setVisibility(View.GONE);
                        ProfileIcon.setVisibility(View.VISIBLE);
                        c.SearchBoxUnFocused();
                    }
                }
            }
        });

        SearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(c != null)
                {
                    c.SearchBoxText(s.toString());
                }
            }
        });

        topBackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                topBackButton.setVisibility(View.GONE);
                ProfileIcon.setVisibility(View.VISIBLE);
                resetSearchBox();
                c.BackFromSearch();
            }
        });

        showCart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                c.ShowCart();
            }
        });
    }

    public void resetSearchBox()
    {
        SearchBar.setText(null);
        SearchBar.clearFocus();
    }

    public void resetSearchBox2()
    {
        topBackButton.setVisibility(View.VISIBLE);
        ProfileIcon.setVisibility(View.GONE);
        SearchBar.setText(null);
        SearchBar.clearFocus();
    }

    public void setListener(_clicked c)
    {
        this.c = c;
    }

    public void updateCartItem(String data)
    {
        CartItemsCounter.setText(data);
    }

    public void updateProfilePicture(String path)
    {
        Picasso.with(ctx)
                .load(new File(path))
                .placeholder(R.drawable.icon_profile)
                .error(R.drawable.icon_profile)
                .into(ProfileIcon);
        System.out.println("Called: "+path);
    }

    public void resetProfilePicture()
    {
        Picasso.with(ctx)
                .load(R.drawable.icon_profile)
                .placeholder(R.drawable.icon_profile)
                .error(R.drawable.icon_profile)
                .into(ProfileIcon);
    }

    public interface _clicked
    {
        void ProfileIcon();
        void SearchBoxFocused();
        void SearchBoxUnFocused();
        void SearchBoxText(String keyword);
        void BackFromSearch();
        void ShowCart();
    }
}
