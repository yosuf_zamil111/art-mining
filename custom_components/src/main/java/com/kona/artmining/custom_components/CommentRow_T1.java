package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.kona.artmining.model.Comment_T1;
import com.kona.artmining.model.RadioButtonInfo;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.File;

public class CommentRow_T1 extends LinearLayout {

    LinearLayout _Layout;
    TypedArray a;
    TextView commenter_commenting_date, commenter_comment, commenter_name;
    String myID;
    hitted _hitted;


    Comment_T1 _thisInfo;

    CircularImageView ProfileIcon;

    Context ctx;

    public CommentRow_T1(Context context) {
        super(context);

        ctx = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.comment_row_t1, this, true);

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);

        commenter_commenting_date = (TextView) main.findViewById(R.id.commenter_commenting_date);
        commenter_comment = (TextView) main.findViewById(R.id.commenter_comment);
        commenter_name = (TextView) main.findViewById(R.id.commenter_name);
        ProfileIcon = (CircularImageView) main.findViewById(R.id.ProfileIcon);


        _Layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void setup(Comment_T1 info)
    {
        this._thisInfo=info;
        commenter_name.setText(info.getCommentor_english_name()+" / "+info.getCommentor_korean_name());
        commenter_comment.setText(info.getComment_details());
        commenter_commenting_date.setText(info.getComment_date_time());
    }

    public void setup2(String name, String comment, String _date, String _image)
    {
        commenter_name.setText(name);
        commenter_comment.setText(comment);
        commenter_commenting_date.setText(_date);


        Picasso.with(ctx)
                .load(_image)
                .placeholder(R.drawable.icon_profile)
                .error(R.drawable.icon_profile)
                .into(ProfileIcon);
    }

    public void setBottomLine()
    {
        _Layout.setBackgroundResource(R.drawable.inputborder);
        _Layout.setPadding(30,30,30,30);
    }

    public String getMyID()
    {
        return myID;
    }

    public void setListener(hitted hit)
    {
        _hitted = hit;
    }

    public interface hitted
    {
        void done(CommentRow_T1 view, RadioButtonInfo id);
    }
}
