package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SquareButton_T1 extends LinearLayout
{
    LinearLayout _Layout;
    TypedArray a;
    Boolean status = true;

    public SquareButton_T1(Context context, AttributeSet attrs) {
        super(context, attrs);

        a = context.obtainStyledAttributes(attrs, R.styleable.SquareButton_T1, 0, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.square_button_t1, this, true);

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);
        _Layout.setBackground(a.getDrawable(R.styleable.SquareButton_T1_SBT1_background));

        TextView LabelText = (TextView) main.findViewById(R.id.TEXT);
        LabelText.setText(a.getString(R.styleable.SquareButton_T1_SBT1_labelText));

        if(a.hasValue(R.styleable.SquareButton_T1_SBT1_labelTextColor)){
            LabelText.setTextColor(a.getColor(R.styleable.SquareButton_T1_SBT1_labelTextColor, getResources().getColor(android.R.color.black)));
        }

        if(a.hasValue(R.styleable.SquareButton_T1_SBT1_labelTextSize)){
            LabelText.setTextSize(TypedValue.COMPLEX_UNIT_PX, a.getDimension(R.styleable.SquareButton_T1_SBT1_labelTextSize, getResources().getDimension(R.dimen.loginbuttonfontsize)));
        }

        ImageView LabelImg = (ImageView) main.findViewById(R.id.IMG);
        if(a.hasValue(R.styleable.SquareButton_T1_SBT1_labelImg)){
            LabelImg.setImageDrawable(a.getDrawable(R.styleable.SquareButton_T1_SBT1_labelImg));
        }else{
            LabelImg.setVisibility(View.GONE);
        }

        a.recycle();
    }

    public void _onClick(final _click clk)
    {
        _Layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status){
                    clk.done();
                }
            }
        });
    }

    public void makeEnable(boolean status)
    {
        this.status=status;
        doEnable();
    }

    private void doEnable()
    {
        if(this.status){
            _Layout.setBackground(a.getDrawable(R.styleable.SquareButton_T1_SBT1_background));
        }else{
            if(a.hasValue(R.styleable.SquareButton_T1_SBT1_background_on_disable)){
                _Layout.setBackground(a.getDrawable(R.styleable.SquareButton_T1_SBT1_background_on_disable));
            }
        }
    }


    public interface _click
    {
        void done();
    }
}
