package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.custom_popup_box.GenderSelectionPopUpBox;

public class FormField_T2_TextBox extends LinearLayout
{
    EditText _TextBox;
    LinearLayout _button;
    Success _success;
    LinearLayout _Layout;
    LinearLayout SubLayout;

    public FormField_T2_TextBox(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FormField_T2_TextBox, 0, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.form_field_t2_text_box, this, true);

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);
        SubLayout = (LinearLayout) main.findViewById(R.id.SubLayout);
        SubLayout.setBackground(null);

        ImageView LabelImg = (ImageView) main.findViewById(R.id.LabelImg);
        LabelImg.setImageDrawable(a.getDrawable(R.styleable.FormField_T2_TextBox_FF_TB_T2_labelImage));

        if(a.hasValue(R.styleable.FormField_T2_TextBox_FF_TB_T2_labelImage_Margin_left))
        {
            LayoutParams lp = new LayoutParams(
                    (int) context.getResources().getDimension(R.dimen.inputIconWidth),
                    LayoutParams.WRAP_CONTENT
            );

            lp.setMargins(
                    (int) a.getDimension(R.styleable.FormField_T2_TextBox_FF_TB_T2_labelImage_Margin_left,0),
                    (int) a.getDimension(R.styleable.FormField_T2_TextBox_FF_TB_T2_labelImage_Margin_top,0),
                    0,
                    0);

            LabelImg.setLayoutParams(lp);
        }

        if(a.hasValue(R.styleable.FormField_T2_TextBox_FF_TB_T2_enable_sublayout))
        {
            SubLayout.setBackground(context.getResources().getDrawable(R.drawable.inputborder));
            _Layout.setBackground(null);
        }

        TextView LabelText = (TextView) main.findViewById(R.id.LabelText);
        if(a.hasValue(R.styleable.FormField_T2_TextBox_FF_TB_T2_labelText))
        {
            LabelText.setText(a.getString(R.styleable.FormField_T2_TextBox_FF_TB_T2_labelText));
        }

        TextView TEXT = (TextView) main.findViewById(R.id.confirm);
        TEXT.setText(a.getString(R.styleable.FormField_T2_TextBox_FF_TB_T2_confirm));


        _TextBox = (EditText) main.findViewById(R.id._TextBox);

        if(!isInEditMode()) {
            _TextBox.setTypeface(Typeface.createFromAsset(context.getResources().getAssets(), "Roboto-Regular.ttf"));
        }

        _TextBox.setHint(a.getString(R.styleable.FormField_T2_TextBox_FF_TB_T2_placeholderText));

        if(a.hasValue(R.styleable.FormField_T2_TextBox_FF_TB_T2_password))
        {
            _TextBox.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }

        _button = (LinearLayout) findViewById(R.id.rightButton);
        _button.setBackground(a.getDrawable(R.styleable.FormField_T2_TextBox_FF_TB_T2_button_background));
        _button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(_success != null)
                {
                    _success.done();
                }
            }
        });

        _TextBox.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus)
                {
                    if(_success != null)
                    {
                        _success.Focused();
                    }
                }else{
                    if(_success != null)
                    {
                        _success.Unfocused();
                    }
                }
            }
        });

        if(a.hasValue(R.styleable.FormField_T2_TextBox_FF_TB_T2_hide_button))
        {
            _button.setVisibility(View.GONE);
        }


        a.recycle();
    }

    public void setText(String txt)
    {
        _TextBox.setText(txt);
    }

    public String getText()
    {
        return _TextBox.getText().toString();
    }

    public void setListener(Success _success)
    {
        this._success = _success;
    }

    public interface Success
    {
        void done();
        void Focused();
        void Unfocused();
    }
}
