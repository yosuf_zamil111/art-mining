package com.kona.artmining.custom_components;


import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.kona.artmining.model.RadioButtonInfo;

import java.util.ArrayList;
import java.util.List;

public class RadioGroup_T1 extends LinearLayout {

    LinearLayout _Layout;
    TypedArray a;
    Context context;
    String _value = null;

    public RadioGroup_T1(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context=context;

        //a = context.obtainStyledAttributes(attrs, R.styleable.RadioButton_T1, 0, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.radio_group_t1, this, true);

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);
        //_Layout.addView(new LinearLayout(context));
    }

    public void setElements(List<RadioButtonInfo> btns)
    {
        int i = 0;

        for (RadioButtonInfo rbi : btns)
        {
            RadioButton_T1 rb_t1=new RadioButton_T1(context);
            rb_t1.setup(rbi.getText(), i, (rbi.getID() + ""), false);
            rb_t1.setListener(new RadioButton_T1.hitted() {
                @Override
                public void done(String id) {
                    _value=id;
                    resetRadioView(id);
                }
            });

            _Layout.addView(rb_t1, i);

            i = i + 1;
        }
    }

    private void resetRadioView(String id)
    {
        int childs = _Layout.getChildCount();
        for(int i=0; i < childs; i++)
        {
            RadioButton_T1 mi = (RadioButton_T1) _Layout.getChildAt(i);
            if(mi.getMyID() != id){
                mi.DoReset();
            }
        }
    }

    public String getValue()
    {
        return _value;
    }
}
