package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RadioButton_T1 extends LinearLayout {

    LinearLayout _Layout;
    TypedArray a;
    TextView CheckedImageText;
    ImageView CheckedImage;
    String myID;
    Boolean _checked = false;
    hitted _hitted;
    int locationID;

    public RadioButton_T1(Context context) {
        super(context);

        /*a = context.obtainStyledAttributes(attrs, R.styleable.RadioButton_T1, 0, 0);*/

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.radio_button_t1, this, true);

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);

        CheckedImageText = (TextView) main.findViewById(R.id.CheckedImageText);
        CheckedImage = (ImageView) main.findViewById(R.id.CheckedImage);

        /*CheckedImageText.setText(a.getString(R.styleable.RadioButton_T1_RBT1_Text));

        if(a.hasValue(R.styleable.RadioButton_T1_RBT1_Checked)){
            DoChecked();
        }*/

        _Layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(_checked){
                    DoUnchecked();
                }else{
                    DoChecked();
                }*/
                DoChecked();
            }
        });
    }

    public void setup(String lbl, int locationID, String ID, boolean Checked)
    {
        CheckedImageText.setText(lbl);
        myID = ID;
        this.locationID = locationID;
        if(Checked){

            DoChecked();
        }
    }

    public String getMyID()
    {
        return myID;
    }

    public void DoChecked()
    {
        _checked = true;
        //CheckedImage.setImageResource(R.drawable.btn_line01_pre);
        CheckedImage.setImageResource(R.drawable.img_radio_on);
        _hitted.done(myID);
    }

    public void DoUnchecked()
    {
        _checked = false;
        CheckedImage.setImageResource(R.drawable.btn_line01_dim);
        _hitted.done(myID);
    }

    public void DoReset()
    {
        CheckedImage.setImageResource(R.drawable.img_radio_off);
    }

    public void setListener(hitted hit)
    {
        _hitted = hit;
    }

    public interface hitted
    {
        void done(String id);
    }
}
