package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.custom_popup_box.OptionListing_T4;
import com.kona.artmining.model.RadioButtonInfo;

import java.util.List;
import java.util.Set;

public class Spinner_T3 extends LinearLayout
{
    LinearLayout _Layout;
    TypedArray a;
    TextView Title;
    TextView SubTitle;
    Success _success;

    OptionListing_T4 dialogueBox;

    RadioButtonInfo _info = new RadioButtonInfo();

    public Spinner_T3(Context context, AttributeSet attrs) {
        super(context, attrs);

        a = context.obtainStyledAttributes(attrs, R.styleable.Spinner_T2, 0, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.spinner_t3, this, true);

        dialogueBox =  new OptionListing_T4(getContext());
        dialogueBox.setListener(new OptionListing_T4.Success() {
            @Override
            public void Done(RadioButtonInfo value) {
                System.out.println("> "+value);
                if(_info.getID() != value.getID())
                {
                    _info = value;
                    SetSubtitleText(value.getText());
                    if(_success!=null)
                    {
                        _success.done();
                    }
                }
            }

            @Override
            public void soldOutChecked(boolean ifChecked) {
                _success.soldOutChecked(ifChecked);
            }
        });

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);
        _Layout.setClickable(true);
        _Layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(_info != null)
                {
                    dialogueBox.setDefault(_info);
                    dialogueBox.show();
                }
            }
        });

        Title = (TextView) main.findViewById(R.id.Title);
        SubTitle = (TextView) main.findViewById(R.id.SubTitle);

        Title.setText(a.getString(R.styleable.Spinner_T2_SPT2_Text));
    }

    public void hideSoldOutCheckBox()
    {
        if(dialogueBox != null)
        {
            dialogueBox.hideSoldOutCheckBox();
        }
    }

    public boolean ifSoldOutChecked()
    {
        return dialogueBox.checked;
    }


    public void setOptions(String title, List<RadioButtonInfo> optns, String defaultValue)
    {
        dialogueBox.setup(title, optns, defaultValue);
    }

    public void setText(String txt)
    {
        Title.setText(txt);
    }

    public void SetSubtitleText(String txt)
    {
        SubTitle.setText(txt);
    }

    public void setListener(Success _success)
    {
        this._success = _success;
    }

    public RadioButtonInfo get_info() {
        return _info;
    }

    public interface Success
    {
        void done();
        void soldOutChecked(boolean ifChecked);
    }
}