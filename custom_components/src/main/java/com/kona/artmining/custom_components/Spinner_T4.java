package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.custom_popup_box.OptionFilterListing_T1;
import com.kona.artmining.custom_popup_box.OptionListing_T2;
import com.kona.artmining.model.RadioButtonInfo;

import java.util.List;

public class Spinner_T4 extends LinearLayout
{
    LinearLayout _Layout;
    TypedArray a;
    TextView Title;
    TextView SubTitle;
    Success _success;

    OptionListing_T2 dialogueBox;
    OptionFilterListing_T1 FilterDialogueBox;

    RadioButtonInfo _info1 = new RadioButtonInfo();
    RadioButtonInfo _info2 = new RadioButtonInfo();

    public RadioButtonInfo get_info1() {
        return _info1;
    }

    public RadioButtonInfo get_info2() {
        return _info2;
    }

    boolean Level2DialogueBox = false;

    themeSelected ts;
    artistSelected as;
    resetHitted rs;

    public Spinner_T4(Context context, AttributeSet attrs) {
        super(context, attrs);

        a = context.obtainStyledAttributes(attrs, R.styleable.Spinner_T2, 0, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.spinner_t3, this, true);

        FilterDialogueBox =  new OptionFilterListing_T1(getContext());
        /*FilterDialogueBox.setListener(new OptionFilterListing_T1.Success() {
            @Override
            public void Done(RadioButtonInfo value1, RadioButtonInfo value2) {
                if(_success!=null && value1!=null && value2!=null)
                {
                    _info1 = value1;
                    _info2 = value2;
                    _success.done();
                }
            }
        });*/
        FilterDialogueBox.setArtistSelectedListener(new OptionFilterListing_T1.artistSelected() {
            @Override
            public void done(int id, String text) {
                _info1.setID(id);
                _info1.setText(text);
                as.done(id, text);
            }
        });

        FilterDialogueBox.setThemeSelectedListener(new OptionFilterListing_T1.themeSelected() {
            @Override
            public void done(int id, String text) {
                _info2.setID(id);
                _info2.setText(text);
                ts.done(id, text);
            }
        });

        FilterDialogueBox.setResetListener(new OptionFilterListing_T1.resetHitted() {
            @Override
            public void done() {
                _info1.setID(0);
                _info1.setText("All");
                _info2.setID(0);
                _info2.setText("All");
                rs.done();
            }
        });

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);
        _Layout.setClickable(true);
        _Layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterDialogueBox.show();
            }
        });

        Title = (TextView) main.findViewById(R.id.Title);
        SubTitle = (TextView) main.findViewById(R.id.SubTitle);

        Title.setText(a.getString(R.styleable.Spinner_T2_SPT2_Text));
    }

    public void HideTheme()
    {
        FilterDialogueBox.HideTheme();
    }

    public void setMultipleOptions(List<RadioButtonInfo> artistsOptions, List<RadioButtonInfo> themeOptions)
    {
        FilterDialogueBox.setup(artistsOptions, themeOptions);
    }

    public void setText(String txt)
    {
        Title.setText(txt);
    }

    public void SetSubtitleText(String txt)
    {
        SubTitle.setText(txt);
    }

    public void EnableLevel2DialogueBox()
    {
        Level2DialogueBox = true;
    }

    public void setListener(Success _success)
    {
        this._success = _success;
    }

    public void setArtistSelectedListener(artistSelected as)
    {
        this.as=as;
    }

    public void setThemeSelectedListener(themeSelected ts)
    {
        this.ts=ts;
    }

    public interface themeSelected
    {
        void done(int id, String text);
    }

    public interface artistSelected
    {
        void done(int id, String text);
    }

    public interface Success
    {
        void done();
    }

    public void setResetListener(resetHitted rs)
    {
        this.rs = rs;
    }

    public interface resetHitted
    {
        void done();
    }
}
