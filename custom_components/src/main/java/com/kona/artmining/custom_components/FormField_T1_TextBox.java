package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FormField_T1_TextBox  extends LinearLayout
{
    EditText _TextBox;
    LinearLayout _Layout;
    _click _c;

    String TheOtherValue;

    public FormField_T1_TextBox(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FormField_T1_TextBox, 0, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.form_field_t1_text_box, this, true);

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);

        ImageView LabelImg = (ImageView) main.findViewById(R.id.LabelImg);
        LabelImg.setImageDrawable(a.getDrawable(R.styleable.FormField_T1_TextBox_labelImage));

        if(a.hasValue(R.styleable.FormField_T1_TextBox_labelImage_Margin_left))
        {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    (int) context.getResources().getDimension(R.dimen.inputIconWidth),
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            lp.setMargins(
                    (int) a.getDimension(R.styleable.FormField_T1_TextBox_labelImage_Margin_left,0),
                    (int) a.getDimension(R.styleable.FormField_T1_TextBox_labelImage_Margin_top,0),
                    0,
                    0);

            LabelImg.setLayoutParams(lp);
        }

        TextView LabelText = (TextView) main.findViewById(R.id.LabelText);
        LabelText.setText(a.getString(R.styleable.FormField_T1_TextBox_labelText));

        _TextBox = (EditText) main.findViewById(R.id._TextBox);

        if(!isInEditMode()) {
            _TextBox.setTypeface(Typeface.createFromAsset(context.getResources().getAssets(), "Roboto-Regular.ttf"));
        }

        _TextBox.setHint(a.getString(R.styleable.FormField_T1_TextBox_placeholderText));

        if(a.hasValue(R.styleable.FormField_T1_TextBox_password))
        {
            _TextBox.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }

        _TextBox.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus)
                {
                    if(_c != null)
                    {
                        _c.Focused();
                    }
                }else{
                    if(_c != null)
                    {
                        _c.Unfocused();
                    }
                }
            }
        });

        a.recycle();
    }

    public String getText()
    {
        return _TextBox.getText().toString();
    }

    public void setText(String TheText)
    {
        _TextBox.setText(TheText);
    }

    public void disableEditable()
    {
        _Layout.setClickable(true);
        _TextBox.setFocusable(false);
    }

    public void _onClick(final _click clk)
    {
        _Layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clk!=null){
                    clk.done();
                }
            }
        });

        _TextBox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clk!=null){
                    clk.done();
                }
            }
        });
    }



    public String getTheOtherValue() {
        return TheOtherValue;
    }

    public void setTheOtherValue(String theOtherValue) {
        TheOtherValue = theOtherValue;
    }

    public void setListener(_click _success)
    {
        this._c = _success;
    }

    public interface _click
    {
        void done();
        void Focused();
        void Unfocused();
    }
}
