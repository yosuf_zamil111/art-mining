package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.custom_popup_box.OptionListing_T1;
import com.kona.artmining.model.RadioButtonInfo;

import java.util.ArrayList;
import java.util.List;

public class Spinner_T2 extends LinearLayout
{
    LinearLayout _Layout;
    TypedArray a;
    TextView Title;
    TextView SubTitle;
    Success _success;

    OptionListing_T1 dialogueBox;

    RadioButtonInfo _info;

    public Spinner_T2(Context context, AttributeSet attrs) {
        super(context, attrs);

        a = context.obtainStyledAttributes(attrs, R.styleable.Spinner_T2, 0, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.spinner_t2, this, true);

        dialogueBox =  new OptionListing_T1(getContext());
        dialogueBox.setListener(new OptionListing_T1.Success() {
            @Override
            public void Done(RadioButtonInfo value) {
                System.out.println("> "+value);
                _info = value;
                //setText(value.getText());
            }
        });

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);
        _Layout.setClickable(true);
        _Layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(_info != null)
                {
                    dialogueBox.setDefault(_info);
                    dialogueBox.show();
                }
            }
        });

        Title = (TextView) main.findViewById(R.id.Title);
        SubTitle = (TextView) main.findViewById(R.id.SubTitle);

        Title.setText(a.getString(R.styleable.Spinner_T2_SPT2_Text));

        if(a.hasValue(R.styleable.Spinner_T2_SPT2_BottomBorder))
        {
            _Layout.setBackground(context.getResources().getDrawable(R.drawable.inputborder));
        }

        main.findViewById(R.id.ifSubtitle).setVisibility(View.GONE);
        main.findViewById(R.id.ifNoSubtitle).setVisibility(View.VISIBLE);

        if(a.hasValue(R.styleable.Spinner_T2_SPT2_Subtitle))
        {
            main.findViewById(R.id.ifSubtitle).setVisibility(View.VISIBLE);
            main.findViewById(R.id.ifNoSubtitle).setVisibility(View.GONE);
            SubTitle.setVisibility(View.VISIBLE);
        }
    }

    public void setOptions(String title, List<RadioButtonInfo> optns, String defaultValue)
    {
        dialogueBox.setup(title, optns, defaultValue);
    }

    public void setText(String txt)
    {
        Title.setText(txt);
    }

    public void SetSubtitleText(String txt)
    {
        SubTitle.setText(txt);
    }

    public void setListener(Success _success)
    {
        this._success = _success;
    }

    public interface Success
    {
        void done();
    }
}
