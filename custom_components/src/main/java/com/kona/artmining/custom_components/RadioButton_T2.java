package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.model.RadioButtonInfo;

public class RadioButton_T2 extends LinearLayout {

    LinearLayout _Layout;
    TypedArray a;
    TextView CheckedImageText;
    ImageView CheckedImage;
    String myID;
    Boolean _checked = false;
    hitted _hitted;
    int locationID;

    RadioButtonInfo _thisInfo;

    public RadioButton_T2(Context context) {
        super(context);

        /*a = context.obtainStyledAttributes(attrs, R.styleable.RadioButton_T1, 0, 0);*/

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.radio_button_t2, this, true);

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);

        CheckedImageText = (TextView) main.findViewById(R.id.CheckedImageText);
        CheckedImage = (ImageView) main.findViewById(R.id.CheckedImage);

        /*CheckedImageText.setText(a.getString(R.styleable.RadioButton_T1_RBT1_Text));

        if(a.hasValue(R.styleable.RadioButton_T1_RBT1_Checked)){
            DoChecked();
        }*/

        _Layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(_checked){
                    DoUnchecked();
                }else{
                    DoChecked();
                }*/
                DoChecked();
            }
        });
    }

    public void setup(RadioButtonInfo info, int locationID, boolean Checked)
    {
        this._thisInfo=info;
        CheckedImageText.setText(info.getText());
        myID = info.getID()+"";
        this.locationID = locationID;
        if(Checked){

            DoChecked();
        }
    }

    public void setBottomLine()
    {
        _Layout.setBackgroundResource(R.drawable.inputborder);
        _Layout.setPadding(30,30,30,30);
    }

    public String getMyID()
    {
        return myID;
    }

    public void DoChecked()
    {
        _checked = true;
        CheckedImage.setImageResource(R.drawable.img_radio_on);
        _hitted.done(_thisInfo);
    }

    public void DoChecked2()
    {
        _checked = true;
        CheckedImage.setImageResource(R.drawable.img_radio_on);
    }

    public void DoUnchecked()
    {
        _checked = false;
        CheckedImage.setImageResource(R.drawable.img_radio_off);
        _hitted.done(_thisInfo);
    }

    public void DoReset()
    {
        CheckedImage.setImageResource(R.drawable.img_radio_off);
    }

    public void DoUnReset()
    {
        CheckedImage.setImageResource(R.drawable.img_radio_on);
    }

    public void setListener(hitted hit)
    {
        _hitted = hit;
    }

    public interface hitted
    {
        void done(RadioButtonInfo id);
    }
}
