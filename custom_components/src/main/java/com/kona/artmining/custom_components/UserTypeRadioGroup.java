package com.kona.artmining.custom_components;


import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UserTypeRadioGroup extends LinearLayout
{
    TypedArray a;
    Context context;
    LinearLayout _Layout_1;
    LinearLayout _Layout_2;
    String _value = null;

    RadioButton_T1 rb_t1;
    RadioButton_T1 rb_t2;
    RadioButton_T1 rb_t3;
    RadioButton_T1 rb_t4;

    Success _success;

    public UserTypeRadioGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        a = context.obtainStyledAttributes(attrs, R.styleable.UserTypeRadioGroup, 0, 0);
        init(context, a);
        a.recycle();
    }

    private void init(Context context,TypedArray a)
    {
        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.user_type_radio_group, this, true);
        _Layout_1 = (LinearLayout) main.findViewById(R.id._Layout_1);
        _Layout_2 = (LinearLayout) main.findViewById(R.id._Layout_2);

        ImageView LabelImg = (ImageView) main.findViewById(R.id.LabelImg);
        LabelImg.setImageDrawable(a.getDrawable(R.styleable.UserTypeRadioGroup_UTRG_labelImage));

        if(a.hasValue(R.styleable.UserTypeRadioGroup_UTRG_labelImage_Margin_left))
        {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    (int) context.getResources().getDimension(R.dimen.inputIconWidth),
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            lp.setMargins(
                    (int) a.getDimension(R.styleable.UserTypeRadioGroup_UTRG_labelImage_Margin_left,0),
                    0,
                    0,
                    0);

            LabelImg.setLayoutParams(lp);
        }

        TextView LabelText = (TextView) main.findViewById(R.id.LabelText);
        LabelText.setText(a.getString(R.styleable.UserTypeRadioGroup_UTRG_labelText));
    }

    public void setElements()
    {

        rb_t1=new RadioButton_T1(context);
        rb_t1.setup("Regular", 0, ("1"), false);
        rb_t1.setPadding(0,10,100,20);
        rb_t1.setListener(new RadioButton_T1.hitted() {
            @Override
            public void done(String id) {
                _value=id;
                resetRadioView(id);
                if(_success != null){
                    _success.done(id);
                }

            }
        });
        rb_t1.DoChecked();

        _Layout_1.addView(rb_t1, 0);


        rb_t2=new RadioButton_T1(context);
        rb_t2.setup("Supporter", 1, ("4"), false);
        rb_t2.setPadding(0,10,80,10);
        rb_t2.setListener(new RadioButton_T1.hitted() {
            @Override
            public void done(String id) {
                _value=id;
                resetRadioView(id);
                if(_success != null){
                    _success.done(id);
                }
            }
        });

        _Layout_1.addView(rb_t2, 1);


        rb_t3=new RadioButton_T1(context);
        rb_t3.setup("Artist", 2, ("2"), false);
        rb_t3.setPadding(0,20,100,20);
        rb_t3.setListener(new RadioButton_T1.hitted() {
            @Override
            public void done(String id) {
                _value=id;
                resetRadioView(id);
                if(_success != null){
                    _success.done(id);
                }
            }
        });

        _Layout_2.addView(rb_t3, 0);


        rb_t4=new RadioButton_T1(context);
        rb_t4.setup("Curator", 3, ("3"), false);
        rb_t4.setPadding(25,20,100,20);
        rb_t4.setListener(new RadioButton_T1.hitted() {
            @Override
            public void done(String id) {
                _value=id;
                resetRadioView(id);
                if(_success != null){
                    _success.done(id);
                }
            }
        });

        _Layout_2.addView(rb_t4, 1);


        /*int i = 0;

        for (RadioButtonInfo rbi : btns)
        {
            RadioButton_T1 rb_t1=new RadioButton_T1(context);
            rb_t1.setup(rbi.getText(), i, (rbi.getID() + ""), false);
            rb_t1.setListener(new RadioButton_T1.hitted() {
                @Override
                public void done(String id) {

                }
            });

            _Layout.addView(rb_t1, i);

            i = i + 1;
        }*/
    }

    private void resetRadioView(String id)
    {
        int childs = _Layout_1.getChildCount();
        for(int i=0; i < childs; i++)
        {
            RadioButton_T1 mi = (RadioButton_T1) _Layout_1.getChildAt(i);
            if(mi.getMyID() != id){
                mi.DoReset();
            }
        }

        int childs2 = _Layout_2.getChildCount();
        for(int i=0; i < childs2; i++)
        {
            RadioButton_T1 mi = (RadioButton_T1) _Layout_2.getChildAt(i);
            if(mi.getMyID() != id){
                mi.DoReset();
            }
        }
    }

    public String getValue()
    {
        return _value;
    }

    public void setValue(int value)
    {
        switch(value) {
            case 1:
                    if(rb_t1!=null)
                    {
                        rb_t1.DoChecked();
                    }
                break;
            case 4:
                    if(rb_t2!=null)
                    {
                        rb_t2.DoChecked();
                    }
                break;
            case 2:
                    if(rb_t3!=null)
                    {
                        rb_t3.DoChecked();
                    }
                break;
            case 3:
                    if(rb_t4!=null)
                    {
                        rb_t4.DoChecked();
                    }
                break;
            default:
                    if(rb_t1!=null)
                    {
                        rb_t1.DoChecked();
                    }
                break;
        }
    }

    public void setListener(Success _success)
    {
        this._success = _success;
    }

    public interface Success
    {
        void done(String id);
    }
}
