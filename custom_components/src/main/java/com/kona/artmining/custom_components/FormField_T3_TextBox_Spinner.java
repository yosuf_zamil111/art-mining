package com.kona.artmining.custom_components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FormField_T3_TextBox_Spinner extends LinearLayout
{
    EditText _TextBox;
    Success _success;
    Spinner_T1 TheSpinner;

    public FormField_T3_TextBox_Spinner(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FormField_T3_TextBox_Spinner, 0, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.form_field_t3_text_box_spinner, this, true);

        ImageView LabelImg = (ImageView) main.findViewById(R.id.LabelImg);
        LabelImg.setImageDrawable(a.getDrawable(R.styleable.FormField_T3_TextBox_Spinner_FF_TB_T3_labelImage));

        TheSpinner = (Spinner_T1) main.findViewById(R.id.TheSpinner);
        TheSpinner.setClickable(true);
        TheSpinner.setListener(new Spinner_T1.Success(){
            @Override
            public void done() {
                if(_success != null)
                {
                    _success.done();
                }
            }
        });

        if(a.hasValue(R.styleable.FormField_T3_TextBox_Spinner_FF_TB_T3_labelImage_Margin_left))
        {
            LayoutParams lp = new LayoutParams(
                    (int) context.getResources().getDimension(R.dimen.inputIconWidth),
                    LayoutParams.WRAP_CONTENT
            );

            lp.setMargins(
                    (int) a.getDimension(R.styleable.FormField_T3_TextBox_Spinner_FF_TB_T3_labelImage_Margin_left,0),
                    (int) a.getDimension(R.styleable.FormField_T3_TextBox_Spinner_FF_TB_T3_labelImage_Margin_top,0),
                    0,
                    0);

            LabelImg.setLayoutParams(lp);
        }

        TextView LabelText = (TextView) main.findViewById(R.id.LabelText);
        LabelText.setText(a.getString(R.styleable.FormField_T3_TextBox_Spinner_FF_TB_T3_labelText));

        _TextBox = (EditText) main.findViewById(R.id._TextBox);
        _TextBox.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        if(!isInEditMode()) {
            _TextBox.setTypeface(Typeface.createFromAsset(context.getResources().getAssets(), "Roboto-Regular.ttf"));
        }

        if(a.hasValue(R.styleable.FormField_T3_TextBox_Spinner_FF_TB_T3_numaric_input_type))
        {
            _TextBox.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        _TextBox.setHint(a.getString(R.styleable.FormField_T3_TextBox_Spinner_FF_TB_T3_placeholderText));

        if(a.hasValue(R.styleable.FormField_T3_TextBox_Spinner_FF_TB_T3_password))
        {
            _TextBox.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }

        _TextBox.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus)
                {
                    if(_success != null)
                    {
                        _success.Focused();
                    }
                }else{
                    if(_success != null)
                    {
                        _success.Unfocused();
                    }
                }
            }
        });

        a.recycle();
    }

    public String getText()
    {
        return TheSpinner.getValue()+_TextBox.getText().toString();
    }

    public String getTextOnly()
    {
        return _TextBox.getText().toString();
    }

    public void setListener(Success _success)
    {
        this._success = _success;
    }

    public void setSpinnerText(String data)
    {
        TheSpinner.setText(data);
    }

    public String getSpinnerText()
    {
        return TheSpinner.getValue();
    }

    public void setText(String data)
    {
        _TextBox.setText(data);
    }

    public interface Success
    {
        void done();
        void Focused();
        void Unfocused();
    }
}
