package com.kona.artmining.custom_popup_box;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kona.artmining.custom_components.R;
import com.kona.artmining.custom_components.RadioButton_T2;
import com.kona.artmining.model.RadioButtonInfo;

import java.util.List;

public class OptionListing_T4 extends Dialog {

    ImageView check_on,check_off;
    LinearLayout _Layout,sort_without_out_of_stock;
    TextView _Title;
    Context context;
    Success _success;
    RadioButtonInfo _value;

    public boolean checked = false;

    public OptionListing_T4(final Context context) {
        super(context);

        this.context = context;

        setContentView(R.layout.option_listing_t4);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        _Layout = (LinearLayout) this.findViewById(R.id._Layout);
        _Title = (TextView) findViewById(R.id._Title);


        check_on = (ImageView) this.findViewById(R.id.check_on);
        check_off = (ImageView) this.findViewById(R.id.check_off);

        sort_without_out_of_stock = (LinearLayout) this.findViewById(R.id.sort_without_out_of_stock);
        sort_without_out_of_stock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if(checked)
                {
                    checked = false;
                    ToggleVisibility_CheckBox();
                }
                else
                {
                    checked = true;
                    ToggleVisibility_CheckBox();
                }
                _success.soldOutChecked(checked);
            }
        });

    }

    void ToggleVisibility_CheckBox()
    {
        if(checked)
        {
            check_on.setVisibility(View.VISIBLE);
            check_off.setVisibility(View.GONE);
        }
        else
        {
            check_on.setVisibility(View.GONE);
            check_off.setVisibility(View.VISIBLE);
        }
        dismiss();
    }

    public void setup(String Title, List<RadioButtonInfo> btns, String defaultValue)
    {
        _Title.setText(Title);
        _Layout.removeAllViews();

        int i = 0;

        for (RadioButtonInfo rbi : btns)
        {
            RadioButton_T2 rb_t1=new RadioButton_T2(context);
            rb_t1.setup(rbi, i, false);
            rb_t1.setBottomLine();
            rb_t1.setListener(new RadioButton_T2.hitted() {
                @Override
                public void done(RadioButtonInfo id) {
                    _value=id;
                    resetRadioView(id);
                }
            });
            if(defaultValue.equals(rbi.getID()+""))
            {
                rb_t1.DoChecked();
            }

            _Layout.addView(rb_t1, i);

            i = i + 1;
        }
    }

    public void hideSoldOutCheckBox()
    {
        if(sort_without_out_of_stock != null)
        {
            sort_without_out_of_stock.setVisibility(View.GONE);
        }
    }

    public void setDefault(RadioButtonInfo id)
    {
        _value=id;
        resetRadioView(id);
    }

    private void resetRadioView(RadioButtonInfo id)
    {
        int childs = _Layout.getChildCount();
        for(int i=0; i < childs; i++)
        {
            RadioButton_T2 mi = (RadioButton_T2) _Layout.getChildAt(i);
            if(!mi.getMyID().contentEquals(id.getID()+"")){
                mi.DoReset();
            }else{
                mi.DoUnReset();
            }
        }

        if(_success != null){
            _success.Done(id);
            dismiss();
        }
    }



    public void setListener(Success cmd)
    {
        this._success=cmd;
    }

    public interface Success
    {
        void Done(RadioButtonInfo value);
        void soldOutChecked(boolean ifChecked);
    }
}
