package com.kona.artmining.custom_popup_box;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.widget.LinearLayout;

import com.kona.artmining.custom_components.R;
import com.kona.artmining.custom_components.RadioButton_T1;
import com.kona.artmining.custom_components.SquareButton_T1;

public class GenderSelectionPopUpBox extends Dialog
{
    LinearLayout _Layout;
    SquareButton_T1 doneBtt;
    Success _success;
    String _value = null;
    String _value2 = null;

    RadioButton_T1 rb_t1;
    RadioButton_T1 rb_t2;

    public GenderSelectionPopUpBox(Context context) {
        super(context);

        setContentView(R.layout.gender_selection_popup_box);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        _Layout = (LinearLayout) this.findViewById(R.id._Layout);
        doneBtt = (SquareButton_T1) findViewById(R.id.doneBtt);
        doneBtt._onClick(new SquareButton_T1._click() {
            @Override
            public void done() {
                _success.Done(_value);
                dismiss();
            }
        });


        rb_t1=new RadioButton_T1(context);
        rb_t1.setPadding(0,0,100,0);
        rb_t1.setup("Male", 0, ("M" + ""), false);
        rb_t1.setListener(new RadioButton_T1.hitted() {
            @Override
            public void done(String id) {
                _value="Male";
                _value2="M";
                resetRadioView(id);
            }
        });
        rb_t1.DoChecked();

        _Layout.addView(rb_t1, 0);

        rb_t2=new RadioButton_T1(context);
        rb_t2.setPadding(0,0,100,0);
        rb_t2.setup("Female", 1, ("F" + ""), false);
        rb_t2.setListener(new RadioButton_T1.hitted() {
            @Override
            public void done(String id) {
                _value="Female";
                _value2="F";
                resetRadioView(id);
            }
        });

        _Layout.addView(rb_t2, 1);
    }

    private void resetRadioView(String id)
    {
        int childs = _Layout.getChildCount();
        for(int i=0; i < childs; i++)
        {
            RadioButton_T1 mi = (RadioButton_T1) _Layout.getChildAt(i);
            if(mi.getMyID() != id){
                mi.DoReset();
            }
        }
    }

    public void setDefaultValue(String theV)
    {
        if(theV.contains("Male"))
        {
            rb_t1.DoChecked();
        }else{
            rb_t2.DoChecked();
        }
    }

    public String get_value2() {
        return _value2;
    }

    public void set_value2(String _value2) {
        this._value2 = _value2;
    }

    public void setDoneBttBg(Drawable bg)
    {
        doneBtt.setBackground(bg);
    }

    public void setListener(Success cmd)
    {
        this._success=cmd;
    }

    public interface Success
    {
        void Done(String value);
    }
}
