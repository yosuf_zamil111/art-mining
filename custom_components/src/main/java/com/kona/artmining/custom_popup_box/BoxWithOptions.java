package com.kona.artmining.custom_popup_box;


import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.custom_components.R;

public class BoxWithOptions extends Dialog
{
    LinearLayout _Layout;
    TextView Title;

    public BoxWithOptions(Context context) {
        super(context);
        setContentView(R.layout.dialog_box_frame);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        _Layout = (LinearLayout) this.findViewById(R.id._Layout);
        Title = (TextView) findViewById(R.id.Title);
    }

    public void setTheTitle(String title)
    {
        Title.setText(title);
    }
}
