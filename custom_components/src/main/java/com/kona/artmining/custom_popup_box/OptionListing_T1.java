package com.kona.artmining.custom_popup_box;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.custom_components.R;
import com.kona.artmining.custom_components.RadioButton_T2;
import com.kona.artmining.model.RadioButtonInfo;

import java.util.List;

public class OptionListing_T1 extends Dialog {

    LinearLayout _Layout;
    TextView _Title;
    Context context;
    Success _success;
    RadioButtonInfo _value;
    
    public OptionListing_T1(Context context) {
        super(context);

        this.context = context;

        setContentView(R.layout.option_list);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        _Layout = (LinearLayout) this.findViewById(R.id._Layout);
        _Title = (TextView) findViewById(R.id._Title);

    }

    public void setup(String Title, List<RadioButtonInfo> btns, String defaultValue)
    {
        _Title.setText(Title);

        int i = 0;

        for (RadioButtonInfo rbi : btns)
        {
            RadioButton_T2 rb_t1=new RadioButton_T2(context);
            rb_t1.setup(rbi, i, false);
            rb_t1.setBottomLine();
            rb_t1.setListener(new RadioButton_T2.hitted() {
                @Override
                public void done(RadioButtonInfo id) {
                    _value=id;
                    resetRadioView(id);
                }
            });
            if(defaultValue.equals(rbi.getText()))
            {
                rb_t1.DoChecked();
            }

            _Layout.addView(rb_t1, i);

            i = i + 1;
        }
    }

    public void setup2(String Title, List<RadioButtonInfo> btns, String defaultValue)
    {
        _Title.setText(Title);

        int i = 0;

        for (RadioButtonInfo rbi : btns)
        {
            RadioButton_T2 rb_t1=new RadioButton_T2(context);
            rb_t1.setup(rbi, i, false);
            rb_t1.setBottomLine();
            rb_t1.setListener(new RadioButton_T2.hitted() {
                @Override
                public void done(RadioButtonInfo id) {
                    _value=id;
                    resetRadioView(id);
                }
            });
            if(defaultValue.equals(rbi.getText()))
            {
                rb_t1.DoChecked2();
            }

            _Layout.addView(rb_t1, i);

            i = i + 1;
        }
    }

    public void setup3(String Title, List<RadioButtonInfo> btns, int defaultValue)
    {
        _Title.setText(Title);

        int i = 0;

        for (RadioButtonInfo rbi : btns)
        {
            RadioButton_T2 rb_t1=new RadioButton_T2(context);
            rb_t1.setup(rbi, i, false);
            rb_t1.setBottomLine();
            rb_t1.setListener(new RadioButton_T2.hitted() {
                @Override
                public void done(RadioButtonInfo id) {
                    _value=id;
                    resetRadioView(id);
                }
            });
            if(defaultValue == rbi.getID())
            {
                rb_t1.DoChecked2();
            }

            _Layout.addView(rb_t1, i);

            i = i + 1;
        }
    }

    public void setDefault(RadioButtonInfo id)
    {
        _value=id;
        resetRadioView(id);
    }

    private void resetRadioView(RadioButtonInfo id)
    {
        int childs = _Layout.getChildCount();
        for(int i=0; i < childs; i++)
        {
            RadioButton_T2 mi = (RadioButton_T2) _Layout.getChildAt(i);
            if(!mi.getMyID().contains(id.getID()+"")){
                mi.DoReset();
            }else{
                mi.DoUnReset();
            }
        }

        if(_success != null){
            _success.Done(id);
            dismiss();
        }
    }



    public void setListener(Success cmd)
    {
        this._success=cmd;
    }

    public interface Success
    {
        void Done(RadioButtonInfo value);
    }
}
