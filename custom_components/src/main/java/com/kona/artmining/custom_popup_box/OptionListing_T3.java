package com.kona.artmining.custom_popup_box;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.custom_components.CheckButton_T1;
import com.kona.artmining.custom_components.R;
import com.kona.artmining.custom_components.RadioButton_T2;
import com.kona.artmining.model.RadioButtonInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OptionListing_T3 extends Dialog {

    LinearLayout _Layout;
    TextView _Title;
    Context context;
    Success _success;
    RadioButtonInfo _value;
    Button submitButton;

    String optionsV1;
    String optionsV2;
    List<String> defaultCountryCodes;
    List<String> defaultCountryNames = new ArrayList<>();

    public OptionListing_T3(Context context) {
        super(context);

        this.context = context;

        setContentView(R.layout.option_listing_t2);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        _Layout = (LinearLayout) this.findViewById(R.id._Layout);
        _Title = (TextView) findViewById(R.id._Title);
        submitButton = (Button) findViewById(R.id.submitButton);

        optionsV1 = "";
        optionsV2 = "";
    }

    public void setup(String Title, List<RadioButtonInfo> btns, String defaultValue)
    {
        _Title.setText(Title);
        _Layout.removeAllViews();

        defaultCountryCodes = new ArrayList<String>(Arrays.asList(defaultValue.split(", ")));

        int i = 0;

        for (RadioButtonInfo rbi : btns)
        {
            CheckButton_T1 rb_t1=new CheckButton_T1(context);
            rb_t1.setup(rbi, i, false);
            rb_t1.setBottomLine();
            rb_t1.setListener(new CheckButton_T1.hitted() {
                @Override
                public void done(CheckButton_T1 view, RadioButtonInfo id) {
                    if(view.checkIfChecked())
                    {
                        view.DoUnchecked2();
                        optionsV1 = optionsV1.replaceAll(id.getsID()+", ", "");
                        optionsV2 = optionsV2.replaceAll(id.getText()+", ", "");

                        defaultCountryCodes.remove(defaultCountryCodes.indexOf(id.getsID()));
                        defaultCountryNames.remove(defaultCountryNames.indexOf(id.getText()));
                    }
                    else
                    {
                        view.DoChecked2();
                        optionsV1 = optionsV1 + id.getsID()+", ";
                        optionsV2 = optionsV2 + id.getText()+", ";

                        defaultCountryCodes.add(id.getsID());
                        defaultCountryNames.add(id.getText());
                    }
                }
            });
            System.out.println(defaultValue);

            String[] defaultValue_split = defaultValue.split(", ");
            for (int i2 = 0; i2 < (defaultValue_split.length); i2++) {

                //System.out.println(defaultValue_split[i2]+" ------ "+i2+" ------- "+rbi.getText());

                if(defaultValue_split[i2].equals(rbi.getsID()))
                {
                    rb_t1.DoChecked2();
                    optionsV1 = optionsV1 + rbi.getsID()+", ";
                    optionsV2 = optionsV2 + rbi.getText()+", ";

                    defaultCountryNames.add(rbi.getText());
                }
            }



            _Layout.addView(rb_t1, i);

            i = i + 1;
        }

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String co = "";
                for (int i2 = 0; i2 < defaultCountryNames.size(); i2++) {

                    if(i2 == 0)
                    {
                        co = defaultCountryNames.get(i2);
                    }
                    else
                    {
                        co = co + ", "+ defaultCountryNames.get(i2);
                    }
                }

                _success.SubmitResult(optionsV1, co);
                dismiss();
            }
        });
    }

    public void setDefault(RadioButtonInfo id)
    {
        _value=id;
        //resetRadioView(id);
    }

    /*
    private void resetRadioView(RadioButtonInfo id)
    {
        int childs = _Layout.getChildCount();
        for(int i=0; i < childs; i++)
        {
            CheckButton_T1 mi = (CheckButton_T1) _Layout.getChildAt(i);
            if(!mi.getMyID().contentEquals(id.getID()+"")){
                mi.DoReset();
            }else{
                mi.DoUnReset();
            }
        }

        if(_success != null){
            _success.Done(id);
            //dismiss();
        }
    }
    */



    public void setListener(Success cmd)
    {
        this._success=cmd;
    }

    public interface Success
    {
        void SubmitResult(String optionsV1, String optionsV2);
    }
}
