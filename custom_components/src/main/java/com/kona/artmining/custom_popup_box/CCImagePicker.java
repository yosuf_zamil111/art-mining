package com.kona.artmining.custom_popup_box;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.LinearLayout;

import com.kona.artmining.custom_components.R;

public class CCImagePicker extends Dialog {

    LinearLayout pickFromCamera, pickFromGallery;
    task _task;

    public CCImagePicker(Context context) {
        super(context);
        setContentView(R.layout.cc_image_picker);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        prepare();
    }

    void prepare()
    {
        pickFromCamera = (LinearLayout) this.findViewById(R.id.pickFromCamera);
        pickFromGallery = (LinearLayout) this.findViewById(R.id.pickFromGallery);

        pickFromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _task.fromCamera();
                dismiss();
            }
        });

        pickFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _task.fromGallery();
                dismiss();
            }
        });
    }

    public void setListener(task _task)
    {
        this._task = _task;
    }

    public interface task
    {
        void fromCamera();
        void fromGallery();
    }
}
