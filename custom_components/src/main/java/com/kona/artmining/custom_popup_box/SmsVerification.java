package com.kona.artmining.custom_popup_box;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kona.artmining.custom_components.R;
import com.kona.artmining.custom_components.SquareButton_T1;

public class SmsVerification extends Dialog {

    LinearLayout _Layout;
    SquareButton_T1 doneBtt;
    Button CancelBtt, resendButton;
    Success _success;
    String _value = null;
    EditText SMS_verification;

    public SmsVerification(Context context) {
        super(context);
        setContentView(R.layout.sms_verification);
        setCancelable(false);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        _Layout = (LinearLayout) this.findViewById(R.id._Layout);
        SMS_verification = (EditText) this.findViewById(R.id.SMS_verification);

        doneBtt = (SquareButton_T1) findViewById(R.id.doneBtt);
        doneBtt._onClick(new SquareButton_T1._click() {
            @Override
            public void done() {
                _success.Done(SMS_verification.getText().toString());
            }
        });

        CancelBtt = (Button) findViewById(R.id.CancelBtt);
        CancelBtt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        resendButton = (Button) findViewById(R.id.resendButton);
        resendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _success.ResendSMS();
            }
        });
    }

    public void setDoneBttBg(Drawable bg)
    {
        doneBtt.setBackground(bg);
    }

    public void setListener(Success cmd)
    {
        this._success=cmd;
    }

    public void hideMe()
    {
        dismiss();
    }

    public interface Success
    {
        void Done(String value);
        void ResendSMS();
    }
}
