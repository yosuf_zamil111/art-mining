package com.kona.artmining.custom_popup_box;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.custom_components.R;
import com.kona.artmining.custom_components.RadioButton_T2;
import com.kona.artmining.model.RadioButtonInfo;

import java.util.ArrayList;
import java.util.List;

public class OptionFilterListing_T1 extends Dialog {

    LinearLayout _Layout;
    TextView _Title;
    Context context;
    Success _success;
    RadioButtonInfo _value;
    LinearLayout reset_btt;

    LinearLayout artistFilter;
    TextView artistFilterSelection;

    LinearLayout themeFilter;
    TextView themeFilterSelection;

    OptionListing_T2 dialogueBox;

    List<RadioButtonInfo> artistsOptions = new ArrayList<RadioButtonInfo>();
    List<RadioButtonInfo> themeOptions = new ArrayList<RadioButtonInfo>();

    String artistsOptionsDefaultValue = "0";
    String themeOptionsDefaultValue = "0";

    RadioButtonInfo artistsOptionsSelectedValue;
    RadioButtonInfo themeOptionsSelectedValue;

    int mode = 1;

    themeSelected ts;
    artistSelected as;
    resetHitted rs;

    public OptionFilterListing_T1(Context context)
    {
        super(context);
        this.context = context;

        setContentView(R.layout.option_filter_listing_t1);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        _Layout = (LinearLayout) this.findViewById(R.id._Layout);
        _Title = (TextView) findViewById(R.id._Title);


        reset_btt = (LinearLayout) this.findViewById(R.id.reset_btt);

        artistFilter = (LinearLayout) this.findViewById(R.id.artistFilter);
        artistFilterSelection = (TextView) this.findViewById(R.id.artistFilterSelection);
        themeFilter = (LinearLayout) this.findViewById(R.id.themeFilter);
        themeFilterSelection = (TextView) this.findViewById(R.id.themeFilterSelection);

        artistFilterSelection.setText("All");
        themeFilterSelection.setText("All");

        artistFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = 1;
                dialogueBox.setup("Artists", artistsOptions, artistsOptionsDefaultValue);
                dialogueBox.show();
            }
        });

        themeFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = 2;
                dialogueBox.setup("Theme", themeOptions, themeOptionsDefaultValue);
                dialogueBox.show();
            }
        });

        dialogueBox =  new OptionListing_T2(getContext());
        dialogueBox.setCancelable(false);
        dialogueBox.setListener(new OptionListing_T2.Success() {
            @Override
            public void Done(RadioButtonInfo value) {
                if(mode == 1)
                {
                    artistsOptionsSelectedValue = value;
                    artistFilterSelection.setText(value.getText());
                    artistsOptionsDefaultValue = ""+value.getID();
                    if(as!=null)
                    {
                        as.done(value.getID(), value.getText());
                    }
                }else{
                    themeFilterSelection.setText(value.getText());
                    themeOptionsSelectedValue = value;
                    themeOptionsDefaultValue = ""+value.getID();
                    if(ts!=null)
                    {
                        ts.done(value.getID(), value.getText());
                    }
                }
                if(_success!=null)
                {
                    _success.Done(artistsOptionsSelectedValue, themeOptionsSelectedValue);
                }
            }
        });

        reset_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                artistFilterSelection.setText("All");
                themeFilterSelection.setText("All");

                artistsOptionsDefaultValue = "0";
                themeOptionsDefaultValue = "0";

                RadioButtonInfo artistsOptionsSelectedValue = new RadioButtonInfo();
                artistsOptionsSelectedValue.setID(0);
                artistsOptionsSelectedValue.setText("All");

                RadioButtonInfo themeOptionsSelectedValue = new RadioButtonInfo();
                themeOptionsSelectedValue.setID(0);
                themeOptionsSelectedValue.setText("All");


                rs.done();

                /*if(_success!=null)
                {
                    _success.Done(artistsOptionsSelectedValue, themeOptionsSelectedValue);
                }
                */
            }
        });
    }

    public void HideTheme()
    {
        artistFilter.setVisibility(View.GONE);
    }

    public void setArtistSelectedListener(artistSelected as)
    {
        this.as=as;
    }

    public void setThemeSelectedListener(themeSelected ts)
    {
        this.ts=ts;
    }

    public void setResetListener(resetHitted rs)
    {
        this.rs = rs;
    }

    public interface resetHitted
    {
        void done();
    }

    public interface themeSelected
    {
        void done(int id, String text);
    }

    public interface artistSelected
    {
        void done(int id, String text);
    }

    public void setup(List<RadioButtonInfo> artistsOptions, List<RadioButtonInfo> themeOptions)
    {
        this.artistsOptions = artistsOptions;
        this.themeOptions = themeOptions;
    }

    /*public void setup(String Title, List<RadioButtonInfo> btns, String defaultValue)
    {
        _Title.setText(Title);

        int i = 0;

        for (RadioButtonInfo rbi : btns)
        {
            RadioButton_T2 rb_t1=new RadioButton_T2(context);
            rb_t1.setup(rbi, i, false);
            rb_t1.setBottomLine();
            rb_t1.setListener(new RadioButton_T2.hitted() {
                @Override
                public void done(RadioButtonInfo id) {
                    _value=id;
                    resetRadioView(id);
                }
            });
            System.out.println(defaultValue+" "+rbi.getText());
            if(defaultValue.equals(rbi.getID()+""))
            {
                rb_t1.DoChecked();
            }

            _Layout.addView(rb_t1, i);

            i = i + 1;
        }
    }

    public void setDefault(RadioButtonInfo id)
    {
        _value=id;
        resetRadioView(id);
    }

    private void resetRadioView(RadioButtonInfo id)
    {
        int childs = _Layout.getChildCount();
        for(int i=0; i < childs; i++)
        {
            RadioButton_T2 mi = (RadioButton_T2) _Layout.getChildAt(i);
            if(!mi.getMyID().contentEquals(id.getID()+"")){
                mi.DoReset();
            }else{
                mi.DoUnReset();
            }
        }

        if(_success != null){
            _success.Done(id);
            dismiss();
        }
    }*/



    public void setListener(Success cmd)
    {
        this._success=cmd;
    }

    public interface Success
    {
        void Done(RadioButtonInfo value1, RadioButtonInfo value2);
    }
}
