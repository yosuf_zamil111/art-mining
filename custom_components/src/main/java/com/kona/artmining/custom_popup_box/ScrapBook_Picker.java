package com.kona.artmining.custom_popup_box;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kona.artmining.custom_components.R;
import com.squareup.picasso.Picasso;

import java.io.File;

public class ScrapBook_Picker extends Dialog {

    Button ok_btt, cancel_btt;
    task _task;
    ImageView theImage;

    public ScrapBook_Picker(Context context) {
        super(context);
        setContentView(R.layout.scrapbook_picker);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        prepare();
    }

    void prepare()
    {
        theImage = (ImageView) this.findViewById(R.id.theImage);
        ok_btt = (Button) this.findViewById(R.id.ok_btt);
        ok_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                dismiss();
                _task.done();
            }
        });

        cancel_btt = (Button) this.findViewById(R.id.cancel_btt);
        cancel_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public void setImage(String url)
    {
        Picasso.with(getContext()).load(url).placeholder(R.drawable.progress_animation).error(R.drawable.transparent).into(theImage);
    }

    public void setListener(task _task)
    {
        this._task = _task;
    }

    public interface task
    {
        void done();
    }
}
