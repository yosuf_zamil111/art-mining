package com.kona.artmining.custom_popup_box;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.custom_components.OvalButton_T1;
import com.kona.artmining.custom_components.R;
import com.kona.artmining.custom_components.SquareButton_T1;

public class ConfirmationBox extends Dialog {

    SquareButton_T1 doneBtt;
    Confirmed _success;
    String _value = null;
    TextView _message, Title;
    OvalButton_T1 negative_btt, positive_btt;

    public ConfirmationBox(Context context) {
        super(context);
        setContentView(R.layout.confirmation_box);
        setCancelable(false);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        _message = (TextView) findViewById(R.id.message);
        Title = (TextView) findViewById(R.id.Title);

        negative_btt = (OvalButton_T1) findViewById(R.id.negative_btt);
        positive_btt = (OvalButton_T1) findViewById(R.id.positive_btt);

        negative_btt._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {
                _success.No();
                dismiss();
            }
        });

        positive_btt._onClick(new OvalButton_T1._click() {
            @Override
            public void done() {
                _success.Yes();
                dismiss();
            }
        });
    }


    public void Setup(String title, String message, String yesBttText, String noBttText)
    {
        Title.setText(title);
        _message.setText(message);
        negative_btt.setLabel(noBttText);
        positive_btt.setLabel(yesBttText);
    }

    public void setListener(Confirmed cmd)
    {
        this._success=cmd;
    }


    public interface Confirmed
    {
        void Yes();
        void No();
    }
}
