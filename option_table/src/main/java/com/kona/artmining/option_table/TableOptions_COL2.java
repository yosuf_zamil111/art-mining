package com.kona.artmining.option_table;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.option_table.modals.Col2RowDetails;

public class TableOptions_COL2 extends LinearLayout
{
    ImageView TheCheckButton;
    Col2RowDetails _value;
    Success _success;
    LinearLayout _Layout;

    TextView column_1;
    TextView column_2;

    public TableOptions_COL2(Context context)
    {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.table_options_col_2, this, true);

        TheCheckButton = (ImageView) main.findViewById(R.id.TheCheckButton);
        TheCheckButton.setImageResource(R.drawable.img_radio_off);

        column_1 = (TextView) main.findViewById(R.id.column_1);
        column_2 = (TextView) main.findViewById(R.id.column_2);

        _Layout = (LinearLayout) main.findViewById(R.id._Layout);
        _Layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(_success != null)
                {
                    System.out.println("Hitted "+GetValue());
                    SetChecked();
                    _success.done(GetValue());
                    /*
                    );*/
                }
            }
        });
    }

    public void setColumn_1(String _col_1)
    {
        column_1.setText(_col_1);
    }

    public void setColumn_2(String _col_1)
    {
        column_2.setText(_col_1);
    }

    public interface Success
    {
        void done(Col2RowDetails _value);
    }

    public void setListener(Success _success)
    {
        this._success = _success;
    }

    public void SetChecked()
    {
        TheCheckButton.setImageResource(R.drawable.icon_alarm);
    }

    public void SetUnchecked()
    {
        TheCheckButton.setImageResource(R.drawable.img_radio_off);
    }

    public void SetValue(Col2RowDetails _value)
    {
        this._value = _value;
    }

    public Col2RowDetails GetValue()
    {
        return _value;
    }
}
