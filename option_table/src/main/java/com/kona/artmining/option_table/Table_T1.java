package com.kona.artmining.option_table;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kona.artmining.option_table.modals.Col2RowDetails;

import java.util.ArrayList;
import java.util.List;

public class Table_T1 extends LinearLayout {

    TextView column_1;
    TextView column_2;
    LinearLayout _container;
    Success _success;

    public Table_T1(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.Table_T1, 0, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View main = inflater.inflate(R.layout.table_t1, this, true);

        column_1 = (TextView) main.findViewById(R.id.column_1);
        column_2 = (TextView) main.findViewById(R.id.column_2);
        _container = (LinearLayout) main.findViewById(R.id._container);
    }

    public void setContainerElements(List<Col2RowDetails> rows, String defaultValue)
    {
        _container.removeAllViews();

        for(int i=0; i<rows.size(); i++){
            TableOptions_COL2 toc1 = new TableOptions_COL2(getContext());

            toc1.SetValue(rows.get(i));

            toc1.setColumn_1(rows.get(i).getCol1_Contents());
            toc1.setColumn_2(rows.get(i).getCol2_Contents());

            toc1.setListener(new TableOptions_COL2.Success() {
                @Override
                public void done(Col2RowDetails _value) {
                    if(_success != null)
                    {
                        resetOptionsView(_value.get_value());
                        System.out.println(_value);
                        _success.done(_value);
                    }
                }
            });

            System.out.println(toc1.GetValue());

            if(toc1.GetValue().get_value().contains(defaultValue))
            {
                toc1.SetChecked();
            }

            _container.addView(toc1, i);
        }
    }

    private void resetOptionsView(String _value)
    {
        int childs = _container.getChildCount();
        for(int i=0; i < childs; i++)
        {
            TableOptions_COL2 toc1 = (TableOptions_COL2) _container.getChildAt(i);
            if(toc1.GetValue().get_value() != _value){
                toc1.SetUnchecked();
            }
        }
    }

    public void setColumnNames(String[] col)
    {
        if(col.length>0){
            column_1.setText(col[0]);
        }

        if(col.length>1){
            column_1.setText(col[0]);
            column_2.setText(col[1]);
        }
    }



    public interface Success
    {
        void done(Col2RowDetails _v);
    }

    public void setListener(Success _success)
    {
        this._success = _success;
    }

}
