package com.kona.artmining.option_table.modals;


public class Col2RowDetails
{
    int id;
    String _value;
    boolean Checked;
    String Col1_Contents;
    String Col2_Contents;

    public String getCol1_Contents() {
        return Col1_Contents;
    }

    public void setCol1_Contents(String col1_Contents) {
        Col1_Contents = col1_Contents;
    }

    public String getCol2_Contents() {
        return Col2_Contents;
    }

    public void setCol2_Contents(String col2_Contents) {
        Col2_Contents = col2_Contents;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isChecked() {
        return Checked;
    }

    public void setChecked(boolean checked) {
        Checked = checked;
    }

    public String get_value() {
        return _value;
    }

    public void set_value(String _value) {
        this._value = _value;
    }

}
